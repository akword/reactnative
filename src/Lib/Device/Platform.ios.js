import NotificationsIOS from 'react-native-notifications'

/**
 * Registers for push notification and resolves with token.
 *
 * @returns {Promise}
 */
export function registerForNotification () {
  return new Promise((resolve, reject) => {
    NotificationsIOS.addEventListener('remoteNotificationsRegistered', token => resolve(token))
    NotificationsIOS.addEventListener('remoteNotificationsRegistrationFailed', e => reject(e))
    NotificationsIOS.requestPermissions()
    // Clear IOS badge count
    NotificationsIOS.setBadgesCount(0)
  })
}

/**
 * Registers for push kit token and resolves with token.
 *
 * @returns {Promise}
 */
export function registerForPushKit () {
  return new Promise((resolve, reject) => {
    NotificationsIOS.addEventListener('pushKitRegistered', token => resolve(token))
    NotificationsIOS.registerPushKit()
  })
}
