import FCM from 'react-native-fcm'

/**
 * Registers for push notification and resolves with token.
 *
 * @returns {Promise}
 */
export function registerForNotification () {
  return FCM.getFCMToken()
}

/**
 * Registers for push kit token and resolves with token.
 *
 * @returns {Promise}
 */
export function registerForPushKit () {

}
