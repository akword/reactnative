import moment from 'moment'

function makeDateTimeString (timestamp) {
  return moment(timestamp).format('[On] ddd, MMM D, YYYY [at] hh:mm A')
}

function checkMailData (data) {
  if (!data.detail) {
    console.error(`details not found for mail id ${data.id} and subject '${data.msg_subject}'`)
  }
}

/**
 * Takes the email event data object (along with detail key
 * containing pickup response) and returns formatted email body
 * for reply.
 *
 * Reply format based on Gmail's implementation.
 *
 * @param data
 * @param signature
 * @return {string}
 */
export function makeReplyBody (data, signature) {
  checkMailData(data)

  const sender = `${data.msg_from_displayname} <${data.msg_from}>`
  const replyMessage = `${makeDateTimeString(data.created_on)}, ${sender} wrote:`
  const quotedBody = (data.detail.plain || '').replace(/(\n|^)/g, '\n> ')

  const body = `\n\n${replyMessage}\n${quotedBody}`

  return signature ? `\n\n${signature}${body}` : body
}

/**
 * Prepare the forward body.
 *
 * @param data
 * @param signature
 *
 * @return {string}
 */
export function makeForwardBody (data, signature) {
  checkMailData(data)

  const sender = `${data.msg_from_displayname} <${data.msg_from}>`
  const recipient = `${data.msg_to_displayname} <${data.msg_to}>`

  const replyMessage = `\nFrom: ${sender}\nDate: ${makeDateTimeString(data.created_on)}\nSubject: ${data.msg_subject}\nTo: ${recipient}`

  const body = `\n\n---------- Forwarded message ----------\n${replyMessage}\n${data.detail.plain}`

  return signature ? `\n\n${signature}${body}` : body
}
