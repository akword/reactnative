import WebRTC, {
  MediaStream, MediaStreamTrack, getUserMedia,
  RTCPeerConnection, RTCSessionDescription, RTCIceCandidate
} from 'react-native-webrtc'
import { Platform } from 'react-native'

import log from 'commons/Services/Log'

export const WebRTCLib = WebRTC

export function areLibrariesAvailable () {
  return (
    typeof WebRTC !== 'undefined' &&
    typeof RTCPeerConnection !== 'undefined' &&
    typeof RTCSessionDescription !== 'undefined' &&
    typeof RTCIceCandidate !== 'undefined' &&
    typeof MediaStream !== 'undefined' &&
    typeof MediaStreamTrack !== 'undefined' &&
    typeof getUserMedia !== 'undefined'
  )
}

export async function scanDevices () {
  const devices = await WebRTCLib.MediaStreamTrack.getSources()
  if (Platform.OS === 'ios') {
    return devices[0]
  }
  return devices
}

// native specific
export function getNativeUserMedia (constraints) {
  return WebRTCLib.getUserMedia(constraints)
}

// native specific
export function getNativeSourceIdByDirection (direction = null, localMedia = null) {
  if (! direction || ! localMedia) {
    return
  }
  try {
    log.debug('getNativeSourceIdByDirection localMedia -', localMedia)
    const n = localMedia.videoDevices.length
    for ( var i = 0; i < n; i++) {
      var track = localMedia.videoDevices[i]
      log.debug('getNativeSourceIdByDirection track -', track)
      if (track.facing === direction) {
        log.debug('getNativeSourceIdByDirection returning track.id -', track.id)
        return track.id
      }
    }
  } catch (e) {
    log.debug('getNativeSourceIdByDirection error -', e)
  }
}

// native specific
export function setNativeRemoteDescription (peer, sdp) {
  return new Promise((resolve, reject) => {
    peer.setRemoteDescription(sdp, function () {
      resolve(true)
    }, function (e) {
      log.debug('FAILED e -', e)
      reject(e)
    })
  })
}

// native specific
export function createNativeAnswer (peer) {
  return new Promise((resolve, reject) => {
    peer.createAnswer(function (answer) {
      resolve(answer)
    }, function (e) {
      log.debug('FAILED e -', e)
      reject(e)
    })
  })
}

export function createNativeOffer (peer) {
  return new Promise((resolve, reject) => {
    peer.createOffer(function (offer) {
      resolve(offer)
    }, function (e) {
      log.debug('FAILED e -', e)
      reject(e)
    })
  })
}

export function setNativeLocalDescription (peer, data) {
  return new Promise((resolve, reject) => {
    peer.setLocalDescription(data, function () {
      resolve(true)
    }, function (e) {
      log.debug('FAILED e -', e)
      reject(e)
    })
  })
}

// native specific
export async function getNativeConstraints ({ video, audio }) {
  const constraints = {}

  if (audio) {
    constraints.audio = true
  }

  if (video) {
    const direction = 'front'

    constraints.video = {
      optional: [{ sourceId: null, facingMode: direction }]
    }

    const devices = await scanDevices()
    for (let d of devices) {
      if (d.kind.startsWith('video') && d.facing === direction) {
        constraints.video.optional.push({ sourceId: d.id, facingMode: d.facing })
      }
    }

    if (Platform.OS === 'android') {
      constraints.video.mandatory = {
        minWidth: 500,
        minHeight: 300,
        minFrameRate: 30
      }
    }

  }

  return constraints
}
