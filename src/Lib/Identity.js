import { DataPicker } from 'rnkit-actionsheet-picker'
import m from 'commons/I18n'
export const showIdentitySelectAndCallback = (identities, callback, fm, myIdentities = {}) => {
  if (!identities || !identities.length) return
  let options = identities.map(i => ({
    value: {
      id: i.id,
      display_name: i.display_name,
      email: i.email
    },
    title: i.email
  })).filter(option => {
    for (let id in myIdentities) {
      if (myIdentities[id] && myIdentities[id].email === option.email) {
        return false
      }
    }
    return true
  })

  // No other identities
  if (options.length === 0) {
    return
  }

  const dataSource = options.map(option => option.title)

  DataPicker.show({
    dataSource,
    titleText: fm(m.native.Contact.selectIdentity),
    doneText: fm(m.app.Common.done),
    cancelText: fm(m.app.Common.cancel),
    numberOfComponents: 1,
    onPickerConfirm: (selectedData, selectedIndex) => {
      callback(options[selectedIndex].value)
    }
  })
}
