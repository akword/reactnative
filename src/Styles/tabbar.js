import EStyleSheet from 'react-native-extended-stylesheet'

import palette from 'app/Styles/colors'

const styles = EStyleSheet.create({
  main: {
    backgroundColor: 'rgb(238, 242, 246)',
  },

  icon: {
    fontSize: '1.5rem',
    color: palette.midnightBlue
  },

  iconActive: {
    color: palette.belizeHole
  },

  iconDisabled: {
    color: palette.concrete
  }
})

export default styles
