/*
 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 Version 2, December 2004
 Copyright (C) 2016 Esa-Matti Suuronen <esa-matti@suuronen.org>
 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.
 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 0. You just DO WHAT THE FUCK YOU WANT TO.

 Source: https://gist.github.com/epeli/10c77c1710dd137a1335
 */

import PropTypes from 'prop-types';

import React, { Component } from 'react';
import { WebView, View, Linking, Platform } from 'react-native'

const BODY_TAG_PATTERN = /\<\/ *body\>/
const SCALE_PAGE_TO_FIT = Platform.OS === 'android'

// Do not add any comments to this! It will break because all line breaks will removed for
// some weird reason when this script is injected.
const script = `
;(function() {
var wrapper = document.createElement("div");
wrapper.id = "height-wrapper";
while (document.body.firstChild) {
    wrapper.appendChild(document.body.firstChild);
}
document.body.appendChild(wrapper);
var i = 0;
function updateHeight() {
    document.title = wrapper.clientHeight;
    window.location.hash = ++i;
}
updateHeight();
window.addEventListener("load", function() {
    updateHeight();
    setTimeout(updateHeight, 1000);
});
window.addEventListener("resize", updateHeight);
}());
`

const style = `
<style>
body, html, #height-wrapper {
  margin: 5px;
  padding: 5px;
}
#height-wrapper {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
}
</style>
<script>
${script}
</script>
`

const codeInject = (html) => {
  if (!html.match(BODY_TAG_PATTERN)) {
    return `<body>${html} ${style}</body>`
  }
  return html.replace(BODY_TAG_PATTERN, style + "</body>")
}


/**
 * Wrapped Webview which automatically sets the height according to the
 * content. Scrolling is always disabled. Required when the Webview is embedded
 * into a ScrollView with other components.
 *
 * Inspired by this SO answer http://stackoverflow.com/a/33012545
 * */
export default class AutoHeightWebView extends Component {

  static propTypes = {
    source: PropTypes.object.isRequired,
    injectedJavaScript: PropTypes.string,
    minHeight: PropTypes.number,
    onNavigationStateChange: PropTypes.func,
    style: WebView.propTypes.style
  }

  static defaultProps = {
    minHeight: 100
  }

  constructor (props) {
    super(props)

    this.handleNavigationChange = this.handleNavigationChange.bind(this)

    this.state = {
      realContentHeight: props.minHeight
    }
  }

  handleNavigationChange(navState) {
    if (navState.title) {
      const realContentHeight = parseInt(navState.title, 10) || 0 // turn NaN to 0
      if (realContentHeight > 0) {
        this.setState({ realContentHeight: realContentHeight + 20 })
      }
    }

    if (typeof this.props.onNavigationStateChange === 'function') {
      this.props.onNavigationStateChange(navState)
    }

    const url = navState.url
    if (url && url.indexOf('about:blank') !== 0 && Platform.OS === 'ios') {
      this.webview.stopLoading()
      Linking.openURL(url)
    }
  }

  render() {
    const {source, style, minHeight, ...otherProps} = this.props
    const html = source.html

    if (!html) {
      throw new Error('WebViewAutoHeight supports only source.html')
    }

    return (
      <View>
        <WebView
          {...otherProps}
          bounces={false}
          ref={(ref) => { this.webview = ref; }}
          source={{html: codeInject(html)}}
          style={[style, {
            height: Math.max(this.state.realContentHeight, minHeight)
          }]}
          javaScriptEnabled
          onNavigationStateChange={this.handleNavigationChange}
          scalesPageToFit={SCALE_PAGE_TO_FIT}
        />
      </View>
    )
  }
}
