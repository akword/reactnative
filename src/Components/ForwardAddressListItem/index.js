import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View } from 'react-native'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

import Avatar from 'app/Components/Avatar'
import Text from 'app/Components/BaseText'
import { ListItem as s } from './style'

class ForwardAddressListItem extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    avatarImage: PropTypes.string,
  }

  render() {
    const { data, avatarImage } = this.props
    return (
      <View style={s.container}>
        <View style={s.avatarOuter}>
          <View style={s.avatarInner}>
            <Avatar
              textStyle={s.avatarText}
              name={data.display_name || data.email}
              avatar={avatarImage}
            />
          </View>
        </View>
        <View style={s.body}>
          <Text style={data.is_confirmed ? s.name : s.nameUnconfirmed}>{data.display_name || data.email}</Text>
          <Text style={data.is_confirmed ? s.email : s.emailUnconfirmed}>{data.email}</Text>
        </View>

        { !data.is_confirmed &&
          <View style={s.bodyUnconfirmed}>
            <FontAwesome style={s.iconUnconfirmed} name='warning' />
          </View>
        }

      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const props = {}
  props.avatarImage = state.avatar.emails[ownProps.data.email]
  return props
}

export default connect(mapStateToProps)(ForwardAddressListItem)
