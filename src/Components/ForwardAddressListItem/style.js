import EStyleSheet from 'react-native-extended-stylesheet'
import palette from 'app/Styles/colors'

export const ListItem = EStyleSheet.create({
  container: {
    marginTop: '-0.7rem',
    marginBottom: '-0.7rem',
    marginLeft: '-1rem',
    marginRight: '-1rem',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  avatarOuter: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    height: 53,
  },

  avatarInner: {
    height: 40,
    width: 40,
  },

  avatar: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: 40,
    width: 40,
    borderWidth: 0.5,
	  borderColor: "rgb(151, 151, 151)"
  },

  avatarText: {
    fontSize: '0.75rem',
    fontWeight: '600',
  },

  avatarWithImage: {
    height: 41,
    width: 41,
  },

  avatarStatus: {
    position: 'absolute',
    width: 12,
    height: 12,
    bottom: -1,
    right: -1,
    borderRadius: 12,
    borderWidth: 0.5,
    borderColor: 'rgb(238, 242, 246)',
    backgroundColor: 'rgb(126, 211, 33)',
  },

  body: {
    paddingTop: '0.25rem',
    flex: 13,
  },

  bodyUnconfirmed: {
    marginRight: '1rem',
  },

  name: {
    color: '#000',
    lineHeight: '1rem',
  },

  nameUnconfirmed: {
    color: '#9b9b9b',
    lineHeight: '1rem',
    fontStyle: 'italic',
  },

  email: {
    lineHeight: '1rem',
    color: 'rgb(155, 155, 155)',
    fontSize: '0.8rem',
  },

  emailUnconfirmed: {
    lineHeight: '1rem',
    color: 'rgb(155, 155, 155)',
    fontSize: '0.8rem',
    fontStyle: 'italic'
  },

  iconUnconfirmed: {
    color: 'red',
    fontSize: '1.2rem',
  },

  timestamp: {
    flex: 4
  },

  unreadCount: {
    flex: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  unreadCountInner: {
    padding: '0.1rem',
    left: 10,
    borderRadius: 20,
    minWidth: 21,
    backgroundColor: 'rgb(240, 75, 76)',
    overflow: 'hidden',
  },

  unreadCountText: {
    padding: '0rem',
    color: '#fff',
    textAlign: 'center',
    fontSize: '0.8rem',
    fontWeight: '600'
  },

  contactEmail: {
    color: '#999',
    fontStyle: 'italic',
    fontSize: '0.75rem'
  },

})
