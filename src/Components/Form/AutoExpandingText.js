import React, { PureComponent } from 'react'

import palette from 'app/Styles/colors'
import TextInput from 'app/Components/BaseTextInput'
import { inputStyles } from './styles'

/**
 * Auto expands the height of the TextInput based on the amount of
 * text.
 *
 * Source - https://github.com/facebook/react-native/commit/481f560f64806ba3324cf722d6bf8c3f36ac74a5
 *
 * Use case - for Body text in compose email scene.
 */
export default class AutoExpandingTextInput extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      height: 0
    }

    this._onChange = this._onChange.bind(this)
    this._onContentSizeChange = this._onContentSizeChange.bind(this)
  }

  _onChange (event) {
    const text = event.nativeEvent.text

    if (typeof this.props.onChangeText === 'function') {
      this.props.onChangeText(text)
    }
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(event)
    }
  }

  _onContentSizeChange (event) {
    this.setState({
      height: event.nativeEvent.contentSize.height
    })
  }

  focus () {
    this.refs.input.focus()
  }

  render () {
    return (
      <TextInput
        {...this.props}
        multiline
        onChange={this._onChange}
        onContentSizeChange={this._onContentSizeChange}
        style={[this.props.style, {height: Math.max(200, this.state.height)}, inputStyles.inputAutoExpanding]}
        placeholderTextColor={palette.silver}
        value={this.props.value}
        ref='input'
      />
    )
  }
}
