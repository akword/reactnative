import React, { PureComponent } from 'react'
import SearchBar from 'react-native-search-bar'

export default class iOSSearchBar extends PureComponent {
  render () {
    return (
      <SearchBar
        onChangeText={this.props.onChangeText}
        onCancelButtonPress={this.props.onCancel}
        showsCancelButton={true}
        placeholder={this.props.placeholder}
        style={this.props.style}
        text={this.props.text}
      />
    )
  }
}
