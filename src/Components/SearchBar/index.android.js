import React, { PureComponent } from 'react'
import Search from 'react-native-search-box'

export default class SearchBar extends PureComponent {
  render () {
    return (
      <Search
        onChangeText={this.props.onChangeText}
        onCancel={this.props.onCancel}
      />
    )
  }
}
