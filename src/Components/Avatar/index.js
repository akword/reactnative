import PropTypes from 'prop-types';
import React from 'react';
import { Image, View, TouchableOpacity, Text } from 'react-native';
import BaseText from 'app/Components/BaseText'

const s = {
  avatarStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  textStyle: {
    color: '#fff',
    fontSize: 16,
    backgroundColor: 'transparent',
    fontWeight: '100',
  },
}

class Avatar extends React.PureComponent {

  static propTypes = {
    name: PropTypes.string,
    avatar: PropTypes.any,
    onPress: PropTypes.func,
    avatarStyle: Image.propTypes.style,
    textStyle: Text.propTypes.style,
  }

  static defaultProps = {
    name: '',
    avatar: null,
    onPress: null,
    avatarStyle: {},
    textStyle: {},
  }

  _getAvatarString () {
    let name = this.props.name || '';
    name = name
      .toUpperCase() // let them be all caps!
      .split(' ') // arrays are easier to work with!
      .map(str => str[0]) // we only want the first letter of the names
      .slice(0,3) // we only show three letters

    return name.join('')
  }

  _getAvatarColor() {
    const userName = this.props.name || '';
    let sumChars = 0;
    for(let i = 0; i < userName.length; i++) {
      sumChars += userName.charCodeAt(i);
    }

    // inspired by https://github.com/wbinnssmith/react-user-avatar
    // colors from https://flatuicolors.com/
    const colors = [
      '#e67e22', // carrot
      '#2ecc71', // emerald
      '#3498db', // peter river
      '#8e44ad', // wisteria
      '#e74c3c', // alizarin
      '#1abc9c', // turquoise
      '#2c3e50', // midnight blue
    ];

    return colors[sumChars % colors.length];
  }

  _renderAvatar() {
    if (typeof this.props.avatar === 'function') {
      return this.props.avatar();
    } else if (typeof this.props.avatar === 'string') {
      return (
        <Image
          source={{uri: this.props.avatar}}
          style={[s.avatarStyle, this.props.avatarStyle]}
        />
      );
    } else if (typeof this.props.avatar === 'number') {
      return (
        <Image
          source={this.props.avatar}
          style={[s.avatarStyle, this.props.avatarStyle]}
        />
      );
    }
    return null;
  }

  render() {
    if (!this.props.name && !this.props.avatar) {
      // render placeholder
      return (
        <View
          style={[
            s.avatarStyle,
            {backgroundColor: 'transparent'},
            this.props.avatarStyle,
          ]}
          accessibilityTraits="image"
        />
      )
    }
    if (this.props.avatar) {
      return (
        <TouchableOpacity
          disabled={this.props.onPress ? false : true}
          onPress={() => {
            const {onPress, ...other} = this.props;
            this.props.onPress && this.props.onPress(other);
          }}
          accessibilityTraits="image"
        >
          {this._renderAvatar()}
        </TouchableOpacity>
      );
    }

    return (
      <TouchableOpacity
        disabled={this.props.onPress ? false : true}
        onPress={() => {
          const {onPress, ...other} = this.props;
          this.props.onPress && this.props.onPress(other);
        }}
        style={[
          s.avatarStyle,
          {backgroundColor: this._getAvatarColor()},
          this.props.avatarStyle,
        ]}
        accessibilityTraits="image"
      >
        <BaseText style={[s.textStyle, this.props.textStyle]}>
          {this._getAvatarString()}
        </BaseText>
      </TouchableOpacity>
    );
  }
}

export default Avatar
