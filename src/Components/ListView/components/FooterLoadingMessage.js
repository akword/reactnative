import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { injectIntl, intlShape } from 'react-intl'
import m from 'commons/I18n'
import Text from 'app/Components/BaseText'

import styles from '../styles'

const FooterLoadingMessage = ({ intl }) => (
  <View style={styles.footerLoadingMessage}>
    <ActivityIndicator />
    <Text style={styles.footerLoadingMessageText}>{intl.formatMessage(m.app.Common.loadingEllipses).toUpperCase()}</Text>
  </View>
)

FooterLoadingMessage.propTypes = {
  intl: intlShape.isRequired
}

export default injectIntl(FooterLoadingMessage);
