import React, { PureComponent } from 'react'
import { Text } from 'react-native'

class BaseText extends PureComponent {
  render () {
    return (
      <Text {...this.props} style={this.props.style}>
        {this.props.children}
      </Text>
    )
  }
}

export default BaseText
