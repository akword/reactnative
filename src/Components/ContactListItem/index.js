import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View } from 'react-native'

import Avatar from 'app/Components/Avatar'
import Text from 'app/Components/BaseText'
import { ListItem as s } from './style'

// Todo: seems we have two places for ContactListItem. Needs to be removed to avoid confusing.

class ContactListItem extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    avatarImage: PropTypes.string,
  }

  render() {
    const { data, avatarImage } = this.props
    const avatarPath = data.thumbnailPath || avatarImage // data.thumbnailPath comes with when DeviceContact

    return (
      <View style={s.container}>
        <View style={s.avatarOuter}>
          <View style={s.avatarInner}>
            <Avatar
              // avatarStyle={[s.avatar, avatarImage && s.avatarWithImage]}
              textStyle={s.avatarText}
              name={data.display_name || data.email}
              avatar={avatarPath}
            />
          </View>
        </View>
        <View style={s.body}>
          <Text style={s.name}>{(data && data.display_name && data.display_name.trim()) || data.email}</Text>
          <Text style={s.email}>{data.email}</Text>
        </View>
      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const props = {}
  props.avatarImage = state.avatar.emails[ownProps.data.email]
  return props
}

export default connect(mapStateToProps)(ContactListItem)
