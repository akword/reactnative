import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, TouchableOpacity } from 'react-native'
import { SwipeListView, SwipeRow } from 'react-native-swipe-list-view'
import EStyleSheet from 'react-native-extended-stylesheet'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import Ripple from 'react-native-material-ripple'

import palette from 'app/Styles/colors'
import commonStyles from 'app/Styles/common'

const styles = EStyleSheet.create({
  itemContainer: {
    backgroundColor: palette.white,
    paddingTop: '0.7rem',
    paddingBottom: '0.7rem',
    paddingLeft: '1rem',
    paddingRight: '1rem',
  },

  selectIcon: {
    color: palette.silver,
    fontSize: '1.1rem',
    width: '2rem',
  },

  selectIconSelected: {
    color: palette.peterRiver
  }
})


export default class ListItem extends PureComponent {
  static propTypes = {
    data: PropTypes.object,
    component: PropTypes.func,
    onPressItem: PropTypes.func,
    onLongPress: PropTypes.func,

    // Enable item selection
    enableItemSelection: PropTypes.bool,
    // Is the item selected
    isSelected: PropTypes.bool,

    // Used for rendering swipe component
    rowId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    rowMap: PropTypes.object,
    swipeComponent: PropTypes.func,
    swipeLeftOpenValue: PropTypes.number,
    swipeRightOpenValue: PropTypes.number,
    isRipple: PropTypes.bool
  }

  static defaultProps = {
    swipeComponent: View,
    swipeLeftOpenValue: 0,
    swipeRightOpenValue: 0,
    isRipple: false
  }

  constructor(props) {
    super(props)

    this._handlePress = this._handlePress.bind(this)
    this._handleLongPress = this._handleLongPress.bind(this)
  }

  _handlePress() {
    const { onPressItem, data } = this.props

    if (!onPressItem) return

    onPressItem(data)
  }

  _handleLongPress() {
    const { onLongPress, data } = this.props

    if (!onLongPress) return

    onLongPress(data)
  }

  _renderSwipeComponent() {
    const { data, rowId, rowMap } = this.props
    const props = { data, rowId, rowMap }

    return <this.props.swipeComponent {...props} />
  }

  _renderSelect() {
    const { enableItemSelection, isSelected, data } = this.props
    if (!enableItemSelection) return null

    return (
      <FAIcon
        name={isSelected || data.isSelected ? 'check-circle' : 'circle-thin'}
        style={[styles.selectIcon, (isSelected || data.isSelected) && styles.selectIconSelected]}
      />
    )
  }

  closeSwipeRow() {
    this.refs.swipe.closeRow()
  }

  render() {
    const { data, swipeLeftOpenValue, swipeRightOpenValue, isRipple } = this.props

    let propOverrides = {}

    if (!swipeLeftOpenValue) {
      propOverrides['disableRightSwipe'] = true
    }

    if (!swipeRightOpenValue) {
      propOverrides['disableLeftSwipe'] = true
    }

    return (
      <SwipeRow
        key={data.id}
        leftOpenValue={swipeLeftOpenValue}
        rightOpenValue={swipeRightOpenValue}
        {...propOverrides}
        ref='swipe'
      >
        { this._renderSwipeComponent() }

        { isRipple?
          <Ripple
            onPress={this._handlePress}
            onLongPress={this._handleLongPress}
          >
            <TouchableOpacity
              style={[styles.itemContainer, commonStyles.horizontalAlign]}
              underlayColor={palette.white}
              activeOpacity={0.99}
            >
              { this._renderSelect() }
              <this.props.component data={data} />
            </TouchableOpacity>
          </Ripple> :
          <TouchableOpacity
            style={[styles.itemContainer, commonStyles.horizontalAlign]}
            underlayColor={palette.white}
            activeOpacity={0.99}
            onPress={this._handlePress}
            onLongPress={this._handleLongPress}
          >
            { this._renderSelect() }
            <this.props.component data={data} />
          </TouchableOpacity>
        }
      </SwipeRow>
    )
  }
}
