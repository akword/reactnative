import React, { Component } from 'react'
import { View } from 'react-native'

import styles from '../styles'

export default (props) => (
  <View style={styles.errorContainer}>
    <View style={styles.error}>
      {props.children}
    </View>
    <View style={styles.errorSpacer} />
  </View>
)
