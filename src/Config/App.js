const SETTINGS = {
  httpTimeout: 30000
}

export const BUILD_NUMBER = '1.0.0'

export default SETTINGS
