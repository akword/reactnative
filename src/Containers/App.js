import React, { Component } from 'react'
import { Platform, NativeEventEmitter, NativeModules, Alert } from 'react-native'
import { Provider } from 'react-redux'
import Immutable from 'seamless-immutable'

import applyConfigSettings from 'app/Config'
import log from 'commons/Services/Log'

import createStore from 'app/Redux/CreateStore'
import { getDeviceInfo } from 'app/Lib/Device'

import RootContainer from './RootContainer'
import PrivacySnapshot from 'react-native-privacy-snapshot'
import translationsData from './translations'
import BFService from '../../rn_modules/BackgroundToForegroundService'
import {bugsnag} from '../Services/BugSnag'

bugsnag.setUser('-1', '', '');

if (Platform.OS === 'ios') {
  const { RNImportService } = NativeModules

  const importServiceEmitter = new NativeEventEmitter(RNImportService)
  importServiceEmitter.addListener(
    RNImportService.FILES_IMPORTED,
    (files) => {
      console.info('File Imported:', files)
      // In iOS files.length = 1
      // In Android files.length >= 1

      // Alert.alert(
      //   'File',
      //   files[0],
      //   [
      //     {text: 'OK', onPress: () => console.log('OK Pressed')}
      //   ],
      //   { cancelable: false }
      // )
    }
  )

  // Triggers the notification if the file is imported from the dead state
  RNImportService.checkImportedFiles()
}

// Apply config overrides
applyConfigSettings()

const device = getDeviceInfo()

// create our store
const store = createStore({
  device: Immutable({
    isReactNative: true,
    platform: Platform.OS,
    uuid: device.device_uuid,
    type: device.device_type,
    ...device
  }),
  user: Immutable({
    data: {}
  }),
  intl: {
    locale: 'en-US',
    messages: translationsData['en-US'],
    allData: translationsData
  }
})

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  constructor (props) {
    super(props)

    try {
      PrivacySnapshot.enabled(true)
    } catch (e) {
      log.debug('Failed to set PrivacySnapshot')
    }
  }

  componentDidMount () {
    // Setup Android CallKit
    if (Platform.OS === 'android') {
      BFService.setUpAccount()
    }
  }

  componentWillUnmount () {
    // importSubscription.remove();
  }

  render () {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}

export default App
