import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native'
import { connect } from 'react-redux'
import EStyleSheet from 'react-native-extended-stylesheet'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Material from 'react-native-vector-icons/MaterialIcons'
import KeepAwake from 'react-native-keep-awake'
import PropTypes from 'prop-types';
import { path } from 'ramda'

import ChatActions from 'commons/Redux/ChatRedux'
import log from 'commons/Services/Log'
import {
  isRemoteVideoPlaying,
  isLocalVideoPlaying,
  getCallData,
  isInboundCall,
} from 'commons/Selectors/VideoChat'
import { ICE_GATHERING_STATE } from 'commons/Redux/WebrtcRedux'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'

import Header from './components/Header'
import MiddleTools from './components/MiddleTools'
import BottomTools from './components/BottomTools'
import Identity from './components/Identity'
import PrimaryVideo from './components/PrimaryVideo'
import SecondaryVideo from './components/SecondaryVideo'
import Logger from './components/Logger'

const s = EStyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: palette.black,
  },
})

class VideoChat extends Component {

  static navigationOptions = ({ navigation }) => ({
    navBarVisible: false,
    tabBarVisible: false,
    header: null
  })

  static propTypes = {
    inboundVideoCallOffer: PropTypes.object,
    videoCallRemoteFeedURL: PropTypes.string,
    videoCallLocalFeedURL: PropTypes.string,
  }

  constructor (props) {
    super(props)

    this.state = {
      containerWidth: 0,
      containerHeight: 0
    }
    
    this._endCall = this._endCall.bind(this)
    this._onLayout = this._onLayout.bind(this)
  }

  componentWillUnmount () {
    log.debug('VideoChat: componentWillUnmount')
    try {
      KeepAwake.deactivate()
    } catch (e) {}
  }

  _endCall () {
    const {
      callIsConnected,
      outboundVideoCallData,
      localIceGatheringState,
    } = this.props
    let isOutboundCancel = false
    if (!callIsConnected &&
      outboundVideoCallData &&
      localIceGatheringState === ICE_GATHERING_STATE.COMPLETE
    ) {
      this.props.cancelOutboundCall()
      isOutboundCancel = true
    }
    log.debug(`VideoChat: _endCall (reject: ${isOutboundCancel}) -`, this.props.callId)
    this.props.endVideoCall(this.props.callId, null, isOutboundCancel)
  }

  _onLayout (e) {
    this.setState({
      containerWidth: e.nativeEvent.layout.width,
      containerHeight: e.nativeEvent.layout.height
    })
  }

  render () {
    const props = {
      ...this.props,
      ...this.state,
      endCall: this._endCall,
    }

    try {
      KeepAwake.activate()
    } catch (e) {}

    return (
      <View style={s.container} onLayout={this._onLayout}>
        <PrimaryVideo {...props} />
        <SecondaryVideo {...props} />
        <Header {...props} />
        <Identity {...props} />
        <MiddleTools {...props} />
        <BottomTools {...props} />
        <Logger {...props} />
      </View>
    )
  }
}

const mapStateToProps = state => {
  const props = {}
  props.callId = path(['chat', 'inboundVideoCallOffer', 'call_id'], state) ||
                 path(['chat', 'outboundVideoCallAnswer', 'call_id'], state) ||
                 path(['chat', 'videoCallLocalId'], state)
  props.inboundVideoCallOffer = state.chat.inboundVideoCallOffer
  props.outboundVideoCallData = state.chat.outboundVideoCallData
  props.videoCallLocalFeedURL = state.chat.videoCallLocalFeedURL
  props.videoCallRemoteFeedURL = state.chat.videoCallRemoteFeedURL
  props.videoCallRemoteToggle = state.chat.videoCallRemoteToggle
  props.videoCallCameraIsMuted = state.chat.videoCallCameraIsMuted
  props.videoCallMicIsMuted = state.chat.videoCallMicIsMuted
  props.callIsConnected = state.chat.callIsConnected
  props.localIceGatheringState = state.webrtc.local.iceGatheringState,
  props.videoCallSpeakerIsMuted = state.chat.videoCallSpeakerIsMuted
  props.isAudioOnly = state.chat.isAudioOnly
  props.isRemoteAudioOnly = state.chat.isRemoteAudioOnly
  props.callData = getCallData(state)
  props.remoteVideoPlaying = isRemoteVideoPlaying(state)
  props.localVideoPlaying = isLocalVideoPlaying(state)
  props.isInboundCall = isInboundCall(state)
  if (props.callData && props.callData.contactEmail) {
    props.contactAvatar = state.avatar.emails[props.callData.contactEmail]
  }
  return props
}

const mapDispatchToProps = {
  endVideoCall: ChatActions.endVideoCall,
  toggleVideoCallCamera: ChatActions.toggleVideoCallCamera,
  toggleVideoCallMic: ChatActions.toggleVideoCallMic,
  toggleVideoCallSpeaker: ChatActions.toggleVideoCallSpeaker,
  uiAnswerInboundCall: ChatActions.uiAnswerInboundCall,
  uiRejectInboundCall: ChatActions.uiRejectInboundCall,
  cancelOutboundCall: ChatActions.cancelOutboundCall,
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoChat)
