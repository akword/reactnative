import React, { Component } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Icon from 'react-native-vector-icons/FontAwesome'

import log from 'commons/Services/Log'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'

const s = EStyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 95,
  },

  tools: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
    marginBottom: '3rem',
  },

  tool: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    borderRadius: 60,
    borderWidth: 1,
    borderColor: '#fff',
  },

  toolActive: {
    backgroundColor: '#fff',
  },

  toolIcon: {
    fontSize: '1.75rem',
    color: '#fff',
    backgroundColor: 'transparent',
  },

  toolIconActive: {
    color: '#000',
  },

  toolImage: {
    width: 27,
    height: 27,
    resizeMode: 'contain',
  },

  toolTxt: {
    color: '#fff',
    position: 'absolute',
    bottom: -22,
    backgroundColor: 'transparent',
  }
})

class VideoChatMiddleTools extends Component {

  constructor (props) {
    super(props)

    this._goToContact = this._goToContact.bind(this)
  }

  _goToContact () {
    const { navigation } = this.props
    navigation.goBack()
  }

  render () {
    const {
      isAudioOnly,
      toggleVideoCallCamera,
      toggleVideoCallMic,
      toggleVideoCallSpeaker,
      videoCallCameraIsMuted,
      videoCallMicIsMuted,
      videoCallSpeakerIsMuted,
      videoCallRemoteFeedURL,
      remoteVideoPlaying,
      localVideoPlaying,
    } = this.props

    if (true) {
      return null
    }

    const toolStyle = s.tool

    return (
      <View style={s.container}>
        <View style={s.tools}>
          <TouchableOpacity style={[toolStyle, videoCallMicIsMuted ? s.toolActive : {}]} onPress={toggleVideoCallMic}>
            <Icon style={[s.toolIcon, videoCallMicIsMuted ? s.toolIconActive : {}]} name='microphone-slash' />
            <Text style={s.toolTxt}>mute</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[toolStyle, videoCallSpeakerIsMuted ? {} : s.toolActive]} onPress={toggleVideoCallSpeaker}>
            <Icon style={[s.toolIcon, videoCallSpeakerIsMuted ? {} : s.toolIconActive]} name='volume-up' />
            <Text style={s.toolTxt}>speaker</Text>
          </TouchableOpacity>
        </View>
        <View style={s.tools}>
          <TouchableOpacity style={s.tool} onPress={this._goToContact}>
            <Image style={s.toolImage} source={require('app/Images/icons/logo.png')} />
            <Text style={s.toolTxt}>msgsafe.io</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[toolStyle, videoCallCameraIsMuted ? {} : s.toolActive]} onPress={toggleVideoCallCamera}>
            <Icon style={[s.toolIcon, videoCallCameraIsMuted ? {} : s.toolIconActive]} name='video-camera' />
            <Text style={s.toolTxt}>video</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

}

export default VideoChatMiddleTools