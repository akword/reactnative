import React, { Component } from 'react'
import { View } from 'react-native'
import { RTCView } from 'react-native-webrtc'
import EStyleSheet from 'react-native-extended-stylesheet'

const s = EStyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
  }
})

class VideoChatPrimaryVideo extends Component {

  render () {

    const {
      videoCallRemoteFeedURL,
      videoCallLocalFeedURL,
      remoteVideoPlaying,
      localVideoPlaying,
      containerWidth,
      containerHeight,
    } = this.props

    if (!remoteVideoPlaying && !localVideoPlaying) {
      return null
    }

    let streamURL = ''
    if (localVideoPlaying) {
      streamURL = videoCallLocalFeedURL
    }

    if (remoteVideoPlaying) {
      streamURL = videoCallRemoteFeedURL
    }

    const videoStyle = [s.container, {
      width: containerWidth,
      height: containerHeight,
    }]

    return (
      <RTCView
        key={0}
        streamURL={streamURL}
        style={videoStyle}
        objectFit='cover'
      />
    )
  }

}

export default VideoChatPrimaryVideo
