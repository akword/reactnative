import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native'
import { connect } from 'react-redux'
import EStyleSheet from 'react-native-extended-stylesheet'

import palette from 'app/Styles/colors'
import Timer from 'app/Components/Timer'

const s = EStyleSheet.create({
  container: {
    zIndex: 95,
  },

  duration: {
    color: '#fff',
    backgroundColor: 'transparent',
    fontSize: '1.25rem',
    marginTop: '0.5rem',
  }
})

class VideoChatCallDuration extends Component {

  static propTypes = {
    videoCallStartTime: PropTypes.instanceOf(Date)
  }

  render () {
    const { videoCallStartTime } = this.props
    if (!videoCallStartTime) {
      return null
    }
    return (
      <View style={s.container}>
        <Timer style={s.duration} startDate={videoCallStartTime} />
      </View>
    )
  }

}

const mapStateToProps = (state) => ({
  videoCallStartTime: state.chat.videoCallStartTime,
})

export default connect(mapStateToProps)(VideoChatCallDuration)