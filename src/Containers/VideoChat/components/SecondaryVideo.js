import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native'
import { RTCView } from 'react-native-webrtc'
import EStyleSheet from 'react-native-extended-stylesheet'

const s = EStyleSheet.create({
  video: {
    transform: [
      { scaleX: -1 },
    ],
    flex: 0,
    right: 19,
    bottom: 20,
    height: 140,
    width: 80,
    position: 'absolute',
    zIndex: 100,
  }
})

class VideoChatSecondaryVideo extends Component {

  constructor (props) {
    super(props)

    this.state = {
      right: 19
    }
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.callIsConnected !== this.props.callIsConnected) {
      this.setState({ right: 20 })
    }
  }

  render () {
    const {
      videoCallLocalFeedURL,
      remoteVideoPlaying,
      localVideoPlaying,
    } = this.props

    if (!localVideoPlaying || !remoteVideoPlaying) {
      return null
    }

    return (
      <RTCView
        key={this.state.key}
        streamURL={videoCallLocalFeedURL}
        mirror={true}
        style={[s.video, { right: this.state.right }]}
        objectFit='cover'
      />
    )
  }
  
}

export default VideoChatSecondaryVideo
