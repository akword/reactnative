import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Icon from 'react-native-vector-icons/FontAwesome'

import log from 'commons/Services/Log'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'

const END_CALL_BUTTON_HEIGHT = 60

const s = EStyleSheet.create({

  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    zIndex: 95,
    marginBottom: '1rem',
    height: 60,
  },

  endCall: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: END_CALL_BUTTON_HEIGHT,
    width: END_CALL_BUTTON_HEIGHT,
    backgroundColor: palette.pomegranate,
    borderRadius: END_CALL_BUTTON_HEIGHT / 2,
  },

  endCallIcon: {
    color: 'white',
    fontSize: '1.65rem',
    backgroundColor: 'transparent',
    transform: [{ rotate: '135deg' }]
  },

  icons: {
    position: 'absolute',
    left: 0,
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingRight: END_CALL_BUTTON_HEIGHT / 2,
  },

  iconWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '0.5rem',
  },

  icon: {
    color: 'white',
    fontSize: '1.5rem',
    backgroundColor: 'transparent',
  },

  iconRed: {
    color: 'red',
    fontSize: '1.5rem',
    backgroundColor: 'transparent'
  },
  
})

class VideoChatBottomToolsInProgress extends Component {

  constructor (props) {
    super(props)

    this._renderEndCall = this._renderEndCall.bind(this)
    this._renderHelpers = this._renderHelpers.bind(this)
  }

  _renderEndCall () {
    return (
      <TouchableOpacity style={[s.endCall]} onPress={this.props.endCall}>
        <Icon name='phone' style={s.endCallIcon} />
      </TouchableOpacity>
    )
  }

  _renderHelpers () {
    const {
      videoCallRemoteToggle,
      toggleVideoCallCamera,
      toggleVideoCallMic,
      toggleVideoCallSpeaker,
      videoCallCameraIsMuted,
      videoCallMicIsMuted,
      videoCallSpeakerIsMuted,
      videoCallRemoteFeedURL,
      remoteVideoPlaying,
      localVideoPlaying,
    } = this.props

    // if (!remoteVideoPlaying && !localVideoPlaying) {
    //   return null
    // }

    return (
      <View style={s.icons}>
        <TouchableOpacity style={s.iconWrapper} onPress={toggleVideoCallCamera}>
          <Icon
            style={videoCallCameraIsMuted ? s.iconRed: s.icon}
            name={videoCallCameraIsMuted ? 'eye-slash' : 'eye'}
          />
        </TouchableOpacity>

        <TouchableOpacity style={s.iconWrapper} onPress={toggleVideoCallMic}>
          <Icon
            style={videoCallMicIsMuted ? s.iconRed : s.icon}
            name={videoCallMicIsMuted ? 'microphone-slash' : 'microphone'}
          />
        </TouchableOpacity>
        <TouchableOpacity style={s.iconWrapper} onPress={toggleVideoCallSpeaker}>
          <Icon
            style={s.icon}
            name={videoCallSpeakerIsMuted ? 'volume-down' : 'volume-up'}
          />
        </TouchableOpacity>
      </View>
    )
  }

  render () {
    return (
      <View style={s.container}>
        {this._renderHelpers()}
        {this._renderEndCall()}
      </View>
    )
  }

}

export default VideoChatBottomToolsInProgress