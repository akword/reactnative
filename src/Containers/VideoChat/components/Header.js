import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Icon from 'react-native-vector-icons/FontAwesome'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'
import Avatar from 'app/Components/Avatar'

import CallDuration from './CallDuration'

const AVATAR_HEIGHT = 60

const s = EStyleSheet.create({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    backgroundColor: 'transparent',
    zIndex: 95,
    marginTop: '2rem',
    paddingHorizontal: '1rem',
    minHeight: AVATAR_HEIGHT,
  },

  titleContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingRight: '5rem',
    backgroundColor: 'transparent'
  },

  title: {
    color: 'white',
    fontSize: '1rem',
    fontWeight: '600'
  },

  avatarContainer: {
    position: 'absolute',
    right: '1rem',
  },

  avatar: {
    width: AVATAR_HEIGHT,
    height: AVATAR_HEIGHT,
    borderRadius: 30,
    borderColor: '#fff',
    borderWidth: 1,
  },

  avatarText: {
    fontSize: '1.5rem',
  },
})

class VideoChatHeader extends Component {

  constructor (props) {
    super(props)
    this._goToContact = this._goToContact.bind(this)
    this._renderName = this._renderName.bind(this)
    this._renderAvatar = this._renderAvatar.bind(this)
  }

  _renderName () {
    const { callData } = this.props
    const title = callData.contactDisplayName || callData.contactEmail
    return (
      <Text style={s.title}>
        {title}
      </Text>
    )
  }

  _renderAvatar () {
    const {
      contactAvatar,
      callData,
      remoteVideoPlaying
    } = this.props
    const title = callData.contactDisplayName || callData.contactEmail

    // do not render if there is incoming video
    if (remoteVideoPlaying) {
      return null
    }

    return (
      <TouchableOpacity style={s.avatarContainer} onPress={this._goToContact}>
        <Avatar
          avatar={contactAvatar}
          name={title}
          avatarStyle={s.avatar}
          textStyle={s.avatarText}
        />
      </TouchableOpacity>
    )  
  }

  _goToContact () {
    const { navigation, callData } = this.props
    navigation.navigate('ContactDetail', { id: callData.contactEmail })
  }

  render () {
    const {
      contactAvatar,
      videoCallRemoteFeedURL,
    } = this.props

    return (
      <View style={s.container}>
          <View style={s.titleContainer}>
            {this._renderName()}
            <CallDuration />
          </View>
          {this._renderAvatar()}
      </View>
    )
  }

}

export default VideoChatHeader