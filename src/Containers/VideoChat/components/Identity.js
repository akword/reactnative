import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'

const s = EStyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    zIndex: 95,
    paddingHorizontal: '1rem',
  },

  titleLabel: {
    color: '#fff',
    marginBottom: '0.5rem',
    fontSize: '1rem',
    backgroundColor: 'transparent',
  },

  title: {
    color: '#fff',
    fontSize: '1.25rem',
    backgroundColor: 'transparent',
  }
})

class VideoChatIdentity extends Component {

  render () {
    const {
      callData,
      remoteVideoPlaying,
      localVideoPlaying,
    } = this.props

    if (true) {
      return null
    }

    const title = callData.identityEmail
    return (
      <View style={s.container}>
        <Text style={s.titleLabel}>your identity is:</Text>
        <Text style={s.title}>{title}</Text>
      </View>
    )
  }

}

export default VideoChatIdentity
