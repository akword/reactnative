import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import PropTypes from 'prop-types';
import { path } from 'ramda'
import { withStateHandlers, compose, setStatic } from 'recompose'

import m from 'commons/I18n'
import NotificationActions from 'commons/Redux/NotificationRedux'
import UserEmailActions from 'commons/Redux/UserEmailRedux'

import Text from 'app/Components/BaseText'
import baseStyles from 'app/Components/ListView/styles'
import FlatListContainer from 'app/Components/FlatList'
import ListViewContainer from 'app/Components/ListView'
import NavButtonsWithSearch from 'app/Components/ListView/NavButtonsWithSearch'
import ForwardAddressListItem from 'app/Components/ForwardAddressListItem'
import ListViewError from 'app/Components/ListView/components/ListViewError'

import ListItemSwipe from './components/ListItemSwipe'

class ForwardAddressList extends Component {
  constructor (props) {
    super(props)

    this._goToDetailView = this._goToDetailView.bind(this)
    this._goToCreateView = this._goToCreateView.bind(this)
    this._handleOnUnmount = this._handleOnUnmount.bind(this)
  }

  _goToDetailView(data) {
    this.props.navigation.navigate('ForwardAddressDetail', { id: data.id })
  }

  _goToCreateView() {
    this.props.navigation.navigate('CreateForwardAddress')
  }

  _renderNoItemError() {
    const fm = this.props.intl.formatMessage
    return (
      <ListViewError>
        <Text style={baseStyles.noneTitleText}>{fm(m.native.ForwardAddress.noForwardAddresses)}</Text>
        <Text style={baseStyles.noneBodyText}>{fm(m.native.ForwardAddress.linkingMsgSafeToYourEmail)}</Text>
        <Text style={baseStyles.noneBodyText}>{fm(m.native.ForwardAddress.helpYouRecoverYourAccount)}</Text>
        <TouchableOpacity style={baseStyles.errorAction} onPress={this._goToCreateView}>
          <Text style={[baseStyles.noneBodyText, baseStyles.noneBodyActionText]}>
            {fm(m.native.ForwardAddress.clickHereToLinkEmailAddress)}
          </Text>
        </TouchableOpacity>
      </ListViewError>
    )
  }

  _handleOnUnmount () {
    const { navigation } = this.props
    if (
      !navigation.state.params ||
      !navigation.state.params.onUnmount ||
      typeof navigation.state.params.onUnmount !== 'function'
    ) {
      return
    }
    navigation.state.params.onUnmount()
  }

  componentWillMount() {
    const {fetchData} = this.props
    if(fetchData) {
      fetchData()
    }
  }

  componentWillUnmount() {
    this._handleOnUnmount()
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onSearchPress: this.props.toggleSearchBar.bind(this)
    })
  }

  render() {
    const { intl, searchBarVisible } = this.props
    const fm = intl.formatMessage
    let noSearchResultMessage = fm(m.native.ForwardAddress.noForwardAddresses)

    return (
      <View style={{flex: 1}}>

        <FlatListContainer
          listItemComponent={ForwardAddressListItem}
          listItemSwipeComponent={ListItemSwipe}
          swipePosition='right'
          swipeLeftOpenValue={0}
          swipeRightOpenValue={-70}
          navigatorActiveKey='contacts'
          selectItem={this._goToDetailView}
          fetchData={this.props.fetchData}
          initialSearchQuery={this.props.searchQuery}
          noDataMessage={this._renderNoItemError()}
          noSearchResultMessage={noSearchResultMessage}
          noTabBar
          noSearchBar={!searchBarVisible}
          {...this.props}
        />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  data: state.useremail.data,
  dataOrder: state.useremail.dataOrder,
  dataTotalCount: state.useremail.dataTotalCount,

  searchQuery: state.useremail.searchQuery,
  searchResultsData: state.useremail.searchResultsData,
  searchResultsDataOrder: state.useremail.searchResultsDataOrder,
  searchResultsDataTotalCount: state.useremail.searchResultsDataTotalCount,

  dataFetchInProgress: state.useremail.dataRequestInProgress,
  dataFetchSuccessful: state.useremail.dataRequestSuccessful,
  dataFetchError: state.useremail.dataRequestError,

  isRefreshing: state.useremail.isRefreshing,
  isPaginating: state.useremail.isPaginating,
  isSearching: state.useremail.isSearching
})

const mapDispatchToProps = {
  fetchData: UserEmailActions.useremailFetch,
  clearSearchResultsData: UserEmailActions.useremailClearSearchData,
  setSearchQuery: UserEmailActions.useremailSetSearchQuery
}

const navigationOptions = ({ navigation, screenProps: {fm} }) => {
  const { state } = navigation
  const onSearchPress = path(['params', 'onSearchPress'], state)

  return {
    tabBarVisible: false,
    title: fm(m.native.ForwardAddress.listTitle),
    headerRight: (
      <NavButtonsWithSearch onSearchPress={onSearchPress}>
        <TouchableOpacity onPress={() => navigation.navigate('CreateForwardAddress')}>
          <EntypoIcon name='plus' style={[baseStyles.navBarMenuIcon, baseStyles.navBarRightIcon]} />
        </TouchableOpacity>
      </NavButtonsWithSearch>
    )
  }
}

const enhance = compose(
  setStatic('navigationOptions', navigationOptions),
  connect(mapStateToProps, mapDispatchToProps),
  withStateHandlers(
    {searchBarVisible: false},
    {
      toggleSearchBar:
        ({ searchBarVisible }, { clearSearchResultsData }) =>
        () => ({ searchBarVisible: !searchBarVisible || !clearSearchResultsData() })
    }
  ),
  injectIntl
)

export default enhance(ForwardAddressList)
