import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import NotificationActions from 'commons/Redux/NotificationRedux'
import UserEmailActions from 'commons/Redux/UserEmailRedux'

import { ListItemSwipe as swipeStyle } from 'app/Components/ListView/styles'
import Text from 'app/Components/BaseText'

class UserEmailListItemSwipe extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    emailDeleteRequest: PropTypes.func.isRequired,
    displayNotification: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this._handleDelete = this._handleDelete.bind(this)
  }

  _handleDelete () {
    const {
      data,
      rowMap,
      rowId,
      emailDeleteRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${rowId}`].closeSwipeRow()
    emailDeleteRequest({id: data.id})
    displayNotification(intl.formatMessage(m.native.ForwardAddress.emailAddressDeleted), 'danger', 3000)
  }

  render () {
    return (
      <View style={[swipeStyle.container, { justifyContent: 'flex-end' }]}>
        <TouchableOpacity
          style={[swipeStyle.button, swipeStyle.buttonDanger]}
          onPress={this._handleDelete}
        >
          <Icon style={swipeStyle.icon} name='trash' />
          <Text style={swipeStyle.text}>{this.props.intl.formatMessage(m.app.Common.delete)}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapDispatchToProps = {
  emailDeleteRequest: UserEmailActions.useremailRemove,
  displayNotification: NotificationActions.displayNotification
}

export default connect(null, mapDispatchToProps)(injectIntl(UserEmailListItemSwipe))
