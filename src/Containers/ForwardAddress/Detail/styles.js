import EStyleSheet from 'react-native-extended-stylesheet'

import palette from 'app/Styles/colors'

const styles = EStyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f9f9f9'
  },

  verificationMessageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: '1rem',
    paddingBottom: '1rem',
    borderTopWidth: 1,
    borderTopColor: palette.clouds
  },

  verificationMessageText: {
    fontSize: '0.75rem',
    color: palette.wetAsphalt
  },

  verificationMessageLastSentText: {
    fontSize: '0.65rem',
    marginTop: '0.5rem',
    color: palette.concrete
  },

  verificationMessageButton: {
    marginTop: '0.5rem',
    fontSize: '0.65rem',
    fontWeight: '800',
    color: palette.belizeHole,
    letterSpacing: '0.04rem'
  }
})

export default styles
