import React, { Component } from 'react';
import { injectIntl } from 'react-intl'
import { View, ScrollView, Button as RNButton, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types';
import { pathOr } from 'ramda'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'

import moment from 'moment'
import { connect } from 'react-redux'

import m from 'commons/I18n'
import { timeAgo } from 'commons/Lib/Utils'
import NotificationActions from 'commons/Redux/NotificationRedux'
import UserEmailActions from 'commons/Redux/UserEmailRedux'
import { getDataItemForId } from 'commons/Redux/_Utils'

import Text from 'app/Components/BaseText'
import SimpleTiles from 'app/Components/SimpleTiles'
import { Header } from 'app/Components/DetailView'
import HeaderButton from 'app/Components/HeaderButton'
import palette from 'app/Styles/colors'

import styles from './styles'

class ForwardAddressDetail extends Component {
  constructor (props) {
    super(props)

    this._getHistoryData = this._getHistoryData.bind(this)
    this._requestConfirmationEmail = this._requestConfirmationEmail.bind(this)
  }

  _getHistoryData() {
    const { data, intl } = this.props
    const fm = intl.formatMessage

    const lastActivityOn = data.last_activity_on || data.created_on
    return [
      { title: fm(m.app.Common.lastActivity), description: `${moment.utc(lastActivityOn).fromNow()}` },
      { title: fm(m.app.Common.modified), description: `${moment.utc(data.modified_on).fromNow()}` },
      { title: fm(m.app.Common.created), description: `${moment.utc(data.created_on).fromNow()}` },
    ]
  }

  _requestConfirmationEmail () {
    const { data, intl, displayNotification, useremailRequestConfirmation } = this.props
    const fm = intl.formatMessage

    useremailRequestConfirmation(
      {id: data.id},
      () => displayNotification(fm(m.native.ForwardAddress.verificationEmailSent), 'success', 3000),
      () => displayNotification(fm(m.native.ForwardAddress.unableToSendVerificationEmail), 'danger', 3000),
    )
  }

  _renderConfirmMessage () {
    const { data, intl } = this.props
    if (data.is_confirmed) return null
    const fm = intl.formatMessage

    return (
      <View style={styles.verificationMessageContainer}>
        <Text style={styles.verificationMessageText}>{fm(m.native.ForwardAddress.addressNotBeenVerified)}</Text>
        <TouchableOpacity onPress={this._requestConfirmationEmail}>
          <Text style={styles.verificationMessageButton}>{fm(m.native.ForwardAddress.sendVerificationEmail).toUpperCase()}</Text>
        </TouchableOpacity>
        { data.confirmation_sent_on && <Text style={styles.verificationMessageLastSentText}>{fm(m.native.ForwardAddress.lastConfirmationRequest, {lastTest: timeAgo(data.confirmation_sent_on, intl)})}</Text> }
      </View>
    )
  }

  render() {
    const { data, intl, avatar } = this.props
    const fm = intl.formatMessage

    return (
      <ScrollView style={styles.main}>
        <Header name={data.display_name} email={data.email} avatar={avatar} />

        { this._renderConfirmMessage() }

        <SimpleTiles
          title={fm(m.app.Billing.history).toUpperCase()}
          titleIconComponent={FontAwesomeIcon}
          titleIconName='history'
          tilesData={this._getHistoryData()}
        />

      </ScrollView>
    )
  }
}

const IntlForwardAddressDetail = injectIntl(ForwardAddressDetail)
IntlForwardAddressDetail.navigationOptions = ({ navigation, screenProps: {fm} }) => ({
  tabBarVisible: false,
  title: fm(m.native.ForwardAddress.listTitle),
  headerLeft: <HeaderButton isBack onPress={() => navigation.goBack()} />,
  headerRight: navigation.state.params.noEdit ? null : (
    <HeaderButton
      title={fm(m.app.Common.edit)}
      onPress={() => navigation.navigate('CreateForwardAddress', { id: navigation.state.params.id })}
      color={palette.link}
    />
  )
})

const mapStateToProps = (state, ownProps) => {
  const props = {}

  props.data = pathOr(
    getDataItemForId(state.useremail, ownProps.navigation.state.params.id),
    ['navigation', 'state', 'params', 'detail'],
    ownProps
  )
  if(props.data && props.data.email) {
    props.avatar = state.avatar.emails[props.data.email]
  }
  return props
}

const mapDispatchToProps = {
  useremailRequestConfirmation: UserEmailActions.useremailRequestConfirmation,
  displayNotification: NotificationActions.displayNotification
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlForwardAddressDetail)
