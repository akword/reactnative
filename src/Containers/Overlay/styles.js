import EStyleSheet from 'react-native-extended-stylesheet'

export const overlayStyles = {
  container: {
    backgroundColor: 'rgba(32, 48, 90, 0.8)',
  },

  childrenWrapper: {
    flex: 1,
    backgroundColor: 'white',
    padding: 0,
    // paddingTop: 40,
    borderRadius: 8
  }
}

export const styles = EStyleSheet.create({

})
