import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { path } from 'ramda'
import { ScrollView, Button as RNButton } from 'react-native'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import EStyleSheet from 'react-native-extended-stylesheet'
import moment from 'moment'
import emojiFlags from 'emoji-flags'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import ContactActions from 'commons/Redux/ContactRedux'
import MailboxActions from 'commons/Redux/MailboxRedux'
import { getDataItemForId } from 'commons/Redux/_Utils'

import Text from 'app/Components/BaseText'
import SimpleTiles from 'app/Components/SimpleTiles'
import {Header, Button, ButtonGroup} from 'app/Components/DetailView'
import HeaderButton from 'app/Components/HeaderButton'
import palette from 'app/Styles/colors'

const styles = EStyleSheet.create({
  main: {
    flex: 1,
    paddingBottom: '3.125rem',
    backgroundColor: '#f9f9f9',
  },

  country: {
    marginTop: '0.3rem',
    flexDirection: 'row',
    alignItems: 'center',
    color: palette.concrete,
    fontSize: '0.7rem',
    marginRight: '0.3rem'
  },
});

class IdentityDetail extends Component {

  constructor(props) {
    super(props)

    this._goToEmailsForIdentity = this._goToEmailsForIdentity.bind(this)
    this._goToContactsForIdentity = this._goToContactsForIdentity.bind(this)
  }

  _goToEmailsForIdentity () {
    const { data, setFilterIdentityIdForEmails, navigation } = this.props
    setFilterIdentityIdForEmails(data.id)
    navigation.navigate('MailboxList' )
  }

  _goToContactsForIdentity() {
    const {
      data,
      setFilterIdentityIdForContacts,
      clearFilterIdentityIdForContacts,
      navigation,
    } = this.props
    setFilterIdentityIdForContacts(data.id)
    navigation.navigate('ContactSelection', {
      identityId: data.id,
      onUnmount: clearFilterIdentityIdForContacts
    })
  }

  _getHistoryData() {
    const { data, intl } = this.props
    const fm = intl.formatMessage
    return [
      { title: fm(m.native.Contact.lastActivity), description: `${moment.utc(data.last_activity_on).fromNow()}` },
      { title: fm(m.native.Contact.modified), description: `${moment.utc(data.modified_on).fromNow()}` },
      { title: fm(m.native.Contact.created), description: `${moment.utc(data.created_on).fromNow()}` },
    ]
  }

  _getEmailsCountData() {
    const { data, intl } = this.props
    const fm = intl.formatMessage
    return [
      {
        title: fm(m.native.Contact.received),
        description: data.lt_mail_recv,
        // footerButtonText: 'View All'
      },
      {
        title: fm(m.native.Contact.sent),
        description: data.lt_mail_sent,
        // footerButtonText: 'View All'
      },
      {
        title: fm(m.native.Contact.blocked),
        description: data.lt_mail_recv_blocked,
        // footerButtonText: 'View All'
      },
    ]
  }

  render() {
    const { data, avatarImage, intl } = this.props
    const fm = intl.formatMessage
    const country = (data.region && data.region !== 'zz') ? emojiFlags.countryCode(data.region) : {name: 'Random', emoji: '🌎'}

    return (
      <ScrollView style={styles.main}>

        <Header name={`${data.display_name} `} email={data.email}  avatar={avatarImage}>
          <Text style={styles.country}>{country.emoji} {country.name}</Text>
        </Header>

        <ButtonGroup>

          <Button
            text={fm(m.native.Contact.emails)}
            iconComponent={EntypoIcon}
            iconName='mail'
            onPress={this._goToEmailsForIdentity}
          />

          <Button
            text={fm(m.native.Contact.associatedContacts)}
            iconComponent={EntypoIcon}
            iconName='v-card'
            onPress={this._goToContactsForIdentity}
          />

        </ButtonGroup>

        <SimpleTiles
          title={fm(m.native.Contact.history)}
          titleIconComponent={FontAwesomeIcon}
          titleIconName='history'
          tilesData={this._getHistoryData()}
        />

        <SimpleTiles
          title={fm(m.native.Contact.emails).toUpperCase()}
          titleIconComponent={FontAwesomeIcon}
          titleIconName='envelope'
          tilesData={this._getEmailsCountData()}
        />

      </ScrollView>
    )
  }
}
const IntlIdentityDetail = injectIntl(IdentityDetail)
IntlIdentityDetail.navigationOptions = ({ navigation, screenProps: {fm} }) => ({
  tabBarVisible: false,
  title: fm(m.native.Contact.identity),
  headerRight: (
    <HeaderButton
      title={fm(m.app.Common.edit)}
      onPress={() => navigation.navigate('EditIdentity', { id: navigation.state.params.id })}
      color={palette.link}
    />
  )
})

const mapStateToProps = (state, ownProps) => {
  const props = {}
  props.data = getDataItemForId(state.identity, ownProps.navigation.state.params.id)
  props.avatarImage = state.avatar.emails[props.data.email]
  return props
}

const mapDispatchToProps = {
  setFilterIdentityIdForContacts: ContactActions.toggleContactIdentityFilter,
  clearFilterIdentityIdForContacts: ContactActions.clearContactIdentityFilter,
  setFilterIdentityIdForEmails: MailboxActions.toggleMailboxIdentityFilter,
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlIdentityDetail)
