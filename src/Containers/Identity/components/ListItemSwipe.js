import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import NotificationActions from 'commons/Redux/NotificationRedux'
import IdentityActions from 'commons/Redux/IdentityRedux'

import { ListItemSwipe as swipeStyle } from 'app/Components/ListView/styles'
import Text from 'app/Components/BaseText'

const noop = () => {}

class IdentityListItemSwipe extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    identityDeleteRequest: PropTypes.func.isRequired,
    displayNotification: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this._handleDelete = this._handleDelete.bind(this)
  }

  _handleDelete () {
    const {
      data,
      rowMap,
      rowId,
      identityDeleteRequest,
      displayNotification,
      intl
    } = this.props
    const fm = intl.formatMessage
    Alert.alert(
      fm(m.native.Contact.deleteIdentityName, {name: data.display_name}),
      fm(m.native.Contact.deleteAllEmailsAssociated),
      [
        { text: fm(m.native.Contact.deleteAllEmails), onPress: () => {
          rowMap[`${rowId}`].closeSwipeRow()
          identityDeleteRequest({ id: data.id, delete_mail_history: true }, noop, noop)
          displayNotification(fm(m.native.Contact.identityDeleted), 'danger', 3000)
        }},
        { text: fm(m.native.Contact.saveAllEmails), onPress: () => {
          rowMap[`${rowId}`].closeSwipeRow()
          identityDeleteRequest({ id: data.id, delete_mail_history: false }, noop, noop)
          displayNotification(fm(m.native.Contact.identityDeleted), 'danger', 3000)
        }},
        { text: fm(m.app.Common.cancel), onPress: () => {
          rowMap[`${rowId}`].closeSwipeRow()
          displayNotification(fm(m.native.Contact.identityIsNotDeleted), 'info', 3000)
        }},
      ]
    )
  }

  render () {
    const fm = this.props.intl.formatMessage
    return (
      <View style={[swipeStyle.container, { justifyContent: 'flex-end' }]}>
        <TouchableOpacity
          style={[swipeStyle.button, swipeStyle.buttonDanger]}
          onPress={this._handleDelete}
        >
          <Icon style={swipeStyle.icon} name='trash' />
          <Text style={swipeStyle.text}>{fm(m.app.Common.delete)}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapDispatchToProps = {
  identityDeleteRequest: IdentityActions.identityRemove,
  displayNotification: NotificationActions.displayNotification
}

export default connect(null, mapDispatchToProps)(injectIntl(IdentityListItemSwipe))
