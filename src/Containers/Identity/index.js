import React, { Component } from 'react'
import { View, TouchableOpacity, Alert } from 'react-native'
import { connect } from 'react-redux'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import { path } from 'ramda'
import { withStateHandlers, compose, setStatic } from 'recompose'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import Text from 'app/Components/BaseText'
import IdentityActions from 'commons/Redux/IdentityRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'
import FlatListContainer from 'app/Components/FlatList'
import ListViewContainer from 'app/Components/ListView'
import ListViewError from 'app/Components/ListView/components/ListViewError'
import NavButtonsWithSearch from 'app/Components/ListView/NavButtonsWithSearch'
import ContactListItem from 'app/Components/ContactListItem'

import baseStyles from 'app/Components/ListView/styles'
import HeaderButton from 'app/Components/HeaderButton'

import ListItemSwipe from './components/ListItemSwipe'

class Identity extends Component {
  constructor (props) {
    super(props)

    this._goToDetailView = this._goToDetailView.bind(this)
    this._goToCreateView = this._goToCreateView.bind(this)
    this._handleOnUnmount = this._handleOnUnmount.bind(this)
    this._renderNoItemError = this._renderNoItemError.bind(this)
  }

  _goToDetailView (data) {
    this.props.navigation.navigate('IdentityDetail', { id: data.id })
  }

  _goToCreateView () {
    this.props.navigation.navigate('CreateIdentity')
  }

  _renderNoItemError () {
    const fm = this.props.intl.formatMessage
    return (
      <ListViewError>
        <Text style={baseStyles.errorText}>{fm(m.native.Contact.noIdentitiesFound)}</Text>
        <TouchableOpacity style={baseStyles.errorAction} onPress={this._goToCreateView}>
          <Text style={[baseStyles.errorText, baseStyles.errorActionText]}>{fm(m.native.Contact.clickHereToAddRecord)}</Text>
        </TouchableOpacity>
      </ListViewError>
    )
  }

  _handleOnUnmount () {
    const { navigation, intl } = this.props
    if (
      !navigation.state.params ||
      !navigation.state.params.onUnmount ||
      typeof navigation.state.params.onUnmount !== 'function'
    ) {
      return
    }
    navigation.state.params.onUnmount()
  }

  componentWillUnmount () {
    this._handleOnUnmount()
  }

  componentDidMount () {
    this.props.navigation.setParams({
      onSearchPress: this.props.toggleSearchBar.bind(this)
    })
  }

  render () {
    const { searchBarVisible, intl } = this.props

    return (
      <FlatListContainer
        listItemComponent={ContactListItem}
        navigatorActiveKey='identity'
        selectItem={this._goToDetailView}
        fetchData={this.props.fetchIdentities}
        initialSearchQuery={this.props.searchQuery}
        noDataMessage={this._renderNoItemError()}
        noSearchResultMessage={intl.formatMessage(m.native.Contact.noIdentitiesFound)}
        listItemSwipeComponent={ListItemSwipe}
        swipePosition='right'
        swipeLeftOpenValue={0}
        swipeRightOpenValue={-70}
        noTabBar
        noSearchBar={!searchBarVisible}
        {...this.props}
      />
    )
  }
}


const mapStateToProps = state => ({
  data: state.identity.data,
  dataOrder: state.identity.dataOrder,
  dataTotalCount: state.identity.dataTotalCount,

  searchQuery: state.identity.searchQuery,
  searchResultsData: state.identity.searchResultsData,
  searchResultsDataOrder: state.identity.searchResultsDataOrder,
  searchResultsDataTotalCount: state.identity.searchResultsDataTotalCount,

  dataFetchInProgress: state.identity.dataRequestInProgress,
  dataFetchSuccessful: state.identity.dataRequestSuccessful,
  dataFetchError: state.identity.dataRequestError,

  isRefreshing: state.identity.isRefreshing,
  isPaginating: state.identity.isPaginating,
  isSearching: state.identity.isSearching
})

const mapDispatchToProps = {
  fetchIdentities: IdentityActions.identityFetch,
  clearSearchResultsData: IdentityActions.clearIdentitySearchData,
  setSearchQuery: IdentityActions.identitySetSearchQuery
}

const navigationOptions = ({ navigation, screenProps: {fm} }) => {
  const headerLeftProps = path(['state', 'params', 'headerLeftProps'], navigation)
  const onSearchPress = path(['state', 'params', 'onSearchPress'], navigation)

  const options = {
    tabBarVisible: false,
    title: fm(m.native.Contact.identities),
    headerRight: (
      <NavButtonsWithSearch onSearchPress={onSearchPress}>
        <TouchableOpacity onPress={() => navigation.navigate('CreateIdentity')}>
          <EntypoIcon name='plus' style={[baseStyles.navBarMenuIcon, baseStyles.navBarRightIcon]} />
        </TouchableOpacity>
      </NavButtonsWithSearch>
    )
  }

  if (!headerLeftProps) { return options }

  return {
    ...options,
    headerLeft: <HeaderButton {...headerLeftProps} onPress={() => navigation.goBack()} />
  }
}

const enhance = compose(
  setStatic('navigationOptions', navigationOptions),
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl,
  withStateHandlers(
    {searchBarVisible: false},
    {
      toggleSearchBar:
        ({ searchBarVisible }, { clearSearchResultsData }) =>
        () => ({ searchBarVisible: !searchBarVisible || !clearSearchResultsData() })
    }
  )
)

export default enhance(Identity)
