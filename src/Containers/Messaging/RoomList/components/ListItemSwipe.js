import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import NotificationActions from 'commons/Redux/NotificationRedux'
import ChatActions from 'commons/Redux/ChatRedux'

import baseStyles, { ListItemSwipe as swipeStyle } from 'app/Components/ListView/styles'
import Text from 'app/Components/BaseText'

class ChatListItemSwipe extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    chatDeleteRequest: PropTypes.func.isRequired,
    displayNotification: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)
    this._handleDelete = this._handleDelete.bind(this)
  }

  _handleDelete () {
    const {
      data,
      rowMap,
      rowId,
      secdId,
      chatDeleteRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${secdId}${rowId}`].closeSwipeRow()
    chatDeleteRequest(data.room_id)
    displayNotification(intl.formatMessage(m.app.Common.deleted), 'danger', 3000)
  }

  render () {
    const fm = this.props.intl.formatMessage
    return (
      <View style={[swipeStyle.container, { justifyContent: 'flex-end' }]}>
        <TouchableOpacity
          style={[swipeStyle.button, swipeStyle.buttonDanger]}
          onPress={this._handleDelete}
        >
          <Icon style={swipeStyle.icon} name='trash' />
          <Text style={swipeStyle.text}>{fm(m.app.Common.delete)}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const mapDispatchToProps = {
  chatDeleteRequest: ChatActions.chatLeaveRoomRequest,
  displayNotification: NotificationActions.displayNotification,
}

export default connect(null, mapDispatchToProps)(injectIntl(ChatListItemSwipe))
