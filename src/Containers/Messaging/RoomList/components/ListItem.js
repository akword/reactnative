import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import moment from 'moment'

import { uuidv1ToDate } from 'commons/Lib/Utils'
import {
  getRoomTitle,
  getContactMember,
  getTotalUnreadForRoomId,
} from 'commons/Selectors/Messaging'

import Avatar from 'app/Components/Avatar'
import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'
import { ListItemStyle as s } from './style'

class ChatListItem extends Component {

  static propTypes = {
    timezone: PropTypes.string,
    contactMember: PropTypes.object,
    avatarImage: PropTypes.string,
  }

  constructor (props) {
    super(props)

    this._getMomentTime = this._getMomentTime.bind(this)
    this._renderTimestamp = this._renderTimestamp.bind(this)
    this._renderUnreadCount = this._renderUnreadCount.bind(this)
  }

  _getMomentTime (dateTime) {
    const { timezone } = this.props
    if (!dateTime) return ''
    let momentTime = null
    if (timezone) {
      momentTime = moment.tz(dateTime, timezone)
    } else {
      const offset = (-1 * (new Date()).getTimezoneOffset())
      momentTime = moment(dateTime).utcOffset(offset)
    }
    return momentTime
  }

  _formatRelative (momentTime) {
    if (momentTime.format('YYYY-MMM-DD') === moment().format('YYYY-MMM-DD')) {
      return momentTime.format('h:mm a')
    }
    if (momentTime.format('YYYY-MMM-DD') === moment().subtract(1, 'days').format('YYYY-MMM-DD')) {
      return 'yesterday'
    }
    return momentTime.format('M/D/YY')
  }

  _renderTimestamp () {
    const { data } = this.props
    let lastTime = uuidv1ToDate(data.last_message_id || data.room_id)
    
    lastTime = this._getMomentTime(lastTime)
    lastTime = this._formatRelative(lastTime)

    return (
      <View style={s.timestamp}>
        <Text>
          {lastTime}
        </Text>
      </View>
    )
  }

  _renderUnreadCount () {
    const { data, totalUnread } = this.props
    if (!totalUnread) return null
    return (
      <View style={s.unreadCount}>
        <View style={s.unreadCountInner}>
          <Text style={s.unreadCountText}>{totalUnread}</Text>
        </View>
      </View>
    )
  }

  render() {
    const { data, contactMember, avatarImage } = this.props
    const roomTitle = getRoomTitle(data, data.member_email)
    const isContactOnline = Boolean(contactMember.public_key)
    return (
      <View style={s.container}>
        <View style={s.avatarOuter}>
          <View style={s.avatarInner}>
            <Avatar
              // avatarStyle={[s.avatar, avatarImage && s.avatarWithImage]}
              name={roomTitle}
              avatar={avatarImage}
            />
            {isContactOnline && <View style={s.avatarStatus} />}
          </View>
        </View>
        <View style={s.body}>
          <Text style={s.name}>{roomTitle}</Text>
          <Text style={s.email}>{contactMember.email}</Text>
        </View>
        {this._renderUnreadCount() || this._renderTimestamp()}
      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const contactMember = getContactMember(ownProps.data)
  const totalUnread = getTotalUnreadForRoomId(state, ownProps.data.room_id)
  const avatarImage = state.avatar.emails[contactMember.email]
  const isAvatarChecked = Boolean(state.avatar.checkedEmails[contactMember.email])
  return {
    timezone: state.user.data.timezone || null,
    contactMember,
    avatarImage,
    isAvatarChecked,
    totalUnread,
  }
}

const ConnectedChatListItem = connect(mapStateToProps)(ChatListItem)

export default ConnectedChatListItem
