import React, { Component } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Entypo'
import { path } from 'ramda'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import log from 'commons/Services/Log'
import { getContactMember, getMapKeyForRoom } from 'commons/Selectors/Messaging'

import Text from 'app/Components/BaseText'
import ChatActions from 'commons/Redux/ChatRedux'
import ListViewContainer from 'app/Components/ListView'
import ListViewError from 'app/Components/ListView/components/ListViewError'
import baseStyles from 'app/Components/ListView/styles'
import ContactListItem from 'app/Components/ContactListItem'

import ChatListItem from './components/ListItem'
import ListItemSwipe from './components/ListItemSwipe'
import ContactSubselect, { FORM_IDENTIFIER } from './components/ContactSubselect'
import NoDataMessage from './components/NoDataMessage'

class ChatRoomList extends Component {

  constructor (props) {
    super(props)

    this._goToChatView = this._goToChatView.bind(this)
    this._updateNavHeader = this._updateNavHeader.bind(this)
    this._renderNoItemError = this._renderNoItemError.bind(this)

    this._updateNavHeader()
  }

  _goToChatView (data) {
    if (!this.props.socketConnected) {
      return
    }

    const contactMember = getContactMember(data)
    if (!contactMember.email) {
      return log.error(`No contact member for room: ${data.room_id}`)
    }
    this.props.navigation.navigate('MessagingRoom', { roomsMapKey: getMapKeyForRoom(data) })
  }

  _updateNavHeader (enabled = false) {
    const { navigation } = this.props
    navigation.setParams({
      createEnabled: enabled
    })
  }

  _renderNoItemError() {
    const { navigation } = this.props
    return (
      <NoDataMessage navigation={navigation} />
    )
  }

  componentWillMount () {
    // this._updateNavHeader(this.props.socketConnected)
  }

  componentWillReceiveProps (nextProps) {
    // if (this.props.socketConnected !== nextProps.socketConnected) {
    //   this._updateNavHeader(nextProps.socketConnected)
    // }
  }

  render() {
    const fm = this.props.intl.formatMessage
    return (
      <ListViewContainer
        listItemComponent={ChatListItem}
        navigatorActiveKey='chat'
        selectItem={this._goToChatView}
        fetchData={this.props.chatFetch}
        noDataMessage={this._renderNoItemError()}
        noSearchResultMessage={fm(m.native.Chat.noChatsFound)}
        listItemSwipeComponent={ListItemSwipe}
        swipePosition='right'
        swipeLeftOpenValue={0}
        swipeRightOpenValue={-70}
        {...this.props}
        dataFetchInProgress={
          this.props.dataFetchInProgress || (!this.props.data && !this.props.socketConnected)
        }
      />
    )
  }
}

const IntlChatRoomList = injectIntl(ChatRoomList)
IntlChatRoomList.navigationOptions = ({ navigation, screenProps: {fm} }) => {
  const createEnabled = path(['state', 'params', 'createEnabled'], navigation)
  return {
    title: fm(m.native.Chat.secureChat),
    headerRight: (
      <TouchableOpacity
        onPress={() => navigation.navigate('ContactSelection', {
          subselectComponent: ContactSubselect,
          reduxFromIdentifier: FORM_IDENTIFIER,
          title: fm(m.native.Chat.selectContact),
          listItemComponent: ContactListItem,
          createScreenIdentifier: 'MessagingCreateRoom',
          tabBarVisible: false,
          headerLeftProps: { isBack: true }
        })}
      >
        <Icon
          name='plus'
          style={[
            baseStyles.navBarMenuIcon,
            baseStyles.navBarRightIcon,
          ]}
        />
      </TouchableOpacity>
    )
  }
}
const mapStateToProps = state => ({
  data: state.chat.data,
  dataOrder: state.chat.dataOrder,
  dataTotalCount: state.chat.dataTotalCount,

  dataFetchInProgress: state.chat.dataRequestInProgress,
  dataFetchSuccessful: state.chat.dataRequestSuccessful,
  dataFetchError: state.chat.dataRequestError,

  isRefreshing: state.chat.isRefreshing,
  isPaginating: state.chat.isPaginating,
  isSearching: state.chat.isSearching,

  socketConnected: state.chat.socketConnected
})

const mapDispatchToProps = {
  chatInit: ChatActions.chatInit,
  chatFetch: ChatActions.chatFetch
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlChatRoomList)
