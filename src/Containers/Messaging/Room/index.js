import React, { Component } from 'react';
import { View, TouchableOpacity, ActivityIndicator, Image, Keyboard } from 'react-native'
import PropTypes from 'prop-types';
import { path } from 'ramda'
import { connect } from 'react-redux'

import { getContactMember, getRoomByMapKey } from 'commons/Selectors/Messaging'
import ChatActions from 'commons/Redux/ChatRedux'
import IdentityActions from 'commons/Redux/IdentityRedux'

import Text from 'app/Components/BaseText'
import Drawer from 'app/Components/Drawer'

import RoomHeader from './components/RoomHeader'
import RoomInfo from './components/RoomInfo'
import RegularRoom from './components/RegularRoom'
import EphemeralRoom from './components/EphemeralRoom'

import s from './styles'
import { Z_INDEX_ROOM_INFO } from './constants'

class TextChat extends Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarVisible: false,
    header: false
  })

  static propTypes = {
    data: PropTypes.object,
    ephemeralData: PropTypes.object,
    contact: PropTypes.object,
    identityCache: PropTypes.object,
    getIdentityApi: PropTypes.object,

    chatGetMessagesRequest: PropTypes.func.isRequired,
    chatCreateRoomRequest: PropTypes.func.isRequired,
    getIdentity: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this.state = {
      messageIds: [],
      messages: {},
      e2ee: false,
      roomInfoOpen: false,
      e2eeTerminated: false
    }

    this.regularRoomLastScrollY = null
    this._turnOnE2ee = this._turnOnE2ee.bind(this)
    this._turnOffE2ee = this._turnOffE2ee.bind(this)
    this._openRoomInfo = this._openRoomInfo.bind(this)
    this._closeRoomInfo = this._closeRoomInfo.bind(this)
    this._ensureChatData = this._ensureChatData.bind(this)
    this._ensureIdentity = this._ensureIdentity.bind(this)
    this._getIdentity = this._getIdentity.bind(this)
    this._renderLoader = this._renderLoader.bind(this)
    this._onRegularRoomScroll = this._onRegularRoomScroll.bind(this)
  }

  _turnOnE2ee () {
    if (this.state.e2ee) return

    this.setState({ e2ee: true })
  }

  _turnOffE2ee () {
    if (!this.state.e2ee) return

    this.setState({ e2ee: false, e2eeTerminated: false })
  }

  _openRoomInfo () {
    this.setState({ roomInfoOpen: true })
  }

  _closeRoomInfo () {
    this.setState({ roomInfoOpen: false })
  }

  _ensureChatData (nextProps) {
    const props = nextProps || this.props
    let { data, chatGetMessagesRequest } = props

    if (!data || data.noMoreMessagesAvailable) {
      return
    }

    if (this.state.e2ee) return

    const messageStore = this._getMessageStore()

    if (data.isLoadingMessages) return
    if (messageStore.messageIds) return

    chatGetMessagesRequest(data.room_id)
  }

  _ensureIdentity (nextProps) {
    const props = nextProps || this.props
    const { data } = props
    if (!data) {
      return
    }
    const identityEmail = data.member_email
    if (!props.getIdentityApi.inProgress && identityEmail) {
      if (!props.identityCache || !props.identityCache[identityEmail]) {
        props.getIdentity({ search: { identity_email: identityEmail }, limit: 1 })
      }
    }
  }

  _getIdentity (nextProps) {
    const props = nextProps || this.props
    const { data } = props
    if (!data) {
      return null
    }
    const identityEmail = data.member_email
    if (!this.props.identityCache) return null
    return this.props.identityCache[identityEmail] || null
  }

  _renderLoader () {
    return (
      <View style={s.container}>
        <ActivityIndicator
          color='#777'
          style={{
            flex: 1
          }}
        />
      </View>
    )
  }

  _onRegularRoomScroll (event) {
    this.regularRoomLastScrollY = event.nativeEvent.contentOffset.y
  }

  componentWillMount () {
    this._ensureIdentity()
  }

  componentWillReceiveProps (nextProps) {
    if ((nextProps && nextProps.contact) && (nextProps.connected !== this.props.connected)) {

      if(!nextProps.connected && this.props.connected) {
        this.setState({ e2eeTerminated: true })
      } else {
        this.setState({ e2eeTerminated: false })
      }
    }

    if (nextProps.e2ee && !nextProps.connected) {
      Keyboard.dismiss()
    }

    this._ensureIdentity(nextProps)
  }

  componentDidMount () {
    this._ensureChatData()
  }

  componentDidUpdate () {
    this._ensureChatData()
  }

  _getMessageStore () {
    const key = this.state.e2ee ? 'e2ee' : 'regular'
    return this.props.data[key] || {}
  }

  _renderBackground () {
    const { e2eeTerminated, e2ee } = this.state
    const { connected } = this.props

    if (!e2ee || connected || e2eeTerminated) {
      return null
    }

    return <Image source={require('app/Images/auth_bg.jpg')} style={s.background} />
  }

  render () {
    const { data, contact, navigation, connected } = this.props
    const { e2eeTerminated, e2ee } = this.state

    if (!data) {
      return this._renderLoader()
    }

    const props = {
      ...this.props,
      e2ee,
      roomInfoOpen: this.state.roomInfoOpen,
      turnOnE2ee: this._turnOnE2ee,
      turnOffE2ee: this._turnOffE2ee,
      identity: this._getIdentity()
    }

    return (
      <View style={s.container}>
        {this._renderBackground()}
        <RoomHeader {...props} onOpenRoomInfo={this._openRoomInfo} />
        <Drawer
          open={this.state.roomInfoOpen}
          onClose={this._closeRoomInfo}
          containerStyle={{ zIndex: Z_INDEX_ROOM_INFO }}
        >
          <RoomInfo {...props} />
        </Drawer>
        { !e2ee &&
          <RegularRoom
            {...props}
            messageStore={this._getMessageStore()}
            e2eeTerminated={e2eeTerminated}
            turnOffE2ee={this._turnOffE2ee}
            initialScrollPosition={this.regularRoomLastScrollY}
            onScroll={this._onRegularRoomScroll}
          />
        }
        { e2ee &&
          <EphemeralRoom
            {...props}
            messageStore={this._getMessageStore()}
            e2eeTerminated={e2eeTerminated}
            turnOffE2ee={this._turnOffE2ee}
          />
        }
      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const roomsMapKey = path(['navigation', 'state', 'params', 'roomsMapKey'], ownProps)
  const data = getRoomByMapKey(state, roomsMapKey)
  const contact = data && getContactMember(data)

  return {
    data,
    contact,
    identityCache: path(['identity', 'cache'], state),
    getIdentityApi: path(['identity', 'api', 'getIdentity'], state),
    connected: contact && contact.public_key,
    contactAvatar: contact && contact.email && path(['avatar', 'emails', contact.email], state)
  }
}

const mapDispatchToProps = {
  chatGetMessagesRequest: ChatActions.chatGetMessagesRequest,
  chatCreateRoomRequest: ChatActions.chatCreateRoomRequest,
  getIdentity: IdentityActions.getIdentityRequest,
}

export default connect(mapStateToProps, mapDispatchToProps)(TextChat)
