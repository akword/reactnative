import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity } from 'react-native'
import { GiftedChat, Bubble } from 'react-native-gifted-chat'
import EStyleSheet from 'react-native-extended-stylesheet'
import ImagePicker from 'react-native-image-picker'
import RNFS from 'react-native-fs'
import uuidv1 from 'uuid/v1'
import PropTypes from 'prop-types';
import { reverse, isEmpty } from 'ramda'
import mime from 'react-native-mime-types'
import { injectIntl } from 'react-intl'
import debounce from 'lodash.debounce'
import { MESSAGE_STATUS } from 'commons/Redux/ChatRedux'

import m from 'commons/I18n'
import log from 'commons/Services/Log'
import NotificationActions from 'commons/Redux/NotificationRedux'
import ChatActions from 'commons/Redux/ChatRedux'
import { uuidv1ToDate } from 'commons/Lib/Utils'
import { convertBase64StrToUint8Array } from 'commons/Lib/Encoding'
import { isWhitespace } from 'commons/Lib/Messaging'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'
import ActionsButton from './ActionsButton'
import MessageText from './MessageText'
import EphemeralEarlierMessage from './EphemeralEarlierMessage'

import { HEADER_HEIGHT } from '../../constants'

const s = EStyleSheet.create({
  chatContainer: {
    position: 'absolute',
    top: HEADER_HEIGHT,
    left: 0,
    right: 0,
    bottom: 0,
  },

  chatEpContainer: {
    // position: 'relative'
  },

  loadEarlierContainer: {
    height: 180,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },

  loadEarlierInner: {
    height: 30,
    backgroundColor: '#aaa',
    paddingHorizontal: 10,
    paddingVertical: 6.5,
    borderRadius: 30,
    marginBottom: 10,
  },

  loadEarlierText: {
    color: '#fff',
    fontSize: '0.75rem',
  },

  noMoreContainer: {
    height: 170,
    paddingBottom: 5,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  noMoreText: {
    fontStyle: 'italic',
    color: '#999',
    fontSize: '0.8rem'
  },

  text: {
    textAlign: 'center',
    fontSize: '1.0rem',
    lineHeight: '1.3rem',
    backgroundColor: 'transparent',
    color: 'rgba(32,48,90,0.4)',
    // letterSpacing: -0.1
  },

  isTypingMessage: {
    color: '#808080',
    marginLeft: 6,
    marginBottom: 3
  }
})

class MessageList extends Component {

  static propTypes = {
    data: PropTypes.object,
    messageStore: PropTypes.object,
    e2ee: PropTypes.bool,
    chatGetMessagesRequest: PropTypes.func.isRequired,
    chatSendMessageRequest: PropTypes.func.isRequired,
    chatAckMessage: PropTypes.func.isRequired,
    chatSendFileRequest: PropTypes.func.isRequired,
    navigation: PropTypes.object.isRequired,
    e2eeTerminated: PropTypes.bool,
    turnOffE2ee: PropTypes.func,
  }

  constructor (props) {
    super(props)

    this.state = {
      messageIds: [],
    }

    // Local cache of the messages.
    this._messages = {}

    this._didMessageAppend = this._didMessageAppend.bind(this)
    this._didMessagePrepend = this._didMessagePrepend.bind(this)
    this._getLastMessageId = this._getLastMessageId.bind(this)
    this._getFirstMessageId = this._getFirstMessageId.bind(this)
    this._updateMessagesCache = this._updateMessagesCache.bind(this)
    this._buildMessages = this._buildMessages.bind(this)
    this._onSend = this._onSend.bind(this)
    this._handleImage = this._handleImage.bind(this)
    this._handleUploadProgress = this._handleUploadProgress.bind(this)
    this._handleInputTextChanged = this._handleInputTextChanged.bind(this)
    this._handleStopTyping = debounce(this._handleStopTyping.bind(this), 1000)
    this._onLoadEarlier = this._onLoadEarlier.bind(this)
    this._updateLastMessageRead = this._updateLastMessageRead.bind(this)
    this._handleActions = this._handleActions.bind(this)
    this._renderLoadEarlier = this._renderLoadEarlier.bind(this)
    this._renderFooter = this._renderFooter.bind(this)
    this._renderActions = this._renderActions.bind(this)
  }

  _didMessageAppend (nextProps) {
    nextProps = nextProps || this.props
    const { messageIds } = nextProps.messageStore
    if (!messageIds || !messageIds.length) return false
    const lastMessageIdIndex = messageIds.indexOf(this._getLastMessageId())
    if (lastMessageIdIndex === -1) return false
    if (lastMessageIdIndex < messageIds.length - 1) return true
  }

  _didMessagePrepend (nextProps) {
    nextProps = nextProps || this.props
    const { messageIds } = nextProps.messageStore
    if (!messageIds || !messageIds.length) return false
    const firstMessageIdIndex = messageIds.indexOf(this._getFirstMessageId())
    if (firstMessageIdIndex === -1) return false
    if (firstMessageIdIndex > 0) return true
  }

  _getLastMessageId () {
    if (!this.state.messageIds || !this.state.messageIds.length) return 0
    return this.state.messageIds[this.state.messageIds.length - 1]
  }

  _getFirstMessageId () {
    if (!this.state.messageIds || !this.state.messageIds.length) return 0
    return this.state.messageIds[0]
  }

  _updateMessagesCache (nextProps) {
    if (!nextProps) return
    if (!nextProps.messageStore) return
    let { messageIds, messages } = nextProps.messageStore
    if (!messageIds) messageIds = []
    if (!messages) messages = {}
    let newMessageIds = []

    if (
      nextProps.messageStore.messageIds === this.props.messageStore.messageIds &&
      nextProps.messageStore.messages === this.props.messageStore.messages &&
      !isEmpty(this.state.messageIds)
    ) {
      return
    }

    const { last_read_message_id } = nextProps.data

    newMessageIds = messageIds.slice(0)

    newMessageIds
      // map those new message ids to messages
      .map((messageId) => messages[messageId])
      // make sure all new messages exist
      .filter((message) => !!message)
      // retrive timestamps from uuidv1 for each message
      .map((message) => {
        const timestamp = uuidv1ToDate(message.message_id)
        return message.setIn(['timestamp'], timestamp)
      })
      // cache messages
      .forEach((message) => {
        const isDelivered = (
          last_read_message_id
          &&
          (
            last_read_message_id === message.message_id
            ||
            uuidv1ToDate(last_read_message_id) > uuidv1ToDate(message.message_id)
          )
        )

        const giftedMessage = {
          message,
          _id: message.message_id,

          // gifted-chat won't render the message if body is null
          // which will result our custom file messages not being rendered
          text: message.body || 'file',
          createdAt: message.timestamp,
          user: {
            _id: message.user_from,
            name: message.user_from
          },
          sent: isDelivered,
          received: isDelivered,
        }
        if (message.is_image) {
          giftedMessage.is_image = true
          giftedMessage.image = `data:${message.data.mime_type};base64,${message.image_data}`
          giftedMessage.text = message.data.file_name
        }
        if (message.is_url) {
          giftedMessage.text = 'data_url'
          giftedMessage.is_url = true
          giftedMessage.url = message.data.data_url
          giftedMessage.filename = message.data.file_name
        }
        this._messages[message.message_id] = giftedMessage
      })

    this.setState({
      messageIds: newMessageIds
    })
  }

  _buildMessages () {
    const { messageIds } = this.state
    const messages = this._messages
    return reverse(messageIds).map(id => messages[id])
  }

  _onSend(messages = []) {
    this.props.chatSendMessageRequest(this.props.data.room_id, messages[0].text, this.props.e2ee)
  }

  _handleImage (response) {
    const {
      data,
      chatSendFileRequest,
      displayNotification,
      chatMessageReceived,
      intl
    } = this.props
    if (response.didCancel) {
      return
    }

    // in case the user takes a live photo, give it a generic filename
    response.fileName = response.fileName || 'Photo.jpg'

    const uploadMessage = {
      message_id: uuidv1(),
      room_id: data.room_id,
      user_from: data.member_email,
      body: null,
      is_file: true,
      progress: null,
      data: response
    }
    chatMessageReceived(uploadMessage, true)
    RNFS.readFile(response.uri, 'base64')
      .then((content) => {
        const uint8Array = convertBase64StrToUint8Array(content)
        chatSendFileRequest(data.room_id, {
          name: response.fileName,
          type: mime.lookup(response.fileName),
          size: response.fileSize,
          data: uint8Array
        }, this._handleUploadProgress(uploadMessage))
      })
      .catch((err) => {
        displayNotification(intl.formatMessage(m.native.Chat.couldNotSendImage), 'danger', 3000)
        log.error('Error sending an image', err)
      })
  }

  _handleUploadProgress (uploadMessage) {
    const { chatMessageModified } = this.props
    return function (event) {
      if (event.percent) {
        chatMessageModified({...uploadMessage, progress: event.percent / 100}, true)
      }
    }
  }

  _onLoadEarlier () {
    this.props.chatGetMessagesRequest(this.props.data.room_id, true)
  }

  _updateLastMessageRead () {
    const { data, e2ee } = this.props
    const messageIds = this.state.messageIds
    if (!data) return
    if (!messageIds || !messageIds.length) return

    const latestMessageId = this._getLastMessageId()
    if (
      // Either there's no last_read_message_id at all
      !data.last_read_message_id ||
      (
        data.regular &&
        data.regular.unreadCount > 0
      ) ||
      // Or the latest message's timestamp is greater than last_read_message_id
      (
        latestMessageId !== data.last_read_message_id &&
        uuidv1ToDate(latestMessageId) > uuidv1ToDate(data.last_read_message_id)
      )
    ) {
      this.props.chatAckMessage(data.room_id, latestMessageId, e2ee)
    }
  }

  _handleActions () {
    ImagePicker.showImagePicker({
      noData: true
    }, this._handleImage)
  }

  _renderLoadEarlier () {
    const { data, e2ee, connected, intl } = this.props
    if (e2ee && connected) {

      return (
        <EphemeralEarlierMessage {...this.props} />
      )
    }

    if (data.noMoreMessagesAvailable) {
      return (
        <View style={s.noMoreContainer}>
          <Text style={s.noMoreText}>{this.state.messageIds.length ? intl.formatMessage(m.native.Chat.noMoreMessages) : intl.formatMessage(m.native.Chat.noMoreHistory)}</Text>
        </View>
      )
    }

    return (
      <View style={s.loadEarlierContainer}>
        {data.isLoadingMessages ? <View style={s.loadEarlierInner}>
          <Text style={s.loadEarlierText}>{intl.formatMessage(m.app.Common.loadingEllipses)}</Text></View>:
          <TouchableOpacity style={s.loadEarlierInner} onPress={this._onLoadEarlier}>
            <Text style={s.loadEarlierText}>{intl.formatMessage(m.native.Chat.loadEarlierMessages)}</Text>
          </TouchableOpacity>
        }
      </View>
    )
  }

  _renderFooter () {
    const { data, connected, intl } = this.props
    if (!data || !data.membersTyping) return null

    if (!connected && this.props.e2ee) {
      return <View style={{ height: 80 }} />
    }

    const membersTyping = Object.keys(data.membersTyping).filter(u => data.membersTyping[u])
    if (!membersTyping || !membersTyping.length) return null

    return (
      <Text style={s.isTypingMessage}>{membersTyping.join(', ')} {intl.formatMessage(m.native.Chat.isTyping)}</Text>
    )
  }

  _renderActions () {
    const { e2ee, data } = this.props
    if (!e2ee) return null
    if (process.env.NODE_ENV === 'production') return null

    return (
      <ActionsButton
        data={data}
        onPress={this._handleActions}
      />
    )
  }

  _renderBubble (props) {
    return (
      <Bubble {...props}
        wrapperStyle={{
          left: {
            backgroundColor: '#fff',
          },
          right: {
            backgroundColor: 'rgb(0, 131, 232)'
          }
        }}
      />
    )
  }

  _handleStartTyping () {
    const { data, chatStartedTyping } = this.props
    if (!data.room_id || this._isTyping) return
    this._isTyping = true
    chatStartedTyping(data.room_id)
  }

  _handleStopTyping () {
    const { data, chatStoppedTyping } = this.props
    if (!data.room_id || !this._isTyping) return
    this._isTyping = false
    chatStoppedTyping(data.room_id)
  }

  _handleInputTextChanged (text) {
    // this.props.input.onChange(...args)
    if (isWhitespace(text || '')) return
    this._handleStartTyping()
    this._handleStopTyping()
  }

  componentWillMount () {
    this._updateMessagesCache(this.props)
  }

  componentDidMount () {
    this._updateLastMessageRead()
  }

  componentDidUpdate () {
    this._updateLastMessageRead()
  }

  componentWillReceiveProps (nextProps) {
    this._updateMessagesCache(nextProps)
  }

  render () {
    const { data, listViewProps, intl } = this.props
    if (!data) return null

    return (
      <View style={s.chatContainer}>
        <GiftedChat
          messages={this._buildMessages()}
          onSend={this._onSend}
          loadEarlier
          renderLoadEarlier={this._renderLoadEarlier}
          isLoadingEarlier={data.isLoadingMessages}
          renderFooter={this._renderFooter}
          renderAvatar={null}
          renderActions={this._renderActions}
          renderMessageText={(data) => <MessageText data={data} />}
          renderBubble={this._renderBubble}
          user={{
              _id: data.member_email,
              name: data.member_email
          }}
          listViewProps = {{
            removeClippedSubviews: false,
            ...listViewProps
          }}
          onInputTextChanged={this._handleInputTextChanged}
          ref={ref => this.chatViewRef = ref}
          placeholder={intl.formatMessage(m.native.Chat.typeAMessage)}
        />
      </View>
    )
  }

}

const mapDispatchToProps = {
  chatGetMessagesRequest: ChatActions.chatGetMessagesRequest,
  chatSendMessageRequest: ChatActions.chatSendMessageRequest,
  chatAckMessage: ChatActions.chatAckMessage,
  chatSendFileRequest: ChatActions.chatSendFileRequest,
  chatMessageReceived: ChatActions.chatMessageReceived,
  chatMessageModified: ChatActions.chatMessageModified,
  chatStartedTyping: ChatActions.chatStartedTyping,
  chatStoppedTyping: ChatActions.chatStoppedTyping,
  displayNotification: NotificationActions.displayNotification,
}

const ConnectedMessageList = connect(null, mapDispatchToProps, null, { withRef: true })(injectIntl(MessageList))

export default ConnectedMessageList
