import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, TouchableHighlight } from 'react-native'
import {connect} from 'react-redux'
import EStyleSheet from 'react-native-extended-stylesheet'
import { injectIntl } from 'react-intl'
import {path} from 'ramda'
import m from 'commons/I18n'
import DancingBubble from 'app/Components/Loading/DancingBubble'
import Text from 'app/Components/BaseText'
import ChatActions from 'commons/Redux/ChatRedux'

import palette from 'app/Styles/colors'

const s = EStyleSheet.create({
  containerForLg: {
    width: '19.25rem',
    height: '22.68rem',
    marginTop: '2.5rem',
    marginBottom: '1.5rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible'
  },

  containerForSm: {
    width: '19.25rem',
    // height: '22.68rem',
    marginTop: '2rem',
    marginBottom: '1.5rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'visible'
  },

  text: {
    textAlign: 'center',
    fontSize: '1.0rem',
    lineHeight: '1.3rem',
    backgroundColor: 'transparent',
    // letterSpacing: -0.1
  },

  smallText: {
    textAlign: 'center',
    fontSize: '0.8rem',
    color: 'rgb(121, 187, 238)',
    lineHeight: '1rem',
    marginTop: '2rem',
    backgroundColor: 'transparent',
  },

  middleContent: {
    display: 'flex',
    marginTop: '2rem',
    justifyContent: 'center',
    alignItems: 'center'
  },

  waitingContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },

  buttonText: {
    fontSize: '0.68rem',
    fontWeight: "800",
    fontStyle: "normal",
    letterSpacing: 0.5,
    textAlign: "center",
    color: "#ffffff",
    alignSelf: 'center'
  },

  button: {
    width: '10rem',
    height: '2.8rem',
    marginTop: '3.7rem',
    backgroundColor: palette.ceruleanBlue,
    shadowColor: "rgba(0, 0, 0, 0.15)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    alignItems: 'center'
  },

  buttonPositionForLg: {
    marginTop: '3.7rem',
  },
  buttonPositionForSm: {
    marginTop: '1.7rem',
  }
})

class EphemeralWaitingOverlay extends PureComponent {

  static propTypes = {
    data: PropTypes.object,
    e2ee: PropTypes.bool.isRequired,
    type: PropTypes.string,
    contact: PropTypes.object,
    e2eeTerminated: PropTypes.bool,
    turnOffE2ee: PropTypes.func,
    deviceWidth: PropTypes.number,
    chatSendMessageRequest: PropTypes.func.isRequired,
  }

  nudge() {
    console.log(this.props.data.room_id, 'hey', this.props.e2ee)
    this.props.chatSendMessageRequest(this.props.data.room_id, 'hey', this.props.e2ee)
  }

  render () {
    const { connected, e2eeTerminated, intl, deviceWidth } = this.props
    const isLgScreen = deviceWidth > 350
    return (
      <View style={isLgScreen ? s.containerForLg : s.containerForSm}>
        <View style={s.waitingContainer}>
          <DancingBubble bubbleStyle={{ width: 9, height: 9, marginHorizontal: 2}} jumpHeight={15} />
          <Text style={s.smallText}>
            {intl.formatMessage(m.native.Chat.waitingForConnection)}
          </Text>
        </View>
        <View style={s.middleContent}>
          <Text style={[s.text, connected ? {color: 'rgba(32,48,90,0.4)'} : {color: 'rgb(255,255,255)'}]}>
            {intl.formatMessage(m.native.Chat.ephemeralChatMessage)}
          </Text>
          <Text style={[s.text, connected ? {color: 'rgba(32,48,90,0.4)'} : {color: 'rgb(255,255,255)'}]}>
            {intl.formatMessage(m.native.Chat.ephemeralChatMessage1)}
          </Text>

          <TouchableHighlight
            underlayColor={'rgba(255, 255, 255, 0.1)'}
            activeOpacity={0.8}
            style={[s.button, isLgScreen ? s.buttonPositionForLg : s.buttonPositionForSm]}
            onPress={this.nudge.bind(this)}
          >
            <View style={s.buttonContent}>
              <Text style={s.buttonText}>{intl.formatMessage(m.native.Chat.sendANudge).toUpperCase()}</Text>
            </View>
          </TouchableHighlight>

        </View>
      </View>
    )
  }
}

const intlEphemeralWaitingOverlay = injectIntl(EphemeralWaitingOverlay)

const mapDispatchToProps = {
  chatSendMessageRequest: ChatActions.chatSendMessageRequest,
}

const mapStateToProps = (state, ownProps) => {
  const width = path(['device', 'display_width'], state)

  return {
    deviceWidth: width
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(intlEphemeralWaitingOverlay)
