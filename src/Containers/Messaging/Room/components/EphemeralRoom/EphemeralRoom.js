import React from 'react'
import { View, ScrollView } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { connect } from 'react-redux'

import MessageList from '../MessageList'
import EphemeralHintMessage from './EphemeralHintMessage'
import EphemeralTerminatedOverlay from './EphemeralTerminatedOverlay'
import EphemeralWaitingOverlay from './EphemeralWaitingOverlay'

import { HEADER_HEIGHT } from '../../constants'
import {path} from 'ramda'

const s = EStyleSheet.create({
  messageContainer: {
    marginTop: HEADER_HEIGHT,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  }
})

const EphemeralRoom = props => (
  <View style={{ flex: 1 }}>
    {!(props.connected || props.e2eeTerminated)
      && <ScrollView><View style={s.messageContainer}><EphemeralWaitingOverlay {...props} /></View></ScrollView>
    }
    {props.e2eeTerminated && <EphemeralTerminatedOverlay {...props} />}
    {props.connected && <MessageList {...props} />}
    {!props.connected && <EphemeralHintMessage e2eeTerminated={props.e2eeTerminated} />}
  </View>
)

const mapStateToProps = (state, ownProps) => {
  const width = path(['device', 'display_width'], state)

  return {
    deviceWidth: width
  }
}

export default connect(mapStateToProps, null)(EphemeralRoom)