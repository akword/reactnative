import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'

const s = EStyleSheet.create({
  container: {
    height: 30,
    display: 'flex',
    flexDirection: 'row'
  },

  tab: {
    padding: 2,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderBottomColor: 'transparent',
  },

  tabText: {
    textAlign: 'center',
    fontSize: '0.75rem',
    color: '#20305a',
    fontWeight: '800',
    marginTop: '-0.5rem',
  },

  tabActive: {
    borderBottomColor: palette.ceruleanBlue
  },

  tabActiveText: {
    color: palette.ceruleanBlue
  },

  unreadCount: {
    borderRadius: 20,
    minWidth: 20,
    padding: '0.1rem',
    backgroundColor: 'rgb(240, 75, 76)',
    overflow: 'hidden',
    marginTop: '-0.5rem',
    marginLeft: '.5rem'
  },

  unreadCountText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: '0.8rem',
    fontWeight: '600',
  }
})

const SwitchTabs = ({ tabs, selected, onSelect}) => (
  <View style={s.container}>
    {tabs.map((tab, index) => {
      const isActive = index === selected
      const tabWidth = { width: `${100 / tabs.length}%` }

      return (
        <TouchableOpacity key={index} style={[s.tab, tabWidth, isActive && s.tabActive]} onPress={() => !isActive && onSelect(index)}>
          <Text style={[s.tabText, isActive && s.tabActiveText]}>{tab.label.toUpperCase()}</Text>
          {!!tab.unreadCount && <View style={s.unreadCount}>
            <Text style={s.unreadCountText}>{tab.unreadCount}</Text>
          </View>}
        </TouchableOpacity>
      )
    })}
  </View>
)

SwitchTabs.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    unreadCount: PropTypes.number
  })),
  selected: PropTypes.number,
  onSelect: PropTypes.func
}

SwitchTabs.defaultProps = {
  selected: 0
}

export default SwitchTabs
