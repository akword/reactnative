import React, { PureComponent } from 'react';
import { View, TouchableOpacity, Platform } from 'react-native'
import { BlurView } from 'react-native-blur';
import EStyleSheet from 'react-native-extended-stylesheet'
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import PropTypes from 'prop-types';
import { path } from 'ramda'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import palette from 'app/Styles/colors'
import Avatar from 'app/Components/Avatar'
import Text from 'app/Components/BaseText'
import HeaderButton from 'app/Components/HeaderButton'
import { HEADER_HEIGHT, Z_INDEX_HEADER } from '../constants'
import SwitchTabs from './SwitchTabs'

export const s = EStyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: HEADER_HEIGHT,
    paddingTop: 20,
    paddingBottom: 32,
    zIndex: Z_INDEX_HEADER,
    backgroundColor: Platform.OS === 'ios' ? 'transparent' : '#fff',
  },

  actionContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    marginBottom: '1.1rem'
  },

  blur: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },

  chateeZone: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  titleContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '0.15rem',
  },

  titleText: {
    marginLeft: '.3rem',
    color: '#20305a',
    fontSize: '.79rem',
    fontWeight: '500',
    backgroundColor: 'transparent',
  },

  titleIcon: {
    color: '#20305a'
  },

  avatarContainer: {
    position: 'relative',
  },

  avatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },

  onlineIndicator: {
    position: 'absolute',
    width: 12,
    height: 12,
    bottom: -1,
    right: -1,
    borderRadius: 12,
    borderWidth: 0.5,
    borderColor: 'rgb(238, 242, 246)',
    backgroundColor: 'rgb(58, 206, 1)',
  },

  back: {
    width: 23
  },

  info: {
    marginTop: '.7rem',
    left: '-1rem'
  }
})

const InfoButton = ({ onOpenRoomInfo, style }) => (
  <TouchableOpacity style={style} onPress={onOpenRoomInfo}>
    <Icon name='info' size={23} style={{ color: palette.ceruleanBlue }} />
  </TouchableOpacity>
)

class MessagingRoomHeader extends PureComponent {

  static propTypes = {
    onOpenRoomInfo: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this._goToContactDetail = this._goToContactDetail.bind(this)
    this._goBack = this._goBack.bind(this)
    this._onTabSelected = this._onTabSelected.bind(this)
  }

  _goToContactDetail () {
    const { contact, navigation, identity } = this.props
    if (!identity || !identity.id) { // Todo: Have to prevent identlty: null in props
      return;
    }

    navigation.navigate('ContactDetail', {
      // Todo: Change id -> email
      id: contact.email,
      createIfNotExisting: {
        email: contact.email,
        identity_id: identity.id,
        display_name: contact.display_name
      }
    })
  }

  _goBack () {
    this.props.navigation.goBack()
  }

  _onTabSelected (selectedIndex) {
    if (selectedIndex) {
      this.props.turnOnE2ee()
    } else {
      this.props.turnOffE2ee()
    }
  }

  _renderBlur () {
    if (Platform.OS !== 'ios') {
      return null
    }

    return <BlurView style={s.blur} blurType='xlight' blurAmount={30}/>
  }

  _renderChatee () {
    const { contact, contactAvatar, connected } = this.props

    return (
      <TouchableOpacity style={s.chateeZone} onPress={this._goToContactDetail}>
        <View style={s.avatarContainer}>
          <Avatar avatar={contactAvatar} avatarStyle={s.avatar} name={contact.display_name} />
          {connected && <View style={s.onlineIndicator} />}
        </View>
        <View style={s.titleContainer}>
          <Icon name='lock' style={s.icon} size={13} />
          <Text style={s.titleText}>{contact.display_name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  _generateTabProps () {
    const isOnline = this.props.connected
    const fm = this.props.intl.formatMessage

    const tabs = [
      { label: fm(m.native.Chat.encrypted), unreadCount: isOnline && path(['data', 'regular', 'unreadCount'], this.props) },
      { label: fm(m.native.Chat.ephemeral), unreadCount: isOnline && path(['data', 'e2ee', 'unreadCount'], this.props) }
    ];

    return {
      tabs,
      selected: this.props.e2ee ? 1 : 0
    }
  }

  render () {
    return (
      <View style={s.container}>
        {this._renderBlur()}
        <View style={s.actionContainer}>
          <HeaderButton isBack onPress={this._goBack} style={s.back} />
          {this._renderChatee()}
          <View style={s.info}>
            <InfoButton onOpenRoomInfo={this.props.onOpenRoomInfo} />
          </View>
        </View>
        <SwitchTabs {...this._generateTabProps()} onSelect={this._onTabSelected} />
      </View>
    )
  }

}

export default injectIntl(MessagingRoomHeader)
