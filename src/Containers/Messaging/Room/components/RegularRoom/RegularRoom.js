import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { path } from 'ramda'
import InteractionManager from 'app/Components/InteractionManager'

import MessageList from '../MessageList'

export default class RegularRoom extends PureComponent {
  static propTypes = {
    onScroll: PropTypes.func,
    initialScrollPosition: PropTypes.number,
  }

  componentDidMount () {
    InteractionManager.runAfterInteractions(() => {
      const { initialScrollPosition } = this.props
      const scrollView = path(['chatViewRef', '_messageContainerRef'], this.msgListRef.getWrappedInstance())

      if (!scrollView || !initialScrollPosition) {
        return
      }

      scrollView.scrollTo({
        y: initialScrollPosition,
        animated: true,
      })
    })
  }

  render () {
    const { onScroll } = this.props

    return (
      <MessageList
        ref={ref => this.msgListRef = ref}
        {...this.props}
        listViewProps={{
          onScroll,
          scrollEventThrottle: 16
        }}
      />
    )
  }
}
