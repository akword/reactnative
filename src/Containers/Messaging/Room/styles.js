import EStyleSheet from 'react-native-extended-stylesheet'

import palette from 'app/Styles/colors'
import { HEADER_HEIGHT } from './constants'

const styles = EStyleSheet.create({
  isTypingMessage: {
    marginTop: '0.4rem',
    marginLeft: '0.5rem',
    marginBottom: '0.2rem',
    color: palette.concrete,
  },

  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgb(232, 240, 247)'
  },

  background: {
    width: '100%',
    height: '100%',
    position: 'absolute'
  },
})

export default styles
