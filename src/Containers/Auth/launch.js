import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, ActivityIndicator, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

import styles from './_Base/styles'
import { AuthContainer } from './_Base'

class Launch extends Component {
  render () {
    return (
      <View style={{flex: 1}} ref='main'>
        <LinearGradient colors={['#000', `#3d6098`, '#000']} style={styles.backgroundOverlay} />
        <View style={[{
          flex: 1,
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }, styles.content]}>
          <Image
            source={require('app/Images/logo.png')}
            style={{
              height: 52,
              width: 275,
              resizeMode: 'contain',
            }} 
          />
          <ActivityIndicator
            animating
            color='white'
            style={{marginTop: 83 }}
          />
        </View>
      </View>
    )
  }
}

export default Launch
