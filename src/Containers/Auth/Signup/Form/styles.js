import EStyleSheet from 'react-native-extended-stylesheet'

export default EStyleSheet.create({

  container: {
    alignItems: 'center',
    marginTop: '0.8rem'
  },

  dots: {
    marginTop: '3rem'
  }

})
