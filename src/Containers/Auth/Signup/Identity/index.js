import React, { Component } from 'react'
import { View, TouchableHighlight } from 'react-native'
import { injectIntl } from 'react-intl'
import { connect } from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import * as R from 'ramda'
import FAIcon from 'react-native-vector-icons/FontAwesome'
import MIcon from 'react-native-vector-icons/MaterialIcons'
import MCIcon from 'react-native-vector-icons/MaterialCommunityIcons'

import m from 'commons/I18n'
import IdentityActions from 'commons/Redux/IdentityRedux'
import AppActions from 'commons/Redux/AppRedux'
import { createValidator, required, minLength, emailUsername, i18nize } from 'commons/Lib/Validators'
import { isEmail } from 'validator'

import { getCurrentRouteName } from 'app/Navigation/utils'
import Text from 'app/Components/BaseText'
import Spinner from 'app/Components/Spinner'

import styles from '../../_Base/styles'
import localStyles from './styles'
import { AuthContainer, AuthPicker, AuthTextInputWithIcon, AuthActionButton } from '../../_Base'
import ProgressDots from '../components/ProgressDots'

export const FORM_IDENTIFIER = 'SignupIdentity'
const DEFAULT_DOMAIN = typeof __DEV__ !== 'undefined' && __DEV__ ? 'stage.msgsafe.io' : 'msgsafe.io'

const signupFormValidator = createValidator({
  localpart: [
    i18nize(required, m.app.AuthValidation.mailboxIsRequired),
    i18nize(minLength, m.app.IdentityValidation.emailUsernameMinLength, [4]),
    i18nize(emailUsername, m.app.IdentityValidation.emailUsernameValid)
  ]
})

class SignupForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      focus: null,
      identityLoading: false
    }

    this._handlePress = this._handlePress.bind(this)
    this._focusDomain = this._setFocus.bind(this, 'domain')
    this._focusLocalpart = this._setFocus.bind(this, 'localpart')
  }

  _setFocus (key) {
    this.setState({focus: key}, () => this.setState({focus: null}))
  }

  _focusProp (key) {
    return {
      focus: this.state.focus === key
    }
  }

  _updateScrollForRef (ref) {
    if (!this.refs[ref] || !this.refs.container) return
    this.refs.container.updateScroll(this.refs[ref].getRenderedComponent())
  }

  _getDomainData (domains) {
    if (!domains) return []
    return domains.map(d => ({
      title: d.name,
      value: d.name
    }))
  }

  _handlePress (values) {
    const { createIdentity, onBoardingComplete } = this.props

    const p = {
      display_name: values.display_name,
      region: 'cw',
      email: `${values.localpart}@${values.domain}`,
      http_pickup: true,
      next_hop_exit: false
    }

    return new Promise(
      (resolve, reject) => createIdentity(p, () => {
        resolve()
        onBoardingComplete()
      }, reject)
    )
  }

  _renderFullEmailAddress () {
    const { formLocalpart, formDomain, valid, intl } = this.props
    if (!valid || !formLocalpart || !formDomain) return null

    return (
      <View>
        <Text style={styles.fieldHelpText}>{intl.formatMessage(m.native.Auth.yourEmailAddressWillBe)}</Text>
        <Text style={[styles.fieldHelpText, localStyles.fieldHelpTextHighlighted]}>{formLocalpart}@{formDomain}</Text>
      </View>
    )
  }

  async _tryGetAvailableIdentity () {
    const { validateIdentityRequest, initialValues } = this.props
    const identityEmail = `${initialValues.localpart}@${initialValues.domain}`
    const possibleIdentityEmails = [identityEmail].concat(
      R.compose(
        R.map(n => (`${initialValues.localpart}${n}@${initialValues.domain}`)),
        R.slice(0, 5),
        R.sort(() => Math.random() - 0.5),
      )(R.range(1, 50))
    ) // Generate random number appended possible identity emails

    const identityAsyncValidate = email => new Promise((res) => validateIdentityRequest({ email }, () => res(email), () => res(false)))
    // const IDENTITY_NOT_AVAILABLE_MSG = 'The email address is not available'
    let availableEmail;

    this.setState({ identityLoading: true })

    for(const email of possibleIdentityEmails) {
      if (!availableEmail) {
        const ret = await identityAsyncValidate(email)
        if (ret) {
          availableEmail = ret;
        }
      }
    }

    if (availableEmail) {
      availableEmail !== identityEmail && this.props.change('localpart', availableEmail.replace(/@.*$/,''))
    } else {
      this.props.asyncValidate()
    }

    this.setState({ identityLoading: false })
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.activeField && nextProps.activeField !== this.props.activeField) {
      this._updateScrollForRef(nextProps.activeField)
    }

    // if (this.props.formLocalpart !== nextProps.formLocalpart && nextProps.formLocalpart) {
    //   this.props.asyncValidate()
    // }
  }

  componentDidMount () {
    // this.props.asyncValidate()
    // this.props.touch('localpart')
    this._tryGetAvailableIdentity()
  }

  render () {
    const { handleSubmit, submitting, intl, valid, asyncValidating, currentRouteName } = this.props;
    const { identityLoading } = this.state;

    const fm = intl.formatMessage

    const disabled = (!valid || asyncValidating || identityLoading)

    if (currentRouteName !== 'SignupIdentity') {
      return null
    }

    return (
      <AuthContainer hideFooter ref='container' logoProps={{align: 'center', small: true}}>
        <View style={localStyles.container}>

          <Text style={styles.pageTitleText}>{fm(m.native.Auth.createFirstEmailAndIdentity)}</Text>
          <View style={[styles.authFieldContainer, styles.onboardingFieldWrapper]}>
            <Field
              name="display_name"
              iconName="person-outline"
              iconSize={20}
              iconComponent={MIcon}
              props={this._focusProp('display_name')}
              style={[styles.textInput, styles.textInputLight]}
              errorStyle={styles.textInputError}
              component={AuthTextInputWithIcon}
              placeholder={fm(m.native.Preferences.displayName)}
              placeholderTextColor={styles.$lightColor}
              returnKeyType="next"
              onSubmitEditing={this._focusLocalpart}
              ref="display_name"
              withRef={true}
              selectionColor='#fff'
              blurOnSubmit={true}
            />
            { this._renderFullEmailAddress() }
          </View>

          <View style={[styles.authFieldContainer, styles.onboardingFieldWrapper]}>
            <Field
              name="localpart"
              iconName="id-badge"
              iconSize={18}
              iconComponent={FAIcon}
              props={this._focusProp('localpart')}
              style={[styles.textInput, styles.textInputLight]}
              errorStyle={styles.textInputError}
              component={AuthTextInputWithIcon}
              placeholder='...'
              placeholderTextColor={styles.$lightColor}
              returnKeyType="next"
              autoCorrect={false}
              autoCapitalize="none"
              onSubmitEditing={this._focusDomain}
              ref="localpart"
              withRef={true}
              selectionColor='#fff'
              loading={identityLoading}
              blurOnSubmit={true}
            />
          </View>
          <View style={[styles.authFieldContainer, styles.onboardingFieldWrapper]}>
            <Field
              name='domain'
              iconName='web'
              iconSize={18}
              iconComponent={MCIcon}
              component={AuthPicker}
              title={fm(m.native.Preferences.selectDomain)}
              initialMessage={fm(m.native.Auth.selectYourChoiceOfDomain)}
              linkText={fm(m.native.Preferences.selectDomain)}
              returnKeyType='next'
              props={this._focusProp('domain')}
              options={this._getDomainData(this.props.domains)}
            />
          </View>
          <AuthActionButton
            style={[
              styles.button,
              disabled && styles.buttonDisabled
            ]}
            onPress={(...args) => !disabled && handleSubmit(this._handlePress)(...args)}
            title={fm(m.app.Misc.next).toUpperCase()}
            titleStyle={[
              styles.buttonText,
              submitting && styles.buttonTextActive,
              disabled && styles.buttonTextDisabled
            ]}
            isSpinning={submitting}
          />
          <Text style={localStyles.fieldHelpText}>{fm(m.native.Auth.signupFormHelpText)}</Text>
        {/* <ProgressDots activeIndex={3} totalCount={4} style={localStyles.dots} /> */}
        </View>
      </AuthContainer>

    )
  }
}

SignupForm = reduxForm({
  form: FORM_IDENTIFIER,
  validate: signupFormValidator,
  asyncValidate: (values, dispatch) => {
    // Dispatch the request along with resolve & reject
    // and the saga will take care of using the one appropriately

    let promises = []

    if (values.localpart && values.domain) {
      promises.push(
        new Promise((resolve, reject) => dispatch(
          IdentityActions.validateIdentityRequest(
            { email: `${values.localpart}@${values.domain}` },
            resolve, e => reject({localpart: e})
          )
        ))
      )
    }

    // use Promise.all to work with multiple promises
    return new Promise.all(promises)
  },
  shouldAsyncValidate: params => {
    if (!params.syncValidationPasses) return false;
    return params.trigger === 'blur'
  },
  asyncBlurFields: ['localpart', 'domain']
})(SignupForm)

const IntlSignupForm = injectIntl(SignupForm)
IntlSignupForm.navigationOptions = {
  header: null,
  tabBarVisible: false
}

const mapDispatchToProps = {
  onBoardingComplete: AppActions.onBoardingComplete,
  createIdentity: IdentityActions.identityCreate,
  validateIdentityRequest: IdentityActions.validateIdentityRequest,
}

const mapStateToProps = (state) => {
  const props = {
    initialValues: {
      localpart: state.user.data.username,  // email consists of localpart@domain
      domain: DEFAULT_DOMAIN
    },
    domains: R.path(['user', 'data', 'system_domains'], state),
    formLocalpart: R.path(['form', FORM_IDENTIFIER, 'values', 'localpart'], state),
    formDomain: R.path(['form', FORM_IDENTIFIER, 'values', 'domain'], state),
    currentRouteName: getCurrentRouteName(state.nav),
  }

  if (props.initialValues.localpart && isEmail(props.initialValues.localpart)) {
    props.initialValues.localpart = props.initialValues.localpart.replace(/@.*$/,'')
  }

  return props
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlSignupForm)
