import { addLocaleData } from 'react-intl'

import enLocaleData from 'react-intl/locale-data/en'
import ruLocaleData from 'react-intl/locale-data/ru'
import arLocaleData from 'react-intl/locale-data/ar'
import esLocaleData from 'react-intl/locale-data/es'
import frLocaleData from 'react-intl/locale-data/fr'
import hiLocaleData from 'react-intl/locale-data/hi'
import deLocaleData from 'react-intl/locale-data/de'
import jaLocaleData from 'react-intl/locale-data/ja'
import zhLocaleData from 'react-intl/locale-data/zh'
import roLocaleData from 'react-intl/locale-data/ro'
import nlLocaleData from 'react-intl/locale-data/nl'
import ptLocaleData from 'react-intl/locale-data/pt'

import { translations } from 'translations/translations.json'

addLocaleData(arLocaleData)
addLocaleData(deLocaleData)
addLocaleData(enLocaleData)
addLocaleData(esLocaleData)
addLocaleData(frLocaleData)
addLocaleData(hiLocaleData)
addLocaleData(jaLocaleData)
addLocaleData(nlLocaleData)
addLocaleData(ptLocaleData)
addLocaleData(roLocaleData)
addLocaleData(ruLocaleData)
addLocaleData(zhLocaleData)

const translationsData = {}
for (let key in translations) {
  if (translations.hasOwnProperty(key)) {
    for (let lang in translations[key]) {
      if (translations[key].hasOwnProperty(lang)) {
        if (!translationsData[lang]) {
          translationsData[lang] = {}
        }
        translationsData[lang][key] = translations[key][lang]
      }
    }
  }
}

export default translationsData
