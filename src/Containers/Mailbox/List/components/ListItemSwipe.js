import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import MailboxActions from 'commons/Redux/MailboxRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'
import Text from 'app/Components/BaseText'
import { ListItemSwipe as s } from 'app/Components/ListView/styles'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'

class MailboxListItemSwipe extends Component {

  static propTypes = {
    data: PropTypes.object.isRequired,
    filterName: PropTypes.string.isRequired,
    mailboxArchiveRequest: PropTypes.func.isRequired,
    mailboxUnarchiveRequest: PropTypes.func.isRequired,
    mailboxMoveToInboxRequest: PropTypes.func.isRequired,
    mailboxTrashRequest: PropTypes.func.isRequired,
    mailboxDeleteRequest: PropTypes.func.isRequired,
    displayNotification: PropTypes.func.isRequired,
  }

  constructor (props) {
    super(props)

    this._handleArchive = this._handleArchive.bind(this)
    this._handleTrash = this._handleTrash.bind(this)
    this._handleDelete = this._handleDelete.bind(this)
    this._handleMoveToInbox = this._handleMoveToInbox.bind(this)
    this._handleLeftAction = this._handleLeftAction.bind(this)
    this._handleRightAction = this._handleRightAction.bind(this)
  }

  _handleArchive () {
    const {
      data,
      rowMap,
      rowId,
      mailboxArchiveRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${rowId}`].closeSwipeRow()
    mailboxArchiveRequest(data.id)
    displayNotification(intl.formatMessage(m.native.Mailbox.emailArchived), 'info', 3000)
  }

  _handleTrash () {
    const {
      data,
      rowMap,
      rowId,
      mailboxTrashRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${rowId}`].closeSwipeRow()
    mailboxTrashRequest(data.id)
    displayNotification(intl.formatMessage(m.native.Mailbox.emailTrashed), 'danger', 3000)
  }

  _handleDelete () {
    const {
      data,
      rowMap,
      rowId,
      mailboxDeleteRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${rowId}`].closeSwipeRow()    // TODOS: Delete should be performed after animation is finished
    mailboxDeleteRequest(data.id)
    displayNotification(intl.formatMessage(m.native.Mailbox.emailDeleted), 'danger', 3000)
  }

  _handleMoveToInbox () {
    const {
      data,
      rowMap,
      rowId,
      filterName,
      mailboxMoveToInboxRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${rowId}`].closeSwipeRow()
    mailboxMoveToInboxRequest(data.id)
    displayNotification(intl.formatMessage(m.native.Mailbox.emailMovedToInbox), 'info', 3000)
  }

  _handleUnarchive () {
    const {
      data,
      rowMap,
      rowId,
      filterName,
      mailboxUnarchiveRequest,
      displayNotification,
      intl
    } = this.props
    rowMap[`${rowId}`].closeSwipeRow()
    mailboxUnarchiveRequest(data.id)
    displayNotification(intl.formatMessage(m.native.Mailbox.emailMovedToInbox), 'info', 3000)
  }

  _handleLeftAction () {
    const { filterName } = this.props
    if (filterName === 'archive') {
      return this._handleUnarchive()
    } else if (filterName === 'trash') {
      return this._handleMoveToInbox()
    }
    return this._handleArchive()
  }

  _handleRightAction () {
    const { filterName } = this.props
    if (filterName === 'trash') {
      return this._handleDelete()
    }
    return this._handleTrash()
  }

  _renderLeftSwipe () {
    const { filterName, intl } = this.props
    const fm = intl.formatMessage
    let icon = 'archive'
    let text = fm(m.native.Mailbox.archive)
    if (filterName === 'archive' || filterName === 'trash') {
      icon = 'inbox'
      text = fm(m.native.Mailbox.moveToInbox)
    }
    return (
      <TouchableOpacity
        style={[s.button, s.buttonPrimary]}
        onPress={this._handleLeftAction}
      >
        <Icon name={icon} style={s.icon} />
        <Text style={s.text}>{text}</Text>
      </TouchableOpacity>
    )
  }

  _renderRightSwipe () {
    const { filterName, intl } = this.props
    const fm = intl.formatMessage
    return (
      <TouchableOpacity
        style={[s.button, s.buttonDanger]}
        onPress={this._handleRightAction}
      >
        <Icon name='trash' style={s.icon} />
        <Text style={s.text}>{filterName === 'trash' ? fm(m.app.Common.delete) : fm(m.native.Mailbox.trash)}</Text>
      </TouchableOpacity>
    )
  }

  render () {
    return  (
      <View style={s.container}>
        {this._renderLeftSwipe()}
        {this._renderRightSwipe()}
      </View>
    )
  }
}

const IntlMailboxListItemSwipe = injectIntl(MailboxListItemSwipe)

const mapStateToProps = state => ({
  filterName: state.mailbox.filterName || 'inbox'
})

const mapDispatchToProps = {
  mailboxArchiveRequest: MailboxActions.mailboxArchiveRequest,
  mailboxUnarchiveRequest: MailboxActions.mailboxUnarchiveRequest,
  mailboxMoveToInboxRequest: MailboxActions.mailboxMoveToInboxRequest,
  mailboxTrashRequest: MailboxActions.mailboxTrashRequest,
  mailboxDeleteRequest: MailboxActions.mailboxDeleteRequest,
  displayNotification: NotificationActions.displayNotification,
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlMailboxListItemSwipe)
