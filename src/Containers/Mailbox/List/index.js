import React, { Component } from 'react';
import { View, TouchableOpacity, Button } from 'react-native'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types';
import { path } from 'ramda'
import { withStateHandlers, compose, setStatic } from 'recompose'

import MailboxActions from 'commons/Redux/MailboxRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'
import ListViewContainer from 'app/Components/ListView'
import FlatListContainer from 'app/Components/FlatList'
import baseStyles from 'app/Components/ListView/styles'
import NavButtonsWithSearch from 'app/Components/ListView/NavButtonsWithSearch'
import Drawer from 'app/Components/Drawer'
import DrawerContent from 'app/Containers/Mailbox/Drawer'
import Text from 'app/Components/BaseText'

import NoItem from './components/NoItem'
import MailboxListItem from './components/ListItem'
import ListItemSwipe from './components/ListItemSwipe'
import BatchActionsTools from './components/BatchActionsTools'
import BatchUpdateProgress from './components/BatchUpdateProgress'
import m from 'commons/I18n'

const getNavigationOptions = ({ navigation, fm }) => {
  const onSearchPress = path(['state', 'params', 'onSearchPress'], navigation)
  const onHamburgerPress = path(['state', 'params', 'onHamburgerPress'], navigation)

  const options = {
    tabBarVisible: true,
    title: fm(m.native.Mailbox.inbox),
    headerLeft: (
      <TouchableOpacity onPress={onHamburgerPress}>
        <EntypoIcon name='menu' style={[baseStyles.navBarMenuIcon, baseStyles.navBarLeftIcon]} />
      </TouchableOpacity>
    ),
    headerRight: (
      <NavButtonsWithSearch onSearchPress={onSearchPress}>
        <TouchableOpacity onPress={() => navigation.navigate('MailboxCompose')}>
          <EntypoIcon name='new-message' style={[baseStyles.navBarAddIcon, baseStyles.navBarRightIcon]} />
        </TouchableOpacity>
      </NavButtonsWithSearch>
    )
  }
  if (navigation.state.params && navigation.state.params.title) {
    options.title = fm(m.native.Mailbox[navigation.state.params.title])
  }
  return options
}

const getSelectionModeNavigationOptions = ({ navigation, fm }) => {
  return {
    headerRight: <BatchActionsTools navigation={navigation} />,
    headerLeft: (
      <Button
        onPress={() => navigation.dispatch(MailboxActions.mailboxClearSelection())}
        title={ fm(m.app.Common.cancel) }
      />
    )
  }
}

class Mailbox extends Component {

  static propTypes = {
    withFilter: PropTypes.string,
  }

  static contextTypes = {
    drawer: PropTypes.object
  }

  constructor(props) {
    super(props)

    this._goToDetailView = this._goToDetailView.bind(this)
    this._updateHeaderTitle = this._updateHeaderTitle.bind(this)
    this._updateHeaderMode = this._updateHeaderMode.bind(this)
    this._handleItemLongPress = this._handleItemLongPress.bind(this)
    this._handleItemSelectToggle = this._handleItemSelectToggle.bind(this)
  }

  _goToDetailView(data) {
    this.props.navigation.navigate('MailboxDetail', { id: data.id })
  }

  _updateHeaderTitle (nextProps) {
    const props = nextProps || this.props
    const { navigation, filterName } = props
    let title = null

    if (filterName) {
      title = filterName.split('')
      // title[0] = title[0].toUpperCase()
      title = title.join('')
    }

    if (nextProps.filterName !== this.props.filterName) {
      navigation.setParams({ title })
    }
  }

  _updateHeaderMode (nextProps) {
    const props = nextProps || this.props
    const { navigation, selectedIds } = props

    let selectionMode = false
    if (selectedIds && selectedIds.length) {
      selectionMode = true
    }

    if (
      navigation.state.params && (navigation.state.params.selectionMode !== undefined && navigation.state.params.selectionMode !== selectionMode)
    ) {
      navigation.setParams({ selectionMode: selectionMode })
    }
  }

  _handleItemLongPress (data) {
    const { selectEmail } = this.props
    selectEmail(data.id)
  }

  _handleItemSelectToggle (id) {
    const {
      selectedIds,
      selectEmail,
      unselectEmail,
    } = this.props
    if (selectedIds.indexOf(id) === -1) {
      selectEmail(id)
    } else {
      unselectEmail(id)
    }
  }

  componentWillReceiveProps (nextProps) {
    this._updateHeaderTitle(nextProps)
    this._updateHeaderMode(nextProps)
  }

  componentDidMount() {
    const { selectedIds } = this.props
    let selectionMode = false
    if (selectedIds && selectedIds.length) {
      selectionMode = true
    }

    this.props.navigation.setParams({
      onSearchPress: this.props.toggleSearchBar.bind(this),
      onHamburgerPress: this.props.toggleDrawer.bind(this),
      selectionMode: selectionMode
    })
  }

  render() {
    const { selectedUpdateInProgress, dataFetchInProgress, isRefreshing, searchBarVisible, intl } = this.props
    return (
      <View style={{flex: 1}}>
        <FlatListContainer
          listItemComponent={MailboxListItem}
          navigatorActiveKey='mailbox'
          selectItem={this._goToDetailView}
          fetchData={this.props.fetchMailbox}
          noDataMessage={<NoItem />}
          noSearchResultMessage={intl.formatMessage(m.native.Mailbox.noEmailsFound)}
          noSearchBar={!searchBarVisible}
          listItemSwipeComponent={ListItemSwipe}
          swipeLeftOpenValue={70}
          swipeRightOpenValue={-70}
          enableItemSelection={Boolean(this.props.selectedIds.length)}
          onItemLongPress={this._handleItemLongPress}
          toggleItemSelection={this._handleItemSelectToggle}
          initiallySelectedItemIDs={this.props.selectedIds}
          {...this.props}
          dataFetchInProgress={!selectedUpdateInProgress && dataFetchInProgress}
          isRefreshing={!selectedUpdateInProgress && isRefreshing}
          isRipple
        />
        {selectedUpdateInProgress && <BatchUpdateProgress />}
        <Drawer
          open={this.props.isDrawerOpen}
          onClose={() => this.props.toggleDrawer()}
          slideFrom='left'
          fullScreen
        >
          <DrawerContent closeDrawer={() => this.props.toggleDrawer()}/>
        </Drawer>
      </View>
    )
  }
}

const mapStateToProps = state => ({
  data: state.mailbox.data,
  dataOrder: state.mailbox.dataOrder,
  dataTotalCount: state.mailbox.dataTotalCount,

  searchResultsData: state.mailbox.searchResultsData,
  searchResultsDataOrder: state.mailbox.searchResultsDataOrder,
  searchResultsDataTotalCount: state.mailbox.searchResultsDataTotalCount,

  isRefreshing: state.mailbox.isRefreshing,
  isPaginating: state.mailbox.isPaginating,
  isSearching: state.mailbox.isSearching,

  dataFetchInProgress: state.mailbox.dataRequestInProgress,
  dataFetchSuccessful: state.mailbox.dataRequestSuccessful,
  dataFetchError: state.mailbox.dataRequestError,

  filterName: state.mailbox.filterName,

  selectedIds: state.mailbox.selectedIds,
  selectedUpdateInProgress: state.mailbox.selectedUpdateInProgress,
})

const mapDispatchToProps = {
  fetchMailbox: MailboxActions.mailboxFetch,
  clearSearchResultsData: MailboxActions.mailboxClearSearchData,
  setSearchQuery: MailboxActions.mailboxSetSearchQuery,
  setMailboxFilter: MailboxActions.setMailboxFilter,
  clearMailboxFilter: MailboxActions.clearMailboxFilter,
  clearMailboxIdentityFilter: MailboxActions.clearMailboxIdentityFilter,
  displayNotification: NotificationActions.displayNotification,
  selectEmail: MailboxActions.mailboxSelectEmail,
  unselectEmail: MailboxActions.mailboxUnselectEmail,
  clearSelection: MailboxActions.mailboxclearSelection
}

const navigationOptions = ({ navigation, screenProps: {fm} }) => {
  if (navigation.state.params && navigation.state.params.selectionMode) {
    return getSelectionModeNavigationOptions({ navigation, fm })
  }
  return getNavigationOptions({ navigation, fm })
}

const enhance = compose(
  setStatic('navigationOptions', navigationOptions),
  connect(mapStateToProps, mapDispatchToProps),
  withStateHandlers(
    {
      searchBarVisible: false,
      isDrawerOpen: false
    },
    {
      toggleSearchBar:
        ({ searchBarVisible }, { clearSearchResultsData }) =>
        () => ({ searchBarVisible: !searchBarVisible || !clearSearchResultsData() }),
      toggleDrawer: ({ isDrawerOpen }) => () => ({ isDrawerOpen: !isDrawerOpen })
    }
  ),
  injectIntl
)

export default enhance(Mailbox)
