import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, ScrollView } from 'react-native'
import { injectIntl } from 'react-intl'
import { connect } from 'react-redux'
import EStyleSheet from 'react-native-extended-stylesheet'

import Text from 'app/Components/BaseText'
import MailboxActions from 'commons/Redux/MailboxRedux'
import SideMenuButton from './FilterButton'
import commonStyles from 'app/Styles/common'
import palette from 'app/Styles/colors'
import MailboxSideMenuItemsFilter from './ItemsFilter'
import m from 'commons/I18n'

const styles = EStyleSheet.create({
  main: {
    backgroundColor: palette.darkStateBlue,
    paddingTop: '1.7rem',
    paddingBottom: '2rem',
    borderLeftWidth: 3,
    borderLeftColor: palette.darkStateBlue
  },

  paddedContent: {
    paddingLeft: '1.2rem',
    paddingRight: '1.2rem'
  },

  section: {
    marginTop: '2rem'
  },

  sectionTitle: {
    marginLeft: '0.6rem',
    marginBottom: '0.5rem'
  },

  sectionTitleText: {
    color: palette.clouds,
    fontWeight: '500',
    fontSize: '0.82rem',
    letterSpacing: '0.04rem'
  },

  sectionTitleIcon: {
    color: palette.clouds,
    fontSize: '0.8rem',
    marginRight: '0.3rem'
  }
})

class MailboxSideMenu extends Component {
  constructor (props) {
    super(props)

    this._goToIdentitySelectionDrawer = this._goToIdentitySelectionDrawer.bind(this)
  }

  _goToIdentitySelectionDrawer () {
    // RouterActions.refresh({key: 'identityFilterModal', visible: new Date()})
  }

  _setMailboxFilter (filter) {
    const { clearMailboxFilter, setMailboxFilter, closeDrawer } = this.props

    return () => {
      filter ? setMailboxFilter(filter) : clearMailboxFilter()

      closeDrawer()
    }
  }

  _getFiltersData () {
    const fm = this.props.intl.formatMessage
    return [
      {text: fm(m.native.Mailbox.inbox), key: null, featherIconName: 'inbox', onPress: this._setMailboxFilter()},
      {text: fm(m.native.Mailbox.unread), key: 'unread', featherIconName: 'bell', onPress: this._setMailboxFilter('unread')},
      {text: fm(m.native.Mailbox.sent), key: 'sent', featherIconName: 'mail', onPress: this._setMailboxFilter('sent')},
      {text: fm(m.native.Mailbox.forwarded), key: 'forwarded', featherIconName: 'corner-up-right', onPress: this._setMailboxFilter('forwarded')},
      {text: fm(m.native.Mailbox.archive), key: 'archive', featherIconName: 'folder', onPress: this._setMailboxFilter('archive')},
      {text: fm(m.native.Mailbox.trash), key: 'trash', featherIconName: 'trash-2', onPress: this._setMailboxFilter('trash')}
    ]
  }

  _renderFilters () {
    const { filterName } = this.props
    const filterData = this._getFiltersData()

    const filters = filterData.map(item => (
      <SideMenuButton active={item.key === filterName} {...item} />
    ))

    return (
      <View style={styles.paddedContent}>
        {filters}
      </View>
    )
  }

  render () {
    const { filterIdentityIdsCount } = this.props

    return (
      <ScrollView style={[styles.main, this.props.style]}>
        { this._renderFilters() }
        <View style={styles.section}>
          <View style={[styles.sectionTitle, commonStyles.horizontalAlign]}>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const IntlMailboxSideMenu = injectIntl(MailboxSideMenu)

const mapDispatchToProps = {
  setMailboxFilter: MailboxActions.setMailboxFilter,
  clearMailboxFilter: MailboxActions.clearMailboxFilter,
}

const mapStateToProps = state => ({
  filterName: state.mailbox.filterName,
  filterIdentityIdsCount: (state.mailbox.filterIdentityIds || []).length
})

export default connect(mapStateToProps, mapDispatchToProps)(IntlMailboxSideMenu)
