import React, { Component } from 'react';
import { injectIntl } from 'react-intl'
import { connect } from 'react-redux'
import { View, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native'
import Tabs from 'react-native-tabs'
import { NavigationActions } from 'react-navigation'
import Icon from 'react-native-vector-icons/FontAwesome'
import PropTypes from 'prop-types';
import { isEmpty, isNil } from 'ramda'

import tabbarStyles from 'app/Styles/tabbar'
import m from 'commons/I18n'

class MailboxDetailTabBar extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    displayNotification: PropTypes.func.isRequired,
    trashMail: PropTypes.func.isRequired,
    archiveMail: PropTypes.func.isRequired,
    mailboxReadRequest: PropTypes.func.isRequired,
    mailboxUnreadRequest: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props)

    this._reply = this._reply.bind(this)
    this._forward = this._forward.bind(this)
  }

  _toggleReadStatus(id) {
    const { data } = this.props

    if (data.is_read) {
      this.props.mailboxUnreadRequest(id)
      this._showNotification('emailMarkedUnread')
    } else {
      this.props.mailboxReadRequest(id)
      this._showNotification('emailMarkedRead')
    }
  }

  _trashMail(id) {
    this.props.trashMail(id)
    this._showNotification('emailTrashed', 'danger')
  }

  _archiveMail(id) {
    this.props.archiveMail(id)
    this._showNotification('emailArchived')
    this.props.navigateBack()
  }

  _showNotification(messageKey, messageType = 'info') {
    const { displayNotification } = this.props
    const fm = this.props.intl.formatMessage

    displayNotification(fm(m.native.Mailbox[messageKey]), messageType, 3000)
  }

  _reply() {
    const { data, navigate } = this.props

    if (!data.detail) return

    navigate({
      routeName: 'MailboxCompose',
      params: {
        mailboxReplyId: data.id,
        title: this.props.intl.formatMessage(m.native.Mailbox.reply),
      }
    })
  }

  _forward() {
    const { data, navigate } = this.props
    if (!data.detail) return

    navigate({
      routeName: 'MailboxCompose',
      params: {
        mailboxForwardId: data.id,
        title: this.props.intl.formatMessage(m.native.Mailbox.forward),
      }
    })
  }

  render() {
    const { data } = this.props

    return (
      <Tabs style={tabbarStyles.main}>
        <Icon
          name='trash'
          style={tabbarStyles.icon}
          onPress={this._trashMail.bind(this, data.id)}
        />
        <Icon
          name='archive'
          style={tabbarStyles.icon}
          onPress={this._archiveMail.bind(this, data.id)}
        />
        <Icon
          name='low-vision'
          style={[tabbarStyles.icon, data.is_read && tabbarStyles.iconActive]}
          onPress={this._toggleReadStatus.bind(this, data.id)}
        />
        <Icon
          name='reply'
          style={[tabbarStyles.icon, !data.detail && tabbarStyles.iconDisabled]}
          onPress={this._reply}
        />
        <Icon
          name='share'
          style={[tabbarStyles.icon, !data.detail && tabbarStyles.iconDisabled]}
          onPress={this._forward}
        />
      </Tabs>
    )
  }
}

const MapDispatchToProps = {
  navigateBack: NavigationActions.back,
  navigate: NavigationActions.navigate
}

const ConnectedMailboxDetailTabBar = connect(null, MapDispatchToProps)(injectIntl(MailboxDetailTabBar))

export default ConnectedMailboxDetailTabBar
