import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import palette from 'app/Styles/colors'
import Text from 'app/Components/BaseText'

const s = EStyleSheet.create({
  container: {
    paddingVertical: '0.75rem',
    paddingHorizontal: '1rem',
    backgroundColor: palette.clouds,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  text: {
    fontSize: '0.75rem',
    color: palette.asbestos
  },

  buttonText: {
    color: palette.belizeHole,
    textDecorationLine: 'underline'
  }
})

class MailboxDetailLoadImages extends Component {

  static propTypes = {
    onPress: PropTypes.func.isRequired,
  }

  render () {
    const fm = this.props.intl.formatMessage
    return (
      <View style={s.container}>
        <Text style={s.text}>{ fm(m.native.Mailbox.remoteImagesBlocked) }</Text>
        <TouchableOpacity style={s.button} onPress={this.props.onPress}>
          <Text style={s.buttonText}>{ fm(m.native.Mailbox.loadRemoteImages) }</Text>
        </TouchableOpacity>
      </View>
    )
  }

}

export default injectIntl(MailboxDetailLoadImages)
