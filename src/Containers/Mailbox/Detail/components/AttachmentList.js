import base64 from 'base-64'
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import { View, ScrollView, TouchableOpacity, ActivityIndicator, Image, Dimensions } from 'react-native'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'

import log from 'commons/Services/Log'
import Text from 'app/Components/BaseText'
import styles from '../styles'
import AttachementItem from './AttachmentItem'

class MailboxAttachmentList extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)

    this._toggleContentVisibility = this._toggleContentVisibility.bind(this)

    this.state = {
      contentVisible: false
    }
  }

  _toggleContentVisibility() {
    this.setState({
      contentVisible: !this.state.contentVisible
    })
  }

  _renderBody() {
    const { data } = this.props
    const { contentVisible } = this.state

    if (!contentVisible) return null

    return (
      <View style={styles.sectionContent}>
        {data.detail.attachmentContentIds.map(i => (
          <AttachementItem
            key={i}
            data={data.detail.attachments[i]}
          />
        ))}
      </View>
    )
  }

  render() {
    const { data } = this.props
    const { contentVisible } = this.state
    const fm = this.props.intl.formatMessage
    if (!data.detail || !data.detail.attachmentContentIds || !data.detail.attachmentContentIds.length) return null

    return (
      <View style={styles.sectionMain}>
        <View style={styles.sectionHeader}>
          <Text style={styles.sectionMessage}>
            {data.detail.attachments.length} {fm(m.native.Mailbox.attachments).toUpperCase()}
          </Text>
          <TouchableOpacity onPress={this._toggleContentVisibility}>
            <Text style={[styles.sectionMessage, styles.sectionAction]}>
              { contentVisible ? fm(m.native.Mailbox.hide).toUpperCase() : fm(m.native.Mailbox.show).toUpperCase() }
            </Text>
          </TouchableOpacity>
        </View>

        {this._renderBody()}
      </View>
    )
  }
}

export default injectIntl(MailboxAttachmentList)
