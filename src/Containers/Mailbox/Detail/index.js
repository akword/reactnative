import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import EStyleSheet from 'react-native-extended-stylesheet'
import PropTypes from 'prop-types';
import { isEmpty, isNil } from 'ramda'
import EntypoIcon from 'react-native-vector-icons/Entypo'

import ContactActions from 'commons/Redux/ContactRedux'
import palette from 'app/Styles/colors'
import MailboxActions from 'commons/Redux/MailboxRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'

import HeaderButton from 'app/Components/HeaderButton'
import Header from './components/Header'
import TabBar from './components/TabBar'
import Body from './components/Body'
import AttachmentList from './components/AttachmentList'
import AnalyticsMap from './components/AnalyticsMap'

const styles = EStyleSheet.create({
  main: {
    backgroundColor: 'white',
    flex: 1,
    paddingBottom: '3.125rem',
  },

  navButtons: {
    flexDirection: 'row',
  },

  navButton: {
    marginLeft: '0.7rem',
  },

  navButtonIcon: {
    fontSize: '1.7rem',
    marginRight: '0.7rem',
    color: palette.link,
  },

  navButtonIconDisabled: {
    color: palette.asbestos,
  },
});

/**
 * Mailbox Detail component.
 *
 * The id of the mailbox item that is to be displayed
 * can be passed through the redux store, which is plucked
 * out as props.navigation.state.id. Once mounted, the component
 * then keeps track of this id in the state.
 *
 */
class MailboxDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
    tabBarVisible: false,
    headerLeft: <HeaderButton isBack onPress={() => navigation.goBack()} />,
    headerRight: (
      <View style={styles.navButtons}>
        <TouchableOpacity style={styles.navButton} onPress={navigation.state.params.goToNextMail}>
          <EntypoIcon
            name='chevron-thin-down'
            style={[
              styles.navButtonIcon,
              !navigation.state.params.nextId && styles.navButtonIconDisabled
            ]}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.navButton} onPress={navigation.state.params.goToPreviousMail}>
          <EntypoIcon
            name='chevron-thin-up'
            style={[
              styles.navButtonIcon,
              !navigation.state.params.prevId && styles.navButtonIconDisabled
            ]}
          />
        </TouchableOpacity>
      </View>
    )
  })

  constructor(props) {
    super(props)

    this._showImages = this._showImages.bind(this)
    this._getData = this._getData.bind(this)
    this._getContact = this._getContact.bind(this)
    this._goToPreviousMail = this._goToPreviousMail.bind(this)
    this._goToNextMail = this._goToNextMail.bind(this)
    this._trashMail = this._trashMail.bind(this)
    this._archiveMail = this._archiveMail.bind(this)
    this._handleMailRemoval = this._handleMailRemoval.bind(this)
    this._updateRemoteContentRules = this._updateRemoteContentRules.bind(this)

    this.state = {
      currentId: props.navigation.state.params.id,
      imagesVisible: false,
      embeddedVisible: false,
    }
  }

  _getData () {
    let data = null
    const currentId = this.state.currentId
    if (currentId) {
      data = this.props.mailboxData[this.state.currentId]
    }
    return data
  }

  _getContact () {
    let contactKey = null
    let contact = null
    const { contactState } = this.props
    const data = this._getData()

    if (data) {
      contactKey = data.direction === 1 ? 'msg_to' : 'msg_from'
      
      if (
        contactState.data &&
        contactState.data[data[contactKey]] &&
        contactState.data[data[contactKey]].email === data[contactKey]
      ) {
        contact = contactState.data[data[contactKey]]
      }
    
      if (
        !contact &&
        contactState.cache &&
        contactState.cache[data[contactKey]] &&
        contactState.cache[data[contactKey]].email === data[contactKey]
      ) {
        contact = contactState.cache[data[contactKey]]
      }
    }
    return contact
  }

  _updateRemoteContentRules (props) {
    props = props || this.props
    const data = this._getData()
    const contact = this._getContact()
    if (!data) {
      return
    }
    const contactKey = data.direction === 1 ? 'msg_to' : 'msg_from'

    if (contact && contact.email === data[contactKey]) {
      if (contact.pref_mail_load_remote_content === null || contact.pref_mail_load_remote_content === undefined) {
        this.setState({ imagesVisible: props.loadRemoteContent })
      } else {
        this.setState({ imagesVisible: contact.pref_mail_load_remote_content })
      }
      if (contact.pref_mail_load_embedded_image === null || contact.pref_mail_load_embedded_image === undefined) {
        this.setState({ embeddedVisible: props.loadEmbeddedImage })
      } else {
        this.setState({ embeddedVisible: contact.pref_mail_load_embedded_image })
      }
    } else {
      // fetch the contact details if not present in redux store
      if (!props.contactRequestInProgress) {
        this.props.getContact({ search: {contact_email: data[contactKey]}, limit: 1 })
      }
    }
  }

  _showImages () {
    this.setState({ imagesVisible: true })
  }

  _goToPreviousMail() {
    this.props.prevId && this.setState({ currentId: this.props.prevId })
  }

  _goToNextMail() {
    this.props.nextId && this.setState({ currentId: this.props.nextId })
  }

  _handleMailRemoval(cb) {
    const { prevId, nextId } = this.props

    if (prevId || nextId) {
      this.setState({ currentId: prevId || nextId }, cb)
    } else {
      this.props.navigation.goBack()
    }
  }

  _trashMail(id) {
    this._handleMailRemoval(() => {
      this.props.mailboxTrashRequest(id)
      this.props.setMailboxIterationIds(this.state.currentId)
    })
  }

  _archiveMail(id) {
    this._handleMailRemoval(() => {
      this.props.mailboxArchiveRequest(id)
      this.props.setMailboxIterationIds(this.state.currentId)
    })
  }

  _updateIteratorIcons(props) {
    props = props || this.props
    const { nextId, prevId } = props

    props.navigation.setParams({
      nextId: nextId,
      prevId: prevId,
      goToNextMail: this._goToNextMail,
      goToPreviousMail: this._goToPreviousMail
    })
  }

  componentDidMount() {
    const { currentId } = this.state
    const thisData = this.props.mailboxData[currentId]

    // Set the mailbox iteration ids
    this.props.setMailboxIterationIds(currentId)

    // Request pickup data
    if (thisData && !thisData.detail && thisData.is_msgsafe_store) {
      this.props.mailboxDetailRequest(currentId)
    }

    // Request analytics data
    if (thisData && !thisData.analytics) {
      this.props.mailboxAnalyticsRequest(currentId)
    }

    // Mark the email read
    if (currentId && !thisData.is_read) {
      this.props.mailboxReadRequest(currentId)
    }

    this._updateIteratorIcons()
    this._updateRemoteContentRules()
  }

  componentWillReceiveProps(nextProps) {
    // If the mailboxDetailId passed as a prop has changed,
    // then update the currentId in state
    if (this.props.navigation.state.params.id !== nextProps.navigation.state.params.id) {
      this.setState({ currentId: nextProps.navigation.state.params.id })
    }

    // If the next or the previous IDs have changed, update the iterator icons
    if ((this.props.nextId !== nextProps.nextId) || (this.props.prevId !== nextProps.prevId))  {
      this._updateIteratorIcons(nextProps)
    }

    const thisData = this.props.mailboxData[this.state.currentId]
    const nextData = nextProps.mailboxData[this.state.currentId]

    // If the pickup data has just come in, then mark the email read
    if (thisData && nextData && nextData.detail && !nextData.is_read && thisData.detail !== nextData.detail) {
      nextProps.mailboxReadRequest(this.state.currentId)
    }

    this._updateRemoteContentRules(nextProps)
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.currentId === nextState.currentId || !nextState.currentId) return

    // If the currentId has changed in state then update the
    // iteration ids in redux store
    this.props.setMailboxIterationIds(nextState.currentId)

    const nextData = nextProps.mailboxData[nextState.currentId]
    if (!nextData) return

    // Make the mailbox detail data request
    if (
      nextData.is_msgsafe_store // and mail is meant for webmail
      && !nextData.detail // and mail detail data is not present
      // and the detail data is not already in progress
      && (nextData.detailStatus ? !nextData.detailStatus.inProgress : true)
    ) {
      // then make the mailbox detail request
      this.props.mailboxDetailRequest(nextState.currentId)
    }

    // Make the mailbox analytics data request
    if (
      !nextData.analytics // and mail analytics data is not present
      // and the detail data is not already in progress
      && (nextData.analyticsStatus ? !nextData.analyticsStatus.inProgress : true)
    ) {
      // then make the mailbox detail request
      this.props.mailboxAnalyticsRequest(nextState.currentId)
    }
  }

  componentWillUnmount() {
    this.props.clearMailboxIterationIds()
  }

  render() {
    const { mailboxData } = this.props
    const { currentId } = this.state
    const data = mailboxData[currentId]

    if (isEmpty(data) || isNil(data)) return null

    return (
      <View style={styles.main}>
        <ScrollView style={{flex: 1}}>
          <Header
            fromName={data.msg_from_displayname}
            subject={data.msg_subject}
            time={data.created_on}
            fromEmail={data.msg_from}
            toEmail={data.msg_to}
            isRead={data.is_read}
          />
          <AttachmentList data={data} />
          { data && data.is_msgsafe_store &&
            <AnalyticsMap analyticsData={data.analytics} />
          }
          <Body
            data={data}
            imagesVisible={this.state.imagesVisible}
            embeddedVisble={this.state.embeddedVisible}
            showImages={this._showImages}
          />
          { data && !data.is_msgsafe_store &&
            <AnalyticsMap analyticsData={data.analytics} defaultOpen={true} withTopBorder={true} />
          }
        </ScrollView>
        <TabBar
          data={data}
          trashMail={this._trashMail}
          archiveMail={this._archiveMail}
          displayNotification={this.props.displayNotification}
          mailboxReadRequest={this.props.mailboxReadRequest}
          mailboxUnreadRequest={this.props.mailboxUnreadRequest}
        />
      </View>
    )
  }
}


const mapStateToProps = state => {
  const props = {
    isListDataAvailable: !!(state.mailbox.data || state.mailbox.searchResultsData),
    mailboxData: state.mailbox.searchResultsData || state.mailbox.data,
    nextId: state.mailbox.nextId,
    prevId: state.mailbox.prevId,
    loadRemoteContent: state.user.data.pref_mail_load_remote_content,
    loadEmbeddedImage: state.user.data.pref_mail_load_embedded_image,
    contactRequestInProgress: state.contact.api.getContact.inProgress,
    contactState: state.contact,
  }
  return props
}

const mapDispatchToProps = {
  mailboxDetailRequest: MailboxActions.mailboxDetailRequest,
  mailboxAnalyticsRequest: MailboxActions.mailboxAnalyticsRequest,
  mailboxReadRequest: MailboxActions.mailboxReadRequest,
  mailboxUnreadRequest: MailboxActions.mailboxUnreadRequest,
  mailboxTrashRequest: MailboxActions.mailboxTrashRequest,
  mailboxArchiveRequest: MailboxActions.mailboxArchiveRequest,
  setMailboxIterationIds: MailboxActions.mailboxSetIterationIds,
  clearMailboxIterationIds: MailboxActions.mailboxClearIterationIds,
  displayNotification: NotificationActions.displayNotification,
  getContact: ContactActions.getContactRequest,
}

export default connect(mapStateToProps, mapDispatchToProps)(MailboxDetail)
