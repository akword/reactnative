import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Text from 'app/Components/BaseText'
import palette from 'app/Styles/colors'
import AttachmentItem from './AttachmentItem'

const s = EStyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderBottomColor: palette.clouds,
    paddingHorizontal: '0.7rem',
    paddingVertical: '0.4rem',
  },

  title: {
    color: palette.silver
  }
})

class AttachmentList extends Component {

  static propTypes = {
    data: PropTypes.array.isRequired,
    onItemRemove: PropTypes.func.isRequired,
  }

  render () {
    const { data } = this.props
    if (!data.length) {
      return null
    }
    return (
      <View style={s.container}>
        <Text style={s.title}>Attachments:</Text>
        {data.map((file, index) => (
          <AttachmentItem
            {...file}
            key={file.id}
            onRemove={() => this.props.onItemRemove(file.id)}
            first={index === 0}
          />
        ))}
      </View>
    )
  }

}

export default AttachmentList
