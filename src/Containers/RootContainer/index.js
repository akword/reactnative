import React, { PureComponent } from 'react'
import PropTypes from 'prop-types';
import { StatusBar, Alert, Modal, TouchableOpacity, View, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import { addNavigationHelpers } from 'react-navigation'
import { IntlProvider } from 'react-intl'
import StatusBarAlert from 'react-native-statusbar-alert'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import StartupActions from 'commons/Redux/StartupRedux'
import AppActions from 'commons/Redux/AppRedux'
import { isLoggedIn } from 'commons/Selectors/User'
import Text from 'app/Components/BaseText'

import Notification from 'app/Components/Notification'
import VideoChatStatusProvider from 'app/Components/VideoChatStatusProvider'
import AppNavigator from 'app/Navigation'
import Overlay from 'app/Containers/Overlay'

const { width } = Dimensions.get('window')

const IntlAppNavigator = injectIntl(({ intl, dispatch, nav }) => (
  <AppNavigator
    navigation={addNavigationHelpers({
      dispatch: dispatch,
      state: nav
    })}
    screenProps={{fm: intl.formatMessage}}
  />)
)

class RootContainer extends PureComponent {

  componentDidMount () {
    this.props.startup()
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loggedIn) {
      if (this.props.isNativeAppNetworkOnline && !nextProps.isNativeAppNetworkOnline) {
        this.props.refreshNetwork() // Force Refresh
      }
    }
  }

  _renderSocketStatus () {
    const { loggedIn, socketDown, socketConnecting, connectionTimeout, intl } = this.props

    // Don't render network status when user is not logged in
    if (!loggedIn) return

    // Display socket status after the timeout
    if (!connectionTimeout) return

    // Don't render network status when neither of socketConnecting
    // or socketDown are true
    if (!socketConnecting && !socketDown) return

    const connectingMsg = (intl.messages || {})[m.native.Snackbar.connecting.id] || m.native.Snackbar.connecting.defaultMessage
    const socketDownMsg = (intl.messages || {})[m.native.Snackbar.socketDown.id] || m.native.Snackbar.socketDown.defaultMessage

    return (
      <StatusBarAlert
        visible={true}
        message={socketConnecting ? connectingMsg : socketDownMsg}
        backgroundColor={socketConnecting ? 'yellow' : '#FC3B3F'}
        color={socketConnecting ? 'black' : 'white'}
        statusbarHeight={25}
      />
    )
  }

  _renderNetworkStatus () {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={!this.props.isNativeAppNetworkOnline && this.props.loggedIn}
        onRequestClose={() => {}}
      >
        <View
          style={{ flex: 1, backgroundColor: '#00000087', alignItems: 'center', flexDirection: 'row' }}
        >
          <View
            style={{ flex: 1, alignItems: 'center', flexDirection: 'column' }}
          >
            <View style={{
              //marginLeft: 60,
              //marginRight: 60,
              width: width * 0.7,
              backgroundColor: '#FFF',
              borderRadius: 10,
              paddingTop: 16,
              paddingBottom: 10
            }}>
              <Text style={{textAlign: 'center', fontSize: 16, fontWeight: '500'}}>There Is No Network Connection</Text>
              <Text style={{textAlign: 'center', marginTop: 6, marginLeft:16, marginRight: 16, fontSize: 12.5}}>Check your network connection and try again!</Text>
              <View
                style={{borderTopWidth: 0.5, marginTop: 18,  borderTopColor: '#00000037'}}
              >
                <TouchableOpacity
                  style={{paddingTop: 12, paddingBottom: 2}}
                  onPress={this.props.refreshNetwork}>
                  <Text style={{textAlign: 'center', fontSize: 16, fontWeight: '500', color: '#0a60fe'}}>Refresh</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  render () {
    //todo: remove defaultLocale props in IntlProvider after all translations added
    return (
      <IntlProvider {...this.props.intl} key={this.props.intl.locale} defaultLocale={this.props.intl.locale} textComponent={Text}>
        <View style={{ flex: 1 }}>
          {this._renderSocketStatus()}
          {/*this._renderNetworkStatus()*/}
          <StatusBar barStyle='default' />
          <VideoChatStatusProvider>
            <IntlAppNavigator
              dispatch={this.props.dispatch}
              nav={this.props.nav}
            />
          </VideoChatStatusProvider>
          <Notification
            message={this.props.notificationMessage}
            type={this.props.notificationMessageType}
          />
          <Overlay />
        </View>
      </IntlProvider>
    )
  }
}

const mapStateToProps = state => ({
  intl: state.intl,
  nav: state.nav,
  notificationMessage: state.notification.message,
  notificationMessageType: state.notification.messageType,
  isNativeAppNetworkOnline: state.app.isNativeAppNetworkOnline,
  refreshNetwork: state.app.refresh,
  socketDown: state.chat.socketDown,
  socketConnecting: state.chat.socketConnecting,
  loggedIn: isLoggedIn(state.user),
  connectionTimeout: state.app.connectionTimeout
})

const mapDispatchToProps = dispatch => ({
  startup: () => dispatch(StartupActions.startup()),
  refreshNetwork: () => dispatch(AppActions.checkNetworkState()),
  dispatch: dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer)
