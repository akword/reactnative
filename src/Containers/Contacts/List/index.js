import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import { path, isEmpty, isNil, sort } from 'ramda'
import RNContacts from 'react-native-contacts'
import { withStateHandlers, compose, setStatic } from 'recompose'
import OpenSettings from 'react-native-open-settings'

import m from 'commons/I18n'
import ContactActions from 'commons/Redux/ContactRedux'

import DeviceContactActions from 'app/Redux/DeviceContactRedux'
import Text from 'app/Components/BaseText'
import { getCurrentRouteName } from 'app/Navigation/utils'
import HeaderButton from 'app/Components/HeaderButton'
import SlimHeader from 'app/Components/SlimHeader'
import baseStyles from 'app/Components/ListView/styles'
import ListViewError from 'app/Components/ListView/components/ListViewError'
import FlatListContainer from 'app/Components/FlatList'
// import ListViewContainer from 'app/Components/ListView'
import NavButtonsWithSearch from 'app/Components/ListView/NavButtonsWithSearch'

import ContactListItemSwipe from './components/ListItemSwipe'
import ContactListItem from './components/ListItem'
import DeviceContactListItem from './components/ListItem/DeviceContact'
import AlphabetPicker from './components/AlphabetPicker'
import styles from './styles'

const diff = (a, b) => {
  const name1 = a.display_name.toUpperCase()
  const name2 = b.display_name.toUpperCase()

  if (name1 < name2) return -1
  if (name1 > name2) return 1
  return name1 < name2
}

// const alphabetList = [{
//   char: 'hash',
//   label: '#',
//   id: '#'
// }]
//
// for( let i = 65; i <= 90; i += 1 ) {
//   alphabetList.push({
//     char: String.fromCharCode( i ).toLowerCase(),
//     label: String.fromCharCode( i ),
//     id: String.fromCharCode( i ),
//   })
// }

class Contacts extends Component {
  constructor (props) {
    super(props)

    this._askForContactsImport = this._askForContactsImport.bind(this)
    this._goToCreateView = this._goToCreateView.bind(this)
    this._handleOnUnmount = this._handleOnUnmount.bind(this)
    this._renderNoItemError = this._renderNoItemError.bind(this)
    this._renderTabs = this._renderTabs.bind(this)
    this._onAlphabetPress = this._onAlphabetPress.bind(this)

    this.flatListRef = null
    this.prevAlphabet = ''
    this.aTozIndexes = {}
  }

  _askForContactsImport (nextProps) {
    if (
      nextProps.contactsPermissionAlert ||
      nextProps.currentRouteName !== 'ContactList' ||
      nextProps.contactsImported ||
      nextProps.contactsPermission !== RNContacts.PERMISSION_UNDEFINED ||
      nextProps.overlay.open // overlay is not opened
    ) {
      return
    }
    this.props.contactsImportRequest()
  }

  _goToCreateView () {
    let data = {}

    // If the contact list view has identityId, pass that to create contact
    // screen so that the identity can be pre-selected
    const identityId = path(['navigation', 'state', 'params', 'identityId'], this.props)
    if (identityId) {
      data['prefillItemData'] = { identity_id: identityId }
    }

    this.props.navigation.navigate('EditContact', data)
  }

  _renderNoItemError () {
    const fm = this.props.intl.formatMessage
    const showDeviceContactsOnly = this._showDeviceContactsOnly()

    if (!showDeviceContactsOnly) {
      return (
        <ListViewError>
          <Text style={baseStyles.errorText}>{fm(m.native.Contact.noContacts)}</Text>
          <TouchableOpacity style={baseStyles.errorAction} onPress={this._goToCreateView}>
            <Text style={[baseStyles.errorText, baseStyles.errorActionText]}>{fm(m.app.Common.clickHereToCreate)}</Text>
          </TouchableOpacity>
        </ListViewError>
      )
    }

    if (this.props.contactsPermission === 'authorized') {
      return fm(m.native.Contact.noDeviceContactsFound)
    }

    return (
      <ListViewError>
        <Text style={baseStyles.errorTextTitle}>{fm(m.native.Contact.importYourContacts)}</Text>
        <Text style={baseStyles.errorText}>{fm(m.native.Contact.integrateYourExistingContacts)}</Text>
        <TouchableOpacity style={baseStyles.errorAction} onPress={() => OpenSettings.openSettings()}>
          <Text style={[baseStyles.errorText, baseStyles.errorActionText]}>{fm(m.native.Contact.updateContactPermission)}</Text>
        </TouchableOpacity>
      </ListViewError>
    )
  }

  _renderHeader () {
    const { identity_display_name, identity_email } = this.props

    if (!identity_display_name) return  // eslint-disable-line

    const subtitle = identity_display_name !== identity_email ? identity_email : null // eslint-disable-line

    return <SlimHeader title={identity_display_name} subtitle={subtitle} /> // eslint-disable-line
  }

  _forceSearch () {
    return !!this.props.identity_id
  }

  _handleOnUnmount () {
    const { navigation } = this.props
    if (
      !navigation.state.params ||
      !navigation.state.params.onUnmount ||
      typeof navigation.state.params.onUnmount !== 'function'
    ) {
      return
    }
    navigation.state.params.onUnmount()
  }

  _renderTabs () {
    const hideDeviceContacts = path(['navigation', 'state', 'params', 'hideDeviceContacts'], this.props)
    const fm = this.props.intl.formatMessage
    if (this.props.contactSearchFiltersPresent || hideDeviceContacts) return null

    const showDeviceContactsOnly = this._showDeviceContactsOnly()

    return (
      <View style={styles.tabs}>
        <TouchableOpacity style={[styles.tab, !showDeviceContactsOnly && styles.tabActive]} onPress={() => this.props.navigation.setParams({ device: false })}>
          <Text style={styles.tabText}>MSGSAFE.IO</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tab, showDeviceContactsOnly && styles.tabActive]} onPress={() => this.props.navigation.setParams({ device: true })}>
          <Text style={styles.tabText}>{fm(m.native.Contact.device).toUpperCase()}</Text>
        </TouchableOpacity>
      </View>
    )
  }

  _showDeviceContactsOnly () {
    return !!path(['navigation', 'state', 'params', 'device'], this.props)
  }

  _onAlphabetPress (letter) {
    letter = letter.toLowerCase()

    try {
      if (this.flatListRef && (this.aTozIndexes[letter] || this.aTozIndexes[letter] === 0)) {
        this.flatListRef.getWrappedInstance().getWrappedInstance()._scrollToIndex(this.aTozIndexes[letter], false)
      }
    } catch (e) {
      return false
    }
  }

  componentWillReceiveProps (nextProps) {
    this._askForContactsImport(nextProps)

    const deviceContact = path(['navigation', 'state', 'params', 'device'], this.props)
    const nextDeviceContact = path(['navigation', 'state', 'params', 'device'], nextProps)

    // if (nextProps.contactPermission !== 'authorized') {
    //   return
    // }
    if (deviceContact !== nextDeviceContact && !nextProps.data) {
      nextProps.fetchContacts()
    }
  }

  componentWillUnmount() {
    this._handleOnUnmount()
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onSearchPress: this.props.toggleSearchBar.bind(this)
    })
  }

  render () {
    const { identity_display_name, intl, searchBarVisible } = this.props
    const fm = intl.formatMessage
    let noSearchResultMessage = fm(m.native.Contact.noContacts)
    let lastTempAlphabet = '';
    this.aTozIndexes = [];

    if (identity_display_name) {
      noSearchResultMessage = fm(m.native.Contact.noContactsForIdentity, { name: identity_display_name })
    }

    const deviceContact = this._showDeviceContactsOnly()

    const listItemComponent = path(['navigation', 'state', 'params', 'listItemComponent'], this.props) ||
      (deviceContact ? DeviceContactListItem : ContactListItem)
    const tabBarVisible = path(['navigation', 'state', 'params', 'tabBarVisible'], this.props)

    const swipeProps = {
      listItemSwipeComponent: deviceContact ? undefined : ContactListItemSwipe,
      swipePosition: 'right',
      swipeLeftOpenValue: 0,
      swipeRightOpenValue: -70
    }

    if (this.props.data) {
      const sortedContact = sort(diff, Object.values(this.props.data))

      sortedContact.forEach((contact, index) => {
        const { display_name } = contact
        const nameInitial = display_name[0].toLowerCase()

        if (
          nameInitial.toLowerCase() !== nameInitial.toUpperCase() &&
          nameInitial !== lastTempAlphabet
        ) {
          this.aTozIndexes[nameInitial] = index
          lastTempAlphabet = nameInitial
        }
      })
    }

    return (
      <View style={{flex: 1}}>
        {this._renderTabs()}
        <FlatListContainer
          listItemComponent={listItemComponent}
          {...(deviceContact ? {} : swipeProps)}
          navigatorActiveKey='contacts'
          initialSearchQuery={this.props.searchQuery}
          fetchData={this.props.fetchContacts}
          forceSearch={this._forceSearch()}
          noDataMessage={this._renderNoItemError()}
          noSearchResultMessage={noSearchResultMessage}
          header={this._renderHeader()}
          noTabBar={!tabBarVisible}
          noSearchBar={!searchBarVisible}
          ref={ref => { this.flatListRef = ref }}
          {...this.props}
        />
        {
          this.props.data && !searchBarVisible &&
          <View style={styles.alphabetsListContainer}>
            <AlphabetPicker onTouchLetter={this._onAlphabetPress.bind(this)} />
          </View>
        }
      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const key = path(['navigation', 'state', 'params', 'device'], ownProps) ? 'deviceContact' : 'contact'
  let dataOrder = state[key].dataOrder

  if (dataOrder && dataOrder.length) {
    const sortedContact = sort(diff, Object.values(state[key].data))

    if (key === 'deviceContact') {
      dataOrder = sortedContact.map(contact => contact.id)
    } else {
      dataOrder = sortedContact.map(contact => contact.email)
    }
  }

  return {
    data: state[key].data,
    dataOrder,
    dataTotalCount: state[key].dataTotalCount,

    searchQuery: state[key].searchQuery,
    searchResultsData: state[key].searchResultsData,
    searchResultsDataOrder: state[key].searchResultsDataOrder,
    searchResultsDataTotalCount: state[key].searchResultsDataTotalCount,

    dataFetchInProgress: state[key].dataRequestInProgress,
    dataFetchSuccessful: state[key].dataRequestSuccessful,
    dataFetchError: state[key].dataRequestError,

    isRefreshing: state[key].isRefreshing,
    isPaginating: state[key].isPaginating,
    isSearching: state[key].isSearching,

    contactSearchFiltersPresent: (
      // A search filter is set
      // (!isEmpty(state.contact.searchFilters) && !isNil(state.contact.searchFilters)) ||
      // Or an identity filter is set
      (!isEmpty(state.contact.filterIdentityIds) && !isNil(state.contact.filterIdentityIds))
    ),

    contactsImported: state.deviceContact.imported,
    contactsPermission: state.deviceContact.permission,
    contactsPermissionAlert: state.deviceContact.alert,
    currentRouteName: getCurrentRouteName(state.nav),
    overlay: state.overlay
  }
}

const mapDispatchToProps = (dispatch, { navigation }) => {
  const deviceContact = path(['state', 'params', 'device'], navigation)

  const fetch = deviceContact ? DeviceContactActions.deviceContactFetch : ContactActions.contactFetch
  const clearSearchResultsData = deviceContact ? DeviceContactActions.deviceContactClearSearchData : ContactActions.contactClearSearchData
  const setSearchQuery = deviceContact ? DeviceContactActions.deviceContactSetSearchQuery : ContactActions.contactSetSearchQuery

  return {
    fetchContacts: (...args) => dispatch(fetch(...args)),
    clearSearchResultsData: (...args) => dispatch(clearSearchResultsData(...args)),
    setSearchQuery: (...args) => dispatch(setSearchQuery(...args)),
    contactsImportRequest: (...args) => dispatch(DeviceContactActions.contactsImportRequest(...args))
  }
}

const navigationOptions = ({ navigation, screenProps: {fm} }) => {
  const { state } = navigation
  const createScreenIdentifier = path(['params', 'createScreenIdentifier'], state) || 'EditContact'
  const headerLeftProps = path(['params', 'headerLeftProps'], state)
  const onSearchPress = path(['params', 'onSearchPress'], state)
  const deviceContactsTabActive = !!path(['state', 'params', 'device'], navigation)

  const options = {
    title: path(['params', 'title'], state) || fm(m.native.Contact.contacts),
    tabBarVisible: path(['params', 'tabBarVisible'], state),
    headerRight: (
      <NavButtonsWithSearch onSearchPress={onSearchPress}>
        <TouchableOpacity onPress={() => navigation.navigate(createScreenIdentifier)} disabled={deviceContactsTabActive}>
          <EntypoIcon name='plus' style={[baseStyles.navBarMenuIcon, baseStyles.navBarRightIcon, deviceContactsTabActive && baseStyles.navBarMenuIconDisabled]} />
        </TouchableOpacity>
      </NavButtonsWithSearch>
    )
  }

  if (!headerLeftProps) { return options }

  return {
    ...options,
    headerLeft: <HeaderButton {...headerLeftProps} onPress={() => navigation.goBack()} />
  }
}

const enhance = compose(
  setStatic('navigationOptions', navigationOptions),
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl,
  withStateHandlers(
    {searchBarVisible: false},
    {
      toggleSearchBar:
        ({ searchBarVisible }, { clearSearchResultsData }) =>
        () => ({ searchBarVisible: !searchBarVisible || !clearSearchResultsData() })
    }
  )
)

export default enhance(Contacts)
