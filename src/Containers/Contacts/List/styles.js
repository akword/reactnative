import EStyleSheet from 'react-native-extended-stylesheet'
import { Dimensions, Platform, PixelRatio } from 'react-native'
import palette from 'app/Styles/colors'

const deviceHeight = Dimensions.get( 'window' ).height;
deviceWidth = Dimensions.get( 'window' ).width;

function fontSizer (screenWidth) {
  if(screenWidth > 400){
    return 15
  } else if(screenWidth > 320){
    return 13
  } else {
    return 12
  }
}

const styles = EStyleSheet.create({
  tabs: {
    flexDirection: 'row'
  },

  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: '0.4rem',
    paddingTop: '0.4rem',
    borderBottomColor: 'transparent',
    borderBottomWidth: 4,
    backgroundColor: 'white'
  },

  tabActive: {
    borderBottomColor: 'rgb(0, 131, 232)'
  },

  tabText: {
    fontSize: '0.75rem',
    color: 'rgb(32, 48, 90)',
    fontWeight: '800'
  },

  alphabetsListContainer: {
    // flex: 1,
    // position: 'absolute',
    // right: 0,
    // top: 32
    position: 'absolute',
    backgroundColor: 'transparent',
    top: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },

  alphabet: {
    color: palette.iosBlue,
    backgroundColor: 'transparent',
    fontSize: Platform.OS === 'ios' ? fontSizer(deviceWidth) : Math.round(PixelRatio.roundToNearestPixel(15)) - 2,
  },

  alphabetSection: {
    paddingRight: 1
  }
})

export default styles
