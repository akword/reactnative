import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { View, PanResponder } from 'react-native'
import Text from 'app/Components/BaseText'
import palette from 'app/Styles/colors'

const LetterPicker = ({ letter }) => (
  <Text style={{ fontSize: 12, fontWeight: 'bold', color: palette.link }}>
    {letter}
  </Text>
)

class AlphabetPicker extends Component {
  constructor (props, context) {
    super(props, context)

    this.alphabet = props.alphabet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')
  }

  componentWillMount () {
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: (e, gestureState) => {
        this.props.onTouchStart && this.props.onTouchStart()

        this.tapTimeout = setTimeout(() => {
          this._onTouchLetter(this._findTouchedLetter(gestureState.y0))
        }, 100)
      },
      onPanResponderMove: (evt, gestureState) => {
        clearTimeout(this.tapTimeout)
        this._onTouchLetter(this._findTouchedLetter(gestureState.moveY))
      },
      onPanResponderTerminate: this._onPanResponderEnd.bind(this),
      onPanResponderRelease: this._onPanResponderEnd.bind(this)
    })
  }

  _onTouchLetter (letter) {
    letter && this.props.onTouchLetter(letter)
  }

  _onPanResponderEnd () {
    requestAnimationFrame(() => { // eslint-disable-line
      this.props.onTouchEnd && this.props.onTouchEnd()
    })
  }

  _findTouchedLetter (y) {
    const top = y - (this.absContainerTop || 0)

    if (top >= 1 && top <= this.containerHeight) {
      return this.alphabet[Math.floor((top / this.containerHeight) * this.alphabet.length)]
    }
  }

  _onLayout (event) {
    this.refs.alphabetContainer.measure((x1, y1, width, height, px, py) => {
      this.absContainerTop = py
      this.containerHeight = height
    })
  }

  render () {
    this._letters = this._letters || this.alphabet.map(l => <LetterPicker letter={l} key={l} />)

    return (
      <View ref='alphabetContainer' {...this._panResponder.panHandlers} onLayout={this._onLayout.bind(this)} style={{
        paddingHorizontal: 5,
        backgroundColor: '#fff',
        borderRadius: 1,
        justifyContent: 'center'
      }}>
        <View>
          {this._letters}
        </View>
      </View>
    )
  }
}

AlphabetPicker.propTypes = {
  onTouchLetter: PropTypes.func.isRequired
}

export default AlphabetPicker
