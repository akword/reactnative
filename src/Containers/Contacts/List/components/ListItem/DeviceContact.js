import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity } from 'react-native'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import { NavigationActions } from 'react-navigation'
import Ripple from 'react-native-material-ripple'
import { DataPicker } from 'rnkit-actionsheet-picker'
import { injectIntl } from 'react-intl'
import m from 'commons/I18n'
import ChatActions from 'commons/Redux/ChatRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'

import Avatar from 'app/Components/Avatar'
import Text from 'app/Components/BaseText'
import { showIdentitySelectAndCallback } from 'app/Lib/Identity'
import { checkWebRTCPermissions, showNoWebRTCPermissionModal } from 'app/Lib/Device'

import { ListItem as s } from './styles'

class DeviceContactListItem extends PureComponent {

  static propTypes = {
    data: PropTypes.object.isRequired,
    avatarImage: PropTypes.string,
  }

  constructor (props) {
    super(props)

    this._goToDetailView = this._goToDetailView.bind(this)

    this._chatWithIdentity = this._chatWithIdentity.bind(this)
    this._chat = this._chat.bind(this)

    this._audioCallWithIdentity = this._audioCallWithIdentity.bind(this)
    this._audioCall = this._audioCall.bind(this)

    this._pickEmailAddress = this._pickEmailAddress.bind(this)
    this._selectIdentity = this._selectIdentity.bind(this)
  }

  _selectIdentity (callback) {
    const { navigate } = this.props
    navigate({ routeName: 'IdentitySelection', params: {
      disableSwipe: true,
      selectItemAndPop: callback
    }})
  }

  _audioCallWithIdentity (contactEmail, identity) {
    const { data } = this.props
    if (!data) return

    let audioOnly = true
    this.props.videoCallRequest(identity.email, contactEmail, data.display_name, audioOnly)
  }

  _audioCall () {
    checkWebRTCPermissions().then(r => {
      if (!r) {
        showNoWebRTCPermissionModal()
        return
      }

      this._pickEmailAddress(contactEmail => this._selectIdentity(identity => this._audioCallWithIdentity(contactEmail, identity)))
    })
  }

  _chatWithIdentity (contactEmail, identity) {
    const { navigate, navigateBack, roomsMap, chatCreateRoomRequest, chatSetupExistingRoom } = this.props

    const roomsMapKey = `${identity.email}__${contactEmail}`

    const roomId = roomsMap[roomsMapKey]
    // Room already exists
    if (roomId) {
      chatSetupExistingRoom(roomId, identity.email, identity.display_name, contactEmail)
    } else {
      chatCreateRoomRequest(identity.email, identity.display_name, contactEmail, false)
    }
    navigateBack()
    navigate({ routeName: 'MessagingRoom', params: { roomsMapKey } })
  }

  _chat () {
    this._pickEmailAddress(contactEmail => this._selectIdentity(identity => this._chatWithIdentity(contactEmail, identity)))
  }

  _pickEmailAddress (callback) {
    const { data, identities, intl } = this.props
    const fm = intl.formatMessage
    if (data.emailAddresses.length === 1) {
      callback(data.emailAddresses[0].email)
      return
    }

    const dataSource = data
      .emailAddresses
      .map(e => e.email)
      .filter(email => {
        if (typeof identities === 'object') {
          for (let id in identities) {
            if (identities[id] && identities[id].email === email) {
              return false
            }
          }
        }
        return true
      })

    DataPicker.show({
      dataSource,
      titleText: fm(m.native.Contact.selectEmail),
      defaultSelected: [dataSource[0]],
      doneText: fm(m.app.Common.done),
      cancelText: fm(m.app.Common.cancel),
      numberOfComponents: 1,
      onPickerConfirm: val => callback(val[0])
    })
  }

  _goToDetailView () {
    const { data, navigate } = this.props
    navigate({ routeName: 'DeviceContactDetail', params: { id: data.id } })
  }

  _getContactName(data) {
    let name = ''
    if (data) {
      if (data.givenName) {
        name = name + data.givenName + ' '
      }
      if (data.familyName) {
        name = name + data.familyName
      }

      if (!name) {
        name = data.display_name
      }

      return name || data.email
    }
    return ''
  }

  render() {
    const { data, avatarImage, isMyIdentity } = this.props
    return (
      <View style={s.container}>
        <Ripple
          onPress={this._goToDetailView}
          style={s.avatarOuter}
        >
          <TouchableOpacity style={s.avatarOuter} activeOpacity={0.9}>
            <View style={s.avatarInner}>
              <Avatar
                textStyle={s.avatarText}
                name={this._getContactName(data)}
                avatar={data.thumbnailPath || avatarImage}
              />
            </View>
            </TouchableOpacity>
        </Ripple>
        <Ripple
          onPress={this._goToDetailView}
          style={s.body}
        >
          <TouchableOpacity
            style={[
              s.contactName,
              this._getContactName(data).length > 25 ? {
                width: 150,
              } : {}
            ]}
            activeOpacity={0.9}
          >
            <Text style={s.name}>{this._getContactName(data)}</Text>
          </TouchableOpacity>
        </Ripple>
        { (data.is_msgsafe_user && !isMyIdentity) &&
          (
            <View style={s.icons}>
              <TouchableOpacity onPress={this._audioCall}>
                <FontAwesomeIcon name='phone' style={s.videoIcon} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this._chat}>
                <FontAwesomeIcon name='comment' style={s.audioIcon} />
              </TouchableOpacity>
            </View>
          )
        }

      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  let isMyIdentity = false

  const { data } = ownProps
  const identities = state.identity.data
  if (data && Array.isArray(data.emailAddresses)) {
    const otherEmails = data
      .emailAddresses
      .map(e => e.email)
      .filter(email => {
        if (typeof identities === 'object') {
          for (let id in identities) {
            // email is my identity
            if (identities[id] && identities[id].email === email) {
              return false
            }
          }
        }
        return true
      })
    if (otherEmails.length === 0) {
      isMyIdentity = true
    }
  }

  return {
    identities: state.identity.data,
    isMyIdentity,
    avatarImage: state.avatar.emails[ownProps.data.email],
    roomsMap: state.chat.roomsMap,
    rooms: state.chat.data,
    socketConnected: state.chat.socketConnected
  }
}

const mapDispatchToProps = {
  navigate: NavigationActions.navigate,
  navigateBack: NavigationActions.back,
  videoCallRequest: ChatActions.makeOutboundVideoCallOffer,
  chatCreateRoomRequest: ChatActions.chatCreateRoomRequest,
  chatSetupExistingRoom: ChatActions.chatSetupExistingRoom,
  displayNotification: NotificationActions.displayNotification
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(DeviceContactListItem))
