import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { connect } from 'react-redux'
import { View, TouchableOpacity } from 'react-native'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import { NavigationActions } from 'react-navigation'
import Ripple from 'react-native-material-ripple'
import { injectIntl } from 'react-intl'

import ChatActions from 'commons/Redux/ChatRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'

import Avatar from 'app/Components/Avatar'
import Text from 'app/Components/BaseText'
import { showIdentitySelectAndCallback } from 'app/Lib/Identity'
import { checkWebRTCPermissions, showNoWebRTCPermissionModal } from 'app/Lib/Device'

import { ListItem as s } from './styles'

class ContactListItem extends PureComponent {

  static propTypes = {
    data: PropTypes.object.isRequired,
    avatarImage: PropTypes.string,
  }

  constructor (props) {
    super(props)

    this._goToDetailView = this._goToDetailView.bind(this)

    this._chatWithIdentity = this._chatWithIdentity.bind(this)
    this._chat = this._chat.bind(this)

    this._audioCallWithIdentity = this._audioCallWithIdentity.bind(this)
    this._audioCall = this._audioCall.bind(this)
  }

  _selectIdentity (callback) {
    const { data, myIdentities, intl } = this.props
    if (!data.identities) return null
    showIdentitySelectAndCallback(data.identities, callback, intl.formatMessage, myIdentities)
  }

  _audioCallWithIdentity (identity) {
    if (!this.props.socketConnected) {
      return
    }
    const { data } = this.props
    if (!data) return

    let audioOnly = true
    this.props.videoCallRequest(identity.email, data.email, data.display_name, audioOnly)
  }

  _audioCall () {
    const { identities } = this.props.data

    checkWebRTCPermissions().then(r => {
      if (!this.props.socketConnected) return

      if (!r) {
        showNoWebRTCPermissionModal()
        return
      }

      if (identities.length === 1) {
        this._audioCallWithIdentity(identities[0])
      } else {
        this._selectIdentity(this._audioCallWithIdentity)
      }
    })
  }

  _chatWithIdentity (identity) {
    if (this.props.isMyIdentity) {
      return
    }

    if (!this.props.socketConnected) {
      return
    }
    const { data, navigate, navigateBack, roomsMap, chatCreateRoomRequest, displayNotification, chatSetupExistingRoom, rooms } = this.props
    const roomsMapKey = `${identity.email}__${data.email}`

    const roomId = roomsMap[roomsMapKey]
    // Room already exists
    if (roomId) {
      chatSetupExistingRoom(roomId, identity.email, identity.display_name, data.email)
    } else {
      chatCreateRoomRequest(identity.email, identity.display_name, data.email, false)
    }
    navigate({ routeName: 'MessagingRoom', params: { roomsMapKey } })
  }

  _chat () {
    if (this.props.isMyIdentity) {
      return
    }
    if (!this.props.socketConnected) {
      return
    }
    const { identities } = this.props.data
    if (identities.length === 1) {
      this._chatWithIdentity(identities[0])
    } else {
      this._selectIdentity(this._chatWithIdentity)
    }
  }

  _goToDetailView () {
    const { data, navigate } = this.props
    // Do nothing if the data is incomplete
    if (!data.created_on) {
      navigate({ routeName: 'DeviceContactDetail', params: { id: data.id } })
    } else {
      navigate({ routeName: 'ContactDetail', params: { id: data.email } })
    }
  }

  render() {
    const { data, avatarImage, isMyIdentity } = this.props
    return (
      <View style={s.container}>
        <Ripple
          onPress={this._goToDetailView}
          style={s.avatarOuter}
        >
          <TouchableOpacity style={s.avatarOuter} activeOpacity={0.9}>
            <View style={s.avatarInner}>
              <Avatar
                textStyle={s.avatarText}
                name={data.display_name || data.email}
                avatar={data.thumbnailPath || avatarImage}
              />
            </View>
          </TouchableOpacity>
        </Ripple>
        <Ripple
          onPress={this._goToDetailView}
          style={s.body}
        >
          <TouchableOpacity style={s.body} activeOpacity={0.9}>
            <Text style={s.name}>{data.display_name.trim() || data.email}</Text>
            <Text style={s.email}>{data.email}</Text>
          </TouchableOpacity>
        </Ripple>
        { (data.is_msgsafe_user && !isMyIdentity) &&
          (
            <View style={s.icons}>
              <TouchableOpacity onPress={this._audioCall}>
                <FontAwesomeIcon name='phone' style={s.videoIcon} />
              </TouchableOpacity>
              <TouchableOpacity onPress={this._chat}>
                <FontAwesomeIcon name='comment' style={s.audioIcon} />
              </TouchableOpacity>
            </View>
          )
        }

      </View>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  // Check if it is my email
  let isMyIdentity = false

  let identities = {}
  const { data } = ownProps
  if (data) {
    identities = state.identity.data
    if (typeof identities === 'object') {
      for (let id in identities) {
        if (identities[id] && identities[id].email === data.email) {
          isMyIdentity = true
          break
        }
      }
    }
  }

  return {
    isMyIdentity,
    myIdentities: identities,
    avatarImage: state.avatar.emails[ownProps.data.email],
    roomsMap: state.chat.roomsMap,
    rooms: state.chat.data,
    socketConnected: state.chat.socketConnected
  }
}

const mapDispatchToProps = {
  navigate: NavigationActions.navigate,
  navigateBack: NavigationActions.back,
  videoCallRequest: ChatActions.makeOutboundVideoCallOffer,
  chatCreateRoomRequest: ChatActions.chatCreateRoomRequest,
  chatSetupExistingRoom: ChatActions.chatSetupExistingRoom,
  displayNotification: NotificationActions.displayNotification
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(ContactListItem))
