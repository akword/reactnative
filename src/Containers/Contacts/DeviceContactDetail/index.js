import React, { Component } from 'react'
import { injectIntl } from 'react-intl'
import { View, ScrollView } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { connect } from 'react-redux'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'

import m from 'commons/I18n'
import { getDataItemForId } from 'commons/Redux/_Utils'
import ChatActions from 'commons/Redux/ChatRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'

import Text from 'app/Components/BaseText'
import {Header, Button, ButtonGroup} from 'app/Components/DetailView'
import HeaderButton from 'app/Components/HeaderButton'
import palette from 'app/Styles/colors'
import { checkWebRTCPermissions, showNoWebRTCPermissionModal } from 'app/Lib/Device'

import { pickDeviceContactEmailAddress } from './utils'

const s = EStyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f9f9f9'
  },

  infoContainer: {
    backgroundColor: 'white',
    padding: '0.8rem',
    borderBottomWidth: 1,
    borderBottomColor: palette.clouds,
  },

  infoItem: {
    marginBottom: '0.9rem'
  },

  infoTitle: {
    color: palette.asbestos
  },

  infoValue: {
    marginTop: '0.3rem'
  },

})

class DeviceContactDetail extends Component {
  constructor (props) {
    super(props)

    this._chatWithIdentity = this._chatWithIdentity.bind(this)
    this._chat = this._chat.bind(this)

    this._videoCallWithIdentity = this._videoCallWithIdentity.bind(this)
    this._videoCall = this._videoCall.bind(this)

    this._audioCallWithIdentity = this._audioCallWithIdentity.bind(this)
    this._audioCall = this._audioCall.bind(this)

    this._composeEmail = this._composeEmail.bind(this)

    this.state = {
      selectedIdentityIdForChat: null
    }
  }

  _selectIdentity (callback) {
    this.props.navigation.navigate('IdentitySelection', {
      disableSwipe: true,
      selectItemAndPop: callback
    })
  }

  _audioCallWithIdentity (contactEmail, identity) {
    const { data } = this.props
    if (!data) return

    let audioOnly = true
    this.props.videoCallRequest(identity.email, contactEmail, data.display_name, audioOnly)
  }

  _audioCall () {
    const { data } = this.props

    checkWebRTCPermissions().then(r => {
      if (r) {
        pickDeviceContactEmailAddress(data, contactEmail => this._selectIdentity(identity => this._audioCallWithIdentity(contactEmail, identity)))
      } else {
        showNoWebRTCPermissionModal()
      }
    })
  }

  _videoCallWithIdentity (contactEmail, identity) {
    const { data } = this.props
    if (!data) return

    let audioOnly = false
    this.props.videoCallRequest(identity.email, contactEmail, data.display_name, audioOnly)
  }

  _videoCall () {
    const { data } = this.props

    checkWebRTCPermissions().then(r => {
      if (r) {
        pickDeviceContactEmailAddress(data, contactEmail => this._selectIdentity(identity => this._videoCallWithIdentity(contactEmail, identity)))
      } else {
        showNoWebRTCPermissionModal()
      }
    })
  }

  _chatWithIdentity (contactEmail, identity) {
    const { navigation, roomsMap, chatCreateRoomRequest, chatSetupExistingRoom } = this.props

    const roomsMapKey = `${identity.email}__${contactEmail}`

    const roomId = roomsMap[roomsMapKey]
    // Room already exists
    if (roomId) {
      chatSetupExistingRoom(roomId, identity.email, identity.display_name, contactEmail)
    } else {
      chatCreateRoomRequest(identity.email, identity.display_name, contactEmail, false)
    }
    navigation.goBack()
    navigation.navigate('MessagingRoom', { roomsMapKey })
  }

  _chat () {
    const { data } = this.props
    pickDeviceContactEmailAddress(data, contactEmail => this._selectIdentity(identity => this._chatWithIdentity(contactEmail, identity)))
  }

  _composeEmail () {
    const { navigation, data } = this.props
    pickDeviceContactEmailAddress(data, email => navigation.navigate('MailboxCompose', { recipientEmail: email }))
  }

  _renderPhoneNumbers () {
    const { data } = this.props

    if (!data.phoneNumbers.length) return

    return (
      <View style={s.infoContainer}>
        {data.phoneNumbers.map(n => (
          <View style={s.infoItem} key={`${n.number}${n.label}`}>
            <Text style={s.infoTitle}>{n.label}</Text>
            <Text style={s.infoValue}>{n.number}</Text>
          </View>
        ))}
      </View>
    )
  }

  _renderEmailAddresses () {
    const { data } = this.props

    if (!data.emailAddresses.length) return

    return (
      <View style={s.infoContainer}>
        {data.emailAddresses.map(e => (
          <View style={s.infoItem} key={e.email}>
            <Text style={s.infoTitle}>{e.label}</Text>
            <Text style={s.infoValue}>{e.email}</Text>
          </View>
        ))}
      </View>
    )
  }

  _renderPostalAddresses () {
    const { data } = this.props

    if (!data.postalAddresses.length) return

    return (
      <View style={s.infoContainer}>
        {data.postalAddresses.map(a => (
          <View style={s.infoItem} key={a.label}>
            <Text style={s.infoTitle}>{a.label}</Text>
            <Text style={s.infoValue}>{a.street}{'\n'}{a.city} {a.state} {a.postCode}</Text>
          </View>
        ))}
      </View>
    )
  }

  render () {
    const { data, navigation, intl, avatarImage, isMyIdentity } = this.props
    const fm = intl.formatMessage

    return (
      <ScrollView style={s.main}>
        <Header name={data.display_name} email={data.email} avatar={data.thumbnailPath || avatarImage} />

        { !isMyIdentity &&
          <ButtonGroup>
            <Button
              text={fm(m.app.Common.email)}
              iconComponent={EntypoIcon}
              iconName='mail'
              onPress={this._composeEmail}
            />

            { data.is_msgsafe_user &&
              <Button
                text={fm(m.native.Chat.call)}
                iconComponent={FontAwesomeIcon}
                iconName='phone'
                onPress={() => this._audioCall() }
              />
            }

            { data.is_msgsafe_user &&
              <Button
                text={fm(m.native.Chat.title)}
                iconComponent={FontAwesomeIcon}
                iconName='video-camera'
                onPress={() => this._videoCall()}
              />
            }

            { data.is_msgsafe_user &&
              <Button
                text={fm(m.native.Chat.chat)}
                iconComponent={FontAwesomeIcon}
                iconName='comment'
                onPress={this._chat}
              />
            }
          </ButtonGroup>
        }

        { !!data.company &&
          <View style={s.infoContainer}>
            <Text style={s.infoTitle}>{fm(m.native.Contact.organization)}</Text>
            <Text style={s.infoValue}>{data.company}</Text>
          </View>
        }

        {this._renderPhoneNumbers()}
        {this._renderEmailAddresses()}
        {this._renderPostalAddresses()}
      </ScrollView>
    )
  }
}

const IntlDeviceContactDetail = injectIntl(DeviceContactDetail)
IntlDeviceContactDetail.navigationOptions = ({ navigation, screenProps }) => ({
  tabBarVisible: false,
  title: screenProps.fm(m.native.Contact.deviceContact),
  headerLeft: <HeaderButton isBack onPress={() => navigation.goBack()} />
})

const mapStateToProps = (state, ownProps) => {
  const data = getDataItemForId(state.deviceContact, ownProps.navigation.state.params.id)

  let isMyIdentity = false

  const identities = state.identity.data
  if (data && Array.isArray(data.emailAddresses)) {
    const otherEmails = data
      .emailAddresses
      .map(e => e.email)
      .filter(email => {
        if (typeof identities === 'object') {
          for (let id in identities) {
            // email is my identity
            if (identities[id] && identities[id].email === email) {
              return false
            }
          }
        }
        return true
      })
     //data.emailAddresses.length > 0 &&
    if (otherEmails.length === 0) {
      isMyIdentity = true
    }
  }

  return {
    avatarImage: state.avatar.emails[ownProps.navigation.state.params.id],
    email: ownProps.navigation.state.params.id,
    data,
    roomsMap: state.chat.roomsMap,
    rooms: state.chat.data,
    isMyIdentity
  }
}

const mapDispatchToProps = {
  videoCallRequest: ChatActions.makeOutboundVideoCallOffer,
  chatCreateRoomRequest: ChatActions.chatCreateRoomRequest,
  chatSetupExistingRoom: ChatActions.chatSetupExistingRoom,
  displayNotification: NotificationActions.displayNotification
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlDeviceContactDetail)
