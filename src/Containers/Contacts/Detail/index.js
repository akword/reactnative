import React, { Component } from 'react'
import { injectIntl } from 'react-intl'
import { View, ScrollView, ActivityIndicator, Alert } from 'react-native'
import * as R from 'ramda'
import EStyleSheet from 'react-native-extended-stylesheet'
import moment from 'moment'
import { connect } from 'react-redux'
import EntypoIcon from 'react-native-vector-icons/Entypo'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'

import m from 'commons/I18n'
import { getDataItemForId } from 'commons/Redux/_Utils'
import ChatActions from 'commons/Redux/ChatRedux'
import ContactActions from 'commons/Redux/ContactRedux'

import { showIdentitySelectAndCallback } from 'app/Lib/Identity'
import { checkWebRTCPermissions, showNoWebRTCPermissionModal } from 'app/Lib/Device'
import SimpleTiles from 'app/Components/SimpleTiles'
import {Header, Button, ButtonGroup} from 'app/Components/DetailView'
import HeaderButton from 'app/Components/HeaderButton'
import palette from 'app/Styles/colors'

import Encryption from './components/Encryption'

const styles = EStyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#f9f9f9'
  },

  buttonWidth: {
    maxWidth: '25%'
  },

  spinnerContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})

class ContactDetail extends Component {
  constructor (props) {
    super(props)

    this._ensureVerboseContactData = this._ensureVerboseContactData.bind(this)

    this._getEmailsCountData = this._getEmailsCountData.bind(this)
    this._getHistoryData = this._getHistoryData.bind(this)

    this._chatWithIdentity = this._chatWithIdentity.bind(this)
    this._chat = this._chat.bind(this)

    this._videoCallWithIdentity = this._videoCallWithIdentity.bind(this)
    this._videoCall = this._videoCall.bind(this)

    this._audioCallWithIdentity = this._audioCallWithIdentity.bind(this)
    this._audioCall = this._audioCall.bind(this)

    this.state = {
      selectedIdentityIdForChat: null
    }
  }

  _ensureVerboseContactData (nextProps) {
    const props = nextProps || this.props
    const { email, getContact, getContactApi, data, navigation, intl } = props
    const fm = intl.formatMessage
    if (getContactApi.inProgress) {
      return
    }
    if (data && data.contacts) {
      return
    }
    if (!(R.compose(R.contains(`"${email}"`), R.pathOr('', ['data', 'search']))(getContactApi))) {
      return getContact({ search: { contact_email: email }, limit: 1 })
    }

    if (
      R.path(['state', 'params', 'createIfNotExisting'], navigation)
    ) {
      Alert.alert(
        fm(m.native.Contact.notExistingContact),
        fm(m.native.Contact.addToYourContactList),
        [
          {text: fm(m.app.Common.later), onPress: () => { navigation.setParams({
              createIfNotExisting: null
            })
              navigation.goBack() }, style: 'cancel'},
          {text: fm(m.app.Common.yes), onPress: () => this._createContact()},
        ],
        { cancelable: false }
      )
    }
  }

  _createContact () {
    const createParams = R.path(['navigation', 'state', 'params', 'createIfNotExisting'], this.props)

    this.props.createContact(createParams)
  }

  _selectIdentity (callback) {
    const { data, intl } = this.props
    if (!data.identities) return null
    showIdentitySelectAndCallback(data.identities, callback, intl.formatMessage)
  }

  _audioCallWithIdentity (identity) {
    const { data } = this.props
    if (!data) return

    let audioOnly = true
    this.props.videoCallRequest(identity.email, data.email, data.display_name, audioOnly)
  }

  _audioCall () {
    const { identities } = this.props.data

    checkWebRTCPermissions().then(r => {
      if (!r) {
        showNoWebRTCPermissionModal()
        return
      }

      if (identities.length === 1) {
        this._audioCallWithIdentity(identities[0])
      } else {
        this._selectIdentity(this._audioCallWithIdentity)
      }
    })
  }

  _videoCallWithIdentity (identity) {
    const { data } = this.props
    if (!data) return

    let audioOnly = false
    this.props.videoCallRequest(identity.email, data.email, data.display_name, audioOnly)
  }

  _videoCall () {
    const { identities } = this.props.data

    checkWebRTCPermissions().then(r => {
      if (!r) {
        showNoWebRTCPermissionModal()
        return
      }

      if (identities.length === 1) {
        this._videoCallWithIdentity(identities[0])
      } else {
        this._selectIdentity(this._videoCallWithIdentity)
      }
    })
  }

  _chatWithIdentity (identity) {
    const { data, navigation, roomsMap, chatCreateRoomRequest, chatSetupExistingRoom } = this.props
    const roomsMapKey = `${identity.email}__${data.email}`

    const roomId = roomsMap[roomsMapKey]
    // Room already exists
    if (roomId) {
      chatSetupExistingRoom(roomId, identity.email, identity.display_name, data.email)
    } else {
      chatCreateRoomRequest(identity.email, identity.display_name, data.email, false)
    }
    if (navigation.state.params.createIfNotExisting) {
      navigation.goBack();  
    } else {
      navigation.navigate('MessagingRoom', { roomsMapKey })
    }
  }

  _chat () {
    const { identities } = this.props.data
    if (identities.length === 1) {
      this._chatWithIdentity(identities[0])
    } else {
      this._selectIdentity(this._chatWithIdentity)
    }
  }

  _getHistoryData () {
    const { data, intl } = this.props
    const fm = intl.formatMessage
    return [
      { title: fm(m.app.Common.lastActivity), description: `${moment.utc(data.last_activity_on).fromNow()}` },
      { title: fm(m.app.Common.modified), description: `${moment.utc(data.modified_on).fromNow()}` },
      { title: fm(m.app.Common.created), description: `${moment.utc(data.created_on).fromNow()}` },
    ]
  }

  _getEmailsCountData () {
    const { data, intl } = this.props
    const fm = intl.formatMessage
    return [
      {
        title: fm(m.app.Common.received),
        description: data.lt_mail_recv,
        // footerButtonText: 'View All'
      },
      {
        title: fm(m.app.Common.sent),
        description: data.lt_mail_sent,
        // footerButtonText: 'View All'
      },
      {
        title: fm(m.app.Common.blocked),
        description: data.lt_mail_recv_blocked,
        // footerButtonText: 'View All'
      },
    ]
  }

  componentDidMount () {
    this._ensureVerboseContactData()
  }

  componentWillReceiveProps (nextProps) {
    this._ensureVerboseContactData(nextProps)
  }

  render () {
    const { data, navigation, intl, avatarImage, isMyIdentity } = this.props
    const fm = intl.formatMessage

    if (!data) {
      return (
        <View style={styles.spinnerContainer}>
          <ActivityIndicator animating />
        </View>
      )
    }

    const canCall = data && data.is_msgsafe_user && !isMyIdentity

    return (
      <ScrollView style={styles.main}>
        <Header name={data.display_name} email={data.email} avatar={avatarImage} />

        <ButtonGroup>
          <Button
            text={fm(m.app.Common.email)}
            iconComponent={EntypoIcon}
            iconName='mail'
            style={styles.buttonWidth}
            onPress={() => navigation.navigate('MailboxCompose', { recipientEmail: data.email })}
          />

          { canCall &&
            <Button
              text={fm(m.native.Chat.call)}
              iconComponent={FontAwesomeIcon}
              iconName='phone'
              style={styles.buttonWidth}
              onPress={() => this._audioCall() }
            />
          }

          { canCall &&
            <Button
              text={fm(m.native.Chat.title)}
              iconComponent={FontAwesomeIcon}
              iconName='video-camera'
              style={styles.buttonWidth}
              onPress={() => this._videoCall()}
            />
          }

          { canCall &&
            <Button
              text={fm(m.native.Chat.chat)}
              iconComponent={FontAwesomeIcon}
              iconName='comment'
              style={styles.buttonWidth}
              onPress={this._chat}
            />
          }
        </ButtonGroup>

        <SimpleTiles
          title={fm(m.native.Chat.history).toUpperCase()}
          titleIconComponent={FontAwesomeIcon}
          titleIconName='history'
          tilesData={this._getHistoryData()}
        />

        <SimpleTiles
          title={fm(m.native.Chat.emails).toUpperCase()}
          titleIconComponent={FontAwesomeIcon}
          titleIconName='envelope'
          tilesData={this._getEmailsCountData()}
        />
        { !!data.notes &&
          <SimpleTiles
            title={fm(m.native.Chat.notes).toUpperCase()}
            titleIconComponent={FontAwesomeIcon}
            titleIconName='file-text-o'
            tilesData={[ {title: null, description: data.notes} ]}
          />
        }
        <Encryption {...this.props} />
      </ScrollView>
    )
  }
}

const IntlContactDetail = injectIntl(ContactDetail)
IntlContactDetail.navigationOptions = ({ navigation, screenProps }) => {
  const data = {
    tabBarVisible: false,
    title: screenProps.fm(m.native.Contact.contact),
    headerLeft: <HeaderButton isBack onPress={() => navigation.goBack()} />
  }

  const hideEditButton = R.path(['state', 'params', 'hideEditButton'], navigation)
  if (!hideEditButton) {
    data.headerRight = (
      <HeaderButton
        title={screenProps.fm(m.app.Common.edit)}
        onPress={() => navigation.navigate('EditContact', { id: navigation.state.params.id })}
        color={palette.link}
      />
    )
  }

  return data
}

const mapStateToProps = (state, ownProps) => {
  const email = R.path(['navigation', 'state', 'params', 'id'], ownProps)

  return {
    email,
    isMyIdentity: R.find(R.propEq('email', email))(R.values(state.identity.data)),
    avatarImage: R.path(['avatar', 'emails', email], state),
    data: R.pathOr(getDataItemForId(state.contact, email), ['contact', 'cache', email], state),
    roomsMap: state.chat.roomsMap,
    rooms: state.chat.data,
    getContactApi: state.contact.api.getContactUnique,
  }
}

const mapDispatchToProps = {
  getContact: ContactActions.getContactUniqueRequest,
  createContact: ContactActions.contactCreate,
  videoCallRequest: ChatActions.makeOutboundVideoCallOffer,
  chatCreateRoomRequest: ChatActions.chatCreateRoomRequest,
  chatSetupExistingRoom: ChatActions.chatSetupExistingRoom
}

export default connect(mapStateToProps, mapDispatchToProps)(IntlContactDetail)
