import { takeLatest } from 'redux-saga/effects'

import { AppTypes } from 'commons/Redux/AppRedux'
import { StartupTypes } from 'commons/Redux/StartupRedux'
import { UserTypes } from 'commons/Redux/UserRedux'
import { PushTypes } from 'commons/Redux/PushRedux'
import { ChatTypes } from 'commons/Redux/ChatRedux'
import { MailboxTypes } from 'commons/Redux/MailboxRedux'
import { IdentityTypes } from 'commons/Redux/IdentityRedux'
import { ContactTypes } from 'commons/Redux/ContactRedux'
import { UserEmailTypes } from 'commons/Redux/UserEmailRedux'
import { refreshProfileRequest } from 'commons/Sagas/UserSagas'
import { DeviceContactTypes } from '../Redux/DeviceContactRedux'
import { FileTypes } from '../Redux/FileRedux'

import * as userSagas from './UserSagas'
import { pushInit } from './PushSagas'
import { sendWelcomeLetters } from './IdentitySagas'

import {
  chatInit,
  inboundVideoCallOfferReceived,
  endVideoCall,
  outboundVideoCallAnswerReceived,
  makeOutboundVideoCallOffer,
  announceVideoCallAnswered,
  onChatMessageReceived,
  chatInitFromCallForAndroid,
  setupCallKit,
  ensureChatData,
  videoChatRingerDaemon,
  coldLaunchStateWatcher
} from './ChatSagas'
import { startup, startupSuccess } from './StartupSagas'
import { mailboxTotalsUpdate, userTotalsUpdate } from './CounterSagas.js'
import { applyLocale, applyLocaleWithUserData, onBoardingComplete, checkNetworkState } from './AppSagas'
import {
  sendMailError,
  multiselectNotifications,
  ensureFreshInbox,
  ensureWelcomeEmailsAutoFetch
} from './MailboxSagas'
import { contactFetchFromCache, removeContactFromCache } from './LocalDBSagas/ContactsCache'
import { clearFiltersSaga } from './FiltersSagas'
import { initKeyboard } from './KeyboardSagas'
import { shareFile } from './FileSagas'
import * as deviceContactSagas from './DeviceContactSagas'

export default () => {
  return [
    takeLatest(AppTypes.APPLY_LOCALE, applyLocale),
    takeLatest([UserTypes.UPDATE_USER, UserTypes.UPDATE_ACCOUNT_SUCCESS], applyLocaleWithUserData),
    takeLatest(AppTypes.ON_BOARDING_COMPLETE, onBoardingComplete),
    takeLatest(AppTypes.CHECK_NETWORK_STATE, checkNetworkState),

    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(StartupTypes.STARTUP, initKeyboard),
    takeLatest(StartupTypes.STARTUP, ensureFreshInbox),
    takeLatest(StartupTypes.STARTUP, videoChatRingerDaemon),
    takeLatest(StartupTypes.STARTUP_SUCCESS, startupSuccess),

    takeLatest(UserTypes.UPDATE_USER, setupCallKit),
    takeLatest(UserTypes.LOGIN_SUCCESS, userSagas.loginSuccess),
    takeLatest(UserTypes.LOGIN_SUCCESS, ensureChatData),
    takeLatest(UserTypes.LOGIN_ERROR, userSagas.loginError),
    takeLatest(UserTypes.LOGOUT, userSagas.logout),

    takeLatest(UserTypes.SIGNUP_SUCCESS, userSagas.signupSuccess),
    takeLatest(UserTypes.SIGNUP_SUCCESS, ensureChatData),

    takeLatest(PushTypes.PUSH_INIT, pushInit),
    takeLatest(ChatTypes.CHAT_INIT, chatInit),
    takeLatest(ChatTypes.INBOUND_VIDEO_CALL_OFFER_RECEIVED, inboundVideoCallOfferReceived),
    takeLatest(ChatTypes.OUTBOUND_VIDEO_CALL_ANSWER_RECEIVED, outboundVideoCallAnswerReceived),
    takeLatest(ChatTypes.END_VIDEO_CALL, endVideoCall),
    takeLatest(ChatTypes.MAKE_OUTBOUND_VIDEO_CALL_OFFER, makeOutboundVideoCallOffer),
    takeLatest(ChatTypes.ANNOUNCE_VIDEO_CALL_ANSWERED, announceVideoCallAnswered),
    takeLatest(ChatTypes.CHAT_MESSAGE_RECEIVED, onChatMessageReceived),
    takeLatest(ChatTypes.INBOUND_VIDEO_CALL_ID_RECEIVED, chatInitFromCallForAndroid),
    takeLatest(ChatTypes.INBOUND_VIDEO_CALL_ID_RECEIVED, coldLaunchStateWatcher),

    // Send identity welcome letters on create
    takeLatest([IdentityTypes.IDENTITY_CREATE_SUCCESS], sendWelcomeLetters),

    takeLatest(MailboxTypes.SEND_MAIL_ERROR, sendMailError),
    takeLatest(MailboxTypes.MAILBOX_AUTO_UPDATE_REQUEST, ensureWelcomeEmailsAutoFetch),

    // Update user.data.totals
    takeLatest([
      MailboxTypes.MAILBOX_SUCCESS,
      MailboxTypes.SEND_MAIL_SUCCESS,
      MailboxTypes.SEND_QUEUED_MAIL_SUCCESS,
      MailboxTypes.MAILBOX_READ_SUCCESS,
      MailboxTypes.MAILBOX_UNREAD_SUCCESS,
      MailboxTypes.MAILBOX_ARCHIVE_SUCCESS,
      MailboxTypes.MAILBOX_TRASH_SUCCESS,
      MailboxTypes.MAILBOX_DELETE_SUCCESS,
      MailboxTypes.MAILBOX_CLEAR_TRASH_SUCCESS,
      MailboxTypes.MAILBOX_SEND_QUEUED_SUCCESS
    ], mailboxTotalsUpdate),

    takeLatest([
      MailboxTypes.MAILBOX_SUCCESS,
      MailboxTypes.SEND_MAIL_SUCCESS,
      MailboxTypes.SEND_QUEUED_MAIL_SUCCESS,
      MailboxTypes.MAILBOX_DELETE_SUCCESS,
      MailboxTypes.MAILBOX_CLEAR_TRASH_SUCCESS,
      MailboxTypes.MAILBOX_SEND_QUEUED_SUCCESS,
      IdentityTypes.IDENTITY_CREATE_SUCCESS,
      IdentityTypes.IDENTITY_REMOVE_SUCCESS,
      ContactTypes.CONTACT_CREATE_SUCCESS,
      ContactTypes.CONTACT_REMOVE_SUCCESS,
      UserEmailTypes.USEREMAIL_CREATE_SUCCESS,
      UserEmailTypes.USEREMAIL_REMOVE_SUCCESS,
    ], userTotalsUpdate),

    // multiselect success notifications
    takeLatest(
      MailboxTypes.MAILBOX_TRASH_SELECTED_SUCCESS,
      multiselectNotifications,
      'trash',
      'danger',
    ),
    takeLatest(
      MailboxTypes.MAILBOX_ARCHIVE_SELECTED_SUCCESS,
      multiselectNotifications,
      'archived',
      'info',
    ),
    takeLatest(
      MailboxTypes.MAILBOX_READ_SELECTED_SUCCESS,
      multiselectNotifications,
      'read',
      'info',
    ),
    takeLatest(
      MailboxTypes.MAILBOX_UNREAD_SELECTED_SUCCESS,
      multiselectNotifications,
      'unread',
      'info',
    ),
    takeLatest(
      MailboxTypes.MAILBOX_DELETE_SELECTED_SUCCESS,
      multiselectNotifications,
      'deleted',
      'danger',
    ),

    takeLatest(ContactTypes.CONTACT_FETCH_FROM_CACHE, contactFetchFromCache),
    takeLatest(ContactTypes.CONTACT_REMOVE_SUCCESS, removeContactFromCache),

    // Account current totals
    takeLatest([
      IdentityTypes.IDENTITY_CREATE_SUCCESS,
      IdentityTypes.IDENTITY_REMOVE_SUCCESS,
      ContactTypes.CONTACT_CREATE_SUCCESS,
      ContactTypes.CONTACT_REMOVE_SUCCESS,
      UserEmailTypes.USEREMAIL_CREATE_SUCCESS,
      UserEmailTypes.USEREMAIL_REMOVE_SUCCESS,
    ], refreshProfileRequest),

    // clear the filter texts when user navigates
    // out of the list screens
    // takeLatest([NavigationTypes.NAVIGATE, NavigationTypes.BACK], clearFiltersSaga),

    takeLatest(FileTypes.SHARE_FILE, shareFile),

    takeLatest([
      DeviceContactTypes.DEVICE_CONTACT_FETCH,
      DeviceContactTypes.DEVICE_CONTACT_SET_SEARCH_QUERY
    ], deviceContactSagas.fetch)
  ]
}
