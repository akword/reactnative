import { put, select, call } from 'redux-saga/effects'

import ContactActions from 'commons/Redux/ContactRedux'

export function * clearFiltersSaga ({ routeName }) {
  const searchQuery = yield select(s => s.contact.searchQuery)
  if (
    (routeName !== 'ContactSelection' || routeName !== 'Contact') &&
    searchQuery
  ) {
    yield put(ContactActions.contactClearSearchData())
  }
}
