import { delay } from 'redux-saga'
import { Platform } from 'react-native'
import { put, call, select, take, actionChannel, spawn, fork } from 'redux-saga/effects'
import { NavigationActions } from 'react-navigation'
import * as Keychain from 'react-native-keychain'
import RNContacts from 'react-native-contacts'
import OpenSettings from 'react-native-open-settings'
import { path } from 'ramda'
import { isEmail } from 'validator'

import log from 'commons/Services/Log'
import AppActions, { AppTypes } from 'commons/Redux/AppRedux'
import IdentityActions from 'commons/Redux/IdentityRedux'
import ContactActions from 'commons/Redux/ContactRedux'
import ChatActions from 'commons/Redux/ChatRedux'
import UserActions from 'commons/Redux/UserRedux'
import { isLoggedIn } from 'commons/Selectors/User'
import { callAPI } from 'commons/Sagas/APISagas'
import {bugsnag} from '../Services/BugSnag'

import OverlayActions, { OverlayIdentifiers } from 'app/Redux/OverlayRedux'

import { resetContactCache } from './LocalDBSagas/ContactsCache'

/**
 * Refreshes user by calling /users endpoint
 * and updating it in redux store
 */
export function * refreshUser () {
  try {
    const user = yield select(s => s.user)
    if (!isLoggedIn(user)) return
    const response = yield callAPI('UserProfile')
    bugsnag.setUser(response.data.username, response.data.display_name, response.data.default_esp && response.data.default_esp)
    yield put(UserActions.updateUser(response.data))
    return response.data
  } catch (e) {
    if (e.status === 404) {
      yield put(UserActions.logout())
    } else {
      yield put(AppActions.setNativeAppNetworkState(false));
    }
  }
}

const resetKeychain = function () {
  return Keychain.resetGenericPassword()
    .then(() => log.debug('Keychain: resetGenericPassword complete'))
    .catch(e => log.debug('Keychain: resetGenericPassword error - ', e))
}

const setKeychain = function (username, data) {
  if (!username || !data) return

  if (typeof data === 'object') {
    data = JSON.stringify(data)
  }

  return Keychain.setGenericPassword(username, data)
    .then(() => log.debug('Keychain: setGenericPassword complete'))
    .catch(e => log.debug('Keychain: setGenericPassword failed err=' + e))
}

export function * checkOnboardingStateAndRedirect (userData) {
  // const espCount = path(['totals', 'num_esp'], userData)
  // if (!espCount) {
  //   yield put(NavigationActions.navigate({ routeName: 'SignupESPImported'}))
  //   return
  // }

  const identityCount = path(['totals', 'num_identity'], userData)
  if (!identityCount) {
    yield put(NavigationActions.navigate({ routeName: 'SignupIdentity'}))
    return
  }

  yield put(NavigationActions.navigate({ routeName: 'UserArea' }))
  yield put(IdentityActions.identityFetch())
  yield put(ContactActions.contactFetch())

  const hasLoggedInOnNative = Platform.OS === 'ios' ? userData.has_logged_in_ios : userData.has_logged_in_android
  if (!hasLoggedInOnNative) {
    yield put(OverlayActions.showOverlay(OverlayIdentifiers.EDUCATION))
  }
}

export function * connectionTimeout ({ timeout }) {
  yield put(AppActions.setConnectionTimeout(false))
  const timeoutFunc = () => (new Promise((resolve) => {
    setTimeout(() => {
      resolve()
    }, timeout)
  }))
  
  yield call(timeoutFunc)
  yield put(AppActions.setConnectionTimeout(true))
}

export function * signupSuccess () {
  const data = yield select(s => s.user.data)
  yield call(setKeychain, data.username, {
    access_id: data.access_id,
    secret_token: data.secret_token,
  })
  bugsnag.setUser(data.username, data.display_name, data.default_esp && data.default_esp)
  yield spawn(connectionTimeout, { timeout: 40000 })
}

export function * loginSuccess ({ data }) {
  yield signupSuccess()
  yield checkOnboardingStateAndRedirect(data)
}

export function * loginError () {
  yield call(resetKeychain)
}

export function * logout () {
  yield call(resetKeychain)
  yield call(resetContactCache)
  bugsnag.setUser('-1', '', '')
  yield put(NavigationActions.navigate({ routeName: 'Launch' }))
  yield delay(1500)
  yield put(NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: 'Auth'})]
  }))
  yield put(AppActions.setConnectionTimeout(false))
}
