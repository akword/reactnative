import { delay } from 'redux-saga'
import { put, take } from 'redux-saga/effects'

import ChatActions, { ChatTypes } from 'commons/Redux/ChatRedux'

const COLD_LAUNCH_STATE_UPDATE_TIMEOUT = 30000

export function * coldLaunchStateWatcher ({ isColdLaunch }) {
  if (!isColdLaunch) {
    return
  }
  yield delay(COLD_LAUNCH_STATE_UPDATE_TIMEOUT)
  yield put(ChatActions.setVideoCallColdLaunchState(false))
}