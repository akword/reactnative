import { delay } from 'redux-saga'
import { select, call, take, race } from 'redux-saga/effects'
import Sound from 'react-native-sound'
import RNCallKit from 'react-native-callkit'

import { ChatTypes } from 'commons/Redux/ChatRedux'
import { WebrtcTypes, ICE_GATHERING_STATE } from 'commons/Redux/WebrtcRedux'
import { createSound } from 'app/Lib/Audio'

const CALLKIT_ICON_FNAME = 'callKitIcon.png'
const RING_SOUND_FNAME = 'ring.mp3'

export function setSpeakerOn () {
  Sound.setMode('VideoChat')
}

export function setSpeakerOff () {
  Sound.setMode('VoiceChat')
}

export function * setupCallKit () {
  yield delay(2000)
  const options = { appName: 'MsgSafe', imageName: CALLKIT_ICON_FNAME }
  const ringtoneSound = yield select(s => s.user.data.video_call_ringtone)
  const { inboundVideoCallOffer, outboundVideoCallData } = yield select(s => s.chat)

  // do not setup callkit while on the call
  // because it probably causes the stale callkit state
  if (inboundVideoCallOffer || outboundVideoCallData) {
    return
  }

  if (ringtoneSound) {
    options.ringtoneSound = ringtoneSound
  }
  RNCallKit.setup(options)
}

export function * videoChatRinger () {
  while (true) {
    const { value } = yield take(WebrtcTypes.UPDATE_LOCAL_ICE_GATHERING_STATE)
    if (value !== ICE_GATHERING_STATE.COMPLETE) {
      continue
    }
    const ringSound = yield createSound(RING_SOUND_FNAME, null, -1, 1.0)
    ringSound.play()
    const stop = yield race({
      videoCallEnded: take(ChatTypes.END_VIDEO_CALL),
      callIsConnected: take(ChatTypes.SET_CALL_IS_CONNECTED_STATE)
    })
    ringSound.stop()
    ringSound.release()
    return
  }
}

export function * videoChatRingerDaemon () {
  while (true) {
    yield take(ChatTypes.MAKE_OUTBOUND_VIDEO_CALL_OFFER)
    yield call(videoChatRinger)
  }
}