import { Platform, Vibration } from 'react-native'
import { delay } from 'redux-saga'
import { put, take, call, select, spawn } from 'redux-saga/effects'
import RNCallKit from 'react-native-callkit'
import { NavigationActions } from 'react-navigation'
import uuid from 'uuid'
import Sound from 'react-native-sound'
import { path } from 'ramda'

import { chatAPI } from 'commons/Sagas/ChatSagas'
import { setupWebRTCForInbound, setupWebRTCForOutbound } from 'commons/Sagas/ChatSagas/WebRTC'
import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import { UserTypes } from 'commons/Redux/UserRedux'
import log from 'commons/Services/Log'
import { getMapKeyForRoom } from 'commons/Selectors/Messaging'
import { uuidv1ToDate } from 'commons/Lib/Utils'

import { OverlayIdentifiers } from 'app/Redux/OverlayRedux'
import { getCurrentRoute, getCurrentRouteName } from '../../Navigation/utils'
import { getNativeUserMedia, getNativeConstraints } from '../../Lib/WebRTC'
import { checkWebRTCPermissions, showNoWebRTCPermissionModal } from '../../Lib/Device'
import { createSound } from '../../Lib/Audio'
import { showPlatformInboundCallUI, createDidActivateAudioSessionChannel, createDidReceiveStartCallActionCallKitChannel } from './Platform'
import { showLocalNotification } from '../PushSagas/Notifications'
import { setSpeakerOn, setSpeakerOff } from './Sound'
import BFService from '../../../rn_modules/BackgroundToForegroundService'

export { setupCallKit, videoChatRingerDaemon } from './Sound'
export { coldLaunchStateWatcher } from './ColdLaunch'

// Note -
// localCallId => only using it for outbound, so that we have a uuid to
// launch CallKit with before the server has the chance to hand us over the real uuid

export function * areNativePermissionsAvailable (media) {
  try {
    const constraints = yield call(getNativeConstraints, media)
    yield call(getNativeUserMedia, constraints)
    return true
  } catch (e) {
    return false
  }
}

export function * chatInit () {
  if (Platform.OS !== 'ios') return

  const channel = yield call(createDidReceiveStartCallActionCallKitChannel)

  while (true) {
    log.debug('waiting for didReceiveStartCallAction...')
    const data = yield take(channel)
    if (!data.handle) continue

    const parsed = data.handle.match(/(Outgoing|Incoming):\s(\S+@\S+\.\S+)\s=>\s(\S+@\S+\.\S+)/)
    // FIXME: magic number
    if (parsed.length !== 4) continue
    if (parsed[1] === 'Incoming') {
      yield put(Actions.makeOutboundVideoCallOffer(parsed[3], parsed[2]))
    } else if (parsed[1] === 'Outgoing') {
      yield put(Actions.makeOutboundVideoCallOffer(parsed[2], parsed[3]))
    } else {
      log.debug('didReceiveStartCallAction: unable to perform any action;  data - ', data)
    }
  }
}

export function * inboundVideoCallOfferReceived ({ data, isColdLaunch }) {
  
  // Register Call id first
  
  log.enableBuffer()
  log.debug('displayIncomingCall data -', data)
  // log.debug('displayIncomingCall for uuid - ', data.call_id)

  if (data.is_expired) {
    log.debug('inboundVideoCallOfferReceived - expired call')
    yield call(cleanupStateAfterCall)
    return
  } else {
    yield put(Actions.setVideoCallLocalId(data.call_id))
  }

  // We should always inspect the callId (timeuuid)
  // If there's a big discrepancy, it could be the local clock,
  // but it'd be worth issuing a webrtc/peek to verify the call isnt expired.
  const callInitiatedMSAgo = (new Date()) - uuidv1ToDate(data.call_id)
  console.log('inboundVideoCallOfferReceived: callInitiatedMSAgo - ', callInitiatedMSAgo)
  if (callInitiatedMSAgo > 75000) {
    log.debug('inboundVideoCallOfferReceived: received stale call...')
    yield call(cleanupStateAfterCall)
    return
  }

  // to extract remote audio settings,
  // we need to parse payload as its a json string for js
  let isRemoteAudioOnly = false
  try {
    const tmpSdpOffer = JSON.parse(data.sdp_offer)
    if (tmpSdpOffer && tmpSdpOffer.is_audio_only !== undefined) {
      isRemoteAudioOnly = tmpSdpOffer.is_audio_only
      yield put(Actions.setRemoteAudioOnly(isRemoteAudioOnly))
      yield put(Actions.setVideoCallCameraMuteState(isRemoteAudioOnly))
    }
  } catch (e) {}

  const isVideo = !isRemoteAudioOnly

  let permission = yield call(checkWebRTCPermissions)
  if (!permission) {
    log.debug('rejecting call, no permissions')
    showNoWebRTCPermissionModal(data.from_email)
    yield put(Actions.rejectVideoCall(true))
    return
  }

  // If it's a cold launch, we release existing stream and
  // let it be re-created
  // Reason: the first stream on cold launch doesn't work for some reason
  // if (isColdLaunch) {
  //   stream.release()
  //   stream = null
  // }
  const response = yield call(showPlatformInboundCallUI, data, isVideo)
  log.debug('response - ', response)
  if (response.timeout) {
    yield call(timeoutCall, {callId: data.call_id})

    // Send webrtc reject w/out calling endCall
    //
    // We should only do this if we are the only device/last device registered to receive offer.
    // yield put(Actions.rejectInboundVideoCallOffer(false))
  } else {
    if (response.answer) {
      yield put(NavigationActions.navigate({ routeName: 'VideoChat' }))
      // yield put(Actions.answerInboundVideoCallOffer())
      //
      if (!chatAPI.ready) {
        yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
      }
      const ldata = yield select(s => s.chat.inboundVideoCallOffer)
      let setupResult = yield call(setupWebRTCForInbound, ldata)
      log.debug('inboundVideoCallOfferReceived: setupResult -', setupResult)
      if (!setupResult) {
        log.debug('inboundVideoCallOfferReceived failed to setup WebRTC. check device permissions')
        yield put(Actions.rejectVideoCall(true))
        return
      }

      yield spawn(listenAndToggleSpeaker)
    } else {
      yield put(Actions.rejectVideoCall(true))
    }
  }
}

export function * makeOutboundVideoCallOffer ({ identityEmail, contactEmail, audioOnly }) {
  log.enableBuffer()

  // Send user to VideoChat and add a delay so that the transition is complete before
  // `WebRTCLib.getUserMedia` blocks the thread, when we call it to create the call stream
  yield put(NavigationActions.navigate({routeName: 'VideoChat'}))
  yield delay(500)

  let permission = yield call(checkWebRTCPermissions)
  if (!permission) {
    log.debug('rejecting call, no permissions')
    yield put(Actions.rejectVideoCall(true))
    return
  }

  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  log.debug('* makeOutboundVideoCallOffer audioOnly -', audioOnly)

  // audioOnly = true
  let localCallId = uuid()
  yield put(Actions.setVideoCallLocalId(localCallId))
  yield put(Actions.setAudioOnly(audioOnly))
  yield put(Actions.setRemoteAudioOnly(audioOnly))
  yield put(Actions.setVideoCallSpeakerMuteState(audioOnly))
  yield put(Actions.setVideoCallCameraMuteState(audioOnly))

  // RNCallKit.setup({ appName: 'MsgSafe' })
  log.debug('startCall - ', localCallId)

  if (Platform.OS === 'ios') {
    const isAudioSetup = yield select(s => s.chat.iOSAudioSessionActivated)

    // todo: pass audioOnly to startCall() so it shows up as Audio call vs Video call
    RNCallKit.startCall(
      localCallId, `Outgoing: ${identityEmail} => ${contactEmail}`, 'generic', audioOnly
    )

    // TODO: should listen audio session deactivated event
    if (!isAudioSetup) {
      // Waiting for didActivateAudioSession event before we initialise WebRTC
      const channel = yield call(createDidActivateAudioSessionChannel)
      yield take(channel)
      yield put(Actions.setAudioSessionActivated(true))
    }
  }

  const setupResult = yield call(setupWebRTCForOutbound, identityEmail, contactEmail, audioOnly)
  log.debug('setupResult -', setupResult)
  if (!setupResult) {
    log.debug(' warning: makeOutboundVideoCallOffer setup failed')
    const localCallId = yield select(s => s.chat.videoCallLocalId)
    yield put(Actions.rejectVideoCall(false))
    yield call(timeoutCall, {callId: localCallId})
    return
  }

  yield spawn(listenAndToggleSpeaker)
}

export function * outboundVideoCallAnswerReceived () {
  const localCallId = yield select(s => s.chat.videoCallLocalId)

  log.debug('outboundVideoCallAnswerReceived: localCallId is ', localCallId)
  if (localCallId) {
    log.debug('outboundVideoCallAnswerReceived: calling reportConnectedOutgoingCallWithUUID - ', localCallId)
    RNCallKit.reportConnectedOutgoingCallWithUUID(localCallId)
  }
}

export function * announceVideoCallAnswered (data) {
  log.debug('* announceVideoCallAnswered')
  let callId = data.data.call_id
  let answeredDeviceUuid = data.data.device_uuid
  const activeInboundOffer = yield select(s => s.chat.inboundVideoCallOffer)
  const ourDeviceUuid = yield select(s => s.device.device_uuid)

  if (!callId || !answeredDeviceUuid) {
    log.debug('announceVideoCallAnswered - bad packet from server')
    return
  }
  if (!activeInboundOffer) {
    log.debug('announceVideoCallAnswered - inboundVideoCallOffer not in state')
    return
  }
  if (!ourDeviceUuid) {
    log.debug('announceVideoCallAnswered - device.device_uuid not in state')
    return
  }
  // Do we know about this call?
  if (activeInboundOffer.call_id === callId) {
    if (ourDeviceUuid !== answeredDeviceUuid) {
      log.debug('announceVideoCallAnswered - we answered this on a different device, timeout call')
      // We answered on a difference device; timeout call.
      yield call(timeoutCall, {callId})
    } else {
      log.debug('announceVideoCallAnswered - announcement for our device, nothing to do.')
    }
  }
}

export function * timeoutCall ({ callId }) {
  log.debug('timeoutCall: callId=', callId)
  // yield put(NavigationActions.back())
  if (Platform.OS === 'android') {
    BFService.hideIncomingCall()
  } else if (Platform.OS === 'ios') {
    yield call([RNCallKit, RNCallKit.endCall], callId)
  }
}

export function * endVideoCall ({ callId, iceState, isOutboundCancel }) {
  // Go back only if user is on VideoChat screen
  const currentRouteName = yield select(s => getCurrentRouteName(s.nav))
  if (currentRouteName === 'VideoChat') {
    // the following line is for debugging purposes only
    // so the testers can see last WebRTC events like
    // iceConnectionState === failed on the VideoChat logger
    yield delay(1000)

    yield put(NavigationActions.back())
  }

  const localCallId = yield select(s => s.chat.videoCallLocalId)
  log.debug(
    '* endVideoCall callId=' + callId + ' localCallId=' + localCallId + ' iceState=' + iceState + ' isOutboundCancel=' + isOutboundCancel
  )

  // calling reject
  if (isOutboundCancel) {
    log.debug(' ** endVideoCall calling reject isOutboundCancel true')
    yield put(Actions.rejectVideoCall(false))
    yield call(timeoutCall, {callId: localCallId})
    return
  }

  // Note: If we decide to add extra state within native module
  // let statusResult = yield call([RNCallKit, RNCallKit.getStatus], callId)

  // Based on our state machine, and RNCallKit, unless we timeout the call through reportCallWithUuid
  // here (and a CXCallEnded), normal endCall will lead to a stale CallKit session.

  if (iceState === 'failed') {
    log.debug('* endVideoCall about to call timeoutCall')
    yield call(timeoutCall, {
      callId: localCallId
    })
    if (Platform.OS === 'android') {
      BFService.hideIncomingCall()
    } else if (Platform.OS === 'ios') {
      yield call([RNCallKit, RNCallKit.endCall], localCallId)
    }
  } else {
    log.debug('* endVideoCall about to call RNCallkit.endCall -', localCallId, callId)
    if (Platform.OS === 'android') {
      BFService.hideIncomingCall()
    } else if (Platform.OS === 'ios') {
      yield call([RNCallKit, RNCallKit.endCall], localCallId)
    }
  }

  const user = yield select(s => s.user.data)
  log.pushBufferToExternalService(user, callId)

  log.debug(' * endVideoCall about to call cleanupStateAfterCall')
  yield call(cleanupStateAfterCall)
}

export function * listenAndToggleSpeaker () {
  // default settings as defined in ChatRedux
  const initialValue = yield select(s => s.chat.videoCallSpeakerIsMuted)
  if (!initialValue) {
    setSpeakerOn()
  } else {
    setSpeakerOff()
  }
  // Now wait for changes
  while (true) {
    let action = yield take([
      ChatTypes.TOGGLE_VIDEO_CALL_SPEAKER,
      ChatTypes.END_VIDEO_CALL
    ])
    if (action.type === 'END_VIDEO_CALL') {
      log.debug('* listenAndToggleSpeaker - shutting down loop')
      return
    }
    const currentValue = yield select(s => s.chat.videoCallSpeakerIsMuted)
    const newValue = currentValue ? false : true
    if (!newValue) {
      setSpeakerOn()
    } else {
      setSpeakerOff()
    }
    log.debug('* listenAndToggleSpeaker - newValue=', newValue)
    yield put(Actions.setVideoCallSpeakerMuteState(newValue))
  }
}

export function * cleanupStateAfterCall () {
  try {
    yield put(Actions.setOutboundCallId(null))
    yield put(Actions.setVideoCallLocalId(null))
    yield put(Actions.setAudioOnly(null))
    yield put(Actions.setRemoteAudioOnly(null))
    yield put(Actions.setVideoCallCameraMuteState(null))
    yield put(Actions.setVideoCallMicMuteState(null))
    yield put(Actions.setVideoCallSpeakerMuteState(null))
    yield put(Actions.setVideoCallRemoteToggle(null))
    yield put(Actions.setCallIsConnectedState(false))
    Sound.setMode('Default')
  } catch (e) {}
}

export function * onChatMessageReceived ({ data }) {
  // Get data for room to which the message belongs
  const room = yield select(s => s.chat.data[data.room_id])
  const roomMapKey = getMapKeyForRoom(room)

  // Get currently open room map key
  const currentRoute = yield select(s => getCurrentRoute(s.nav))
  const currentlyOpenRoomMapKey = path(['params', 'roomsMapKey'], currentRoute)

  // Get current app state
  const nativeAppState = yield select(s => s.app.nativeAppState)

  // If we are already on the room, then don't show the notification
  if (roomMapKey === currentlyOpenRoomMapKey && (nativeAppState == null || nativeAppState === 'active')) {
    return
  }

  // Show local notification for the message
  yield call(showLocalNotification, { room_id: data.room_id })

  // play notification sound if required
  yield call(inAppNotificationSound)

  // vibrate notification if required
  yield call(inAppNotificationVibrate)
}

export function * inAppNotificationSound () {
  const soundsOn = yield select(s => s.user.data.in_app_sounds_on)
  if (!soundsOn) {
    return
  }
  const filename = yield select(s => s.user.data.notification_sound)
  const sound = yield createSound(filename)
  sound.play()
  yield delay(10000)
  sound.release()
}

export function * inAppNotificationVibrate () {
  const vibrateOn = yield select(s => s.user.data.in_app_vibrate_on)
  if (!vibrateOn) {
    return
  }
  Vibration.vibrate()
}

/**
 * Wired to INBOUND_VIDEO_CALL_ID_RECEIVED.
 *
 * This is for Android, because the delay/setTimeout doesn't work on Android
 * when the app is behind the lock screen.
 */
export function * chatInitFromCallForAndroid () {
  // Exit if not Android
  if (Platform.OS !== 'android') return

  // Exit if chat API has already been initiated
  if (chatAPI.initiated) return

  while (true) {
    // Check if username is available yet in the store
    const username = yield select(s => path(['user', 'data', 'username'], s))
    if (username) {
      // If the chat api hasn't yet been initiated, dispatch CHAT_INIT
      if (!chatAPI.initiated) {
        yield put(Actions.chatInit())
      }
      break
    } else {
      // If username is still not there, wait for UPDATE_USER
      yield take(UserTypes.UPDATE_USER)
    }
  }
}

export function * ensureChatData ({ type }) {
  while (true) {
    yield delay(2000)
    const totalIdentities = yield select(s => s.user.data.total_identities || 0)
    const overlay = yield select(s => s.overlay)

    // there should be at least one identity
    if (totalIdentities < 1) {
      continue
    }

    // the education overlay should not be open
    if (overlay.open && overlay.identifier === OverlayIdentifiers.EDUCATION) {
      continue
    }

    // check if we need to populate the chat data
    const chatData = yield select(s => s.chat.data)
    if (chatData) {
      return
    }

    // all actions that trigger permissions
    // should go here
    yield put(Actions.chatInit())
    return
  }
}
