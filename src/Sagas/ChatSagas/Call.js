import { takeEvery, put, call } from 'redux-saga/effects'

import { CallTypes } from 'commons/Redux/CallRedux'

export function * displayIncomingCallScreen ({ clientId }) {
  let permission = yield call(checkWebRTCPermissions)
  if (!permission) {
    showNoWebRTCPermissionModal(data.from_email)
    yield put(Actions.rejectVideoCall(false))
    yield put(Actions.endVideoCall(clientId))
    return
  }

  const response = yield call(showPlatformInboundCallUI, data, isVideo)

  log.debug('response - ', response)
  if (response.timeout) {
    yield call(timeoutCall, {callId: data.call_id})

    // Send webrtc reject w/out calling endCall
    //
    // We should only do this if we are the only device/last device registered to receive offer.
    // yield put(Actions.rejectInboundVideoCallOffer(false))
  } else {
    if (response.answer) {
      yield put(NavigationActions.navigate({ routeName: 'VideoChat' }))
      // yield put(Actions.answerInboundVideoCallOffer())
      //
      if (!chatAPI.ready) {
        yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
      }
      const ldata = yield select(s => s.chat.inboundVideoCallOffer)
      let setupResult = yield call(setupWebRTCForInbound, ldata)
      log.debug('inboundVideoCallOfferReceived: setupResult -', setupResult)
      if (!setupResult) {
        log.debug('inboundVideoCallOfferReceived failed to setup WebRTC. check device permissions')
        yield put(Actions.rejectVideoCall(true))
      }

      yield spawn(listenAndToggleSpeaker)
    } else {
      yield put(Actions.rejectVideoCall(true))
    }
  }
}

const sagaRules = [
  // takeEvery(CallTypes.DISPLAY_INCOMING_CALL_SCREEN, displayIncomingCallScreen)
]

export default sagaRules