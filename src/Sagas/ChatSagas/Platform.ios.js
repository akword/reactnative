import { delay, eventChannel } from 'redux-saga'
import { take, race, call } from 'redux-saga/effects'
import RNCallKit from 'react-native-callkit'

import log from 'commons/Services/Log'

const INBOUND_CALL_TIMEOUT_MS = 30000

export function * createDidActivateAudioSessionChannel () {
  return eventChannel(emit => {
    RNCallKit.addEventListener('didActivateAudioSession', () => emit({test: 1}))
    return () => { RNCallKit.removeEventListener('didActivateAudioSession', emit) }
  })
}

export function * createEndCallKitChannel () {
  return eventChannel(emit => {
    RNCallKit.addEventListener('endCall', emit)
    return () => { RNCallKit.removeEventListener('endCall', emit) }
  })
}

export function * createDidReceiveStartCallActionCallKitChannel () {
  return eventChannel(emit => {
    RNCallKit.addEventListener('didReceiveStartCallAction', emit)
    return () => { RNCallKit.removeEventListener('didReceiveStartCallAction', emit) }
  })
}

export function * showPlatformInboundCallUI (data, isVideo) {
  RNCallKit.displayIncomingCall(
    data.call_id, `Incoming: ${data.from_email} => ${data.to_email}`, 'generic', isVideo
  )

  const answerCallKitChannel = yield call(createDidActivateAudioSessionChannel)
  const endCallKitChannel = yield call(createEndCallKitChannel)

  log.debug('inboundVideoCallOfferReceived - about to yield race on [answer, reject, timeout]')
  // Note: When on real device, and remote debugging is set, timeout doesnt seem to fire.
  return yield race({
    answer: take(answerCallKitChannel),
    reject: take(endCallKitChannel),
    timeout: call(delay, INBOUND_CALL_TIMEOUT_MS)
  })
}
