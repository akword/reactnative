import { eventChannel } from 'redux-saga'
import { put, take, race, call, select } from 'redux-saga/effects'
import { DeviceEventEmitter } from 'react-native'
import { path } from 'ramda'

import { getAnEmailAvatar } from 'commons/Sagas/AvatarSagas'

import DefaultAvatar from 'app/Fixtures/avatar'
import BFService from '../../../rn_modules/BackgroundToForegroundService'

const INBOUND_CALL_TIMEOUT_MS = 30000

const isScreenLockedPromisified = () => new Promise((resolve, reject) => BFService.isScreenLocked(x => x ? resolve() : reject()))

function createCallAcceptChannel () {
  return new Promise((resolve, reject) => {
    const cleanUp = () => { 
      DeviceEventEmitter.removeListener(BFService.ACCEPT_CALL_ANDROID, accept) 
      DeviceEventEmitter.removeListener(BFService.REJECT_CALL_ANDROID, decline)
    }

    const accept = () => {
      cleanUp();
      resolve(true) 
    }

    const decline = () => { 
      cleanUp();
      resolve(false) 
    }
    // Timeout
    setTimeout(() => {
      resolve('timeout')
    }, 30 * 1000)

    DeviceEventEmitter.addListener(BFService.ACCEPT_CALL_ANDROID, accept)
    DeviceEventEmitter.addListener(BFService.REJECT_CALL_ANDROID, decline)
  })
}

export function * showPlatformInboundCallUI (data, isVideo) {
  // try {
  //   yield call(isScreenLockedPromisified)
  //   console.log('screen is locked...')
  //   BFService.isShowingInboundScreen(visible => {
  //     if (visible) {
  //       console.log('ignoring the call...')
  //     } else {
  //       BFService.showIncomingCall(data.from_email, callerAvatar, data.to_email, calleeAvatar, isVideo, "")
  //       setTimeout(() => { // Hide incoming call after 30 seconds
  //         BFService.hideIncomingCall()
  //       }, 30 * 1000)
  //     }
  //   })
  // } catch (e) {
  //   console.log('screen is unlocked...')
  // }

  // yield put(NavigationActions.navigate({ routeName: 'VideoChat' }))

  let callerAvatar = null
  if (yield select(s => s.avatar.checkedEmails[data.from_email])) {
    callerAvatar = yield select(s => s.avatar.emails[data.from_email])
  } else {
    callerAvatar = yield call(getAnEmailAvatar, data.from_email)
  }

  let calleeAvatar = null
  if (yield select(s => s.avatar.checkedEmails[data.to_email])) {
    calleeAvatar = yield select(s => s.avatar.emails[data.to_email])
  } else {
    calleeAvatar = yield call(getAnEmailAvatar, data.to_email)
  }
  // TODO: the last parameter must be specified - ring tone sound ( any file name in BackgroundToForegroundService/android/src/main/assets/sounds )
  // "" if you don't want to play

  const ringtone = yield select(s => path(['user', 'data', 'video_call_ringtone'], s))

  BFService.showIncomingCall(data.from_email, callerAvatar, data.to_email, calleeAvatar, isVideo, ringtone || 'classic.mp3')

  const result = yield call(createCallAcceptChannel);
  return {
    answer: result === true,
    reject: result === false,
    timeout: result === 'timeout'
  }
}
