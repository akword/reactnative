import { put, call, select } from 'redux-saga/effects'
import { Alert } from 'react-native'
import RNContacts from 'react-native-contacts'
import OpenSettings from 'react-native-open-settings'
import { isEmail } from 'validator'
import { formatMessage } from 'commons/I18n/sagas'
import m from 'commons/I18n/'
import DeviceContactActions, { DeviceContactTypes } from 'app/Redux/DeviceContactRedux'
import { OverlayIdentifiers } from 'app/Redux/OverlayRedux'

const PERMISSION_DENIED = 'denied'
const PERMISSION_UNDEFINED = 'undefined'

const requestPermission = () =>
  new Promise((resolve, reject) => RNContacts.requestPermission((err, res) => err ? reject(err) : resolve(res)))

const checkPermission = () =>
  new Promise((resolve, reject) => RNContacts.checkPermission((err, res) => err ? reject(err) : resolve(res)))

const askPermissionUpdate = ({permission, title, message, cancel, settings}) =>
  new Promise((resolve, reject) => {
      Alert.alert(
        title,
        message,
        [
          {
            text: cancel,
            style: 'cancel',
            onPress: () => {
              return resolve(permission)}
          },
          {
            text: settings,
            onPress: () => {
              OpenSettings.openSettings()
              resolve(permission)
            }
          }
        ]
      )
  }
)

export function * contactsPermissionRequest () {
  let permission
  try {
    // get permission status from redux state
    permission = yield select(s => s.deviceContact.permission)

    // if it is undefined in redux state, get it via RNContacts api
    if (permission === PERMISSION_UNDEFINED) {
      permission = yield call(checkPermission)

      // if permission in redux store was "undefined" and RNContacts returns
      // "denied" then we asked before but not for this session. So we ask
      // to update it via IOS preferences
      if (permission === PERMISSION_DENIED) {
        yield put(DeviceContactActions.showContactsPermissionAlert())
        const title = yield formatMessage(m.native.Chat.noAccessToContacts)
        const message = yield formatMessage(m.native.Chat.enableAccessInSetting)
        const cancel = yield formatMessage(m.app.Common.cancel)
        const settings = yield formatMessage(m.app.Common.settings)
        permission = yield call(askPermissionUpdate, {permission, title, message, cancel, settings})
        // yield put(DeviceContactActions.hideContactsPermissionAlert())
      }
    }

    // at this point if the permission is undefined then we did not ask the user
    // before, so ask for it for the first time
    if (permission === PERMISSION_UNDEFINED) {
      permission = yield call(requestPermission)
    }

  // if something went wrong then the permission is still undefined
  } catch (e) {
    permission = PERMISSION_UNDEFINED
  }

  // update the permission state in redux store
  yield put(DeviceContactActions.contactsPermissionSuccess(permission))
}
