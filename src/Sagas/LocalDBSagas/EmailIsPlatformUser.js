import { take, call } from 'redux-saga/effects'

import { ContactTypes } from 'commons/Redux/ContactRedux'

import { db } from './index'

export function * listenForIsEmailPlatformUserAndCache (db) {
  while (true) {
    const { data } = yield take(ContactTypes.CACHE_IS_EMAIL_PLATFORM_USER)

    for (let c of data) {
      try {
        const query = `
          INSERT INTO email_is_platform_user
            (email, is_platform_user)
          VALUES
            ('${c.email.toLowerCase()}', ${c.is_msgsafe_user ? 1 : 0});
        `
        yield call([db, db.executeSql], query)
      } catch (e) {
        try {
          const query = `
            UPDATE email_is_platform_user
            SET is_platform_user='${c.is_msgsafe_user ? 1 : 0}'
            WHERE email='${c.email.toLowerCase()}';
          `
          yield call([db, db.executeSql], query)
        } catch (e) {}
      }
    }
  }
}

export function * getCachedEmailIsPlatformUserData () {
  const query = 'SELECT * FROM email_is_platform_user;'
  const [{ rows }] = yield call([db, db.executeSql], query)
  const emailIsPlatformUserMap = {}
  for (let i = 0; i < rows.length; i++) {
    const row = rows.item(i)
    emailIsPlatformUserMap[row.email] = !!row.is_platform_user
  }
  return emailIsPlatformUserMap
}
