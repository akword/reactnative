import { Platform } from 'react-native'
import { take, call, put, select } from 'redux-saga/effects'
import moment from 'moment'
import { path } from 'ramda'
import { decode as utf8decode, encode as utf8encode } from 'src/Lib/UTF8'
import base64 from 'base-64'

import { AppTypes } from 'commons/Redux/AppRedux'
import ContactActions, { ContactTypes } from 'commons/Redux/ContactRedux'

import { db } from './index'

const LIMIT = 15

export function * listenForContactsAndCache (db) {
  while (true) {
    const { data, requestType } = yield take(ContactTypes.CONTACT_SUCCESS_FOR_CACHE)

    for (let c of data.contacts) {
      const lastActivityOn = moment(c.last_activity_on).unix()
      const modifiedOn = moment(c.modified_on).unix()
      const encodedData = base64.encode(utf8encode(JSON.stringify(c)))

      try {
        const query = `
          INSERT INTO contacts_cache
            (email, display_name, data, last_activity_on, modified_on, is_msgsafe_user)
          VALUES
            ('${c.email.toLowerCase()}', '${(c.display_name || '').toLowerCase()}', '${encodedData}', '${lastActivityOn}', '${modifiedOn}', '${c.is_msgsafe_user ? 1 : 0}');
        `
        yield call([db, db.executeSql], query)
      } catch (e) {
        try {
          const query = `
            UPDATE contacts_cache
            SET display_name='${(c.display_name || '').toLowerCase()}',
                last_activity_on='${lastActivityOn}',
                modified_on='${modifiedOn}',
                data='${encodedData}',
                email='${c.email.toLowerCase()}',
                is_msgsafe_user='${c.is_msgsafe_user ? 1 : 0}'
            WHERE email='${c.email}';
          `
          yield call([db, db.executeSql], query)
        } catch (e) {}
      }
    }
    yield put(ContactActions.contactFetchFromCache(requestType, data))
  }
}

function * getTotalCount (db, whereClause) {
  const query = `SELECT count(*) FROM contacts_cache ${whereClause};`
  const countQueryResult = yield call([db, db.executeSql], query)
  // console.log('countQueryResult - ', countQueryResult)
  return countQueryResult[0].rows.item(0)['count(*)']
}

export function * contactFetchFromCache ({ requestType = {}, dataFromAPI }) {
  if (!db) {
    yield take(AppTypes.CACHE_D_B_IS_READY)
  }
  // console.log('contactFetchFromCache called => ', requestType, dataFromAPI)

  let offset = 0
  let whereClause = ''

  const slice = yield select(s => s.contact)

  const isSearching = !!(slice.searchQuery || slice.searchFilters.is_msgsafe_user)

  if (requestType.isPaginating) {
    offset = path([isSearching ? 'searchResultsDataOrder' : 'dataOrder', 'length'], slice) || 0
  }

  if (slice.searchQuery) {
    const q = slice.searchQuery.toLowerCase()
    whereClause = `WHERE (email LIKE '%${q}%' OR display_name LIKE '%${q}%')`
  }

  if (slice.searchFilters.is_msgsafe_user) {
    whereClause = whereClause ? whereClause + ' AND is_msgsafe_user=1' : 'WHERE is_msgsafe_user=1'
  }

  const query = `SELECT * FROM contacts_cache ${whereClause} ORDER BY last_activity_on desc LIMIT ${LIMIT} OFFSET ${offset};`
  // console.log('contactFetchFromCache: query is => ', query)
  const [{ rows }] = yield call([db, db.executeSql], query)
  const data = []
  for (let i = 0; i < rows.length; i++) {
    const row = rows.item(i)
    // console.log('row - ', row)
    try {
      const decodedData = utf8decode(base64.decode(row.data))
      data.push(JSON.parse(decodedData))
    } catch (e) {
      yield call([db, db.executeSql], 'DELETE FROM contacts_cache;')
      yield call(contactFetchFromCache, { requestType, dataFromAPI })
      return
    }
  }

  const cacheTotalCount = yield call(getTotalCount, db, whereClause)
  let totalCount = dataFromAPI ? dataFromAPI.total : cacheTotalCount

  // If there isn't any data from API, then override the request type
  // flags with the ones from request;  i.e., if the request is in
  // progress, we don't want success action to reset the flags
  const requestTypeOverride = dataFromAPI || (offset < cacheTotalCount) ? {} : {
    dataRequestInProgress: true,
    dataRequestSuccessful: false,
    dataRequestError: null,
    ...requestType,
    isPaginating: data.length > 0 || requestType.isPaginating
  }

  if (isSearching) {
    requestTypeOverride.isSearching = true
  }

  yield put(ContactActions.contactSuccess(
    { contacts: data, total: totalCount },
    requestType,
    // If there isn't any data from API, then override the request type
    // flags with the ones from request;  i.e., if the request is in
    // progress, we don't want success action to reset the flags
    requestTypeOverride
  ))
}

export function * removeContactFromCache ({ id }) {
  if (!db) {
    yield take(AppTypes.CACHE_D_B_IS_READY)
  }

  yield call([db, db.executeSql], `DELETE FROM contacts_cache WHERE email='${id}';`)
}

export function * resetContactCache () {
  if (Platform.OS === 'android') return

  if (!db) {
    yield take(AppTypes.CACHE_D_B_IS_READY)
  }

  yield call([db, db.executeSql], 'DELETE FROM contacts_cache;')
}
