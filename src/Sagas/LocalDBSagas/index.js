import SQLite from 'react-native-sqlite-storage'
import { spawn, call, put } from 'redux-saga/effects'

import AppActions from 'commons/Redux/AppRedux'

import { listenForContactsAndCache } from './ContactsCache'
import { listenForIsEmailPlatformUserAndCache } from './EmailIsPlatformUser'
import {
  CONTACTS_CACHE_SCHEMA, CONTACTS_CACHE_INDEXES,
  EMAIL_IS_PLATFORM_USER_SCHEMA, EMAIL_IS_PLATFORM_USER_INDEXES
} from './Schema'

/**
 * Creates table is it doesn't already exists or (re-creates if) it has changed.
 *
 * @param tableName
 * @param schema
 * @param indexSchema
 */
function * createTableIfNecessary (tableName, schema, indexSchema) {
  const [{ rows }] = yield call([db, db.executeSql], `SELECT sql FROM sqlite_master WHERE name = '${tableName}';`)
  const schemaInDB = rows.item(0)
  if (!schemaInDB || !schemaInDB.sql || schemaInDB.sql !== schema) {
    if (schemaInDB && schemaInDB.sql) {
      console.log('dropping and recreating db....')
      yield call([db, db.executeSql], 'DROP TABLE contacts_cache;')
    }
    console.log('creating table', tableName)
    yield call([db, db.executeSql], schema)
    console.log('creating indexes')
    yield call([db, db.executeSql], indexSchema)
  }
}

export let db
SQLite.enablePromise(true)

const noop = () => 0

export function * initDatabase () {
  db = yield call([SQLite, SQLite.openDatabase],
    { name : 'data.db', createFromLocation : 1 },
    (...args) => { console.log('database open successful - ', args) },
    (...args) => { console.log('database error - ', args) }
  )

  yield call(createTableIfNecessary, 'contacts_cache', CONTACTS_CACHE_SCHEMA, CONTACTS_CACHE_INDEXES)
  yield call(createTableIfNecessary, 'email_is_platform_user', EMAIL_IS_PLATFORM_USER_SCHEMA, EMAIL_IS_PLATFORM_USER_INDEXES)

  yield put(AppActions.cacheDBIsReady())
  yield spawn(listenForContactsAndCache, db)
  yield spawn(listenForIsEmailPlatformUserAndCache, db)
}
