export const CONTACTS_CACHE_SCHEMA = `CREATE TABLE contacts_cache (
  email char(255) primary key not null,
  display_name char(255),
  data text not null,
  last_activity_on int,
  is_msgsafe_user int,
  modified_on int
)`

export const CONTACTS_CACHE_INDEXES = `
  CREATE INDEX idx_contacts_cache_display_name ON contacts_cache (display_name);
  CREATE INDEX idx_contacts_cache_email ON contacts_cache (email);
  CREATE INDEX idx_contacts_cache_last_activity_on ON contacts_cache (last_activity_on);
  CREATE INDEX idx_contacts_cache_is_msgsafe_user ON contacts_cache (is_msgsafe_user);
`

export const EMAIL_IS_PLATFORM_USER_SCHEMA = `CREATE TABLE email_is_platform_user (
  email char(255) primary key not null,
  is_platform_user int
)`

export const EMAIL_IS_PLATFORM_USER_INDEXES = `
  CREATE INDEX idx_email_is_platform_user_email ON email_is_platform_user (email);
  CREATE INDEX idx_email_is_platform_user ON email_is_platform_user (is_platform_user);
`
