import { Platform } from 'react-native'
import { delay } from 'redux-saga'
import { put, call, fork, select, spawn } from 'redux-saga/effects'
import { NavigationActions } from 'react-navigation'
import * as Keychain from 'react-native-keychain'
import RNFS from 'react-native-fs'

import log from 'commons/Services/Log'
import { api as MsgSafeAPI } from 'commons/Sagas/APISagas'
import ChatActions from 'commons/Redux/ChatRedux'
import PushActions from 'commons/Redux/PushRedux'
import UserActions from 'commons/Redux/UserRedux'
import IdentityActions from 'commons/Redux/IdentityRedux'
import ContactActions from 'commons/Redux/ContactRedux'
import { periodicallyRefreshRoomState } from 'commons/Sagas/ChatSagas/Polling'

import OverlayActions, { OverlayIdentifiers } from 'app/Redux/OverlayRedux'

import { DOCS_DIRECTORY_PATH, TEMP_DOCS_DIRECTORY_PATH } from '../Lib/FS'
import { refreshUser, checkOnboardingStateAndRedirect, connectionTimeout } from './UserSagas'
import { initDatabase } from './LocalDBSagas'
import { areNativePermissionsAvailable } from './ChatSagas'
import { refreshChatOnAppStateChange, trackNetworkStateChange } from './AppSagas'

// set the environment header flag
// this makes all http requests include a `X-Msgsafe-IsNative` header
// so the server can tell if the request was made from ios/android app
MsgSafeAPI.httpAPI.isNative(true)

export function * startup () {
  yield put(PushActions.pushInit())

  // create directories if not exists
  yield RNFS.mkdir(DOCS_DIRECTORY_PATH)
  yield RNFS.mkdir(TEMP_DOCS_DIRECTORY_PATH)
  yield spawn(permissionsInitiator)
  yield spawn(refreshChatOnAppStateChange)
  yield spawn(trackNetworkStateChange)
}

export function * startupSuccess () {
  const keychain = yield call(Keychain.getGenericPassword)
  yield call(initDatabase)

  if (!keychain || !keychain.password) {
    yield put(NavigationActions.navigate({ routeName: 'AuthInitial'}))

    return
  }

  let userData = null
  try {
    userData = JSON.parse(keychain.password)
  } catch (e) {
    log.debug('failed to parse userData. stale keychain?')
    return
  }

  yield put(UserActions.updateUser(userData))
  const freshUserData = yield call(refreshUser)

  if (!freshUserData) {
    yield put(NavigationActions.navigate({ routeName: 'Login'}))
    return
  }

  yield checkOnboardingStateAndRedirect(freshUserData)
  yield put(IdentityActions.identityFetch())
  yield put(ContactActions.contactFetch())

  const hasLoggedInOnNative = Platform.OS === 'ios' ? freshUserData.has_logged_in_ios : freshUserData.has_logged_in_android
  if (!hasLoggedInOnNative && freshUserData.sum_identity > 0) {
    yield put(OverlayActions.showOverlay(OverlayIdentifiers.EDUCATION))
  }

  yield spawn(connectionTimeout, { timeout: 40000 })
}

let permissionsInitiated = false
export function * permissionsInitiator () {
  while (!permissionsInitiated) {
    yield delay(1000)
    const totalIdentities = yield select(s => s.user.data.total_identities || 0)
    const overlay = yield select(s => s.overlay)

    // there should be at least one identity
    if (totalIdentities < 1) {
      continue
    }

    // the education overlay should not be open
    if (overlay.open && overlay.identifier === OverlayIdentifiers.EDUCATION) {
      continue
    }

    permissionsInitiated = true

    // all actions that trigger permissions
    // should go here
    yield put(ChatActions.chatInit())

    // Ask/check for audio and video permissions
    yield call(areNativePermissionsAvailable, { audio: true })
    yield call(areNativePermissionsAvailable, { video: true })
  }
}
