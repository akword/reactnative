import { select, spawn } from 'redux-saga/effects'

import log from 'commons/Services/Log'
import {api} from 'commons/Sagas/APISagas'

import { 
	listenToNotifications, 
	listenToInboundActions 
} from './Notifications'

export function * pushInit () {
  try {
    const deviceUUID = yield select(s => s.device.uuid)
    yield spawn(listenToNotifications)
    yield spawn(listenToInboundActions)
  } catch (e) {
    log.error('pushInit caught -', e)
  }
}
