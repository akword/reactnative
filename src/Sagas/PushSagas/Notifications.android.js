import { eventChannel } from 'redux-saga'
import { put, select, take, call } from 'redux-saga/effects'
import { NavigationActions } from 'react-navigation'
import FCM, { FCMEvent } from 'react-native-fcm'
import { path } from 'ramda'

import ChatActions, { ChatTypes } from '/commons/Redux/ChatRedux'
import { getMapKeyForRoom } from 'commons/Selectors/Messaging'

import { getCurrentRoute } from '../../Navigation/utils'
import BFService from '../../../rn_modules/BackgroundToForegroundService'
import log from 'commons/Services/Log'

export function * listenToInboundActions () {
  console.log('listenToInboundActions entered...')
  const channel = yield call(createGotRemoteStreamChannel)
  while (true) {
    const action = yield take(channel)
    if (action === BFService.REJECT_CALL_ANDROID) {
      // Reject Call here
      //yield put()
    }
  }
}

function createGotRemoteStreamChannel () {
  return eventChannel(emit => {
    FCM.on(FCMEvent.Notification, n => emit(n))
    return () => {}
  })
}

export function * showLocalNotification (data) {
  // console.log('showLocalNotification - ', data)
  FCM.presentLocalNotification({
    id: 'msgsafe',
    title: 'You have a new message',
    body: 'Tap to see',
    priority: 'high',
    show_in_foreground: true,
    my_custom_data: data
  })
}

function * navigateToRoomWithMapKey (notif) {
  // Get data for room to which the message belongs
  const room = yield select(s => s.chat.data[notif.my_custom_data.room_id])
  if (!room) return

  const roomMapKey = getMapKeyForRoom(room)

  // Get the currently open room map key
  const currentRoute = yield select(s => getCurrentRoute(s.nav))
  const currentlyOpenRoomMapKey = path(['params', 'roomsMapKey'], currentRoute)

  // Check if we are already on the room
  if (roomMapKey === currentlyOpenRoomMapKey) {
    console.log('room already open...')
    return
  }

  // If we are already in a chat room, then reset and navigate
  if (currentRoute && currentRoute.routeName === 'MessagingRoom') {
    yield put(NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'MessagingRoom', params: { roomsMapKey: roomMapKey } }),
      ]
    }))
    return
  }

  // Navigate to the room
  yield put(NavigationActions.navigate({
    routeName: 'MessagingRoom',
    params: { roomsMapKey: roomMapKey }
  }))
}

export function * listenToNotifications () {

  const initialNoti = yield call([FCM, FCM.getInitialNotification])
  if (initialNoti) {
    if (initialNoti.call_id) {
      yield put(ChatActions.inboundVideoCallIdReceived(initialNoti.call_id, true))
    }

    if (initialNoti.my_custom_data && initialNoti.my_custom_data.room_id) {
      yield take(ChatTypes.CHAT_INIT)
      yield put(NavigationActions.navigate({ routeName: 'MessagingRoomList'}))
      yield take(ChatTypes.CHAT_SUCCESS)
      yield call(navigateToRoomWithMapKey, initialNoti)
    }    
  }

  //FCM.getFCMToken().then(token => {
  //  console.info('FCM TOKEN:', token)
    // store fcm token in your server
  //});

  const channel = yield call(createGotRemoteStreamChannel)

  while (true) {
    const notif = yield take(channel)
    console.info('Notification:', notif)

    // If there's `my_custom_data` on the notification and there's a room_id
    if (notif.my_custom_data && notif.my_custom_data.room_id) {
      yield call(navigateToRoomWithMapKey, notif)
    }

    // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
    if (notif.local_notification) {
      //this is a local notification
    }

    if (notif.opened_from_tray) {
      //app is open/resumed because user clicked banner
    }

    if (notif.call_id) {
      const localCallId = yield select(s => s.chat.videoCallLocalId)
      if (localCallId === notif.call_id) {
        log.debug('Duplicated Call -', localCallId)
        continue
      }
      yield put(ChatActions.inboundVideoCallIdReceived(notif.call_id, true))
    }
  }
}
