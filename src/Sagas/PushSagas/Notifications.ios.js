import { eventChannel } from 'redux-saga'
import { call, take, race, put, select } from 'redux-saga/effects'
import NotificationsIOS from 'react-native-notifications'
import { path } from 'ramda'
import { NavigationActions } from 'react-navigation'
import { formatMessage } from 'commons/I18n/sagas'
import m from 'commons/I18n/'
import log from 'commons/Services/Log'
import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import { chatAPI } from 'commons/Sagas/ChatSagas'
import { getContactMember, getMapKeyForRoom } from 'commons/Selectors/Messaging'
import NotificationActions from 'commons/Redux/NotificationRedux'

function * createForegroundEventListenerChannel () {
  return eventChannel(emit => {
    NotificationsIOS.addEventListener('notificationReceivedForeground', emit)
    return () => { NotificationsIOS.removeEventListener('notificationReceivedForeground', emit) }
  })
}

function * createBackgroundEventListenerChannel () {
  return eventChannel(emit => {
    NotificationsIOS.addEventListener('notificationReceivedBackground', emit)
    return () => { NotificationsIOS.removeEventListener('notificationReceivedBackground', emit) }
  })
}

function * createNotificationOpenedEventListenerChannel () {
  return eventChannel(emit => {
    NotificationsIOS.addEventListener('notificationOpened', emit)
    return () => { NotificationsIOS.removeEventListener('notificationOpened', emit) }
  })
}

export function * listenToInboundActions () {}

export function * showLocalNotification () {
  const message = yield formatMessage(m.native.Mailbox.receivedANewMessage)
  yield put(NotificationActions.displayNotification(message, 'info', 3000))

  // NotificationsIOS.localNotification({
  //   alertBody: "Local notificiation!",
  //   alertTitle: "Local Notification Title",
  //   soundName: "chime.aiff",
  //   silent: false,
  //   category: "SOME_CATEGORY",
  //   userInfo: { }
  // })
}

export function * listenToNotifications () {
  const backgroundChannel = yield call(createBackgroundEventListenerChannel)
  const foregroundChannel = yield call(createForegroundEventListenerChannel)
  const notificationOpenedChannel = yield call(createNotificationOpenedEventListenerChannel)

  NotificationsIOS.consumeBackgroundQueue()

  while (true) {
    const event = yield race({
      notification: take(notificationOpenedChannel),
      background: take(backgroundChannel),
      foreground: take(foregroundChannel)
    })
    log.debug('listenToNotifications event - ', event)

        // Incoming voip call push
    const eventData = event.background || event.foreground

    const callId = path(['_data', 'call_id'], eventData)
    if (callId) {
      log.debug('VOIP CALL - ', eventData)
      yield put(Actions.inboundVideoCallIdReceived(callId, true))
      continue
    }

    // Incoming chat message notification
    if (event.notification) {
      const roomId = path(['notification', '_data', 'room_id'], event)
      if (!roomId) continue

      // If chat is not ready, wait for the bootstrap and room fetch
      if (!chatAPI.ready) {
        yield take(ChatTypes.CHAT_FETCH)
        yield put(NavigationActions.navigate({ routeName: 'MessagingRoomList'}))
        yield take(ChatTypes.CHAT_SUCCESS)
      }

      // Get the room object with room_id
      const room = yield select(s => path(['chat', 'data', roomId], s))
      // log.debug('message notification - room - ', room)
      if (!room) {
        log.debug(`Room ${roomId} not found for message notification...`)
        continue
      }

      // Extract contact detail
      const contact = getContactMember(room)
      // log.debug('message notification - contact - ', contact)
      if (!contact.email) continue

      // Move to chat room
      yield put(NavigationActions.navigate({
        routeName: 'MessagingRoom',
        params: { roomsMapKey: getMapKeyForRoom(room) }
      }))

      continue
    }

  }
}
