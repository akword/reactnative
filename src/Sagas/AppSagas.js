import { updateIntl } from 'react-intl-redux'
import { eventChannel } from 'redux-saga'
import { put, select, take, call, spawn } from 'redux-saga/effects'
import { NavigationActions } from 'react-navigation'
import { AppState, NetInfo, Platform } from 'react-native'
import { path } from 'ramda'

import AppActions from 'commons/Redux/AppRedux'
import UserActions from 'commons/Redux/UserRedux'
import { IdentityTypes } from 'commons/Redux/IdentityRedux'
import { periodicallyRefreshRoomState } from 'commons/Sagas/ChatSagas/Polling'
import { chatAPI } from 'commons/Sagas/ChatSagas'
import { isCallInProgress } from 'commons/Selectors/VideoChat'

import OverlayActions, { OverlayIdentifiers } from 'app/Redux/OverlayRedux'

export function * applyLocaleWithUserData ({ data: { locale } }) {
  if (!locale) return

  const currentLocale = yield select(s => s.intl.locale)
  if(currentLocale !== locale) {
    yield applyLocale({locale})
  }
}

export function * applyLocale ({ locale }) {
  if (!locale) return

  const translationsData = yield select(s => s.intl.allData)
  if (!translationsData[locale]) return

  yield put(updateIntl({ locale, messages: translationsData[locale] }))
}

export function * onBoardingComplete () {
  // Send user to Mailbox

  yield put(NavigationActions.navigate({ routeName: 'Mailbox' }))

  yield put(OverlayActions.showOverlay(OverlayIdentifiers.EDUCATION))

  // Set display_name on account

  const identityCount = yield select(s => path(['identity', 'data', 'length'], s))
  if (!identityCount) {
    yield take(IdentityTypes.IDENTITY_CREATE_SUCCESS)
  }

  const identityId = yield select(s => path(['identity', 'dataOrder', 0], s))
  if (!identityId) return

  const identity = yield select(s => path(['identity', 'data', identityId], s))
  if (identity && identity.display_name && identity.display_name !== identity.email) {
    yield put(UserActions.updateAccountRequest({ display_name: identity.display_name }))
  }
}

export const createAppStateChangeEventChannel = () => eventChannel(emit => {
  AppState.addEventListener('change', emit)
  return () => AppState.removeEventListener('change', emit)
})

export function * refreshChatOnAppStateChange () {
  const channel = yield call(createAppStateChangeEventChannel)
  while (true) {
    const state = yield take(channel)
    yield put(AppActions.nativeAppStateChanged(state))

    const callInProgress = yield select(isCallInProgress)

    if (!callInProgress) {
      if (Platform.OS === 'android' && state === 'background') {
        yield call(console.log, 'chatAPI.close(false)')
        chatAPI.close(false)
      }
      if (Platform.OS === 'ios' && state === 'inactive') {
        yield call(console.log, 'chatAPI.close(false)')
        chatAPI.close(false)
      }
    }
      
    if (state === 'active') {
      yield spawn(periodicallyRefreshRoomState, false, true)
    }
  }
}

const createAppNetworkStateChangeEventChannel = () => eventChannel(emit => {
  NetInfo.isConnected.addEventListener('connectionChange', emit)
  return () => NetInfo.isConnected.removeEventListener('connectionChange', emit)
})

export function * trackNetworkStateChange () {
  const channel = yield call(createAppNetworkStateChangeEventChannel)
  while (true) {
    const state = yield take(channel)
    console.log('new network state = ', state)
    yield put(AppActions.setNativeAppNetworkState(state))
  }
}

export function * checkNetworkState () {
  const state = yield call(NetInfo.isConnected.fetch)
  yield put(AppActions.setNativeAppNetworkState(state))
}
