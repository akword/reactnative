import { put, call, take, select } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { formatMessage } from 'commons/I18n/sagas'
import m from 'commons/I18n/'
import NotificationActions from 'commons/Redux/NotificationRedux'
import MailboxActions, { MailboxTypes } from 'commons/Redux/MailboxRedux'
import { isLoggedIn } from 'commons/Selectors/User'

export function * sendMailError ({ error }) {
  const message = yield formatMessage(m.native.Mailbox.failedToSendEmail)
  yield put(NotificationActions.displayNotification(message, 'danger', 3000))
}

export function * multiselectNotifications (successStr, notificationType, { selectedIds }) {
  let messageId = ''
  const multi = selectedIds.length > 1

  switch (successStr) {
    case 'trash':
      messageId = multi ? m.native.Mailbox.mailPluralToTrash : m.native.Mailbox.oneMailToTrash
      break
    case 'archived':
      messageId = multi ? m.native.Mailbox.mailPluralArchived : m.native.Mailbox.oneMailArchived
      break
    case 'read':
      messageId = multi ? m.native.Mailbox.mailPluralRead : m.native.Mailbox.oneMailRead
      break
    case 'unread':
      messageId = multi ? m.native.Mailbox.mailPluralUnread : m.native.Mailbox.oneMailUnread
      break
    case 'deleted':
      messageId = multi ? m.native.Mailbox.mailPluralDeleted : m.native.Mailbox.oneMailDeleted
      break
  }
  const emailsMessage = yield formatMessage(messageId, {emailCount: selectedIds.length})
  yield put(NotificationActions.displayNotification(
    emailsMessage,
    notificationType,
    3000
  ))
}

export function * ensureFreshInbox () {
  while (true) {
    yield take(MailboxTypes.SET_MAILBOX_FILTER)
    yield take([
      MailboxTypes.MAILBOX_MOVE_TO_INBOX_SUCCESS,
      MailboxTypes.MAILBOX_UNARCHIVE_SUCCESS,
    ])
    yield take(MailboxTypes.CLEAR_MAILBOX_FILTER)
    yield put(MailboxActions.mailboxFetch())
  }
}

export function * ensureWelcomeEmailsAutoFetch () {
  while (true) {
    yield delay(5000)
    const user = yield select(s => s.user)
    if (!isLoggedIn(user)) {
      return
    }
    const emailIds = yield select(s => s.mailbox.dataOrder)
    if (emailIds && emailIds.length) {
      return
    }
    yield put(MailboxActions.mailboxAutoUpdateRequest())
  }
}