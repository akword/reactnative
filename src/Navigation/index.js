import React from 'react'
import { StackNavigator, TabNavigator, DrawerNavigator } from 'react-navigation'

import MainTabBar from 'app/Components/MainTabBar'
import Terms from 'app/Components/Terms'
import Privacy from 'app/Components/Privacy'

import Launch from 'app/Containers/Auth/launch'
import AuthInitial from 'app/Containers/Auth/initial'
import SignupForm from 'app/Containers/Auth/Signup/Form'
import SignupContacts from 'app/Containers/Auth/Signup/Contacts'
import SignupIdentity from 'app/Containers/Auth/Signup/Identity'
import SignupESPImported from 'app/Containers/Auth/Signup/ESPImported'
import LoginScreen from 'app/Containers/Auth/login'
import PasswordResetScreen from 'app/Containers/Auth/passwordReset'
import PasswordResetRequestScreen from 'app/Containers/Auth/passwordResetRequest'

import Dashboard from 'app/Containers/Dashboard'

import MailboxList from 'app/Containers/Mailbox/List'
import MailboxDetail from 'app/Containers/Mailbox/Detail'
import MailboxCompose from 'app/Containers/Mailbox/Create'
import IdentityFilterModal from 'app/Containers/Mailbox/Drawer/IdentityFilterModal'

import IdentityList from 'app/Containers/Identity'
import EditIdentity from 'app/Containers/Identity/edit'
import IdentityDetail from 'app/Containers/Identity/detail'

import ForwardAddressList from 'app/Containers/ForwardAddress/List'
import ForwardAddressDetail from 'app/Containers/ForwardAddress/Detail'
import CreateForwardAddress from 'app/Containers/ForwardAddress/Create'

import ContactList from 'app/Containers/Contacts/List'
import DeviceContactDetail from 'app/Containers/Contacts/DeviceContactDetail'
import ContactDetail from 'app/Containers/Contacts/Detail'
import EditContact from 'app/Containers/Contacts/edit'
import ContactKeyViewer from 'app/Containers/Contacts/KeyViewer'
import ContactKeyAdder from 'app/Containers/Contacts/KeyAdder'

import AboutApp from 'app/Containers/User/About'
import UserAccount from 'app/Containers/User/Account'
import EditUserProfile from 'app/Containers/User/EditProfile'
import ChangePassword from 'app/Containers/User/ChangePassword'
import IdentitySettings from 'app/Containers/User/IdentitySettings'
import NotificationSettings from 'app/Containers/User/NotificationSettings'
import UpgradePlan from 'app/Containers/User/UpgradePlan'


import MessagingList from 'app/Containers/Messaging/RoomList'
import MessagingRoom from 'app/Containers/Messaging/Room'
import MessagingCreate from 'app/Containers/Messaging/Create'

import VideoChat from 'app/Containers/VideoChat'
import FileViewer from 'app/Containers/FileViewer'
import SoundSelection from 'app/Containers/SoundSelection'

export const NavigationTypes = {
  BACK: 'Navigation/BACK',
  INIT: 'Navigation/INIT',
  NAVIGATE: 'Navigation/NAVIGATE',
  RESET: 'Navigation/RESET',
  SET_PARAMS: 'Navigation/SET_PARAMS',
  URI: 'Navigation/URI',
}

const AuthNavigator = StackNavigator({
  AuthInitial: { screen: AuthInitial },
  Login: { screen: LoginScreen },
  SignupForm: { screen: SignupForm },
  SignupContacts: { screen: SignupContacts },
  SignupESPImported: { screen: SignupESPImported },
  SignupIdentity: { screen: SignupIdentity },
  PasswordReset: { screen: PasswordResetScreen },
  PasswordResetRequest: { screen: PasswordResetRequestScreen }
}, {
  tabBarVisible: false
})

const MailboxNavigator = StackNavigator({
  MailboxList: { screen: MailboxList },
  MailboxDetail: { screen: MailboxDetail },
  MailboxCompose: { screen: MailboxCompose },
  IdentitySelection: { screen: IdentityList },
  ContactSelection: { screen: ContactList },
  ForwardAddressDetail: { screen: ForwardAddressDetail },
  CreateForwardAddress: { screen: CreateForwardAddress },
}, {
  headerMode: 'float'
})

const MessagingNavigator = StackNavigator({
  MessagingRoomList: { screen: MessagingList },
  MessagingRoom: { screen: MessagingRoom },
  MessagingCreateRoom: { screen: MessagingCreate },
  IdentitySelection: { screen: IdentityList },
  ContactSelection: { screen: ContactList },
  ContactDetail: { screen: ContactDetail },
  EditContact: { screen: EditContact }
}, {
  headerMode: 'screen'
})

const IdentityNavigator = StackNavigator({
  IdentityList: { screen: IdentityList },
  CreateIdentity: { screen: EditIdentity },
  EditIdentity: { screen: EditIdentity },
  IdentityDetail: { screen: IdentityDetail },
  ContactSelection: { screen: ContactList },
}, {
  headerMode: 'none'
})

const ContactNavigator = StackNavigator({
  ContactList: { screen: ContactList },
  IdentitySelection: { screen: IdentityList },
  DeviceContactDetail: { screen: DeviceContactDetail },
  ContactDetail: { screen: ContactDetail },
  EditContact: { screen: EditContact },
  MailboxCompose: { screen: MailboxCompose },
  MessagingRoom: { screen: MessagingRoom },
  ContactKeyViewer: { screen: ContactKeyViewer },
  ContactKeyAdder: { screen: ContactKeyAdder },
})

const ForwardAddressNavigator = StackNavigator({
  ForwardAddressList: { screen: ForwardAddressList },
  ForwardAddressDetail: { screen: ForwardAddressDetail },
  CreateForwardAddress: { screen: CreateForwardAddress },
}, {
  headerMode: 'none'
})

const AccountNavigator = StackNavigator({
  UserAccount: { screen: UserAccount },
  EditUserProfile: { screen: EditUserProfile },
  ChangePassword: { screen: ChangePassword },
  IdentitySettings: { screen: IdentitySettings },
  NotificationSettings: { screen: NotificationSettings },
  Identity: { screen: IdentityNavigator },
  ForwardAddress: { screen: ForwardAddressNavigator },
  UpgradePlan: { screen: UpgradePlan },
  SoundSelection: { screen: SoundSelection },
  About: { screen: AboutApp },
})

const UserAreaNavigator = TabNavigator({
  // Dashboard: { screen: Dashboard },
  Mailbox: { screen: MailboxNavigator },
  Messaging: { screen: MessagingNavigator },
  Contact: { screen: ContactNavigator },
  Account: { screen: AccountNavigator }
}, {
  lazy: false,
  animationEnabled: false,
  tabBarComponent: MainTabBar,
  tabBarPosition: 'bottom',
  navigationOptions: {
    gesturesEnabled: false
  },
  swipeEnabled: false
})

const AppNavigator = StackNavigator({
  Launch: { screen: Launch },
  Auth: { screen: AuthNavigator },
  UserArea: { screen: UserAreaNavigator },
  VideoChat: { screen: VideoChat },
  FileViewer: { screen: FileViewer },
  Terms: { screen: Terms },
  Privacy: { screen: Privacy },
}, {
  headerMode: 'none',
  navigationOptions: {
    gesturesEnabled: false
  }
})

export default AppNavigator
