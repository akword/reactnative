import request from 'superagent'

import { getDeviceInfo } from 'app/Lib/Device'

const LOGGER_URI = 'https://graylog.msgsafe.io:5048/gelf'

export function pushToExternalLogger (data, user, callId) {
  request.post(LOGGER_URI).send({
    data: data,
    host: 'mobile',
    short_message: callId,
    call_id: callId,
    display_name: user.display_name,
    username: user.username,
    ...getDeviceInfo()
  }).end()
}
