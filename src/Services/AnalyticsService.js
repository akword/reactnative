const fakeAnalytics = {
  track: () => null,
  identify: () => null,
  alias: () => null,
  people: {
    set: () => null,
    setOnce: () => null
  }
}

const createService = key => {
  // const mixpanel = require('react-native-mixpanel')
  // mixpanel.sharedInstanceWithToken(key)
  // return mixpanel

  // using fakeAnalytics for now
  // todo: use real react-native-mixpanel instance
  // after updating to latest react-native
  return fakeAnalytics
}

export default createService
