import { AppRegistry } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Sound from 'react-native-sound'
// Enable playback in silence mode
Sound.setCategory('Playback')

// Uncomment next line to view network requests in chrome debugger
GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest

import App from './src/Containers/App'

EStyleSheet.build();

AppRegistry.registerComponent('MsgSafe', () => App);
