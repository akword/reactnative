# Running

## Window 1
react-native start --reset-cache 

## Window 2
react-native run-ios --simulator "iPhone 7 Plus"


# Relevant bugs to keep track of –

- https://github.com/react-community/react-navigation/issues/1338
- https://github.com/facebook/react-native/issues/1831


# Pushing to iTunes Connect (and TestFlight)

First, make sure that both iTunes Connect *and* Apple Developer Program accounts are added in Xcode > Preferences > Accounts. If the appropriate `Apple Developer Program` account is added, Xcode should automatically deal with provisioning and certs stuff. And if appropriate `iTunes Connect` is added in Xcode, this will let you push app build to iTunes Connect.

## Preparing Archive

- Bump the build number – select MsgSafe project and then “General tab" (we probably won’t need to bump version number, until we are ready push new versions of app build to app store)
- in Xcode, go to menubar's Product > Scheme > Edit Scheme
- with “Run” tab selected on the left, set “Build Configuration” to Release and close this dialog
- set ‘Generic iOS device’ as the target device at top left (right where we select simulator device type)
- in menubar, Product > Archive – this will create an archive that can be uploaded to iTunes Connect or exported for manually installing on a device with iTunes

## Pushing to iTunes Connect using Application Loader

- Export -> Save for iOS App Store Deployment (save it where you can easily find it)
- Menu Bar -> Xcode -> Open developer tool -> Application Loader
- Choose "Deliver your app" and select the exported .ipa file

## Pushing to iTunes Connect directly (sucks)

- in menubar, Window > Organizer – select the archive build that you just did and hit “Upload to App Store…” button on right. bad wording by Apple, this won’t cause it to be queued for App Store publishing but just make the build available in iTunes Connect
- rest of the stuff after hitting the button is straightforward, just choose the Team and hit next buttons
- once the upload is finished on Xcode’s end, open iTunes Connect, wait/check for the build status, takes ~10min to process
- once the build is processed, open the “Test Flight” page, select the version to test, choose the latest build and it will be made available to all the testers


# Android

- Follow instructions at for Android at – https://facebook.github.io/react-native/docs/getting-started.html
- In Android Studio's launch screen, go to Configure => SDK Tools, check NDK and hit OK to install it
- The AVG manager icon might be disabled when MsgSafe android project is opened in Android Studio.  In order to access AVD manager, create a new default project in Android and the AVD manager icon should now be available.
- Create an emulator based on instructions in the above link and launch the emulator
- Open two terminals and run –
    - react-native start --reset-cache
    - react-native run-android

Build signed APK – https://facebook.github.io/react-native/docs/signed-apk-android.html


# Notes
  - Use `BaseText` instead of `Text` to have a common font family

---

App boilerplate and structure based on https://github.com/infinitered/ignite

---

Instructions for adding custom launch screen for iOS –

- Use the `misc/generate_iOS_launch_images.sh`
- Place Default-Input.png with preferably 2208x2208 or larger dimension in the same directory and run the above script
- Create a new LaunchImage in Xcode and drag the generated files as per the following mappings
    - `iPhone Portrait (iOS 5,6)`
        - `1x` -> 320x480
        - `2x` -> 640x960
        - `Retina 4` -> 640x1136
    - `iPhone Portrait (iOS 8,9)`
        - `Retina HD 5.5"` -> 1242x2208
        - `Retina HD 4.7"` -> 750x1134
    - `iPhone Landscape (iOS 8,9)`
        - `Retina HD 5.5"` -> 2208x1242
    - `iPhone Portrait (iOS 7-9)`
        - `2x` -> 640x960
        - `Retina 4` -> 640x1136

Size guide - https://medium.com/@jigarm/ios-app-icon-and-launch-image-sizes-e8d5990cb72b

---

Look into scrolling the TextInput component up along with the keyboard. Some resources –

- http://kpetrovi.ch/2015/09/30/react-native-ios-keyboard-events.html
- https://gist.github.com/dbasedow/f5713763802e27fbde3fc57a600adcd3
- https://gist.github.com/tpisto/4975c802f2d15f1c7f283cc2e324495c
- https://stackoverflow.com/questions/29313244/
- https://blog.brainsandbeards.com/react-native-keyboard-handling-tips-a2472b74c114#.1a6c90ovd
- https://github.com/Andr3wHur5t/react-native-keyboard-spacer

Wasn't able to make it work on Signup screen unfortunately (Oct 11, 2016).


# 2018 Build notes

jq -r ".version" package.json
1.0.1
jq -r ".build" package.json
561

json -I -f package.json -e 'this.build="562"'

obviously, package.json was updated to seed CI processes that are incomplete -- I will revisit -- paused 20180206
