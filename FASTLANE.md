# Installation

brew cask install fastlane

Add the following line to bash profile:

export PATH="$HOME/.fastlane/bin:$PATH"

fastlane match development --readonly

brew install imagemagick

