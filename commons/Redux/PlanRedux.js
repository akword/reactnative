import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { UserTypes } from './UserRedux'
import { merge } from 'ramda'

import createAPIPackage from '../Lib/Redux/createAPIPackage'
import { upgradePlanRequest } from '../Sagas/PlanSagas'

/* ------------- Types and Action Creators ------------- */

const { APITypes, APICreators, APIState, APIReducers, APITypesToSagas } = createAPIPackage(
  'plan', [
    { type: 'plans', endpoint: 'Plans' },
    { type: 'couponCheck', endpoint: 'CheckCoupon', getTranslatedError: true },
    { type: 'getUpgradePrice', endpoint: 'GetUpgradePrice', getTranslatedError: true },
    { type: 'upgradePlan', endpoint: 'UpdateUserAccount', getTranslatedError: true, requestSagas: upgradePlanRequest }
  ]
)

const { Types, Creators } = createActions({
  newUserSelectPlan: ['id', 'term']
})

export const PlanTypes = merge(Types, APITypes)
export const PlanCreators = merge(Creators, APICreators)
export const PlanAPITypesToSagas = APITypesToSagas
export default PlanCreators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  ...APIState,

  newUserSelectedPlan: null,
  newUserSelectedPlanTerm: null
})

/* ------------- Reducers ------------- */

const newUserSelectPlan = (state, { id, term }) => state.merge({
  newUserSelectedPlan: id,
  newUserSelectedPlanTerm: term
})

const reset = () => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  ...APIReducers,

  [PlanTypes.NEW_USER_SELECT_PLAN]: newUserSelectPlan,

  [UserTypes.LOGOUT]: reset
})
