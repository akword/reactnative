import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { UserTypes } from './UserRedux'
import { ChatTypes } from './ChatRedux'

// RTCIceConnectionState enum
// https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceConnectionState
export const ICE_CONNECTION_STATE = {
  NEW: 'new',
  CHECKING: 'checking',
  CONNECTED: 'connected',
  COMPLETED: 'completed',
  FAILED: 'failed',
  DISCONNECTED: 'disconnected',
  CLOSED: 'closed',
}

// RTCIceGatheringState enum
// https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceGatheringState
export const ICE_GATHERING_STATE = {
  NEW: 'new',
  GATHERING: 'gathering',
  COMPLETE: 'complete',
}

// Custom event type constants we use to pass webrtc event types
export const EVENT_TYPE = {
  ICE_CONNECTION_STATE_UPDATE: 'WEBRTC_ICE_CONNECTION_STATE_UPDATE',
  ICE_GATHERING_STATE_UPDATE: 'WEBRTC_ICE_GATHERING_STATE_UPDATE',
  ICE_CANDIDATE_ADDITION: 'ICE_CANDIDATE_ADDITION',
}

// Custom event type constants we use to track WebRTC.getUserMedia function
export const GET_USER_MEDIA_STATE = {
  START_AUDIO_VIDEO: 'START_GET_USER_MEDIA_AUDIO_VIDEO',
  END_AUDIO_VIDEO: 'END_GET_USER_MEDIA_AUDIO_VIDEO',
  START_AUDIO: 'START_GET_USER_MEDIA_AUDIO',
  END_AUDIO: 'END_GET_USER_MEDIA_AUDIO',
}
 
/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  // ice connection state
  updateLocalIceConnectionState: ['value'],
  updateRemoteIceConnectionState: ['value'],

  // ice gathering state
  updateLocalIceGatheringState: ['value'],
  updateRemoteIceGatheringState: ['value'],
  addLocalIceCandidate: ['value'],
  addRemoteIceCandidate: ['value'],
  clearLocalIceCandidates: [],

  // session description
  updateLocalDescription: ['value'],
  updateRemoteDescription: ['value'],

  // webrtc.getUserMedia function is expensive
  // we track it to make sure we are not abusing it
  updateLocalGetUserMediaState: ['value'],
  updateRemoteGetUserMediaState: ['value'],

  // custom actions that is sole purpose is to log
  // the code flow
  updateLocalFlowState: ['value'],
  updateRemoteFlowState: ['value'],

  // reset
  webrtcReset: null,
})

export const WebrtcTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  local: {
    iceConnectionState: null,
    iceGatheringState: null,
    iceCandidates: Immutable([]),
    description: null,
    getUserMediaState: null,
    flowState: null,
  },
  remote: {
    iceConnectionState: null,
    iceGatheringState: null,
    iceCandidates: Immutable([]),
    description: null,
    getUserMediaState: null,
    flowState: null,
  }
})

/* ------------- Reducers ------------- */

// ice connection state
export const updateLocalIceConnectionState = (state, { value }) =>
  state.setIn(['local', 'iceConnectionState'], value)

export const updateRemoteIceConnectionState = (state, { value }) =>
  state.setIn(['remote', 'iceConnectionState'], value)

// ice gathering state
export const updateLocalIceGatheringState = (state, { value }) =>
  state.setIn(['local', 'iceGatheringState'], value)

export const updateRemoteIceGatheringState = (state, { value }) =>
  state.setIn(['remote', 'iceGatheringState'], value)

export const addLocalIceCandidate = (state, { value }) =>
  state.setIn(['local', 'iceCandidates'], state.local.iceCandidates.concat([value]))

export const clearLocalIceCandidates = (state, {}) =>
  state.setIn(['local', 'iceCandidates'], Immutable([]))

export const addRemoteIceCandidate = (state, { value }) =>
  state.setIn(['remote', 'iceCandidates'], state.remote.iceCandidates.concat([value]))

// session description
export const updateLocalDescription = (state, { value }) =>
  state.setIn(['local', 'description'], value)

export const updateRemoteDescription = (state, { value }) =>
  state.setIn(['remote', 'description'], value)

export const updateLocalGetUserMediaState = (state, { value }) =>
  state.setIn(['local', 'getUserMediaState'], value)

export const updateRemoteGetUserMediaState = (state, { value }) =>
  state.setIn(['remote', 'getUserMediaState'], value)

export const updateLocalFlowState = (state, { value }) =>
  state.setIn(['local', 'flowState'], value)

export const updateRemoteFlowState = (state, { value }) =>
  state.setIn(['remote', 'flowState'], value)

const reset = () => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  // ice connection state
  [Types.UPDATE_LOCAL_ICE_CONNECTION_STATE]: updateLocalIceConnectionState,
  [Types.UPDATE_REMOTE_ICE_CONNECTION_STATE]: updateRemoteIceConnectionState,
  
  // ice gathering state
  [Types.UPDATE_LOCAL_ICE_GATHERING_STATE]: updateLocalIceGatheringState,
  [Types.UPDATE_REMOTE_ICE_GATHERING_STATE]: updateRemoteIceGatheringState,
  [Types.ADD_LOCAL_ICE_CANDIDATE]: addLocalIceCandidate,
  [Types.ADD_REMOTE_ICE_CANDIDATE]: addRemoteIceCandidate,
  [Types.CLEAR_LOCAL_ICE_CANDIDATES]: clearLocalIceCandidates,

  // session description
  [Types.UPDATE_LOCAL_DESCRIPTION]: updateLocalDescription,
  [Types.UPDATE_REMOTE_DESCRIPTION]: updateRemoteDescription,
  
  // WebRTC.getUserMedia()
  [Types.UPDATE_LOCAL_GET_USER_MEDIA_STATE]: updateLocalGetUserMediaState,
  [Types.UPDATE_REMOTE_GET_USER_MEDIA_STATE]: updateRemoteGetUserMediaState,

  // code flow state
  [Types.UPDATE_LOCAL_FLOW_STATE]: updateLocalFlowState,
  [Types.UPDATE_REMOTE_FLOW_STATE]: updateRemoteFlowState,

  // reset
  [Types.WEBRTC_RESET]: reset,
  [UserTypes.LOGOUT]: reset,
})

/* ------------- Selectors ------------- */
