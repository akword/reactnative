import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { merge, compose, uniq, without } from 'ramda'

import log from 'commons/Services/Log'

import { UserTypes } from './UserRedux'

import {
  deleteItemSuccessReducer,
  createToggleFilterIdReducer,
  clearToggleFilterIdReducer
} from '../Lib/Redux/reducers'
import { BASE_STATE_API_RESPONSE, baseActionsReadApi, baseApiReadReducerInit } from '../Lib/Redux/CRUD'
import createAPIPackage from '../Lib/Redux/createAPIPackage'

export const REDUX_CONFIG = {
  statePrefix: 'mailbox',
  reducerPrefix: 'MAILBOX_',
  apiDataKey: 'emailevents',
  apiDataIndex: 'id'
}

export const MSG_STATE = {
  SEND_QUEUED: 36
}

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  ...baseActionsReadApi(REDUX_CONFIG.statePrefix),

  mailboxAutoUpdateRequest: null,
  mailboxAutoUpdateSuccess: ['data'],
  mailboxAutoUpdateFailure: ['error'],

  // Fetch details of an email
  mailboxDetailRequest: ['id'],
  mailboxDetailSuccess: ['data'],
  mailboxDetailFailure: ['error', 'id'],

  // Fetch analytics data of an email
  mailboxAnalyticsRequest: ['id'],
  mailboxAnalyticsSuccess: ['data', 'id'],
  mailboxAnalyticsFailure: ['error', 'id'],

  // Send mail
  sendMailRequest: ['payload', 'resolve', 'reject'],
  sendMailSuccess: null,
  sendMailError: ['error'],
  sendQueuedMailSuccess: null,

  // Actions for mailbox events
  // In success action, we want the id so that the redux store can be updated

  mailboxReadRequest: ['id'],
  mailboxReadSuccess: ['id'],

  mailboxUnreadRequest: ['id'],
  mailboxUnreadSuccess: ['id'],

  mailboxArchiveRequest: ['id'],
  mailboxArchiveSuccess: ['id'],

  mailboxUnarchiveRequest: ['id'],
  mailboxUnarchiveSuccess: ['id'],

  mailboxMoveToInboxRequest: ['id'],
  mailboxMoveToInboxSuccess: ['id'],

  mailboxTrashRequest: ['id'],
  mailboxTrashSuccess: ['id'],

  mailboxDeleteRequest: ['id'],
  mailboxDeleteSuccess: ['id'],

  mailboxClearTrashRequest: null,
  mailboxClearTrashSuccess: null,

  mailboxSendQueuedRequest: ['id'],
  mailboxSendQueuedSuccess: ['id'],

  mailboxSaveAttachment: ['id', 'attachmentIndex'],

  // multi-select functionality
  mailboxSelectEmail: ['id'],
  mailboxUnselectEmail: ['id'],
  mailboxClearSelection: null,
  mailboxTrashSelectedRequest: ['resolve', 'reject'],
  mailboxTrashSelectedSuccess: ['selectedIds'],
  mailboxArchiveSelectedRequest: ['resolve', 'reject'],
  mailboxArchiveSelectedSuccess: ['selectedIds'],
  mailboxReadSelectedRequest: ['resolve', 'reject'],
  mailboxReadSelectedSuccess: ['selectedIds'],
  mailboxUnreadSelectedRequest: ['resolve', 'reject'],
  mailboxUnreadSelectedSuccess: ['selectedIds'],
  mailboxDeleteSelectedRequest: ['resolve', 'reject'],
  mailboxDeleteSelectedSuccess: ['selectedIds'],

  // A common action for all mailbox update errors
  mailboxUpdateError: ['error'],

  setMailboxFilter: ['filter'],
  clearMailboxFilter: null,

  toggleMailboxIdentityFilter: ['id'],
  clearMailboxIdentityFilter: null,

  toggleMailboxDomainFilter: ['id'],
  clearMailboxDomainFilter: null,

  clearAllMailboxRelationFilters: null,

  toggleUnreadFilter: null,

  setCustomComposeContactEmail: ['email'],
  clearCustomComposeContactEmail: null,

  drawerTotalsOptimisticUpdate: ['originalAction'],
})

const { APITypes, APICreators, APIState, APIReducers, APITypesToSagas } = createAPIPackage(
  'mailbox', [
    {
      type: 'drawerTotals',
      endpoint: 'MailboxDrawerTotals',
      successReducer: (state, action) => state.setIn(['drawerTotals'], action.data)
    }
  ]
)

export const MailboxTypes = merge(Types, APITypes)
export const MailboxCreators = merge(Creators, APICreators)
export const MailboxAPITypesToSagas = APITypesToSagas
export default MailboxCreators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  ...BASE_STATE_API_RESPONSE,
  ...APIState,

  orderBy: 'id',

  filterName: null,
  filterIdentityIds: [],
  filterDomainIds: [],
  unreadOnlyFilter: false,
  searchQuery: null,

  customComposeContactEmail: null,
  numQueuedOutbound: null,

  selectedIds: [],
  selectedUpdateInProgress: false,
})

/* ------------- Reducers ------------- */

export const mailboxAutoUpdateRequest = state => state.merge({
  dataRequestInProgress: true,
  dataRequestSuccessful: false,
  dataRequestError: null,
  isRefreshing: false,
  isPaginating: false,
  isSearching: false
})

export const mailboxAutoUpdateFailure = (state, { error }) => state.merge({
  dataRequestInProgress: false,
  dataRequestSuccessful: false,
  dataRequestError: error,
  isRefreshing: false,
  isPaginating: false,
  isSearching: false
})

export const mailboxAutoUpdateSuccess = (state, { data }) => {
  let _data = {}
  let _dataOrder = []

  data.emailevents.map(e => {
    _data[e.id] = e
    _dataOrder.push(e.id)
  })

  _dataOrder = _dataOrder.reverse()

  const numQueuedOutbound = data.num_queued_outbound
  const dataTotalCount = state.dataTotalCount + Math.min(data.total, 30)
  const autoUpdateDataPendingCount = Math.max(0, data.total - 30)

  return state.merge({
    dataRequestInProgress: false,
    dataRequestSuccessful: true,
    dataRequestError: null,
    isRefreshing: false,
    isPaginating: false,
    isSearching: false,
    numQueuedOutbound: numQueuedOutbound,
    dataTotalCount: dataTotalCount,
    autoUpdateDataPendingCount: autoUpdateDataPendingCount,
    data: (state.data || Immutable({})).merge(_data),
    dataOrder: Immutable(uniq((_dataOrder || []).concat(state.dataOrder || [])))
  })
}

// Reducers - mailbox detail

export const detailRequest = (state, { id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'
  return state.setIn([key, id, 'detailStatus'], { inProgress: true, error: null })
}

export const detailSuccess = (state, { data }) => {
  const key = (state.searchResultsData && state.searchResultsData[data.msg_id]) ? 'searchResultsData' : 'data'

  return state
    .setIn([key, data.msg_id, 'detailStatus'], { inProgress: false, error: null })
    .setIn([key, data.msg_id, 'detail'], data)
}

export const detailError = (state, { error, id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'
  return state.setIn([key, id, 'detailStatus'], { inProgress: false, error: error })
}

// Reducers - mailbox analytics

export const analyticsRequest = (state, { id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'
  return state.setIn([key, id, 'analyticsStatus'], { inProgress: true, error: null })
}

export const analyticsSuccess = (state, { data, id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'

  return state
    .setIn([key, id, 'analyticsStatus'], { inProgress: false, error: null })
    .setIn([key, id, 'analytics'], data.analytics)
}

export const analyticsError = (state, { error, id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'
  return state.setIn([key, id, 'analyticsStatus'], { inProgress: false, error: error })
}

// Reducers - Mailbox update

export const mailboxReadSuccess = (state, { id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'

  if (state.unreadOnlyFilter) {
    state = deleteItemSuccessReducer(state, { id })
  }

  return state.setIn([key, id, 'is_read'], true)
}

export const mailboxUnreadSuccess = (state, { id }) => {
  const key = (state.searchResultsData && state.searchResultsData[id]) ? 'searchResultsData' : 'data'
  return state.setIn([key, id, 'is_read'], false)
}

// multi-select functionality
export const mailboxSelectEmail = (state, { id }) => {
  const idIndex = state.selectedIds.indexOf(id)
  if (idIndex !== -1) {
    return state
  }
  return state.setIn(['selectedIds'], state.selectedIds.concat([id]))
}

export const mailboxUnselectEmail = (state, { id }) =>
  state.setIn(['selectedIds'], Immutable(without([id], state.selectedIds)))

export const mailboxClearSelection = (state) =>
  state
    .setIn(['selectedIds'], Immutable([]))
    .setIn(['selectedUpdateInProgress'], false)

export const selectedUpdateRequest = state =>
  state.setIn(['selectedUpdateInProgress'], true)

// Reducers - mailbox folder filters

export const setMailboxFilter = (state, { filter }) => state.set('filterName', filter === 'inbox' ? null : filter)
export const clearMailboxFilter = state => state.set('filterName', null)

export const toggleMailboxIdentityFilter = createToggleFilterIdReducer('filterIdentityIds')
export const clearMailboxIdentityFilter = clearToggleFilterIdReducer('filterIdentityIds')

export const toggleMailboxDomainFilter = createToggleFilterIdReducer('filterDomainIds')
export const clearMailboxDomainFilter = clearToggleFilterIdReducer('filterDomainIds')

const clearAllMailboxRelationFilters = compose(clearMailboxIdentityFilter, clearMailboxDomainFilter)

export const toggleUnreadFilter = state => state.set('unreadOnlyFilter', !state.unreadOnlyFilter)

export const setCustomComposeContactEmail = (state, { email }) => state.set('customComposeContactEmail', email)
export const clearCustomComposeContactEmail = state => state.set('customComposeContactEmail', null)

const clearTrashSuccess = state => {
  if (state.filterName !== 'trash') return state

  return state.merge({
    searchResultsData: {},
    searchResultsDataOrder: [],
    searchResultsDataTotalCount: 0
  })
}

const incrementTotal = (state, totalKey) => {
  if (!state.drawerTotals) return state
  const total = state.drawerTotals[totalKey] + 1
  return state.setIn(['drawerTotals', totalKey], total)
}

const decrementTotal = (state, totalKey) => {
  if (!state.drawerTotals) return state
  let total = state.drawerTotals[totalKey] - 1
  total = total > 0 ? total : 0
  return state.setIn(['drawerTotals', totalKey], total)
}

const drawerTotalsOptimisticUpdate = (state, { originalAction }) => {
  const keys = {
    sent: 'total_is_sent',
    trash: 'total_is_trash',
    unread: 'total_is_unread',
    queued: 'total_is_2factor',
    forwarded: 'total_is_forward',
    archive: 'total_is_archive'
  }

  if (!originalAction) return state

  const { type } = originalAction

  if (type === Types.SEND_MAIL_REQUEST) {
    if (state.filterName === 'queued') {
      return incrementTotal(decrementTotal(state, keys.sent), keys.queued)
    }
    return incrementTotal(state, keys.sent)
  }

  if (type === Types.MAILBOX_READ_REQUEST && state.data[originalAction.id]) {
    return decrementTotal(state, keys.unread)
  }

  if (type === Types.MAILBOX_UNREAD_REQUEST && state.data[originalAction.id]) {
    return incrementTotal(state, keys.unread)
  }

  if (type === Types.MAILBOX_ARCHIVE_REQUEST) {
    const filterKey = state.filterName
    if (filterKey) {
      return incrementTotal(decrementTotal(state, keys[filterKey]), keys.archive)
    }
    return incrementTotal(state, keys.archive)
  }

  if (type === Types.MAILBOX_TRASH_REQUEST) {
    const filterKey = state.filterName
    if (filterKey) {
      return incrementTotal(decrementTotal(state, keys[filterKey]), keys.trash)
    }
    return incrementTotal(state, keys.trash)
  }

  if (type === Types.MAILBOX_DELETE_REQUEST) {
    return decrementTotal(state, keys[state.filterName])
  }

  if (type === Types.MAILBOX_SEND_QUEUED_REQUEST) {
    return incrementTotal(decrementTotal(state, keys.queued), keys.sent)
  }

  if (type === Types.MAILBOX_CLEAR_TRASH_SUCCESS) {
    return state.setIn(['drawerTotals', 'total_is_trash'], 0)
  }

  return state
}

// Reducer - mailbox reset

const reset = () => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

const extraTopLevelMap = {
  num_queued_outbound: 'numQueuedOutbound'
}

const BASE_REDUCERS_READ_API = baseApiReadReducerInit(
  REDUX_CONFIG.reducerPrefix, Types, REDUX_CONFIG.apiDataKey,
  REDUX_CONFIG.apiDataIndex, extraTopLevelMap
)

export const reducer = createReducer(INITIAL_STATE, {
  ...BASE_REDUCERS_READ_API,
  ...APIReducers,

  [Types.MAILBOX_AUTO_UPDATE_REQUEST]: mailboxAutoUpdateRequest,
  [Types.MAILBOX_AUTO_UPDATE_SUCCESS]: mailboxAutoUpdateSuccess,
  [Types.MAILBOX_AUTO_UPDATE_FAILURE]: mailboxAutoUpdateFailure,

  [Types.MAILBOX_DETAIL_REQUEST]: detailRequest,
  [Types.MAILBOX_DETAIL_SUCCESS]: detailSuccess,
  [Types.MAILBOX_DETAIL_FAILURE]: detailError,

  [Types.MAILBOX_ANALYTICS_REQUEST]: analyticsRequest,
  [Types.MAILBOX_ANALYTICS_SUCCESS]: analyticsSuccess,
  [Types.MAILBOX_ANALYTICS_FAILURE]: analyticsError,

  [Types.MAILBOX_READ_REQUEST]: mailboxReadSuccess,
  [Types.MAILBOX_UNREAD_REQUEST]: mailboxUnreadSuccess,
  [Types.MAILBOX_ARCHIVE_REQUEST]: deleteItemSuccessReducer,
  [Types.MAILBOX_UNARCHIVE_REQUEST]: deleteItemSuccessReducer,
  [Types.MAILBOX_MOVE_TO_INBOX_REQUEST]: deleteItemSuccessReducer,
  [Types.MAILBOX_TRASH_REQUEST]: deleteItemSuccessReducer,
  [Types.MAILBOX_DELETE_REQUEST]: deleteItemSuccessReducer,
  [Types.MAILBOX_SEND_QUEUED_REQUEST]: deleteItemSuccessReducer,
  [Types.MAILBOX_CLEAR_TRASH_SUCCESS]: clearTrashSuccess,

  [Types.MAILBOX_SELECT_EMAIL]: mailboxSelectEmail,
  [Types.MAILBOX_UNSELECT_EMAIL]: mailboxUnselectEmail,
  [Types.MAILBOX_CLEAR_SELECTION]: mailboxClearSelection,
  [Types.MAILBOX_TRASH_SELECTED_REQUEST]: selectedUpdateRequest,
  [Types.MAILBOX_ARCHIVE_SELECTED_REQUEST]: selectedUpdateRequest,
  [Types.MAILBOX_READ_SELECTED_REQUEST]: selectedUpdateRequest,
  [Types.MAILBOX_UNREAD_SELECTED_REQUEST]: selectedUpdateRequest,
  [Types.MAILBOX_DELETE_SELECTED_REQUEST]: selectedUpdateRequest,

  [Types.SET_MAILBOX_FILTER]: setMailboxFilter,
  [Types.CLEAR_MAILBOX_FILTER]: clearMailboxFilter,

  [Types.TOGGLE_MAILBOX_IDENTITY_FILTER]: toggleMailboxIdentityFilter,
  [Types.CLEAR_MAILBOX_IDENTITY_FILTER]: clearMailboxIdentityFilter,

  [Types.TOGGLE_MAILBOX_DOMAIN_FILTER]: toggleMailboxDomainFilter,
  [Types.CLEAR_MAILBOX_DOMAIN_FILTER]: clearMailboxDomainFilter,

  [Types.CLEAR_ALL_MAILBOX_RELATION_FILTERS]: clearAllMailboxRelationFilters,

  [Types.TOGGLE_UNREAD_FILTER]: toggleUnreadFilter,

  [Types.SET_CUSTOM_COMPOSE_CONTACT_EMAIL]: setCustomComposeContactEmail,
  [Types.CLEAR_CUSTOM_COMPOSE_CONTACT_EMAIL]: clearCustomComposeContactEmail,

  [Types.DRAWER_TOTALS_OPTIMISTIC_UPDATE]: drawerTotalsOptimisticUpdate,

  [UserTypes.LOGOUT]: reset
})

/* ------------- Selectors ------------- */
