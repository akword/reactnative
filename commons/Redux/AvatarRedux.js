import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

import { UserTypes } from './UserRedux'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setEmailAvatar: ['email', 'data'],
})

export const AvatarTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  // Keep track of checked emails and domains
  checkedEmails: {},
  checkedDomains: {},

  // Cache avatar data for emails and domains
  emails: {},
  domains: {}
})

/* ------------- Reducers ------------- */

export const setEmailAvatar = (state, { email, data }) => {
  let _state = state.setIn(['checkedEmails', email], true)
  if (data) {
    _state = _state.setIn(['emails', email], data)
  }
  return _state
}

const reset = () => INITIAL_STATE

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_EMAIL_AVATAR]: setEmailAvatar,

  [UserTypes.LOGOUT]: reset
})

/* ------------- Selectors ------------- */
