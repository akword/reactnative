import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

import { createItemSuccessReducer } from '../Lib/Redux/reducers'
import { UserTypes } from './UserRedux'

import generateDevice from 'commons/Lib/GenerateDevice'

const { Types, Creators } = createActions({
  createDeviceRequest: ['payload', 'resolve', 'reject'],
  createDeviceSuccess: ['data']
})

export const DeviceTypes = Types
export default Creators

const device = generateDevice()
export const INITIAL_STATE = Immutable(device)

const reset = () => INITIAL_STATE

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CREATE_DEVICE_REQUEST]: createItemSuccessReducer(),
  [UserTypes.LOGOUT]: reset
})
