import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { merge, path, uniq, findIndex, sortBy, reverse, without } from 'ramda'

import {
  BASE_STATE_API_RESPONSE,
  baseActionsReadApi, baseActionsWriteApi,
  baseApiReadReducerInit, baseApiWriteReducerInit
} from '../Lib/Redux/CRUD'
import createAPIPackage from '../Lib/Redux/createAPIPackage'
import {
  createItemSuccessReducer,
  createUpdateItemSuccessReducer,
  createDataSuccessReducer
} from '../Lib/Redux/reducers'
import { getContactMember, getUnreadMessagesCount } from 'commons/Selectors/Messaging'
import { uuidv1ToDate } from 'commons/Lib/Utils'

import { getActiveChatRoomId } from 'app/Lib/Chat'

import { AppTypes } from './AppRedux'
import { UserTypes } from './UserRedux'

export const MESSAGE_STATUS = {
  SENT: 'chat_message_sent',
  PENDING: 'chat_message_pending',
  ERROR: 'chat_message_error',
}

export const REDUX_CONFIG = {
  statePrefix: 'chat',
  reducerPrefix: 'CHAT_',
  apiDataKey: 'rooms',
  apiDataIndex: 'room_id'
}

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  ...baseActionsReadApi(REDUX_CONFIG.statePrefix),
  ...baseActionsWriteApi(REDUX_CONFIG.statePrefix),

  // Text chat actions

  // Initialise chat
  chatInit: null,

  // Chat key handshake is successful
  chatKeyHandshakeSuccessful: null,

  // Chat socket disconnected
  chatSocketConnecting: null,
  chatSocketConnected: null,
  chatSocketDisconnected: null,
  resetChatSocketConnectionState: null,

  chatAutoJoinedRoom: ['data'],

  // Create room
  chatCreateRoomRequest: ['identityEmail', 'identityName', 'contactEmail'],
  chatCreateRoomSuccess: ['data'],
  chatCreateRoomError: ['error'],

  // Join a room
  chatJoinRoomRequest: ['roomId', 'identityEmail'],
  chatJoinRoomSuccess: ['data'],
  chatJoinRoomError: ['error'],

  // Leave a room
  chatLeaveRoomRequest: ['roomId'],
  chatLeaveRoomSuccess: ['data'],
  chatLeaveRoomError: ['error'],

  // update a room settings
  chatUpdateRoomSettingsRequest: ['roomId', 'settings'],
  chatUpdateRoomSettingsSuccess: ['data', 'settings'],
  chatUpdateRoomSettingsError: ['error'],

  chatRefreshRoomRequest: ['roomId'],
  chatRefreshRoomSuccess: ['data'],
  chatRefreshRoomError: ['error'],

  // Send message on a room
  chatSendMessageRequest: ['roomId', 'message', 'isE2EE'],
  chatSendMessageSuccess: ['data', 'message'],
  chatSendMessageError: ['error'],

  chatSendFileRequest: ['roomId', 'data', 'onProgress'],

  // Fetch messages on room
  chatGetMessagesRequest: ['roomId', 'paginate', 'paginateNew'],
  chatGetMessagesSuccess: ['data', 'paginateNew'],
  chatGetMessagesError: ['error'],

  // Received a message on a room
  chatMessageReceived: ['data', 'isE2EE'],
  chatMessageModified: ['data', 'isE2EE'],
  chatMessageChangeId: ['roomId', 'messageId', 'newMessageId', 'isE2EE'],

  // User starts/stops typing
  chatStartedTyping: ['roomId'],
  chatStoppedTyping: ['roomId'],

  // Other user starts/stops typing
  chatOtherUserStartedTyping: ['roomId', 'member'],
  chatOtherUserStoppedTyping: ['roomId', 'member'],

  // Ack a message (mark as read)
  chatAckMessage: ['roomId', 'messageId', 'isE2EE'],

  chatPeekNewMessageCountRequest: ['roomId', 'lastReadMessageId'],
  chatPeekNewMessageCountSuccess: ['data'],

  // file
  chatSaveMessageFile: ['roomId', 'messageId'],

  chatSetupExistingRoom: ['roomId', 'identityEmail', 'identityName', 'contactEmail'],

  // Video chat actions

  // new
  // Make an outbound video call offer
  makeOutboundVideoCallOffer: ['identityEmail', 'contactEmail', 'contactDisplayName', 'audioOnly'],

  // Answer/reject an inbound video call offer
  // Sagas for this should just pick the current call data from store
  // instead of passing it around again
  answerInboundVideoCallOffer: null,
  rejectVideoCall: ['shouldEndCall'],

  // Received just the inbound video call id
  inboundVideoCallIdReceived: ['callId', 'isColdLaunch'],

  // Received an inbound video call offer
  inboundVideoCallOfferReceived: ['data', 'isColdLaunch'],

  // Received an answer to outbound video call offer
  outboundVideoCallAnswerReceived: ['data'],

  // Outbound call was rejected
  outboundVideoCallRejected: ['data'],

  // Peer in WebRTC toggled video on/off (packet received)
  announceVideoCallRemoteVideoToggle: ['data'],
  setVideoCallRemoteToggle: ['data'],

  // Answered on our device or another
  announceVideoCallAnswered: ['data'],

  localVideoFeedReady: ['url'],

  setVideoCallLocalId: ['id'],

  setAudioOnly: ['value'],
  setRemoteAudioOnly: ['value'],
  setOutboundCallId: ['id'],

  // This is fired when we have everything for the call and are ready to go
  beginVideoCall: ['url'],

  // End the call and clear state
  endVideoCall: ['callId', 'iceState', 'isOutboundCancel'],

  // End an outbound call that has not been answered yet
  cancelOutboundCall: null,

  toggleVideoCallCamera: null,
  toggleVideoCallMic: null,
  toggleVideoCallSpeaker: null,
  setVideoCallCameraMuteState: ['value'],
  setVideoCallMicMuteState: ['value'],
  setVideoCallSpeakerMuteState: ['value'],
  setVideoCallColdLaunchState: ['value'],

  displayInboundCall: null,
  hideInboundCall: null,

  setRoomsMap: ['data'],

  uiAnswerInboundCall: null,
  uiRejectInboundCall: null,

  // FIXME: UI specific
  endChatCall: ['payload'],

  // Track `iceConnectionState` === 'connected'
  setCallIsConnectedState: ['value'],

  // iOS only
  setAudioSessionActivated: ['value']
})

const { APITypes, APICreators, APIState, APIReducers, APITypesToSagas } = createAPIPackage(
  'chat', [
    { type: 'sendInvite', endpoint: 'ChatSendInvite', getTranslatedError: true },
    { type: 'canMemberChat', endpoint: 'ChatMember', getTranslatedError: true }
  ]
)

export const ChatTypes = merge(Types, APITypes)
export const ChatCreators = merge(Creators, APICreators)
export const ChatAPITypesToSagas = APITypesToSagas
export default ChatCreators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  ...BASE_STATE_API_RESPONSE,
  ...APIState,

  socketConnecting: false,
  socketConnected: false,
  socketDown: false,

  // Keep track of existing rooms with
  // identity - contact email
  // `${room.member_email}__${contact.email}` => true
  roomsMap: {},

  outboundVideoCallData: null,
  outboundVideoCallAnswer: null,
  announceVideoCallAnswered: null,
  announceVideoCallRemoteVideoToggle: null,
  inboundVideoCallOffer: null,
  isAudioOnly: null,
  isRemoteAudioOnly: null,
  videoCallLocalFeedURL: null,
  videoCallRemoteFeedURL: null,
  videoCallStartTime: null,
  videoCallCameraIsMuted: false,
  videoCallMicIsMuted: false,
  videoCallSpeakerIsMuted: false, // by default speaker is not on
  videoCallLocalId: null,
  videoCallRemoteToggle: null, // remote peer toggling video
  inboundCallUIVisible: false,
  callIsConnected: false,
  isColdLaunch: false,
})

/* ------------- Reducers ------------- */

const getKey = (isE2EE) => isE2EE ? 'e2ee' : 'regular'

const reset = () => INITIAL_STATE

const chatJoinRoomSuccess = (state, { data }) => {
  if (path(['data', data.room_id], state)) {
    return createUpdateItemSuccessReducer('room_id')(state, { data: {
      ...data,
      joined: true,
      membersTyping: {},
      isLoadingMessages: false,
      noMoreMessagesAvailable: false
    }})
  }

  return createItemSuccessReducer('room_id')(state, {
    data: {
      ...data,
      joined: true,
      regular: {
        messages: {},
        messageIds: [],
      },
      e2ee: {
        messages: {},
        messageIds: [],
      },
      membersTyping: {},
      isLoadingMessages: false,
      noMoreMessagesAvailable: false
    }
  })
}

const chatLeaveRoomRequest = (state, { roomId }) => state.setIn(['data', roomId, 'isLeavingRoom'], true)

const chatLeaveRoomSuccess = (state, { data }) => {
  const room = state.data[data.room_id]
  const contact = getContactMember(room)
  return state
  // remove the id from the dataOrder
  .set('dataOrder', state.dataOrder.filter((roomId) => roomId !== data.room_id))
  // remove the room from data
  .merge({ data: state.data.without(data.room_id) })
}

const chatLeaveRoomError = (state, { roomId }) => state.setIn(['data', roomId, 'isLeavingRoom'], false)

const chatUpdateRoomSettingsSuccess = (state, { data, settings }) => {
  const room = state.data[data.room_id]
  return state.setIn(['data', data.room_id], room.merge(settings))
}

const chatGetMessagesRequest = (state, { roomId }) => state.setIn(['data', roomId, 'isLoadingMessages'], true)

const chatGetMessagesError = (state, { roomId }) => state.setIn(['data', roomId, 'isLoadingMessages'], false)

const chatGetMessagesSuccess = (state, { data, paginateNew }) => {
  const existingMessages = path(['data', data.room_id, 'regular', 'messages'], state) || Immutable([])
  const existingMessageIds = path(['data', data.room_id, 'regular', 'messageIds'], state) || Immutable([])

  if (!paginateNew && (!data.messages || !data.messages.length)) {
    return state
      .setIn(['data', data.room_id, 'noMoreMessagesAvailable'], true)
      .setIn(['data', data.room_id, 'isLoadingMessages'], false)
  }

  const newMessageIds = data.messages.map(m => m.message_id)
  newMessageIds.reverse()
  let combinedMessageIds = paginateNew
    ? existingMessageIds.concat(newMessageIds)
    : newMessageIds.concat(existingMessageIds)

  combinedMessageIds = uniq(combinedMessageIds)

  const newMessages = {}
  data.messages.map(m => {
    newMessages[m.message_id] = m
  })

  let _state = state
    .setIn(['data', data.room_id, 'regular', 'messageIds'], combinedMessageIds)
    .setIn(['data', data.room_id, 'regular', 'messages'], merge(existingMessages, newMessages))
    .setIn(['data', data.room_id, 'isLoadingMessages'], false)

  if (paginateNew && data.messages.length) {
    const activeChatRoomId = getActiveChatRoomId()
    const lastReadMessageId = path(['data', data.room_id, 'last_read_message_id'], _state)
    if (
      activeChatRoomId !== data.room_id &&
      path(['data', data.room_id], _state) &&
      lastReadMessageId
    ) {
      _state = _state.setIn(['data', data.room_id, 'regular', 'unreadCount'], getUnreadMessagesCount(lastReadMessageId, combinedMessageIds))
    }
  }

  return _state
}

const chatRefreshRoomSuccess = (state, { data }) => createUpdateItemSuccessReducer('room_id')(state, {
  data: { ...(data.rooms[0]), joined: true, e2ee: null }
})

const chatMessageReceived = (state, { data, isE2EE }) => {
  const key = getKey(isE2EE)

  // if message is already in the room then ignore
  // because existing messages should be modified
  // only via CHAT_MESSAGE_MODIFIED action
  const existingMessage = path(['data', data.room_id, key, 'messages', data.message_id], state)
  if (existingMessage) {
    return state
  }

  const existingMessageIds = path(['data', data.room_id, key, 'messageIds'], state) || Immutable([])
  const combinedMessageIds = uniq(existingMessageIds.concat([data.message_id]))

  const existingMessages = path(['data', data.room_id, key, 'messages'], state) || Immutable([])
  const combinedMessages = merge(existingMessages, { [data.message_id]: data })

  let _state = state
    .setIn(['data', data.room_id, key, 'messages'], combinedMessages)
    .setIn(['data', data.room_id, key, 'messageIds'], combinedMessageIds)
    .setIn(['data', data.room_id, key, 'last_message_id'], data.message_id)
    .setIn(['data', data.room_id, 'last_message_id'], data.message_id)

  // increment the unread count for the room
  // in case if room is active, the CHAT_ACK_MESSAGE action
  // should be dispatched immidiately, turning unreadCount to 0
  // so active room does not show unreadCount to user
  const currentCount = path(['data', data.room_id, key, 'unreadCount'], state) || 0
  _state = _state.setIn(['data', data.room_id, key, 'unreadCount'], currentCount + 1)

  if (_state.dataOrder[0] !== data.room_id) {
    const roomIdIndex = findIndex(r => r === data.room_id, _state.dataOrder)
    if (roomIdIndex > -1) {
      _state = _state.set('dataOrder', [
        data.room_id,
        ...(_state.dataOrder.slice(0, roomIdIndex)),
        ...(_state.dataOrder.slice(roomIdIndex + 1))
      ])
    }
  }

  return _state
}

const chatMessageModified = (state, { data, isE2EE }) => {
  const key = getKey(isE2EE)
  const currentMessageData = path(['data', data.room_id, key, 'messages', data.message_id], state)

  // if there is no message found at that place then nothing to modify
  if (!currentMessageData) {
    return state
  }

  return state
    .setIn(['data', data.room_id, key, 'messages', data.message_id], currentMessageData.merge(data))
}

const chatMessageChangeId = (state, { roomId, messageId, newMessageId, isE2EE }) => {
  const key = getKey(isE2EE)

    // if message does not exist then can't do a thing
    const message = path(['data', roomId, key, 'messages', messageId], state)
    if (!message) {
      return state
    }

    // replace the id in ids array
    let existingMessageIds = path(['data', roomId, key, 'messageIds'], state) || Immutable([])
    existingMessageIds = existingMessageIds.asMutable()
    const messageIdIndex = existingMessageIds.indexOf(messageId)
    if (messageIdIndex === -1) {
      return state
    }
    existingMessageIds.splice(messageIdIndex, 1, newMessageId)
    const modifiedMessageIds = Immutable(existingMessageIds)

    // set old id to null and assign old message to a new id
    const mutableMessage = message.asMutable()
    const existingMessages = path(['data', roomId, key, 'messages'], state) || Immutable({})
    const modifiedMessages = existingMessages.merge({
      [messageId]: null,
      [newMessageId]: {
        ...mutableMessage,
        message_id: newMessageId,
      }
    })

    let _state = state
      .setIn(['data', roomId, key, 'messages'], modifiedMessages)
      .setIn(['data', roomId, key, 'messageIds'], modifiedMessageIds)

    return _state
}

const chatOtherUserStartedTyping = (state, { roomId, member }) => state.setIn(
  ['data', roomId, 'membersTyping', member], true
)

const chatOtherUserStoppedTyping = (state, { roomId, member }) => state.setIn(
  ['data', roomId, 'membersTyping', member], false
)

const chatAckMessage = (state, { roomId, messageId, isE2EE }) => {
  const key = getKey(isE2EE)
  if (!isE2EE) {
    state = state.setIn(['data', roomId, 'last_read_message_id'], messageId)
  }
  return state
    .setIn(['data', roomId, key, 'unreadCount'], 0)
}

const chatPeekNewMessageCountSuccess = (state, { data }) => state.setIn(
  ['data', data.room_id, 'regular', 'unreadCount'], data.total
)

const chatAutoJoinedRoom = (state, { data }) => {
  const _state = state
  .setIn(['data', data.room_id], data)
  .setIn(['dataOrder'], Immutable([data.room_id]).concat(state.dataOrder))

  const contact = getContactMember(data)
  return _state.setIn(['roomsMap', `${data.member_email}__${contact.email}`], data.room_id)
}

/* ------------- Hookup Reducers To Types ------------- */

const BASE_REDUCERS_READ_API = baseApiReadReducerInit(
    REDUX_CONFIG.reducerPrefix, Types,
    REDUX_CONFIG.apiDataKey, REDUX_CONFIG.apiDataIndex
)
const BASE_REDUCERS_WRITE_API = baseApiWriteReducerInit(
    REDUX_CONFIG.reducerPrefix, Types
)

const displayInboundCallUI = state => state.set('inboundCallUIVisible', true)
const hideInboundCallUI = state => state.set('inboundCallUIVisible', false)

// New video call stuff
const makeOutboundVideoCallOffer =
  (state, { identityEmail, contactEmail, contactDisplayName, audioOnly }) => state
    .set('outboundVideoCallData', { identityEmail, contactEmail, contactDisplayName })
    .set('videoCallCameraIsMuted', audioOnly)

const localVideoFeedReady = (state, { url }) => state.set('videoCallLocalFeedURL', url)

const beginVideoCall = (state, { url }) => state.merge({
  videoCallRemoteFeedURL: url,
  videoCallStartTime: new Date()
})

const endVideoCall = state => state.merge({
  outboundVideoCallData: null,
  outboundVideoCallAnswer: null,
  inboundVideoCallOffer: null,
  videoCallLocalFeedURL: null,
  videoCallRemoteFeedURL: null,
  videoCallStartTime: null
})

// Piggy back on createDataSuccessReducer and populate roomsMap
const chatSuccess = (state, payload) => {
  let _state = createDataSuccessReducer('rooms', 'room_id')(state, payload)

  const sortedRoomIds = sortBy(id => uuidv1ToDate(_state.data[id].last_message_id || _state.data[id].room_id), _state.dataOrder)
  _state = _state.set('dataOrder', reverse(sortedRoomIds))

  return _state
}

const chatCreateRoomSuccess = (state, payload) => {
  let _state = createItemSuccessReducer('room_id')(state, payload)
  const contact = getContactMember(payload.data)
  return _state.setIn(['roomsMap', `${payload.data.member_email}__${contact.email}`], payload.data.room_id)
}

// iOS only

const setAudioSessionActivated = (state, { value }) => state.set('iOSAudioSessionActivated', !!value)

export const reducer = createReducer(INITIAL_STATE, {
  ...BASE_REDUCERS_READ_API,
  ...BASE_REDUCERS_WRITE_API,
  ...APIReducers,

  [Types.CHAT_SOCKET_CONNECTED]: state => state.merge({
    socketConnected: true,
    socketDown: false,
    socketConnecting: false
  }),

  [Types.CHAT_SOCKET_DISCONNECTED]: state => state.merge({
    socketConnected: false,
    socketDown: true,
    socketConnecting: false
  }),

  [Types.RESET_CHAT_SOCKET_CONNECTION_STATE]: state => state.merge({
    socketConnected: false,
    socketDown: false,
    socketConnecting: false
  }),

  [Types.CHAT_SOCKET_CONNECTING]:  state => state.set('socketConnecting', true),

  [Types.SET_CALL_IS_CONNECTED_STATE]:  (state, { value }) => state.set('callIsConnected', value),

  [Types.INBOUND_VIDEO_CALL_ID_RECEIVED]: (state, { callId, isColdLaunch }) => state.set('isColdLaunch', isColdLaunch),
  [Types.INBOUND_VIDEO_CALL_OFFER_RECEIVED]: (state, { data }) => state.set('inboundVideoCallOffer', data),
  [Types.OUTBOUND_VIDEO_CALL_ANSWER_RECEIVED]: (state, { data }) => state.set('outboundVideoCallAnswer', data),
  [Types.SET_VIDEO_CALL_REMOTE_TOGGLE]: (state, { data }) => state.set('videoCallRemoteToggle', data),

  [Types.SET_VIDEO_CALL_CAMERA_MUTE_STATE]: (state, { value }) => state.set('videoCallCameraIsMuted', value),
  [Types.SET_VIDEO_CALL_MIC_MUTE_STATE]: (state, { value }) => state.set('videoCallMicIsMuted', value),
  [Types.SET_VIDEO_CALL_SPEAKER_MUTE_STATE]: (state, { value }) => state.set('videoCallSpeakerIsMuted', value),
  [Types.SET_VIDEO_CALL_COLD_LAUNCH_STATE]: (state, { value }) => state.set('isColdLaunch', value),
  [Types.SET_VIDEO_CALL_LOCAL_ID]: (state, { id }) => state.set('videoCallLocalId', id),
  [Types.SET_OUTBOUND_CALL_ID]: (state, { id }) => state.set('outboundCallId', id),
  [Types.SET_AUDIO_ONLY]: (state, { value }) => state.set('isAudioOnly', value),
  [Types.SET_REMOTE_AUDIO_ONLY]: (state, { value }) => state.set('isRemoteAudioOnly', value),
  [Types.SET_ROOMS_MAP]: (state, { data }) => state.set('roomsMap', data),
  [Types.BEGIN_VIDEO_CALL]: beginVideoCall,
  [Types.END_VIDEO_CALL]: endVideoCall,
  [Types.LOCAL_VIDEO_FEED_READY]: localVideoFeedReady,
  [Types.MAKE_OUTBOUND_VIDEO_CALL_OFFER]: makeOutboundVideoCallOffer,
  [Types.SET_AUDIO_SESSION_ACTIVATED]: setAudioSessionActivated,

  [Types.CHAT_SUCCESS]: chatSuccess,

  [Types.CHAT_MESSAGE_RECEIVED]: chatMessageReceived,
  [Types.CHAT_MESSAGE_MODIFIED]: chatMessageModified,
  [Types.CHAT_MESSAGE_CHANGE_ID]: chatMessageChangeId,
  [Types.CHAT_CREATE_ROOM_SUCCESS]: chatCreateRoomSuccess,
  // [Types.CHAT_SEND_MESSAGE_SUCCESS]: chatSendMessageSuccess,
  [Types.CHAT_JOIN_ROOM_SUCCESS]: chatJoinRoomSuccess,

  [Types.CHAT_AUTO_JOINED_ROOM]: chatAutoJoinedRoom,

  [Types.CHAT_LEAVE_ROOM_REQUEST]: chatLeaveRoomRequest,
  [Types.CHAT_LEAVE_ROOM_SUCCESS]: chatLeaveRoomSuccess,
  [Types.CHAT_LEAVE_ROOM_ERROR]: chatLeaveRoomError,
  [Types.CHAT_UPDATE_ROOM_SETTINGS_SUCCESS]: chatUpdateRoomSettingsSuccess,

  [Types.CHAT_REFRESH_ROOM_SUCCESS]: chatRefreshRoomSuccess,

  [Types.CHAT_GET_MESSAGES_REQUEST]: chatGetMessagesRequest,
  [Types.CHAT_GET_MESSAGES_SUCCESS]: chatGetMessagesSuccess,
  [Types.CHAT_GET_MESSAGES_ERROR]: chatGetMessagesError,

  [Types.CHAT_OTHER_USER_STARTED_TYPING]: chatOtherUserStartedTyping,
  [Types.CHAT_OTHER_USER_STOPPED_TYPING]: chatOtherUserStoppedTyping,

  [Types.CHAT_ACK_MESSAGE]: chatAckMessage,

  [Types.CHAT_PEEK_NEW_MESSAGE_COUNT_SUCCESS]: chatPeekNewMessageCountSuccess,

  [Types.DISPLAY_INBOUND_CALL]: displayInboundCallUI,
  [Types.HIDE_INBOUND_CALL]: hideInboundCallUI,

  [UserTypes.LOGOUT]: reset
})

/* ------------- Selectors ------------- */
