import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  applyLocale: ['locale'],

  newBuildAvailable: ['build'],

  webViewEmbedded: ['isWebView'],

  onBoardingComplete: null,

  cacheDBIsReady: null,

  setNativeAppNetworkState: ['networkState'],

  nativeAppStateChanged: ['newState'],

  checkNetworkState: null,

  scheduleConnectionTimeout: ['delay'],

  setConnectionTimeout: ['isTimeout']
})

export const AppTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  build: null,
  newBuild: null,
  isNewBuildAvailable: false,
  isWebViewEmbedded: false,
  isNativeAppNetworkOnline: true,
  nativeAppState: null,
  connectionTimeout: false
})

/* ------------- Reducers ------------- */

const newBuildAvailable = (state, { build }) => state.merge({
  isNewBuildAvailable: true,
  newBuild: build
})

const webViewEmbedded = (state, { isWebView }) => state.setIn(['isWebViewEmbedded'], isWebView)

const setNativeAppNetworkState = (state, { networkState }) => state.setIn(['isNativeAppNetworkOnline'], networkState)

const nativeAppStateChanged = (state, { newState }) => state.set('nativeAppState', newState)

const setConnectionTimeout = (state, { isTimeout }) => {
  return  state.set('connectionTimeout', !!isTimeout) 
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.NEW_BUILD_AVAILABLE]: newBuildAvailable,
  [Types.WEB_VIEW_EMBEDDED]: webViewEmbedded,
  [Types.SET_NATIVE_APP_NETWORK_STATE]: setNativeAppNetworkState,
  [Types.NATIVE_APP_STATE_CHANGED]: nativeAppStateChanged,
  [Types.SET_CONNECTION_TIMEOUT]: setConnectionTimeout
})

/* ------------- Selectors ------------- */
