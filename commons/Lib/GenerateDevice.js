import platform from 'platform'
import uuidv4 from 'uuid/v4'

import log from 'commons/Services/Log'

const generateDevice = () => {
  let base = {
    isReactNative: true,
    platform: platform,
    os_ver: platform.os.version,
    raw_agent: navigator.userAgent,
    uuid: uuidv4(),
    type: 5,
    webrtc: {
      is_webrtc_supported: null,
      has_microphone: null,
      has_camera: null,
      display_width: null,
      display_height: null
    }
  }
  return base
}

export default generateDevice
