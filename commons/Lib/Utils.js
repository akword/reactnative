import moment from 'moment'
import momentTimezones from 'moment-timezone' // eslint-disable-line no-unused-vars

/**
 * Truncates the passed string down the provided character length.
 * Appends the result with ellipsis.
 *
 * @param string
 * @param len
 * @return {String}
 */
function truncate (string, len) {
  if (string.length > len) {
    return string.substring(0, len) + '...'
  } else {
    return string
  }
}

/**
 * Converts an object with key-value pairs into a query string.
 *
 * @param params
 * @return {string}
 */
function makeQueryString (params) {
  return Object.keys(params)
    .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
    .join('&')
}

/**
 * Returns a capitalised version of the string
 *
 * @param string
 * @returns {string}
 */
function capitalize (string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
}

/**
 * Returns `HH:mm` formatted string if the timestamp is for
 * the same timezone in the local timezone, otherwise returns
 * the time ago string.
 *
 * @param dateTime
 * @param intl
 * @param timezone
 * @return {string}
 */
function timeAgo (dateTime, intl, timezone) {
  if (!dateTime) return ''

  const localDateTime = timezone ? getZoneTimeForUTC(dateTime, timezone) : getLocalTimeForUTC(dateTime)
  const today = moment(new Date()).hour(0).minutes(0).seconds(0)
  const yesterday = moment(new Date()).hour(0).minutes(0).seconds(0).subtract(1, 'days')

  if (localDateTime.diff(today) >= 0) {
    return intl ? intl.formatTime(localDateTime) : localDateTime.format('h:mm a')
  } else if (localDateTime.diff(today) < 0 && localDateTime.diff(yesterday) >= 0) {
    return intl ? intl.formatRelative(Date.now() - 1000 * 60 * 60 * 24) : 'yesterday'
  } else {
    return intl ? intl.formatDate(localDateTime) : localDateTime.format('l')
  }
}

/**
 * Returns the local time for a given UTC time.
 *
 * @param dateTime
 * @returns {number|Moment}
 */
function getLocalTimeForUTC (dateTime) {
  const offset = getTimezoneOffset()
  return moment.utc(dateTime).utcOffset(offset)
}

/**
 * Returns the timezone time for a given UTC time.
 *
 * @param dateTime
 * @param timezone
 * @returns {number|Moment}
 */
function getZoneTimeForUTC (dateTime, timezone) {
  if (!timezone) return getLocalTimeForUTC(dateTime)
  const offset = moment.tz.zone(timezone).parse(Date.UTC(dateTime))
  return moment(dateTime)[offset > 0 ? 'subtract' : 'add']({ minutes: Math.abs(offset) })
}

/**
 * Returns the timezone offset in minutes.
 *
 * @returns {number}
 */
function getTimezoneOffset () {
  return (-1 * (new Date()).getTimezoneOffset())
}

/**
 * Returns a random integer between start and end.
 *
 * @param start
 * @param end
 * @returns {number}
 */
function randRange (start, end) {
  return Math.floor(Math.random() * end) + start
}

/**
 * Returns the error message from the error object, if available;
 * otherwise the default message.
 *
 * @param e
 * @param defaultMessage
 * @return {string}
 */
function getError (e, defaultMessage = 'An error occurred') {
  return e && e.message && e.message !== 'null' ? e.message : defaultMessage
}

function getWindowHeight () {
  const body = document.body
  const html = document.documentElement
  return Math.max(
    body.offsetHeight,
    html.clientHeight,
    html.offsetHeight
  )
}

function getWindowWidth () {
  const body = document.body
  const html = document.documentElement
  return Math.max(
    body.offsetWidth,
    html.clientWidth,
    html.offsetWidth
  )
}

function chunkStr (str, n) {
  var res = []
  var i
  var len
  for (i = 0, len = str.length; i < len; i += n) {
    res.push(str.substr(i, n))
  }
  return res
}

function bytesToReadableStr (bytes) {
  const threshold = 1024
  if (Math.abs(bytes) < threshold) return `${bytes} B`
  const units = ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
  let i
  for (i = -1; Math.abs(bytes) >= threshold && i < units.length - 1; i += 1) {
    bytes = bytes / threshold
  }
  return `${bytes.toFixed()} ${units[i]}`
}

/**
 * Converts UUID v1 string to milliseconds.
 *
 * @param uuid_str
 * @returns {Number}
 */
function uuidv1ToTimeInt (uuid_str) {
  let uuid_arr = uuid_str.split('-')
  let time_str = [
    uuid_arr[2].substring(1),
    uuid_arr[1],
    uuid_arr[0]
  ].join('')
  return parseInt(time_str, 16)
}

/**
 * Convert UUID v1 string to Date object.
 *
 * @param uuid_str
 * @returns {Date}
 */
function uuidv1ToDate (uuid_str) {
  const int_time = uuidv1ToTimeInt(uuid_str) - 122192928000000000
  const int_millisec = Math.floor(int_time/10000)
  return new Date(int_millisec)
}

/**
 * User can choose a browser time as a timezone, the value
 * of which is an empty string. This function returns a moment
 * instance for it
 * @param {string} timezone The timezone value at state.user.timezone
 */
function userTimezoneToMoment (timezone) {
  return timezone ? moment.tz(timezone) : moment()
}

/**
 * Calculates the size of the file based on base64 string
 * @param {string} data The base64 string
 * @return {number} the filesize in bytes
 */
export const base64ToFilesize = (data = '') => {
  if (!data) {
    return 0
  }
  return Math.round(data.length * 3 / 4)
}

module.exports = {
  truncate,
  makeQueryString,
  timeAgo,
  getLocalTimeForUTC,
  getTimezoneOffset,
  randRange,
  getError,
  capitalize,
  getWindowHeight,
  getWindowWidth,
  chunkStr,
  bytesToReadableStr,
  userTimezoneToMoment,
  getZoneTimeForUTC,
  uuidv1ToDate,
  base64ToFilesize
}
