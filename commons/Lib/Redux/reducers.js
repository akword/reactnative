import Immutable from 'seamless-immutable'
import { uniq, without, omit, merge, path, findIndex, mapObjIndexed, isEmpty } from 'ramda'

import log from 'commons/Services/Log'

/**
 * A re-usable reducers for API requests.
 *
 * Updates the progress, successfu and error flags.
 *
 *
 *
 * @param state
 * @param requestType
 * @return {*}
 */
export const dataRequestReducer = (state, { requestType }) => {
  requestType = requestType || {}

  return state.merge({
    dataRequestInProgress: true,
    dataRequestSuccessful: false,
    dataRequestError: null,
    isRefreshing: requestType.isRefreshing || false,
    isPaginating: requestType.isPaginating || false,
    isSearching: requestType.isSearching || false
  })
}

/**
 * A factory for data success reducer.
 *
 * Reason for factory: This reducer needs to update the store with the
 * response data from the API and this responseDataKey is used to lookup the
 * relevant value out from the response.
 *
 * @param responseDataKey // The top level api result key
 * @param responseDataInnerKey // The per record unique key
 * @param extraTopLevelMap // Object describing the response key => state key
 *   mapping for extra top level values that should be extracted from response
 */
export const createDataSuccessReducer = (responseDataKey, responseDataInnerKey = 'id', extraTopLevelMap = {}) => (state, { data, requestType = {}, requestTypeOverride = {} }) => {
  // data and dataOrder variables to be populated from successful API response
  let _data = {}
  let _dataOrder = []

  // Pick out the request type information
  const { isRefreshing, isPaginating, isSearching } = {...requestType, ...requestTypeOverride}

  // Populate the data and dataOrder variables
  data[responseDataKey].map(item => {
    const innerKey = item[responseDataInnerKey]

    if (!innerKey) {
      console.error('INVALID responseDataInnerKey=' + responseDataInnerKey)
      return null
    }
    _dataOrder.push(innerKey)
    _data[innerKey] = item
  })

  // Update the state (common keys)
  state = state.merge({
    dataRequestInProgress: false,
    dataRequestSuccessful: true,
    dataRequestError: null,
    isRefreshing: false,
    isPaginating: false,
    isSearching: false,
    lastRequestTimestamp: data.req_ts
  })

  const topLevelMerge = {}

  mapObjIndexed((reduxKey, responseKey) => {
    topLevelMerge[reduxKey] = data[responseKey]
  }, extraTopLevelMap)

  if (!isEmpty(topLevelMerge)) {
    state = state.merge(topLevelMerge)
  }

  // Normal data fresh/re-fresh fetch
  // Overwrite existing data & dataOrder
  if ((isRefreshing || !isPaginating) && !isSearching) {
    return state.merge({
      data: Immutable(_data),
      dataOrder: Immutable(_dataOrder),
      dataTotalCount: data.total
    })

  // Search data fresh/re-fresh fetch
  } else if ((isRefreshing || !isPaginating) && isSearching) {
    return state.merge({
      searchResultsData: Immutable(_data),
      searchResultsDataOrder: Immutable(_dataOrder),
      searchResultsDataTotalCount: data.total
    })

  // Pagination - normal
  // Merge the data
  } else if (isPaginating && !isSearching) {
    return state.merge({
      data: (state.data || Immutable({})).merge(_data),
      dataOrder: Immutable(uniq((state.dataOrder || []).concat(_dataOrder || []))),
      dataTotalCount: data.total
    })

  // Pagination - searching
  } else if (isPaginating && isSearching) {
    return state.merge({
      searchResultsData: (state.searchResultsData || Immutable({})).merge(_data),
      searchResultsDataOrder: Immutable(uniq((state.searchResultsDataOrder || []).concat(_dataOrder || [])))
    })
  }

  return state
}

/**
 * Reusable data failure reducers.
 *
 * Updates the flags in store and the error value.
 *
 * @param state
 * @param error
 */
export const dataFailureReducer = (state, { error }) => state.merge({
  dataRequestInProgress: false,
  dataRequestSuccessful: false,
  dataRequestError: error,
  isRefreshing: false,
  isPaginating: false,
  isSearching: false
})

/**
 * Reusable reducer for setting the search query.
 *
 * Usage – to be used with an action that is dispatched
 * on every change in the search field value.
 *
 * @param state
 * @param query
 */
export const setSearchQueryReducer = (state, { query }) => state.set('searchQuery', query)

/**
 * Reusable reducer for setting search filters.
 *
 * @param state
 * @param filters
 */
export const setSearchFiltersReducer = (state, { filters }) => state.set('searchFilters', filters)

/**
 * Reusable reducer for clearing the search data (searchResultsData and searchResultsDataOrder).
 *
 * @param state
 */
export const clearSearchDataReducer = state =>
  state.merge({ searchResultsData: null, searchResultsDataOrder: null, searchQuery: null })

/**
 * Reusable reducer for item creation success.
 *
 * @param responseDataKey
 * @param responseDataInnerKey
 * @return {*}
 */
export const createItemSuccessReducer = (responseDataInnerKey = 'id') => (state, { data }) => {
  let key = 'data'
  let orderKey = 'dataOrder'
  let totalCountKey = 'dataTotalCount'

  if (state.searchResultsData && state.searchResultsDataOrder) {
    key = 'searchResultsData'
    orderKey = 'searchResultsDataOrder'
    totalCountKey = 'searchResultsDataTotalCount'
  }

  return state
    .setIn([key, data[responseDataInnerKey]], data)
    .set(orderKey, Immutable([data[responseDataInnerKey]]).concat(state[orderKey] || []))
    .set(totalCountKey, state[totalCountKey] + 1)
}

/**
 * Reusable reducer for item edit success.
 *
 * @param responseDataInnerKey
 * @return {*}
 */
export const editItemSuccessReducer = (responseDataInnerKey = 'id') => (state, { data }) => {
  let key = state.searchResultsData && state.searchResultsData[data[responseDataInnerKey]] ? 'searchResultsData' : 'data'
  return state.setIn([key, data[responseDataInnerKey]], data)
}

export const createUpdateItemSuccessReducer = key => (state, { data, requestPayload = {} }) => {
  const id = data[key] || requestPayload[key]
  const dataKey = state.searchResultsData && state.searchResultsData[id] ? 'searchResultsData' : 'data'
  const current = path([dataKey, id], state)
  const updated = merge(current, data)
  return state.setIn([dataKey, id], updated)
}

export const createItemSuccessAnyKey = (responseDataKey, stateKey) => (state, { data, requestType }) => {

  const resData = data[responseDataKey]
  return state.setIn([stateKey], resData)
}

export const updateItemSuccessReducer = (state, { data }) => {
  let dataKey = state.searchResultsData && state.searchResultsData[data.id] ? 'searchResultsData' : 'data'
  const current = path([dataKey, data.id], state)
  const updated = merge(current, data)
  return state.setIn([dataKey, data.id], updated)
}

// warning: broken
// todo: fix
// not in use anywhere
export const createUpdateItemAtKeySuccessReducer = (idKey = 'id', itemUpdateKey = null, responseKey = null) => (state, { data, requestPayload = {} }) => {
  let dataKey = state.searchResultsData && state.searchResultsData[data.id] ? 'searchResultsData' : 'data'

  let id = requestPayload[idKey] || data[idKey]
  if (!id) {
    console.error(`createUpdateItemAtKeySuccessReducer – id not found`)
    return state
  }

  if (!path([dataKey, id], state)) {
    console.error(`createUpdateItemAtKeySuccessReducer – unable to find data item for id ${id}`)
    return state
  }

  const response = responseKey ? data[responseKey] : data

  if (!response) {
    console.error(`createUpdateItemAtKeySuccessReducer – unable to find response value for key ${responseKey} for id ${data.id}`)
    return state
  }

  // If server returns an array, in case of GET request,
  // just set the array response on the key
  if (response instanceof Array) {
    return state.setIn([dataKey, id, itemUpdateKey], response)
  }

  const current = path([dataKey, id, itemUpdateKey], state)
  if (current instanceof Array) {
    const index = findIndex(m => m.id === response.id, current)
    if (index === -1) {
      return state.setIn([dataKey, id, itemUpdateKey], current.concat(Immutable([response])))
    } else {
      return state.setIn([dataKey, id, itemUpdateKey, index], response)
    }
  }

  const updated = merge(current, response)
  return state.setIn([dataKey, id, itemUpdateKey], updated)
}

/**
 * Reusable reducer for item deletion reducer.
 *
 * @param state
 * @param id
 * @param payload
 * @return {*}
 */
export const deleteItemSuccessReducer = (state, { id, payload }) => {
  id = id || payload.id

  let _state = state

  if (state.searchResultsData && state.searchResultsDataOrder && state.searchResultsData[id]) {
    _state = _state
      // .setIn(['searchResultsData', id], null)
      .merge({
        searchResultsDataOrder: without([id], state.searchResultsDataOrder),
        searchResultsDataTotalCount: state.searchResultsDataTotalCount - 1
      })
  }

  if (state.data && state.dataOrder && state.data[id]) {
    _state = _state
    // .setIn(['data', id], null)
    .merge({
      dataOrder: without([id], state.dataOrder),
      dataTotalCount: state.dataTotalCount - 1
    })
  }

  // Such reducers, like Domain, has validations and details key
  if (state.details && state.details[id]) {
    _state = _state.merge({
      details: omit([String(id)], state.details)
    })
  }

  if (state.validations && state.validations[id]) {
    _state = _state.merge({
      validations: omit([String(id)], state.validations)
    })
  }

  return _state
}

/**
 * Set the sort order.
 *
 * @param state
 * @param sortBy
 * @param orderBy
 */
export const setSortOrder = (state, { sortBy, orderBy }) => state.set('sortBy', sortBy).set('orderBy', orderBy)

/**
 * Toggle a filter id.
 *
 * @param key
 */
export const createToggleFilterIdReducer = key => (state, { id }) => {
  const index = state[key].indexOf(id)
  if (index === -1) {
    return state.set(key, state[key].concat([id]))
  } else {
    return state.set(key, Immutable([
      ...state[key].slice(0, index),
      ...state[key].slice(index + 1, state[key].length)
    ]))
  }
}

export const setIterationIdsReducer = (state, { id }) => {
  const dataOrder = state.searchResultsDataOrder || state.dataOrder
  if (!dataOrder || !dataOrder.length) return state

  const previousIndex = dataOrder.indexOf(parseInt(state.currentId) || state.currentId || 0)
  const currentIndex = dataOrder.indexOf(parseInt(id) || id)
  if (currentIndex === -1) return state

  let currentDirection = state.currentDirection || 1
  if (Math.abs(currentIndex - previousIndex) > 1) {
    currentDirection = currentIndex > previousIndex ? 1 : -1
  }

  let prevId = null
  let nextId = null

  if (currentIndex !== 0) {
    prevId = dataOrder[currentIndex - (1 * currentDirection)]
  }

  if (currentIndex !== (dataOrder.length - 1)) {
    nextId = dataOrder[currentIndex + (1 * currentDirection)]
  }

  return state.merge({
    nextId,
    prevId,
    currentDirection,
    currentId: id
  })
}

export const clearIterationIdsReducer = state => state.merge({ nextId: null, prevId: null })

/**
 * Clear the toggle filter ids.
 *
 * @param key
 */
export const clearToggleFilterIdReducer = key => state => state.set(key, Immutable([]))

/**
 * Auto update request.
 *
 * @param state
 */
export const autoUpdateRequestReducer = state => state.merge({
  dataRequestInProgress: true,
  dataRequestSuccessful: false,
  dataRequestError: null,
  isRefreshing: false,
  isPaginating: false,
  isSearching: false
})

/**
 * Auto update failure.
 *
 * @param state
 * @param error
 */
export const autoUpdateFailureReducer = (state, { error }) => state.merge({
  dataRequestInProgress: false,
  dataRequestSuccessful: false,
  dataRequestError: error,
  isRefreshing: false,
  isPaginating: false,
  isSearching: false
})

/**
 * Auto update success.
 *
 * @param idKey
 * @param responseKey
 * @returns {*}
 */
export const createAutoUpdateSuccessReducer = (responseKey, idKey = 'id') => (state, { data }) => {
  if (!data || !data[responseKey] || !data[responseKey].length) return state

  const _data = {}
  const _dataOrder = []

  data[responseKey].map(c => {
    _data[c[idKey]] = c
    _dataOrder.push(c[idKey])
  })

  return state.merge({
    dataRequestInProgress: false,
    dataRequestSuccessful: true,
    dataRequestError: null,
    isRefreshing: false,
    isPaginating: false,
    isSearching: false,
    dataTotalCount: data.total,
    data: (state.data || Immutable({})).merge(_data),
    dataOrder: Immutable(uniq(_dataOrder.concat(state.dataOrder || []))),
    lastRequestTimestamp: data.req_ts
  })
}
