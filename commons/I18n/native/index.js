import Mailbox from './Mailbox'
import Contact from './Contact'
import Dashboard from './Dashboard'
import ForwardAddress from './ForwardAddress'
import Crypto from './Crypto'
import Preferences from './Preferences'
import Snackbar from './Snackbar'
import Chat from './Chat'
import ChangePassword from './ChangePassword'
import Setting from './Setting'
import Auth from './Auth'

export default {
  Chat,
  ChangePassword,
  Contact,
  Crypto,
  Dashboard,
  ForwardAddress,
  Mailbox,
  Preferences,
  Snackbar,
  Setting,
  Auth
}
