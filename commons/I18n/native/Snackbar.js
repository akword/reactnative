import { defineMessages } from 'react-intl'

const ns = 'native.Snackbar'
const m = defineMessages({

  contactDeleted: {
    id: `${ns}.contact-deleted`,
    defaultMessage: 'Contact deleted',
  },

  couldntDeleteContact: {
    id: `${ns}.couldnt-delete-contact`,
    defaultMessage: 'Could not Delete the Contact',
  },

  deletingContact: {
    id: `${ns}.deleting-contact`,
    defaultMessage: 'Deleting contact',
  },
  forwardAddressDeleted: {
    id: `${ns}.forward-address-deleted`,
    defaultMessage: 'Forward address deleted',
  },
  couldntDeleteForwardAddress: {
    id: `${ns}.couldnt-delete-forward-address`,
    defaultMessage: 'Couldn\'t delete forward address',
  },
  connecting: {
    id: `${ns}.connecting`,
    defaultMessage: 'Connecting...',
  },
  socketDown: {
    id: `${ns}.socket-down`,
    defaultMessage: 'Socket down',
  },
  noItemsFound: {
    id: `${ns}.no-items-found`,
    defaultMessage: 'No items found...',
  },
  noSearchResults: {
    id: `${ns}.no-search-results`,
    defaultMessage: 'No search results...',
  },
  touchToReturnToCall: {
    id: `${ns}.touch-to-return-to-call`,
    defaultMessage: 'Touch to return to call',
  },
  areYouSure: {
    id: `${ns}.are-you-sure`,
    defaultMessage: 'Are you sure?',
  },
  changesWillBeDiscarded: {
    id: `${ns}.changes-will-discarded`,
    defaultMessage: 'Your changes will be discarded.',
  },
  establishCommunicationWithYourFriend: {
    id: `${ns}.establish-communication-with-your-friend`,
    defaultMessage: 'Without contact access, you won’t be able to establish secure communications with your friends.',
  },
  dontAllow: {
    id: `${ns}.dont-allow`,
    defaultMessage: 'Don\'t Allow'
  },
  allow: {
    id: `${ns}.allow`,
    defaultMessage: 'Allow'
  }
})

export default m
