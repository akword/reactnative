export default [
  ['en-US', 'English'],
  ['ar-SA', 'العربية'],
  ['zh-CN', '中国'],
  ['de-DE', 'Deutsch'],
  ['es-ES', 'Español'],
  ['fr-FR', 'Français'],
  ['hi-IN', 'हिंदी'],
  ['ja-JP', '日本語'],
  ['nl-NL', 'Nederlands'],
  ['pt-BR', 'Português'],
  ['ro-RO', 'Română'],
  ['ru-RU', 'Русский']
]

/*       http_pickup:
 *               base_uri: https://www.msgsafe.io/app/mailbox/read/
 */
