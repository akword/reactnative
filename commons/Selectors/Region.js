import React from 'react'

export const extractRegionDataFromUser = (userSlice, CountryLabel) => ({
  data: userSlice.data && userSlice.data.system_regions && userSlice.data.system_regions.map(r => ({
    value: r,
    label: <CountryLabel code={r} />
  }))
})

export const extractRegionListFromUser = userSlice => userSlice.data && userSlice.data.system_regions