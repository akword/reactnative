export const isRemoteCameraEnabled = (state) => {
  const { videoCallRemoteToggle } = state.chat
  return videoCallRemoteToggle && videoCallRemoteToggle.videoEnabled
}

export const isRemoteVideoPlaying = (state) => {
  const {
    callIsConnected,
    isRemoteAudioOnly,
    videoCallRemoteToggle,
  } = state.chat

  // if call is not connected there is no video to show
  if (!callIsConnected) {
    return false
  }

  if (callIsConnected) {
    // if call started as an audio only and camera was never
    // toggled, then there is still no video to show
    if (isRemoteAudioOnly && !videoCallRemoteToggle) {
      return false
    }

    // if camera was toggled, however it is disabled then there is
    // no video to show
    if (videoCallRemoteToggle && !isRemoteCameraEnabled(state)) {
      return false
    }
  }

  return true
}

export const isLocalVideoPlaying = (state) => {
  const { videoCallLocalFeedURL, videoCallCameraIsMuted } = state.chat
  if (!videoCallLocalFeedURL || videoCallCameraIsMuted) {
    return false
  }
  return true
}

export const getIdentityEmail = (state) => {
  const { outboundVideoCallData, inboundVideoCallOffer } = state.chat
  if (outboundVideoCallData) {
    return outboundVideoCallData.identityEmail
  }
  if (inboundVideoCallOffer) {
    return inboundVideoCallOffer.to_email
  }
}

export const getContactEmail = (state) => {
  const { outboundVideoCallData, inboundVideoCallOffer } = state.chat
  if (outboundVideoCallData) {
    return outboundVideoCallData.contactEmail
  }
  if (inboundVideoCallOffer) {
    return inboundVideoCallOffer.from_email
  }
}

export const getCallData = (state) => {
  const { outboundVideoCallData, inboundVideoCallOffer } = state.chat
  const data = {
    contactEmail: '',
    contactDisplayName: '',
    identityEmail: '',
    identityDisplayName: '',
  }

  if (outboundVideoCallData) {
    const callData = outboundVideoCallData
    data.identityEmail = callData.identityEmail
    data.identityDisplayName = callData.identityEmail
    data.contactEmail = callData.contactEmail
    data.contactDisplayName = callData.contactDisplayName
  }

  if (inboundVideoCallOffer) {
    const callData = inboundVideoCallOffer
    data.identityEmail = callData.to_email
    data.identityDisplayName = callData.to_email
    data.contactEmail = callData.from_email
    data.contactDisplayName = callData.from_email
  }

  return data
}

export const getCallId = (state) => {
  const {
    inboundVideoCallOffer,
    outboundVideoCallData,
    videoCallLocalId,
    outboundCallId,
  } = state.chat

  if (inboundVideoCallOffer && inboundVideoCallOffer.call_id) {
    return inboundVideoCallOffer.call_id
  }

  if (outboundCallId) {
    return outboundCallId
  }

  if (videoCallLocalId) {
    return videoCallLocalId
  }

  return null
}

export const isInboundCall = (state) => {
  const { inboundVideoCallOffer } = state.chat
  return Boolean(inboundVideoCallOffer)
}

export const isCallInProgress = (state) => {
  const { inboundVideoCallOffer, outboundVideoCallData } = state.chat
  return Boolean(inboundVideoCallOffer || outboundVideoCallData)
}

export const getLocalIceCandidates = (state) => {
  const { iceCandidates } = state.webtrc.local
  return iceCandidates.toObject()
}

export const getLocalIceGatheringState = (state) => {
  const { iceGatheringState } = state.webtrc.local
  return iceGatheringState
}

export const getLocalWebRTCState = (state) => {
  return state.webrtc.local
}