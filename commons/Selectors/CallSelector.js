export const getClientIdForServerId = (serverId) => (state) => {
  const clientIds = Object.keys(state.call.data)
  if (!clientIds.length) {
    return null
  }
  const call = clientIds.find((clientId) => {
    const item = state.call.data[clientId]
    return item.serverId === serverId
  })
  if (!call) {
    return null
  }
  return call.clientId
}
