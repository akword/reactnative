export const getContactByEmail = (email) => (state) => {
  const contacts = state.contact
  if (contacts.searchResultsData && contacts.searchResultsData[email]) {
    return contacts.searchResultsData[email]
  }
  return contacts.data[email]
}
