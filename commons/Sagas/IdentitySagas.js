import { put, call, select } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/IdentityRedux'
import { getError } from 'commons/Lib/Utils'
import ax from 'commons/Services/Analytics/index'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = {
      type: 'search_or',
      columns: [ 'identity_email', 'identity_display_name' ]
    }
    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.identityClearSearchData, [], {order: 'identity_last_activity_on'}
    )
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.identityRequest(requestType))
    const response = yield callAPI('Identities', payload)
    yield put(Actions.identitySuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch. Please try again.')
    yield put(Actions.identityFailure(err))
  }
}

export function * autoUpdateRequest () {
  const slice = yield select(s => s.identity)

  if (!slice.dataOrder || !slice.dataOrder.length) {
    yield call(fetch, {})
    return
  }

  if (!slice.lastRequestTimestamp) return

  const payload = {
    order: 'identity_last_activity_on',
    sort: 'desc',
    limit: 100,
    gt_req_ts: slice.lastRequestTimestamp
  }

  try {
    const response = yield callAPI('Identities', payload)
    yield put(Actions.identityAutoUpdateSuccess(response.data))
  } catch (e) {
    const err = getError(e, 'Failed to fetch the contacts. Please try again.')
    yield put(Actions.identityAutoUpdateFailure(err))
  }
}

export function * create ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('CreateIdentity', payload)
    ax.form.createSuccess(ax.EVENTS.IDENTITY_FORM, {
      'Region': payload.region
    })
    resolve(response.data)
    yield put(Actions.identityCreateSuccess(response.data))
  } catch (e) {
    ax.form.createError(ax.EVENTS.IDENTITY_FORM, e.message, e.code)
    reject(new SubmissionError({_error: getError(e, 'Failed to create. Please try again.')}))
  }
}

export function * edit ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('EditIdentity', payload)
    ax.form.editSuccess(ax.EVENTS.IDENTITY_FORM, {
      'Region': payload.region,
      'ID': payload.id,
      'Strip HTML': payload.strip_html,
      'Allow New Contacts': payload.auto_create_contact,
      'Forward': payload.http_pickup,
      'Encrypt ESP PGP': payload.encrypt_esp_pgp,
      'Encrypt ESP SMIME': payload.encrypt_esp_smime,
      'Forward Prefix Origin Flag': payload.country_flag_emoji,
      'Signature Reply': payload.include_signature_reply,
      'Signature Compose': payload.include_signature_compose
    })
    resolve()
    yield put(Actions.identityEditSuccess(response.data))
  } catch (e) {
    ax.form.editError(ax.EVENTS.IDENTITY_FORM, e.message, e.code)
    reject(new SubmissionError({_error: getError(e, 'Failed to update. Please try again.')}))
  }
}

export function * remove ({ payload, resolve, reject }) {
  try {
    const id = payload.id
    yield callAPI('DeleteIdentity', payload)
    ax.delete(ax.EVENTS.IDENTITY)
    yield put(Actions.identityRemoveSuccess(id))
    resolve()
  } catch (e) {
    log.error('Delete API request failed - ', e)
    reject(new SubmissionError({_error: getError(e, 'Failed to update. Please try again.')}))
  }
}
