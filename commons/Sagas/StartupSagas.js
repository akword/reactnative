import { select, call, put } from 'redux-saga/effects'

import log from 'commons/Services/Log'
import ax from 'commons/Services/Analytics/index'
import { isLoggedIn } from 'commons/Selectors/User'
import StartupActions from 'commons/Redux/StartupRedux'

import { bootstrapSession } from './UserSagas'
import { api } from './APISagas'

export function * startup () {
  try {
    const userData = yield select(s => s.user)
    const deviceData = yield select(s => s.device)
    api.setDevice(deviceData)

    if (isLoggedIn(userData)) {
      yield call(bootstrapSession)
      ax.service.identify(userData.data.mp)
      ax.service.people.set('Last Activity', new Date())
    }

    yield put(StartupActions.startupSuccess())
  } catch (e) {
    log.error(e.message)
  }
}
