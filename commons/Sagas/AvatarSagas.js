import { put, select, fork, call } from 'redux-saga/effects'
import { filter } from 'ramda'
import request from 'superagent'
import md5 from 'md5'

import { convertUint8ArrayToBase64Str } from 'commons/Lib/Encoding'
import AvatarActions from 'commons/Redux/AvatarRedux'

export function * processAvatarForContacts ({ data }) {
  if (!data || !data.contacts || !data.contacts.length) return

  const emails = data.contacts.map(c => c.email)
  yield fetchEmailAvatars({ data: emails })
}

export function * processAvatarForUserEmails ({ data }) {
  if (!data || !data.useremails || !data.useremails.length) return

  const emails = data.useremails.map(c => c.email)
  yield fetchEmailAvatars({ data: emails })
}

export function * processAvatarForIdentities ({ data }) {
  if (!data || !data.identities || !data.identities.length) return

  const emails = data.identities.map(i => i.email)
  yield fetchEmailAvatars({ data: emails })
}

export function * processAvatarForMailbox ({ data }) {
  if (!data || !data.emailevents || !data.emailevents.length) return

  const emails = data.emailevents.map(i => i.msg_from)
  yield fetchEmailAvatars({ data: emails })
}

const blobToDataURL = blob => new Promise((resolve, reject) => {
  const reader = new FileReader()
  reader.onload = function () {
    resolve(this.result)
  }

  reader.readAsDataURL(blob)
})

export function * processAvatarForChats ({ data }) {
  if (!data.rooms || !data.rooms.length) {
    return
  }
  const emails = data.rooms.reduce((emails, room) => {
    if (!room.members || !room.members.length) {
      return emails
    }
    return room.members.reduce((emails, member) => {
      if (emails.indexOf(member.email) === -1) {
        emails.push(member.email)
      }
      return emails
    }, emails)
  }, [])
  yield fetchEmailAvatars({ data: emails })
}

const fetchBase64FromUrl = url => new Promise((resolve, reject) => {
  const RNFetchBlob = require('react-native-fetch-blob').default
  RNFetchBlob.fetch('GET', url,
    {'Content-Type': 'application/octet;BASE64', 'Cache-Control': 'no-store'})
    .then((res) => {
      if (res.data.startsWith('404') || res.text().startsWith('404')) {
        resolve(null)
      } else {
        resolve(`data:image/jpeg;base64,${res.base64()}`)
      }
    })
    .catch((errorMessage, statusCode) => {
      resolve(null)
    })
})

export function * getAnEmailAvatar (email) {
  const isReactNative = yield select(s => s.device.isReactNative)
  try {
    if(email == 'no-reply@notify.msgsafe.com') {
      return null
    }
    if (isReactNative) {
      const res = yield call(fetchBase64FromUrl, `https://www.msgsafe.io/avatar/${md5(email)}?s=50&d=404`)
      return res || null
    } else {
      const req = request
        .get(`https://www.msgsafe.io/avatar/${md5(email)}?s=50&d=404`)
        .responseType('blob')
      const res = yield req
      return yield call(blobToDataURL, res.body) || null
    }
  } catch (e) {
    return null
  }
}

function * fetchAnEmailAvatar (email) {
  try {
    const imageB64 = yield call(getAnEmailAvatar, email)
    yield put(AvatarActions.setEmailAvatar(email, imageB64))
  } catch (e) {
    yield put(AvatarActions.setEmailAvatar(email, null))
  }
}

function * fetchEmailAvatars ({ data }) {
  const checkedEmails = yield select(s => s.avatar.checkedEmails)
  const uncheckedEmails = filter(e => !checkedEmails[e], data)

  for (let e of uncheckedEmails) {
    yield fork(fetchAnEmailAvatar, e)
  }
}

export function * fetchDomainAvatars ({ data }) {

}
