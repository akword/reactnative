import { delay } from 'redux-saga'
import { put, call } from 'redux-saga/effects'

import NotificationActions from 'commons/Redux/NotificationRedux'

export function * handleNotificationTimeout ({ timeout }) {
  if (typeof timeout !== 'number') {
    return
  }

  yield call(delay, timeout)
  yield put(NotificationActions.hideNotification())
}
