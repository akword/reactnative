import { put, select } from 'redux-saga/effects'

import { callAPI, api } from './APISagas'
import DeviceActions from 'commons/Redux/DeviceRedux'
import WebRTCDevice from 'app/Services/WebRTCDevice'
import log from 'commons/Services/Log'


export function * createDevice () {
  try {
    const d = yield select(s => s.device)

    // We have to set this here, instead of INITITAL_STATE,
    // since WebRTCDevice is async.
    const media = yield WebRTCDevice()

    // API specific payload
    const payload = {
      device_uuid: d.uuid,
      device_type: d.type,
      os_ver: d.os_ver,
      raw_agent: d.raw_agent,
      ...media
    }
    api.setDevice(payload)

    // fake api result
    yield put(DeviceActions.createDeviceSuccess(payload))

    // Startup WebRTC listener socket if supported
    if (media.is_webrtc_supported) {
      log.debug('webrtc-supported media=', media)
    } else {
      log.debug('webrtc-not-supported media=', media)
    }

  } catch (e) {
    log.debug('createDevice saga caught - ', e)
  }
}
