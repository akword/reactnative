import { put, call } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/CryptoRedux'
import UserEmailActions from 'commons/Redux/UserEmailRedux'
import ax from 'commons/Services/Analytics/index'

import { getError } from 'commons/Lib/Utils'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = {
      type: 'search',
      columns: [ 'fingerprint' ]
    }
    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.cryptoClearSearchData
    )
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.cryptoRequest(requestType))
    const response = yield callAPI('Crypto', payload)
    yield put(Actions.cryptoSuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch. Please try again.')
    yield put(Actions.cryptoFailure(err))
  }
}

export function * getKey ({ payload, resolve, reject }) {
  try {
    const new_payload = {
      filter: payload
    }
    const response = yield callAPI('Crypto', new_payload)
    typeof resolve === 'function' && resolve(response.data)
    yield put(Actions.cryptoGetKeySuccess(response.data))
  } catch (e) {
    yield put(Actions.cryptoFailure(e))
    // reject(new SubmissionError({_error: getError(e, 'Request Failed. Please try again.')}))
  }
}

export function * userEmailSmime ({ payload, resolve, reject }) {
  try {
    const r = yield callAPI('CryptoGenerateSmime', payload)
    ax.action(ax.EVENTS.FORWARD_ADDRESS, 'SMIME Request')
    resolve(r.data)
    yield put(Actions.cryptoUserEmailSmimeSuccess(r.data))

    // refresh useremail cache
    const upayload = {filter: { id: r.data.useremail_id }}
    const uresponse = yield callAPI('UserEmail', upayload)
    yield put(UserEmailActions.useremailSuccess(uresponse.data))

  } catch (e) {
    reject(new SubmissionError({_error: getError(e, 'Request Failed. Please try again.')}))
  }
}

/*
export function * create ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('CreateTemplate', payload)
    resolve()
    yield put(Actions.templateCreateSuccess(response.data))
  } catch (e) {
    reject(new SubmissionError({_error: getError(e, 'Failed to create. Please try again.')}))
  }
}

export function * edit ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('EditTemplate', payload)
    resolve()
    yield put(Actions.templateEditSuccess(response.data))
  } catch (e) {
    reject(new SubmissionError({_error: getError(e, 'Failed to update. Please try again.')}))
  }
}

export function * remove ({ payload }) {
  try {
    yield callAPI('DeleteTemplate', payload)
  } catch (e) {
    log.error('Delete contact API request failed - ', e)
  }
}
*/
