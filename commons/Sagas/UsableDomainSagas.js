import { put, call } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/UsableDomainRedux'
import { getError } from 'commons/Lib/Utils'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = {
      type: 'search',
      columns: [ 'name' ],
    }

    const filterConfig = []
    const basePayload = { filter: { is_active: true }, limit: 100 }
    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.usableDomainClearSearchData, filterConfig, basePayload
    )
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.usableDomainRequest(requestType))
    const response = yield callAPI('Domains', payload)
    yield put(Actions.usableDomainSuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch domains. Please try again.')
    yield put(Actions.usableDomainFailure(err))
  }
}
