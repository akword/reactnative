import { put, call, select, take } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import { getError } from 'commons/Lib/Utils'
import { isLoggedIn } from 'commons/Selectors/User'
import { getContactEmail, getIdentityEmail, getCallId, isInboundCall } from '../../Selectors/VideoChat'
import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import WebrtcActions, { WebrtcTypes } from '../../Redux/WebrtcRedux'
import log from 'commons/Services/Log'

import { chatAPI } from './index'
import { callAPI } from '../APISagas'
import { setupWebRTCForOutbound, setupWebRTCForInbound, receivedOutboundCallAnswer } from './WebRTC'

export function * answerInboundVideoCallOffer () {
  log.debug('answerInboundVideoCallOffer...')
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  const data = yield select(s => s.chat.inboundVideoCallOffer)
  if (!data) return
  let setupResult = yield call(setupWebRTCForInbound, data)
  if (!setupResult) {
    log.debug('answerInboundVideoCallOffer - setupWebRTCForInbound failed')
  }
  return setupResult
}

export function * rejectVideoCall ({shouldEndCall = true}) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  log.debug('** rejectVideoCall shouldEndCall - ', shouldEndCall)

  // This could have been called from inbound or outbound (cancel)
  let callId = null
  const data = yield select(s => s.chat.inboundVideoCallOffer)
  if (! data) {
    callId = yield select(s => s.chat.outboundCallId)
  } else {
    callId = data.call_id
  }
  if (! callId) {
    log.debug('* rejectVideoCall STATE ERROR - undable to determine callId')
    return
  }

  yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/webrtc/reject',
    args: {
      call_id: callId
    }
  })
  log.debug('* rejectVideoCall sent /webrtc/reject callId - ', callId)

  if (shouldEndCall) {
    log.debug('* rejectVideoCall calling endVideoCall callId - ', callId)
    yield put(Actions.endVideoCall(callId))
  }
}

export function * outboundVideoCallAnswerReceived ({ data }) {
  yield call(receivedOutboundCallAnswer, data)
}

export function * announceCallEvent (action) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }
  const callId = yield select(getCallId)
  const res = yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/webrtc/announce/event',
    args: {
      call_id: callId,
      call_event_data: JSON.stringify(action),
    }
  })
}

export function * handleCallEvent (data) {
  const { call_id, call_event_data } = data
  const action = JSON.parse(call_event_data)

  // handle an array of actions
  if (Array.isArray(action)) {
    for (let i = 0; i < action.length; i += 1) {
      yield call(processCallEvent, call_id, action[i])
    }
  } else {
    yield call(processCallEvent, call_id, action)
  }
}

export function * processCallEvent (callId, action) {
  const { type, value } = action
  if (type === ChatTypes.CANCEL_OUTBOUND_CALL) {
    yield put(Actions.endVideoCall(callId))
    return
  }

  if (type === WebrtcTypes.UPDATE_LOCAL_ICE_CONNECTION_STATE) {
    yield put(WebrtcActions.updateRemoteIceConnectionState(value))
    return
  }

  if (type === WebrtcTypes.UPDATE_LOCAL_ICE_GATHERING_STATE) {
    yield put(WebrtcActions.updateRemoteIceGatheringState(value))
    return
  }

  if (type === WebrtcTypes.ADD_LOCAL_ICE_CANDIDATE) {
    yield put(WebrtcActions.addRemoteIceCandidate(value))
    return
  }

  yield call(console.log, 'unhandled call event!', action)
}