import { call, take } from 'redux-saga/effects'

import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'

import { chatAPI } from './index'

/**
 * Create contact.
 *
 * @param data
 */
export function * createContact (data) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      'cmd': '/contact/add',
      'args': data
    })
    console.log('/contact/add response – ', res)
  } catch (e) {}
}

/**
 * Update contact.
 *
 * @param data
 */
export function * updateContact (data) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      'cmd': '/contact/update',
      'args': data
    })
    console.log('/contact/update response – ', res)
  } catch (e) {}
}

/**
 * Delete contact.
 *
 * @param id
 */
export function * deleteContact (id) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      'cmd': '/contact/delete',
      'args': { contact_id: id }
    })
    console.log('/contact/delete response – ', res)
  } catch (e) {}
}

/**
 * Get all contacts.
 */
export function * getContacts (args) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      'cmd': '/contact/list',
      'args': args
    })
    console.log('/contact/list response – ', res)
    return res
  } catch (e) {}
}
