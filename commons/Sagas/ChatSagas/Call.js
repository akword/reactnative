import { put, call, takeEvery } from 'redux-saga/effects';
import uuidv1 from 'uuid/v1'

import { uuidv1ToDate } from 'commons/Lib/Utils'
import { ChatTypes } from 'commons/Redux/ChatRedux'
import CallActions, { CALL_STATUS } from 'commons/Redux/CallRedux'

export function * inboundCallReceived ({ data, isColdLaunch }) {
  const clientId = uuidv1()
  yield put(CallActions.create(clientId))
  yield put(CallActions.setIsColdLaunchState(clientId, isColdLaunch))
  yield put(CallActions.setIsIncomingState(clientId, true))
  yield put(CallActions.setSdp(clientId, data.sdp_offer))
  yield put(CallActions.setServerId(clientId, data.call_id))
  yield put(CallActions.setFromEmail(clientId, data.from_email))
  yield put(CallActions.setToEmail(clientId, data.to_email))

  let sdpOffer = {}
  try {
    sdpOffer = JSON.parse(data.sdp_offer)
  } catch (e) {
    log.error('Could not parse `sdp_offer`', e)
    return
  }

  if (sdpOffer && sdpOffer.offer) {
    yield put(CallActions.setSdp(clientId, sdpOffer.offer.sdp))
  }

  if (sdpOffer && sdpOffer.candidates && sdpOffer.candidates.length) {
    for (let i = 0; i < sdpOffer.candidates.length; i += 1) {
      yield put(CallActions.addRemoteRtcIceCandidate(clientId, sdpOffer.candidates[i]))
    }
  }

  if (sdpOffer.is_audio_only) {
    yield put(CallActions.turnOnLocalAudio(clientId))
    yield put(CallActions.turnOnRemoteAudio(clientId))
    yield put(CallActions.turnOffLocalVideo(clientId))
    yield put(CallActions.turnOffRemoteVideo(clientId))
    yield put(CallActions.turnOffSpeaker(clientId))
  } else {
    yield put(CallActions.turnOnLocalAudio(clientId))
    yield put(CallActions.turnOnRemoteAudio(clientId))
    yield put(CallActions.turnOnLocalVideo(clientId))
    yield put(CallActions.turnOnRemoteVideo(clientId))
    yield put(CallActions.turnOnSpeaker(clientId))
  }

  /**
   * @vivekagr
   * What does data.is_expired mean? 
   */
  if (data.is_expired) {
    yield put(CallActions.setStatus(clientId, CALL_STATUS.EXPIRED))
    return
  }
  
  /**
   * @vivekagr
   * What does data.status === false mean? 
   */
  if (data.status === false) {
    yield put(CallActions.setStatus(clientId, CALL_STATUS.INVALID))
    return
  }
  
  // We should always inspect the callId (timeuuid)
  // If there's a big discrepancy, it could be the local clock,
  // but it'd be worth issuing a webrtc/peek to verify the call isnt expired.
  const callInitiatedMSAgo = (new Date()) - uuidv1ToDate(data.call_id)
  if (callInitiatedMSAgo > 75000) {
    yield put(CallActions.setStatus(clientId, CALL_STATUS.EXPIRED))
    return
  }

  // yield put(CallActions.displayIncomingCallScreen(clientId))
}

const sagaRules = [
  takeEvery(ChatTypes.INBOUND_VIDEO_CALL_OFFER_RECEIVED, inboundCallReceived),
]

export default sagaRules