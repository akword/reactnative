import { delay } from 'redux-saga'
import { call, select, take, spawn } from 'redux-saga/effects'

import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import log from 'commons/Services/Log'

import { chatAPI, chatLeaveRoomRequest } from './index'
import { setupExistingRoom } from './NewRoom'
import { getMessagesRequest } from './Message'
import { refreshRoom } from './Room'
import { DATA_POLL_INTERVAL_MS } from './Constants'

export function * periodicallyRefreshRoomList () {
  // Get the latest available room list
  const roomListRes = yield call([chatAPI, chatAPI.sendRequest], {
    'cmd': '/room/list',
    'args': {}
  })

  if(!roomListRes)
    return

  const latestRooms = roomListRes.rooms || []
  const latestRoomIdsMap = {}
  latestRooms.map(r => { latestRoomIdsMap[r.room_id] = true })

  // Get current know room data from redux
  const currentRoomsData = yield select(s => s.chat)

  // if the chat state is not initiated yet then do nothing
  if (!currentRoomsData.data) {
    return
  }

  // Check latest rooms and find the ones that client isn't aware of
  for (let x of latestRooms) {
    if (!currentRoomsData.data[x.room_id]) {
      log.debug('periodicallyRefreshRoomList: found new room... ', x.room_id)
      yield call(setupExistingRoom, x.room_id, x.member_email)
    }
  }

  // Check current known rooms and remove the ones
  // not present in /room/list response
  for (let y of currentRoomsData.dataOrder) {
    if (!latestRoomIdsMap[y]) {
      log.debug('periodicallyRefreshRoomList: room lost... ', y)
      yield call(chatLeaveRoomRequest, { roomId: y })
    }
  }
}

/**
 * For periodically updating –
 *
 * - room list - new or lost rooms that client isn't aware of
 * - e2ee rooms - update room object (containing members)
 * - non-e2ee rooms - poll latest messages
 */
export function * periodicallyRefreshRoomState (delayInitially, onlyOnce) {
  if (!chatAPI.userSessionStarted) return

  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  if (delayInitially) {
    yield delay(DATA_POLL_INTERVAL_MS)
  }

  yield spawn(periodicallyRefreshRoomList)

  // If room data isn't there yet, wait for CHAT_SUCCESS
  let chatData = yield select(s => s.chat)
  if (!chatData.dataOrder) {
    yield take(ChatTypes.CHAT_SUCCESS)
    chatData = yield select(s => s.chat)
  }

  for (let roomId of chatData.dataOrder) {
    // Refresh the room
    yield spawn(refreshRoom, { roomId })
    // Get latest messages on it
    yield spawn(getMessagesRequest, { roomId, paginateNew: true })
  }

  // Wait 10s before next call
  yield delay(DATA_POLL_INTERVAL_MS)

  if (!onlyOnce) {
    yield spawn(periodicallyRefreshRoomState)
  }
}
