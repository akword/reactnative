import { put, call, spawn, select } from 'redux-saga/effects'

import m from 'commons/I18n/'
import { formatMessage } from 'commons/I18n/sagas'
import Actions from 'commons/Redux/ChatRedux'
import NotificationActions from 'commons/Redux/NotificationRedux'
import { getContactMember } from 'commons/Selectors/Messaging'

import { EVENT_TYPE, ROOM_UPDATE_TYPE } from './Constants'

import { chatAPI } from './index'
import { sendRoomEvent } from './Room'

export function * setupExistingRoom ({ roomId, identityEmail, identityName, contactEmail }) {
  const existingRoomInRedux = yield select(s => s.chat.data[roomId])
  if (existingRoomInRedux) return

  // Join the room
  const joinRes = yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/room/join',
    args: { room_id: roomId, member: identityEmail },
    push_type: EVENT_TYPE.MESSAGE_INLINE
  })

  // If the room doesn't exist, then just create a new room and be done
  if (joinRes.msg === 'ROOM_NOT_FOUND') {
    yield put(Actions.chatCreateRoomRequest(identityEmail, identityName, contactEmail))
    return
  }

  // Get the room details
  const roomGetRes = yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/room/get',
    args: { room_id: [roomId] }
  })

  // Dispatch auto joined room action
  if (roomGetRes && roomGetRes.rooms && roomGetRes.rooms.length) {
    const newRoom = roomGetRes.rooms[0]
    const contact = getContactMember(newRoom, identityEmail)
    yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/map/set',
      args: { room_id: roomId, member_map: `${identityEmail}__${contact.email}` }
    })

    yield put(Actions.chatAutoJoinedRoom({
      ...newRoom,
      member_email: identityEmail,
      joined: true,
      unreadCount: 0
    }))

    // Notify about new room join
    const message = yield formatMessage(m.app.Chat.joinedRoomBy, { contact: newRoom.owner_identity_email })

    yield put(NotificationActions.displayNotification(message, 'info', 3000))
  }

  // Make presence on the room known to other members
  yield spawn(sendRoomEvent, ROOM_UPDATE_TYPE.MEMBERSHIP_CHANGE, { roomId: roomId })
}
