import { call, select, take, put } from 'redux-saga/effects'
import { path } from 'ramda'
import uuidv1 from 'uuid/v1'

import Actions, { ChatTypes, MESSAGE_STATUS } from 'commons/Redux/ChatRedux'

import { chatAPI } from './index'
import { sendE2EEMessage } from './E2EE'

/**
 * Get messages on a room.
 *
 * @param roomId
 * @param paginate
 * @param paginateNew
 */
export function * getMessagesRequest ({ roomId, paginate, paginateNew }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  const args = { room_id: roomId }

  if (paginate || paginateNew) {
    const messageIds = yield select(s => path(['chat', 'data', roomId, 'regular', 'messageIds'], s))
    if (messageIds && messageIds.length) {
      if (paginate) args['lt_message_id'] = messageIds[0]
      if (paginateNew) args['gt_message_id'] = messageIds[messageIds.length - 1]
    }
  }

  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/message/list',
      args: args
    })

    yield put(Actions.chatGetMessagesSuccess(res, paginateNew))
  } catch (e) {
    yield put(Actions.chatGetMessagesError(e))
  }
}

/**
 * Acknowledge that a message on a channel has been read.
 *
 * @param roomId
 * @param messageId
 */
export function * ackMessage ({ roomId, messageId, isE2EE }) {
  // the message last read logic is handled locally
  // since there could not be a shared state between
  // devices for e2ee chat
  if (isE2EE) {
    return
  }

  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    yield call([chatAPI, chatAPI.sendRequestWithoutResponse], {
      'cmd': '/message/ack',
      'args': { room_id: roomId, message_id: messageId }
    })
  } catch (e) {}
}

/**
 * Send message on a channel.
 *
 * @param roomId
 * @param message
 * @param isE2EE
 */
export function * sendMessageRequest ({ roomId, message, isE2EE }) {
  const seqId = uuidv1()
  const room = yield select(s => s.chat.data[roomId])
  const data = {
    body: message,
    message_id: seqId,
    room_id: roomId,
    user_from: room.member_email,
    is_file: false,
    status: MESSAGE_STATUS.PENDING,
  }
  // go ahead and push the message to redux store
  // so user sees it instantly
  yield put(Actions.chatMessageReceived(data, isE2EE))

  // wait for chat api if not ready yet
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_SUCCESS)
  }

  // If end-to-end-encrypted
  if (isE2EE) {
    yield call(sendE2EEMessage, { roomId, message })
    return
  }

  try {
    // if contact left the room, autojoin him before sending a message
    const contact = room.history.find(item => item.email !== room.member_email)
    if (!contact.is_joined) {
      yield call([chatAPI, chatAPI.sendRequest], {
        cmd: '/room/invite',
        args: {
          room_id: roomId,
          member_email: contact.email,
          override_identity_email: room.member_email
        }
      })
    }

    // send the message to chatd
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/message/add',
      args: {
        room_id: roomId,
        enc_msg: message,
        member: room.member_email
      }
    })

    // the response returns the real, database id of the message
    // so we update the id of the message that we already pushed to
    // redux store, so we are consistent with chatd
    yield put(Actions.chatMessageChangeId(res.room_id, seqId, res.message_id, false))

    // the message is considered sent if we get here, because
    // we got the response for `/message/add` event
    yield put(Actions.chatMessageModified({
      ...data,
      message_id: res.message_id,
      status: MESSAGE_STATUS.SENT,
    }, false))
    yield put(Actions.chatSendMessageSuccess(res, message))
  } catch (e) {
    yield put(Actions.chatSendMessageError(e))
  }
}

/**
 * Peek the total number of unread messages on a room.
 *
 * @param roomId
 * @param lastReadMessageId
 */
export function * peekNewMessageCountRequest ({ roomId, lastReadMessageId }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const response = yield call([chatAPI, chatAPI.sendRequest], {
      'cmd': '/message/list/peek',
      'args': { room_id: roomId, message_id: lastReadMessageId }
    })
    yield put(Actions.chatPeekNewMessageCountSuccess(response))
  } catch (e) {}
}
