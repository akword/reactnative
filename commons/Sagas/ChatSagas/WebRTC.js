import { eventChannel, delay } from 'redux-saga'
import { put, call, select, take, spawn, race } from 'redux-saga/effects'
import { filter, path, findIndex, remove, prepend } from 'ramda'
import sdpTransform from 'sdp-transform'

import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import WebrtcActions, {
  ICE_CONNECTION_STATE,
  GET_USER_MEDIA_STATE,
} from 'commons/Redux/WebrtcRedux'
import { 
  getLocalIceCandidates,
  getLocalIceGatheringState,
  getLocalWebRTCState,
} from 'commons/Selectors/VideoChat'

import log from 'commons/Services/Log'

import {
  areLibrariesAvailable, scanDevices, WebRTCLib,
  getNativeUserMedia, getNativeSourceIdByDirection,
  setNativeRemoteDescription, createNativeAnswer,
  setNativeLocalDescription, createNativeOffer,
  getNativeConstraints
} from 'app/Lib/WebRTC'
import { announceCallEvent } from './Video'

import { chatAPI } from './index'

const OUTBOUND_CALL_TIMEOUT_MS = 60000

// DEFAULT_ICE is TCP
const DEFAULT_ICE = {
// we need to fork react-native-webrtc for relay-only to work.
//  iceTransportPolicy: "relay",
  iceServers: [
    {
      urls: 'turn:turn.msgsafe.io:443?transport=tcp',
      username: 'a9a2b514',
      credential: '00163e7826d6'
    },
    // {
    //   urls: 'turn:turn.msgsafe.io:443?transport=tcp',
    //   username: 'a9a2b514',
    //   credential: '00163e7826d6'
    // },
    /* Native libraries DO NOT fail over correctly.
    {
      urls: 'turn:turn.msgsafe.io:443',
      username: 'a9a2b514',
      credential: '00163e7826d6'
    }
    */
  ]
}

const DEFAULT_ICE_UDP = {
  iceServers: [
    {
      urls: 'turn:turn.msgsafe.io:443',
      username: 'a9a2b514',
      credential: '00163e7826d6'
    },
  ]
}

const audioOnlyConstraints = {
  audio: true,
  video: false,
}


/* In prod we shouldnt bother with HOST sdp */
let ALLOW_HOST_SDP = false
if (process.env.NODE_ENV !== 'production') {
  ALLOW_HOST_SDP = false
}

// browser specific
const DEFAULT_BROWSER_CONSTRAINTS = {
  video: true,
  audio: true
}
//

export function * getLocalMedia () {
  const localMedia = {
    audioDevices: [],
    videoDevices: [],
    isCapable: false,
    haveScanned: false,
    haveAudioDevice: false,
    haveVideoDevice: false,
    isReactNative: yield select(s => s.device.isReactNative)
  }
  log.debug('getLocalMedia: initial localMedia -', localMedia)

  const libsAvailable = yield call(areLibrariesAvailable)
  if (!libsAvailable) return localMedia

  const devices = yield call(scanDevices)
  localMedia.haveScanned = true

  if (!localMedia || !localMedia.haveScanned || !devices || !devices.length) return localMedia

  for (let device of devices) {
    if (device.kind.startsWith('video')) {
      log.debug('found video device=', device)
      localMedia.haveVideoDevice = true
      localMedia.videoDevices.push(device)
    } else if (device.kind.startsWith('audio')) {
      log.debug('found audio device=', device)
      localMedia.haveAudioDevice = true
      localMedia.audioDevices.push(device)
    }
  }
  if (localMedia.haveAudioDevice || localMedia.haveVideoDevice) {
    localMedia.isCapable = true
  }

  return localMedia
}

// Saga channel for `onicecandidate` callback on peer
function createIceCandidateChannel (peer) {
  return eventChannel(emit => {
    const prevCallback = peer.onicecandidate
    peer.onicecandidate = e => {
      emit(e)
    }
    return () => { peer.onicecandidate = null }
  })
}

// Saga channel for `onaddstream` callback on peer
function createGotRemoteStreamChannel (peer) {
  return eventChannel(emit => {
    peer.onaddstream = e => emit(e)
    return () => { peer.onaddstream = null }
  })
}

// Saga channel for `onaddstream` callback on peer
function createIceConnectionStateChangeChannel (peer) {
  return eventChannel(emit => {
    peer.oniceconnectionstatechange = e => emit(e)
    return () => { peer.oniceconnectionstatechange = null }
  })
}

// Parses the candidate string
function parseCandidate (c) {
  if (!c) return
  try {
    const needle = 'candidate'
    const pos = c.indexOf(needle) + needle.length
    const flds = c.substr(pos).split(' ')
    // log.debug(flds)
    return {
      'component': flds[1],
      'type': flds[7],
      'protocol': flds[2],
      'address': flds[4],
      'port': flds[5],
      'priority': flds[3]
    }
  } catch (e) {
    log.debug('_parseCandidate caught e=', e)
  }
}

export function * setupWebRTC (processCall, processCallData, stream) {
  const isAudioOnly = yield select(s => s.chat.isAudioOnly)
  const isRemoteAudioOnly = yield select(s => s.chat.isRemoteAudioOnly)
  log.debug('in setupWebRTC... isAudioOnly=' + isAudioOnly + ' isRemoteAudioOnly=' + isRemoteAudioOnly)
  const localMedia = yield call(getLocalMedia)
  log.debug('WebRTC: localMedia - ', localMedia)
  // todo: handle if there's no local media

  if (! localMedia || ! localMedia.isCapable) {
    log.debug('WebRTC: not capable')
    return null
  }

  // browser specific
  if (!localMedia.isReactNative) {
    stream = yield call([navigator.mediaDevices, navigator.mediaDevices.getUserMedia], DEFAULT_BROWSER_CONSTRAINTS)
  }

  if (localMedia.isReactNative) {
    if (!stream) {
      // native specific
      // log.debug('localMedia.videoDevices -', localMedia.videoDevices)
      let constraints = yield call(getNativeConstraints, { video: true, audio: true })
      // log.debug('constraints -', constraints)
      // log.debug('constraints -', JSON.stringify(constraints))

      try {
        yield put(WebrtcActions.updateLocalGetUserMediaState(GET_USER_MEDIA_STATE.START_AUDIO_VIDEO))
        stream = yield call(getNativeUserMedia, constraints)
        yield put(WebrtcActions.updateLocalGetUserMediaState(GET_USER_MEDIA_STATE.END_AUDIO_VIDEO))
      } catch (e) {
        log.debug('getNativeUserMedia e -', e)
      }
      
      if (!stream) {
        log.debug('WebRTC: warning: failing over to audio only constraints')
        try {
          yield put(WebrtcActions.updateLocalGetUserMediaState(GET_USER_MEDIA_STATE.START_AUDIO))
          stream = yield call(getNativeUserMedia, audioOnlyConstraints)
          yield put(WebrtcActions.updateLocalGetUserMediaState(GET_USER_MEDIA_STATE.END_AUDIO))
        } catch (e) {
          log.debug('WebRTC: getNativeUserMedia (audioOnly) failed e -', e)
        }
      }
    }

    if (!stream) {
      log.debug('WebRTC: failed to establish stream. mic and video probably disabled')
      return null
    }

    log.debug('stream -', stream)
    // disable video tracks if set to audio only call
    if (isAudioOnly || isRemoteAudioOnly) {
      log.debug('* setupWebRTC disabing video tracks')
      const tracks = stream.getVideoTracks()
      let isEnabled = false
      tracks.map(t => t.enabled = isEnabled)
      yield put(Actions.setVideoCallMicMuteState(isEnabled))
    }
  }

  if (!stream) {
    log.debug('WebRTC: missing stream')
    return null
  }
  log.debug('WebRTC: stream - ', stream)

  let iceConfig = DEFAULT_ICE
  const isPrefUDP = yield select(s => s.user.data.pref_webrtc_udp)
  if (isPrefUDP === null || isPrefUDP === false) {
    // TCP (already default)
    log.debug('* WebRTC: TCP ice')
  } else if (isPrefUDP === true) {
    iceConfig = DEFAULT_ICE_UDP
    log.debug('* WebRTC: UDP ice')
  }

  const peer = new WebRTCLib.RTCPeerConnection(iceConfig)
  yield spawn(gatherIceCandidates, peer)

  // Uncaught TypeError: Cannot read property 'candidate' of null
  // means gathering is done on chrome
  // peer.onicecandidate = e => log.debug('onicecandidate - ', parseCandidate(e.candidate.candidate))
  if (processCall && processCallData) {
    yield spawn(processCall, peer, processCallData)
  }
  yield spawn(listenForIceConnectionStateChangeChannel, peer)
  yield spawn(listenForRemoteStream, peer)
  // peer.oniceconnectionstatechange = (...args) => log.debug('oniceconnectionstatechange - ', ...args)
  // peer.onaddstream = (...args) => log.debug('onaddstream - ', ...args)
  peer.addStream(stream)

  const isRN = yield select(s => s.device.isReactNative)
  let localStreamURL = isRN ? stream.toURL() : URL.createObjectURL(stream)
  yield put(Actions.localVideoFeedReady(localStreamURL))

  yield spawn(listenAndToggleCamera, stream)
  yield spawn(listenAndToggleViewFromRemoteCamera)
  yield spawn(listenAndToggleMic, stream)
  yield spawn(cleanupLocalStreamOnEnd, peer, stream)

  return peer
}

function * cleanupLocalStreamOnEnd (peer, stream) {
  yield take(ChatTypes.END_VIDEO_CALL)
  log.debug('in cleanupLocalStreamOnEnd...')

  const isRN = yield select(s => s.device.isReactNative)
  if (!isRN) return

  yield call([peer, peer.removeStream], stream)
  yield call([peer, peer.close])
  yield call([stream, stream.release])
}

function * cleanupRemoteStreamOnEnd (stream) {
  yield take(ChatTypes.END_VIDEO_CALL)
  log.debug('in cleanupRemoteStreamOnEnd...')

  const isRN = yield select(s => s.device.isReactNative)
  if (!isRN) return

  const tracks = yield call([stream, stream.getTracks])
  for (let track of tracks) {
    yield call([track, track.stop])
  }

  yield call([stream, stream.release])
}

export function * setupWebRTCForOutbound (identityEmail, contactEmail, audioOnly, stream) {
  log.debug('setupWebRTCForOutbound...')
  const isRN = yield select(s => s.device.isReactNative)
  const peer = yield call(setupWebRTC, processOutboundCall, { identityEmail, contactEmail }, stream)
  log.debug('setupWebRTCForOutbound peer -', peer)
  if (!peer) {
    log.debug('setupWebRTCForOutbound - setupWebRTC failed. invalid peer')
    return null
  }
  const createOffer = isRN ? [createNativeOffer, peer] : [[peer, peer.createOffer]]
  const offer = yield call(...createOffer)
  offer.sdp = prioritiseH264OnSDP(offer.sdp)
  log.debug('peer offer - ', offer)

  // this kicks off the ICE process in chrome
  // attaches the offer on the peer object
  // as the `onicecandidate` callback fires, this offer is automatically updated
  if (isRN) {
    yield call(setNativeLocalDescription, peer, offer)
  } else {
    yield call([peer, peer.setLocalDescription], offer)
  }
  yield put(WebrtcActions.updateLocalDescription(offer))
  return true
}

function * listenAndToggleCamera (stream) {
  log.debug('* listenAndToggleCamera starting')
  while (true) {
    let action = yield take([
        ChatTypes.TOGGLE_VIDEO_CALL_CAMERA,
        ChatTypes.END_VIDEO_CALL
    ])

    if (action.type === 'END_VIDEO_CALL') {
      log.debug('* listenAndToggleCamera - shutting down loop')
      return
    }
    // log.debug('toggleCamera stream - ', stream)
    const tracks = stream.getVideoTracks()
    // log.debug('toggleCamera - tracks - ', tracks)
    let isEnabled = null
    tracks.map(t => t.enabled = isEnabled = !t.enabled)
    yield put(Actions.setVideoCallCameraMuteState(!isEnabled))

    // send chatRequest to peer to let them know we changed our local state
    const callId = yield select(s =>
        path(['chat', 'outboundVideoCallAnswer', 'call_id'], s) ||
          path(['chat', 'inboundVideoCallOffer', 'call_id'], s)
      )
    if (callId ) {
      const resz = yield call([chatAPI, chatAPI.sendRequest], {
        cmd: '/webrtc/remote/video/toggle',
        args: {
          call_id: callId,
          is_enabled: isEnabled,
        },
      })
    } else {
      log.debug('listenAndToggleCamera: error UNABLE TO EXTRACT CALL_ID')
    }
  }
}

function * listenAndToggleViewFromRemoteCamera () {
  while (true) {
    let action = yield take([
        ChatTypes.ANNOUNCE_VIDEO_CALL_REMOTE_VIDEO_TOGGLE,
        ChatTypes.END_VIDEO_CALL
    ])
    if (action.type === 'END_VIDEO_CALL') {
      log.debug('* listenAndToggleViewFromRemoteCamera - shutting down loop')
      return
    }
    log.debug('* listenAndToggleViewFromRemoteCamera - got event action - ', action)
    if (action.data.is_enabled === null) {
      log.debug('* listenAndToggleViewFromRemoteCamera invalid packet')
      return
    }
    if (action.data.is_enabled) {
      yield put(Actions.setVideoCallRemoteToggle({videoEnabled: true}))
    } else {
      yield put(Actions.setVideoCallRemoteToggle({videoEnabled: false}))
    }
  }
}

function * listenAndToggleMic (stream) {
  log.debug('* listenAndToggleMic starting')
  while (true) {
    let action = yield take([
        ChatTypes.TOGGLE_VIDEO_CALL_MIC,
        ChatTypes.END_VIDEO_CALL
    ])
    if (action.type === 'END_VIDEO_CALL') {
      log.debug('* listenAndToggleMic - shutting down loop')
      return
    }
    const tracks = stream.getAudioTracks()
    let isEnabled = null
    tracks.map(t => t.enabled = isEnabled = !t.enabled)
    yield put(Actions.setVideoCallMicMuteState(!isEnabled))
  }
}

function prioritiseH264OnSDP (sdpString) {
  const parsedSDP = sdpTransform.parse(sdpString)

  const videoMediaIndex = findIndex(m => m.type === 'video', parsedSDP.media)
  if (videoMediaIndex === -1) return sdpString

  const h264Index = findIndex(
    c => c.codec === 'H264',
    path(['media', videoMediaIndex, 'rtp'], parsedSDP)
  )
  if (h264Index === -1) return sdpString

  log.debug('before munging - ', JSON.stringify(parsedSDP['media'][videoMediaIndex]['rtp']))
  const h264Codec = parsedSDP['media'][videoMediaIndex]['rtp'].splice(h264Index, 1)
  parsedSDP['media'][videoMediaIndex]['rtp'].unshift(h264Codec[0])
  log.debug('after munging - ', JSON.stringify(parsedSDP['media'][videoMediaIndex]['rtp']))

  return sdpTransform.write(parsedSDP)
}

export function * setupWebRTCForInbound (data, stream) {
  const isRN = yield select(s => s.device.isReactNative)
  const peer = yield call(setupWebRTC, null, null, stream)

  // peer.onicecandidate = (c, processed) => {
  //   if (!processed) {
  //     debugger
  //     log.debug('New Candidate:', c)
  //   }
  // }

  if (!peer) {
    log.debug('setupWebRTCForInbound - RTCPeerConnection not available')
    return null
  }
  try {
    const parsed = JSON.parse(data.sdp_offer)
    log.debug(
      'setupWebRTCForInbound REMOTE parsed.candidates=', parsed.candidates
    )
    const sdp = new WebRTCLib.RTCSessionDescription(parsed.offer)
    if (!sdp) {
      log.debug('RTCSessionDescription failed on parsed.offer')
      return null
    }
    let res
    if (isRN) {
      res = yield call(setNativeRemoteDescription, peer, sdp)
    } else {
      res = yield call([peer, peer.setRemoteDescription], sdp)
    }
    yield put(WebrtcActions.updateRemoteDescription(sdp))
    if (!res) {
      log.debug('failed to setRemoteDescription sdp -', sdp)
      // return null
    }

    const createAnswer = isRN ? [createNativeAnswer, peer] : [[peer, peer.createAnswer]]
    const answer = yield call(...createAnswer)
    answer.sdp = prioritiseH264OnSDP(answer.sdp)

    if (!answer) {
      log.debug('setupWebRTCForInbound: createNativeAnswer failed')
      return null
    }
    log.debug('setupWebRTCForInbound: answer - ', answer)

    if (isRN) {
      yield call(setNativeLocalDescription, peer, answer)
    } else {
      yield call([peer, peer.setLocalDescription], answer)
    }
    yield put(WebrtcActions.updateLocalDescription(answer))

    log.debug('setupWebRTCForInbound: candidates - ', parsed.candidates)

    const candidatePromises = parsed.candidates.map(c => peer.addIceCandidate(new WebRTCLib.RTCIceCandidate(c)))
    yield call(() => {
      return Promise.all(candidatePromises)
    })

    yield spawn(processInboundCall, peer, data.call_id, answer)
    return true
  } catch (e) {
    log.debug('setupWebRTCForInbound error - ', e)
    return null
  }
}

export function * receivedOutboundCallAnswer (data) {
  // log.debug('receivedOutboundCallAnswer data - ', data)
}

/**
 * Listens for `onaddstream` callback and dispatches the url to redux.
 *
 * Used by both – caller and callee.
 *
 * @param peer
 */
function * listenForRemoteStream (peer) {
  const isReactNative = yield select(s => s.device.isReactNative)
  const channel = yield call(createGotRemoteStreamChannel, peer)
  const ev = yield take(channel)
  let url = null
  if (!isReactNative ) {
    url = URL.createObjectURL(ev.stream)
  } else {
    url = ev.stream.toURL()
  }
  if (!url) {
    log.debug('ERROR unable to extract URL from ev.stream=', ev.stream)
    return null
  }
  log.debug('stream url - ', url)
  yield put(Actions.beginVideoCall(url))
  //yield spawn(cleanupRemoteStreamOnEnd, ev.stream)
}

function * listenForIceConnectionStateChangeChannel (peer) {
  log.debug('* listenForIceConnectionStateChangeChannel starting')
  const channel = yield call(createIceConnectionStateChangeChannel, peer)

  while (true) {
    const event = yield take(channel)
    log.debug('event.target.iceConnectionState - ', event.target.iceConnectionState)
    yield put(WebrtcActions.updateLocalIceConnectionState(event.target.iceConnectionState))
    switch (event.target.iceConnectionState) {
      case ICE_CONNECTION_STATE.CLOSED:
      case ICE_CONNECTION_STATE.DISCONNECTED:
      case ICE_CONNECTION_STATE.FAILED:
        const _event = event
        const callId = yield select(s =>
          path(['chat', 'outboundVideoCallAnswer', 'call_id'], s) ||
            path(['chat', 'inboundVideoCallOffer', 'call_id'], s)
        )
        if (! callId) {
          // This is because END_VIDEO_CALL clears outboundVideoCallAnswer, inboundVideoCallOffer
          // And a recently destroyed peerConnection ice state can still fire on closed
          log.debug('listenForIceConnectionStateChangeChannel - no callId')
          return
        }
        log.debug('call over callId - ', callId)
        let iceState = event.target.iceConnectionState
        yield announceCallEvent(WebrtcActions.updateLocalIceConnectionState(event.target.iceConnectionState))
        yield put(Actions.endVideoCall(callId, iceState))
        log.debug('listenForIceConnectionStateChangeChannel - exiting loop')
        return

      case ICE_CONNECTION_STATE.CONNECTED:
        yield put(Actions.setCallIsConnectedState(true))
    }
  }
}

function * getCandidates (peer) {
  const channel = yield call(createIceCandidateChannel, peer)
  const filteredCandidates = []

  // clear old candidates
  yield put(WebrtcActions.clearLocalIceCandidates())
  // Set initial ice candidate state
  yield put(WebrtcActions.updateLocalIceGatheringState(peer.iceGatheringState))

  // chrome specific behavior - get null candidate as value when its done
  while (true) {
    const data = yield take(channel)

    // update the ice gathering state
    yield put(WebrtcActions.updateLocalIceGatheringState(peer.iceGatheringState))

    // if we got null from channel then it's done
    if (!data || !data.candidate) break

    
    // parse the candidate
    const parsed = parseCandidate(data.candidate.candidate)

    // include candidate into collection only if ALLOW_HOST_SDP is true
    if (ALLOW_HOST_SDP) {
      if (parsed.type === 'srflx' || parsed.type === 'relay' || parsed.type === 'host') {
//        yield put(WebrtcActions.addLocalIceCandidate(parsed))
        yield put(WebrtcActions.addLocalIceCandidate(data.candidate))
        filteredCandidates.push(data.candidate)
      } else {
        log.debug('Invalid Local ICE Candidate:', parsed)
      }
    } else {
      if (parsed.type === 'srflx' || parsed.type === 'relay') {
        yield put(WebrtcActions.addLocalIceCandidate(data.candidate))
        filteredCandidates.push(data.candidate)
      } else {
        log.debug('Invalid Local ICE Candidate:', parsed)
      }
    }
        
    // stop if iceGathering state is 'complete'
    if (peer.iceGatheringState === 'complete') break
  }

  return filteredCandidates
}

// Return saved candidates in Redux Store
function* getSavedCandidates() {
  let seconds = 0
  while (true) {
    // Check every 1 second
    yield delay(1000)
    const local = yield select(getLocalWebRTCState)
    if (local.iceGatheringState === 'complete' || seconds === 60) { // 60 seconds timeout
      return local.iceCandidates
    }
    seconds ++
  }
}

/**
 * Process Inbound Call.
 *
 * 1) Finds good candidates.
 * 2) Makes the `/webrtc/answer` call with sdp answer.
 *
 * @param peer
 * @param callId
 * @param answer
 */
function * processInboundCall (peer, callId, answer) {
  const goodCandidates = yield call(getSavedCandidates)

  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }
  const resz = yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/webrtc/answer',
    args: {
      call_id: callId,
      sdp_answer: JSON.stringify({
        answer: peer.localDescription,
        candidates: goodCandidates,
      })
    }
  })
  log.debug('processInboundCall - /webrtc/answer response - ', resz)
}

/**
* Gather ICE candidates and register them to redux store
*
*/
function * gatherIceCandidates (peer) {
  yield call(getCandidates, peer)
}

/**
 * Process Outbound Call.
 *
 * 1) Finds good candidates
 * 2) Makes the `/webrtc/call` call with sdp offer.
 * 3) Waits for answer, reject or times out after 120 seconds.
 *
 * @param peer
 * @param outboundData
 */
function * processOutboundCall (peer, outboundData) {
  const isAudioOnly = yield select(s => s.chat.isAudioOnly)
  const goodCandidates = yield call(getSavedCandidates)
  // circular print in xcode
  //log.debug('goodCandidates - ', goodCandidates)

  if (!outboundData) return

  log.debug('outboundData - ', outboundData)
  const res = yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/webrtc/call',
    args: {
      from_email: outboundData.identityEmail,
      to_email: outboundData.contactEmail,
      sdp_offer: JSON.stringify({
        offer: peer.localDescription,
        candidates: goodCandidates,
        is_audio_only: isAudioOnly
      })
    }
  })

  log.debug('/webrtc/call response - ', res)
  if (res && res.status) {
    yield put(Actions.setOutboundCallId(res.call_id))
  } else {
    peer.close()
    yield put(Actions.endVideoCall(localId))
    // Show alert here
    return
  }

  log.debug('processOutboundCall about to yield race on [answered, rejected, timeout]')
  const result = yield race({
    answered: take(ChatTypes.OUTBOUND_VIDEO_CALL_ANSWER_RECEIVED),
    rejected: take(ChatTypes.OUTBOUND_VIDEO_CALL_REJECTED),
    timeout: call(delay, OUTBOUND_CALL_TIMEOUT_MS)
  })

  log.debug('/webrtc/call response race result - ', result)
  const localId = yield select(s => s.chat.videoCallLocalId)
  if (result.answered) {
    const answer = result.answered.data
    log.debug('answer - ', answer)
    try {
      const parsed = JSON.parse(answer.sdp_answer)
      const sdp = new WebRTCLib.RTCSessionDescription(parsed.answer)
      let description
      const isRN = yield select(s => s.device.isReactNative)
      if (isRN) {
        description = yield call(setNativeRemoteDescription, peer, sdp)
      } else {
        description = yield call([peer, peer.setRemoteDescription], sdp)
      }
      yield put(WebrtcActions.updateRemoteDescription(sdp))

      const candidatePromises = parsed.candidates.map(c => peer.addIceCandidate(new WebRTCLib.RTCIceCandidate(c)))
      const candidateResults = yield call(() => {
        return Promise.all(candidatePromises)
      })
      log.debug('peer.setRemoteDescription - ', description)
    } catch (e) {
      log.debug('errror - ', e)
    }
  } else if (result.rejected) {
    log.debug('call rejected.........')
    peer.close()
    yield put(Actions.endVideoCall(localId))
  } else if (result.timeout) {
    log.debug('call timed out........')
    peer.close()
    yield put(Actions.endVideoCall(localId))
  }
}

export function * inboundVideoCallIdReceived ({ callId, isColdLaunch }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  log.debug('inboundVideoCallIdReceived - callId - ', callId)
  const res = yield call([chatAPI, chatAPI.sendRequest], {
    cmd: '/webrtc/offer/get',
    args: {
      call_id: callId
    }
  })
  if (res.status) {
    yield put(Actions.inboundVideoCallOfferReceived(res, isColdLaunch))
  } else {
    log.debug('Invalid inboundVideoCallIdReceived - callId - ', callId)
  }
}
