import { put, call, select, take } from 'redux-saga/effects'

import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import { getContactMember } from 'commons/Selectors/Messaging'

import { chatAPI } from './index'
import { EVENT_TYPE, ROOM_UPDATE_TYPE } from './Constants'

/**
 * Send an event on the room, like ROOM_UPDATE, etc.
 *
 * @param event
 * @param roomId
 */
export function * sendRoomEvent (event, { roomId }) {
  if (!chatAPI.ready) return
  const room = yield select(s => s.chat.data[roomId])
  try {
    yield call([chatAPI, chatAPI.sendRequestWithoutResponse], {
      'cmd': '/room/event',
      'args': {
        room_id: roomId,
        event: event,
        member: room.member_email
      }
    })
  } catch (e) {}
}

/**
 * Chat refresh room.
 *
 * @param roomId
 */
export function * refreshRoom ({ roomId }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const roomGetRes = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/get',
      args: { room_id: [roomId] }
    })

    yield put(Actions.chatRefreshRoomSuccess(roomGetRes))
  } catch (e) {
    yield put(Actions.chatRefreshRoomError(e))
  }
}

/**
 * Create room.
 *
 * Also joins the newly created room.
 *
 * @param identityEmail
 * @param identityName
 * @param contactEmail
 */
export function * createRoomRequest ({ identityEmail, identityName, contactEmail }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/add',
      args: {
        title: 'title',
        owner_identity_email: identityEmail
      }
    })

    // autojoin the user into room
    yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/join',
      args: {
        room_id: res.room_id,
        member: identityEmail,
        display_name: identityName
      }
    })

    yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/invite',
      args: {
        room_id: res.room_id,
        member_email: contactEmail
      }
    })

    yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/map/set',
      args: { room_id: res.room_id, member_map: `${identityEmail}__${contactEmail}` }
    })

    // Get the room details
    const roomGetRes = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/get',
      args: { room_id: [res.room_id] }
    })

    yield put(Actions.chatCreateRoomSuccess({
      ...roomGetRes.rooms[0],
      joined: true,
      member_email: identityEmail,
      unreadCount: 0
    }))
  } catch (e) {
    yield put(Actions.chatCreateRoomError(e))
  }
}

/**
 * Join room.
 *
 * Also fetches the messages on the room.
 *
 * @param roomId
 * @param identityEmail
 */
export function * joinRoomRequest ({ roomId, identityEmail }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    if (!identityEmail) {
      const room = yield select(s => s.chat.data[roomId])
      if (!room) return
      identityEmail = room.member_email
    }

    // Get the room details
    const roomGetRes = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/get',
      args: { room_id: [roomId] }
    })

    yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/join',
      args: { room_id: roomId, member: identityEmail },
      push_type: EVENT_TYPE.MESSAGE_INLINE
    })

    const contact = getContactMember(roomGetRes.rooms[0])
    yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/map/set',
      args: { room_id: roomGetRes.rooms[0].room_id, member_map: `${roomGetRes.member_email}__${contact.email}` }
    })

    yield put(Actions.chatJoinRoomSuccess(roomGetRes.rooms[0]))

    // Dispatch action for loading messages on the channel
    yield put(Actions.chatGetMessagesRequest(roomId))

    // Send room update event regarding subscription change
    yield call(sendRoomEvent, ROOM_UPDATE_TYPE.MEMBERSHIP_CHANGE, { roomId })
  } catch (e) {
    yield put(Actions.chatJoinRoomError(e))
  }
}

/**
 * Leave room.
 *
 * @param roomId
 */
export function * leaveRoomRequest ({ roomId }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const room = yield select(s => s.chat.data[roomId])
    const roomLeaveRes = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/leave',
      args: { room_id: roomId, member: room.member_email }
    })

    yield put(Actions.chatLeaveRoomSuccess(roomLeaveRes))

    // Send room update event regarding subscription change
    yield call(sendRoomEvent, ROOM_UPDATE_TYPE.MEMBERSHIP_CHANGE, { roomId })
  } catch (e) {
    yield put(Actions.chatLeaveRoomError(e))
  }
}

/**
 * Update room settings
 * @param roomId
 * @param settings
 */
export function * updateRoomSettingsRequest ({ roomId, settings }) {
  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }
  try {
    const res = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/member/settings',
      args: { room_id: roomId, ...settings }
    })
    yield put(Actions.chatUpdateRoomSettingsSuccess(res, settings))
  } catch (e) {
    yield put(Actions.chatUpdateRoomSettingsError(e))
  }
}