import { put, call, select, take, fork, spawn, all, race } from 'redux-saga/effects'
import { eventChannel, delay } from 'redux-saga'
import { filter, path, any, isEmpty, isNil } from 'ramda'
import uuidv1 from 'uuid/v1'
import request from 'superagent'

import log from 'commons/Services/Log'
import Actions, { ChatTypes } from 'commons/Redux/ChatRedux'
import { AppTypes } from 'commons/Redux/AppRedux'
import { isLoggedIn } from 'commons/Selectors/User'
import MsgSafeChatAPI from 'commons/Services/Chat'
import { createSignature } from 'commons/Services/API/Utils/signature'
import { promiseUint8ArrayDataFromFile } from 'commons/Lib/FileReader'

import { getDeviceInfoWithToken } from 'app/Lib/Device'

import { DISCONNECT_MESSAGE, EVENT_TYPE, ROOM_UPDATE_TYPE, IMAGE_MIME_TYPES } from './Constants'
import { setupExistingRoom } from './NewRoom'
import { sendFileRequest } from './E2EE'
import { periodicallyRefreshRoomState } from './Polling'
import {
  sendRoomEvent,
  createRoomRequest,
  joinRoomRequest,
  leaveRoomRequest,
  refreshRoom,
  updateRoomSettingsRequest,
} from './Room'
import {
  getMessagesRequest,
  ackMessage,
  sendMessageRequest,
  peekNewMessageCountRequest,
} from './Message'
import {
  answerInboundVideoCallOffer,
  outboundVideoCallAnswerReceived,
  rejectVideoCall,
  announceCallEvent,
  handleCallEvent,
} from './Video'
import { inboundVideoCallIdReceived } from './WebRTC'
export { chatMessageStatusWatch } from './Status'

export const chatSendFileRequest = sendFileRequest

export const chatCreateRoomRequest = createRoomRequest
export const chatJoinRoomRequest = joinRoomRequest
export const chatLeaveRoomRequest = leaveRoomRequest
export const chatUpdateRoomSettingsRequest = updateRoomSettingsRequest
export const chatRefreshRoom = refreshRoom
export const chatSendRoomEvent = sendRoomEvent
export const chatGetMessagesRequest = getMessagesRequest

export const chatSetupExistingRoom = setupExistingRoom
export const chatAckMessage = ackMessage
export const chatSendMessageRequest = sendMessageRequest
export const chatPeekNewMessageCountRequest = peekNewMessageCountRequest
export const chatAnswerInboundVideoCallOffer = answerInboundVideoCallOffer
export const chatRejectVideoCall = rejectVideoCall
export const chatAnnounceCallEvent = announceCallEvent
export const chatOutboundVideoCallAnswerReceived = outboundVideoCallAnswerReceived
export const chatInboundVideoCallIdReceived = inboundVideoCallIdReceived

// Create socket
export let chatAPI = new MsgSafeChatAPI()

/**
 * Create a websocket channel for listening on messages from,
 * and passing it into saga pipeline.
 *
 * @param setMessageHandler
 * @param removeMessageHandler
 * @param setDisconnectHandler
 * @returns {Channel<T>}
 */
function createSocketChannel (setMessageHandler, removeMessageHandler, setDisconnectHandler) {
  // `eventChannel` takes a subscriber function
  // the subscriber function takes an `emit` argument to put messages onto the channel
  return eventChannel(emit => {
    // puts event payload into the channel
    // this allows a Saga to take this payload from the returned channel
    const messageHandler = event => emit(event.data)

    // setup the subscription
    setMessageHandler(messageHandler)

    // Set a disconnect handler that emits disconnect message
    setDisconnectHandler(() => emit(DISCONNECT_MESSAGE))

    // the subscriber must return an unsubscribe function
    // this will be invoked when the saga calls `channel.close` method
    return removeMessageHandler
  })
}

export function * chatInit () {
  yield spawn(bootstrapChatSocket)
  yield spawn(periodicallyRefreshRoomState, true)
  yield spawn(listenForAppToGoActiveAndCorrectWebsocketState)
}

/**
 * Set 'socket connecting' state only after waiting some time
 * as we don't want to render the connecting UI immediately...
 */
function * chatSocketConnectingStateTimeout () {
  const result = yield race({
    timeout: delay(8000),
    connected: take(ChatTypes.CHAT_SOCKET_CONNECTED)
  })

  if (result.timeout) {
    yield put(Actions.chatSocketConnecting())
  }
}

/**
 * When the app state goes back to active, the AppState.addEventListener fires first
 * and then the app tries to write to socket, which fails and then CHAT_SOCKET_DISCONNECTED
 * is dispatched.
 *
 * This saga takes care of that scenario, where if app state goes active and then immediately
 * the socket disconnects, the chat socket connection state is reset, to prevent the top red
 * banner with 'socket disconnected' message is not rendered.
 */
function * listenForAppToGoActiveAndCorrectWebsocketState () {
  while (true) {
    // console.log('listenForAppToGoActiveAndCorrectWebsocketState listening...')
    const { newState } = yield take(AppTypes.NATIVE_APP_STATE_CHANGED)
    // console.log('listenForAppToGoActiveAndCorrectWebsocketState state - ', newState)
    if (newState !== 'active') continue

    const startTime = new Date()
    // Have a timeout on listening for socket disconnect
    // as the socket can stay alive across app state changes
    // for e.g. if user quickly minimizes and restores the app
    const raceResult = yield race({
      disconnect: take(ChatTypes.CHAT_SOCKET_DISCONNECTED),
      timeout: delay(3000)
    })

    if (raceResult.timeout) continue

    const delta = (new Date()) - startTime
    // console.log('listenForAppToGoActiveAndCorrectWebsocketState delta - ', delta)
    if (delta > 1000) continue

    yield put(Actions.resetChatSocketConnectionState())
  }
}

/**
 * Create socket and listen for messages.
 *
 * Wired with following redux actions –
 * - StartupTypes.STARTUP_SUCCESS
 * - ChatTypes.CHAT_SOCKET_DISCONNECTED
 */
export function * bootstrapChatSocket () {
  const user = yield select(s => s.user)
  if (!isLoggedIn(user)) return

  const username = user.data.username

  // Create a channel for listening on messages
  const socketChannel = yield call(
    createSocketChannel,
    chatAPI.setMessageHandler,
    chatAPI.removeMessageHandler,
    chatAPI.setDisconnectHandler
  )

  // Bootstrap key pair
  yield call([chatAPI, chatAPI.bootstrapKeyPair])

  // Connect the socket
  yield spawn(chatSocketConnectingStateTimeout)
  chatAPI.connectSocket()

  let firstMessage

  // Keep pulling in messages until server's public_key has been received
  while (true) {
    firstMessage = yield take(socketChannel)
    try {
      firstMessage = JSON.parse(firstMessage)
      if (firstMessage.public_key) break
    } catch (e) {
      log.debug('first message on socket doesn\'t contain server\'s public key - ', firstMessage)
    }
  }

  // Set the server's public key on chatAPI instance
  chatAPI.setBase64ServerPublicKey(firstMessage.public_key)

  // Send payload for key handshake
  const testResponse = yield call([chatAPI, chatAPI.encryptMessageAsBase64], 'test')
  const device = yield call(getDeviceInfoWithToken)
  const handShakePayload = {
    // Client public key, towards which the server will encrypt
    public_key: chatAPI.getClientPublicKeyAsBase64(),
    // Encrypted message binary as base 64
    enc_msg: testResponse,
    // Display name for the client
    display_name: username,
    push_type: EVENT_TYPE.MESSAGE_INLINE,
    jwt: createSignature(user.data.access_id, user.data.secret_token)
  }
  if (!isNil(device) && !isEmpty(device)) {
    handShakePayload.device = device
  }
  // console.log('sendHandShakePayload ---', handShakePayload)
  yield call([chatAPI, chatAPI.sendHandShakePayload], handShakePayload)

  // Response for the key handshake
  const encryptedKeyHandshakeBase64Response = yield take(socketChannel)
  const decrypted = yield call(
    [chatAPI, chatAPI.decryptBase64Message],
    encryptedKeyHandshakeBase64Response
  )
  log.debug('key handshake response', decrypted)
  // Return if the key handshake response isn't as expected
  if (!decrypted || !decrypted.status) {
    log.debug('chat api: key handshake failed')
    return
  }

  if (!chatAPI.ready) {
    yield put(Actions.chatFetch())
    chatAPI.isReady()
    chatAPI.userSessionStarted = true
  }

  yield put(Actions.chatSocketConnected())

  // Mark key handshake as successful on chatAPI
  yield put(Actions.chatKeyHandshakeSuccessful())

  yield spawn(listenForMessages, socketChannel)
}

/**
 * Keep listening for messages on socket
 * and deal with different event types.
 *
 * @param socketChannel
 */
export function * listenForMessages (socketChannel) {
  while (true) {
    if (!chatAPI.userSessionStarted) break

    const encryptedBase64Response = yield take(socketChannel)

    // First check if it's the disconnect message
    // If so, dispatch chatSocketDisconnected redux action
    // and break the loop
    if (encryptedBase64Response === DISCONNECT_MESSAGE) {
      // log.debug('disconnected...')
      yield put(Actions.chatSocketDisconnected())
      // If app state goes background or active, reset socket connection state
      const appState = yield select(s => s.app.nativeAppState)
      if (appState === 'background' || appState === 'inactive') {
        yield put(Actions.resetChatSocketConnectionState())
      }
      log.debug('Message Handler disconnected!')
      break
    }

    const decrypted = yield call([chatAPI, chatAPI.decryptBase64Message], encryptedBase64Response)

    log.debug('Socket Event:' + decrypted.type)


    if (decrypted.type === EVENT_TYPE.WEBRTC_ANNOUNCE_EVENT) {
      yield call(handleCallEvent, decrypted)
    }

    if (decrypted.type === EVENT_TYPE.WEBRTC_OFFER) {
      const localCallId = yield select(s => s.chat.videoCallLocalId)
      if (localCallId === decrypted.call_id) {
        log.debug('Duplicated Call -', localCallId)
        continue
      }
      
      yield put(Actions.inboundVideoCallOfferReceived(decrypted))
      continue
    }

    if (decrypted.type === EVENT_TYPE.WEBRTC_ANSWER) {
      yield put(Actions.outboundVideoCallAnswerReceived(decrypted))
      continue
    }

    if (decrypted.type === EVENT_TYPE.WEBRTC_REJECT) {
      yield put(Actions.outboundVideoCallRejected(decrypted))
      continue
    }

    if (decrypted.type === EVENT_TYPE.WEBRTC_ANNOUNCE_ANSWER) {
      yield put(Actions.announceVideoCallAnswered(decrypted))
      continue
    }

    if (decrypted.type === EVENT_TYPE.WEBRTC_ANNOUNCE_REMOTE_VIDEO_TOGGLE) {
      yield put(Actions.announceVideoCallRemoteVideoToggle(decrypted))
      continue
    }

    // For auto room join events
    if (decrypted.type === EVENT_TYPE.AUTO_JOINED_ROOM) {
      yield spawn(setupExistingRoom, { roomId: decrypted.room_id, identityEmail: decrypted.member_email })
      continue
    }

    // Don't process the event if the chat data isn't yet available
    const chatData = yield select(s => s.chat.data)
    if (isNil(chatData) || isEmpty(chatData)) continue

    const roomId = path(['room_id'], decrypted) || path(['data', 'room_id'], decrypted)
    const room = chatData[roomId]
    if (!room) {
      log.debug('room not found.........', room)
      continue
    }

    switch (decrypted.type) {
      // Normal message received
      case EVENT_TYPE.MESSAGE_INLINE:
        yield put(Actions.chatMessageReceived(decrypted.data))
        break

      // E2EE message or file received
      case EVENT_TYPE.MESSAGE_INLINE_E2EE:
      case EVENT_TYPE.FILE_GET_E2EE:
        const sender = room.members.filter(m => m.email === decrypted.user_from)
        if (!sender || !sender.length || !sender[0].public_key || !sender[0].public_key.length) {
          log.debug('MESSAGE_INLINE_E2EE: sender\'s public key not found...')
          break
        }

        // Iterate through all of the sender's public keys and try to decrypt
        for (let key of sender[0].public_key) {
          try {
            // If the message is for E2EE inline file and there's a data_url
            // do an http fetch on the message
            if (decrypted.type === EVENT_TYPE.FILE_GET_E2EE && decrypted.data_url) {
              if (decrypted.file_size < 10 * 1024 * 1024 && IMAGE_MIME_TYPES[decrypted.mime_type]) {
                const encryptedFile = yield request.get(decrypted.data_url).responseType('blob')
                const encryptedBinary = yield promiseUint8ArrayDataFromFile(encryptedFile.body)
                const decryptedString = yield call([chatAPI, chatAPI.decryptBinaryMessage], encryptedBinary, key, true)
                yield put(Actions.chatMessageReceived({
                  body: null,
                  message_id: uuidv1(),
                  room_id: decrypted.room_id,
                  user_from: decrypted.user_from,
                  is_image: true,
                  data: decrypted,
                  image_data: decryptedString
                }, true))
              } else {
                yield put(Actions.chatMessageReceived({
                  body: null,
                  message_id: uuidv1(),
                  room_id: decrypted.room_id,
                  user_from: decrypted.user_from,
                  is_url: true,
                  data: decrypted
                }, true))
              }
              continue
            } else {
              const decryptedBody = yield call([chatAPI, chatAPI.decryptBase64Message], decrypted.data, key)
              yield put(Actions.chatMessageReceived({
                body: decryptedBody,
                message_id: uuidv1(),
                room_id: decrypted.room_id,
                user_from: decrypted.user_from,
                is_file: decrypted.type === EVENT_TYPE.FILE_GET_E2EE
              }, true))
            }
            break
          } catch (e) {
            log.debug('failed to decrypt a message...', e)
          }
        }
        break

      case EVENT_TYPE.ROOM_UPDATE:
        // Do nothing if there's no data
        if (!decrypted.data) break

        // if the update is for the same user
        if (decrypted.data.member === room.member_email) break

        if (decrypted.data.event === ROOM_UPDATE_TYPE.MEMBERSHIP_CHANGE) {
          yield put(Actions.chatRefreshRoomRequest(decrypted.room_id))
        } else if (decrypted.data.event === ROOM_UPDATE_TYPE.STARTED_TYPING) {
          yield put(Actions.chatOtherUserStartedTyping(decrypted.room_id, decrypted.data.member))
        } else if (decrypted.data.event === ROOM_UPDATE_TYPE.STOPPED_TYPING) {
          yield put(Actions.chatOtherUserStoppedTyping(decrypted.room_id, decrypted.data.member))
        }
        break

      case EVENT_TYPE.ROOM_LEAVE:
        yield put(Actions.chatRefreshRoomRequest(roomId))
        break
    }
  }
}

/**
 * Reconnect the chat websocket, wait until key handshake is successful
 * and then join back all the previously joined rooms.
 */
export function * reconnectChatSocket () {
  const user = yield select(s => s.user)
  if (!isLoggedIn(user)) return

  // Spawn a bootstrapChatSocket
  yield spawn(bootstrapChatSocket)

  // Wait until the key handshake is successful
  yield take(ChatTypes.CHAT_SUCCESS)

  // Extract chat slice
  const chatSlice = yield select(s => s.chat)

  // Extract ids of previously joined rooms
  // and join back all of the rooms
  if(chatSlice.dataOrder) {
    const previouslyJoinedRoomIds = filter(id => chatSlice.data[id].joined, chatSlice.dataOrder)
    for (let id of previouslyJoinedRoomIds) {
      yield fork(joinRoomRequest, { roomId: id })
    }
  }
}

/**
 * Fetch chat rooms.
 */
export function * chatFetch () {
  let isRefreshing = false
  const chatDataOrder = yield select(s => s.chat.dataOrder)
  if (chatDataOrder) {
    isRefreshing = true
  }
  yield put(Actions.chatRequest({ isRefreshing }))

  if (!chatAPI.ready) {
    yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
  }

  try {
    const roomsMapRes = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/map/list',
      args: {}
    })
    const roomsMap = {}
    roomsMapRes.maps.map(m => { roomsMap[m.member_map] = m.room_id })
    yield put(Actions.setRoomsMap(roomsMap))

    const roomListRes = yield call([chatAPI, chatAPI.sendRequest], {
      'cmd': '/room/list',
      'args': {}
    })

    // If there are no rooms, don't proceed with /room/get
    if (!roomListRes.rooms || !roomListRes.rooms.length) {
      yield put(Actions.chatSuccess(roomListRes))
      return
    }

    // Join all the rooms first
    const joins = []

    roomListRes.rooms.map(r => {
      joins.push(call(
        [chatAPI, chatAPI.sendRequest], {
          cmd: '/room/join',
          args: { room_id: r.room_id, member: r.member_email, display_name: r.member_display_name },
          push_type: EVENT_TYPE.MESSAGE_INLINE
        }
      ))
    })

    // Make map of 'room_id' to room properties from '/room/list
    const roomListResMap = roomListRes.rooms.reduce((map, room) => {
      map[room.room_id] = room
      return map
    }, {})

    // Do all room joins
    const joinResponse = yield all(joins)

    // Note - it is possible that /room/list returns non-existing room ids
    // and if the client joins such a room, it should check and leave it
    const nonExistingRooms = {}
    for (let r of joinResponse) {
      // Check join status
      if (r.status) continue

      // Keep track of non-existing room ID and leave the room
      nonExistingRooms[r.room_id] = true
      yield call([chatAPI, chatAPI.sendRequest], {
        cmd: '/room/leave',
        args: { room_id: r.room_id, member: memberEmailMap[r.room_id] }
      })
    }

    // Make list of available room IDs so that only these are used for /room/get
    const availableRoomIds = roomListRes.rooms.filter(r => !nonExistingRooms[r.room_id]).map(r => r.room_id)

    if (!availableRoomIds || !availableRoomIds.length) {
      yield put(Actions.chatSuccess({ rooms: [] }))
      return
    }

    // Get the room details
    const roomGetRes = yield call([chatAPI, chatAPI.sendRequest], {
      cmd: '/room/get',
      args: { room_id: availableRoomIds }
    })

    // Inject last_read_message_id into room object
    for (let i = 0; i < roomGetRes.rooms.length; i++) {
      const roomId = roomGetRes.rooms[i].room_id
      roomGetRes.rooms[i] = {
        ...roomListResMap[roomId],
        ...roomGetRes.rooms[i],
        joined: true,
      }
      yield put(Actions.chatPeekNewMessageCountRequest(roomId, roomListResMap[roomId].last_read_message_id))
    }

    // yield call([chatAPI, chatAPI.resendMessagesInSeq])

    // Dispatch CHAT_SUCCESS
    yield put(Actions.chatSuccess(roomGetRes))

    const membershipChanges = availableRoomIds.map(roomId =>
      spawn(sendRoomEvent, ROOM_UPDATE_TYPE.MEMBERSHIP_CHANGE, { roomId })
    )

    // Send all membership change events on rooms
    yield all(membershipChanges)
  } catch (e) {
    yield put(Actions.chatFailure(e))
  }
}

export function * cleanupChatSocket () {
  chatAPI.userSessionStarted = false
  chatAPI.close()
  chatAPI = new MsgSafeChatAPI()
}
