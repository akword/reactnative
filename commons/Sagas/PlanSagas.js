import { find } from 'ramda'

import ax from 'commons/Services/Analytics/index'

import PlanAction from 'commons/Redux/PlanRedux'
import { put } from 'redux-saga/effects'

import { callAPI } from './APISagas'

export function * upgradePlanRequest ({ payload, resolve, reject }) {
  // Keep track of previously default card ID, if we change it
  let previouslyDefaultCardId

  try {
    if (payload.ccinfo) {
      const card = yield callAPI('AddCard', { ccinfo: payload.ccinfo, email: payload.email })
      if (card.data && card.data.id) {
        delete payload.ccinfo
        delete payload.email
        payload.ccid = card.data.id
      }
    }

    if (payload.ccid) {
      // Fetch all cards and find the default one
      const cards = yield callAPI('Cards')
      if (cards && cards.data && cards.data.data && cards.data.data.length > 0) {
        const defaultCard = find(c => c.is_default, cards.data.data)

        // If the default card is different that what user just selected
        // then just set the selected card as default for now
        // and update previouslyDefaultCardId
        if (defaultCard && defaultCard.id && defaultCard.id !== payload.ccid) {
          previouslyDefaultCardId = defaultCard.id
          yield callAPI('UpdateCard', { id: payload.ccid, is_default: true })
        }
      }
    }

    const response = yield callAPI('UpdateUserAccount', payload)
    typeof resolve === 'function' && resolve(response.data)
    yield put(PlanAction.upgradePlanSuccess(response.data, payload))

    // If previouslyDefaultCardId was ever set, set it back as default
    if (previouslyDefaultCardId) {
      yield callAPI('UpdateCard', { id: previouslyDefaultCardId, is_default: true })
    }
  } catch (e) {
    typeof reject === 'function' && reject(e.message)
    yield put(PlanAction.upgradePlanError(e.message, e))

    // If previouslyDefaultCardId was ever set, set it back as default
    if (previouslyDefaultCardId) {
      yield callAPI('UpdateCard', { id: previouslyDefaultCardId, is_default: true })
    }
  }
}

export function * upgradePlanSuccess ({ data, requestPayload }) {
  ax.form.success(ax.EVENTS.USER_PLAN, {
    'Plan name': data.plan_name,
    'Plan': requestPayload.plan_id,
    'Coupon': requestPayload.coupon,
    'Frequency': 'Annually',
    'Payment Method': 'Credit Card'
  })
}

export function * upgradePlanError ({ e }) {
  ax.form.error(ax.EVENTS.USER_PLAN, e.message, e.code)
}
