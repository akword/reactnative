import { put, call } from 'redux-saga/effects'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/AccountHistoryRedux'
import { getError } from 'commons/Lib/Utils'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = null
    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.accounthistoryClearSearchData
    )
    payload.order = 'created_on'
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.accounthistoryRequest(requestType))
    const response = yield callAPI('AccountHistory', payload)
    yield put(Actions.accounthistorySuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch the contacts. Please try again.')
    yield put(Actions.accounthistoryFailure(err))
  }
}
