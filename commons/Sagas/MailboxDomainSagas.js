import { put, call } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/MailboxDomainRedux'
import { getError } from 'commons/Lib/Utils'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = {
      type: 'search',
      columns: [ 'name' ]
    }

    const filterConfig = [
      { stateKey: 'filterIdentityIds', payloadKey: 'identity_id' }
    ]

    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.mailboxDomainClearSearchData, filterConfig
    )
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.mailboxDomainRequest(requestType))
    const response = yield callAPI('MailboxDomains', payload)
    yield put(Actions.mailboxDomainSuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch domains. Please try again.')
    yield put(Actions.mailboxDomainFailure(err))
  }
}
