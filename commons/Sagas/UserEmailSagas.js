import { put, call, select } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/UserEmailRedux'
import { getError } from 'commons/Lib/Utils'
import ax from 'commons/Services/Analytics/index'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = {
      type: 'search_or',
      columns: [ 'email', 'display_name' ]
    }
    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.useremailClearSearchData
    )
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.useremailRequest(requestType))
    const response = yield callAPI('UserEmail', payload)
    yield put(Actions.useremailSuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch. Please try again.')
    yield put(Actions.useremailFailure(err))
  }
}

export function * autoUpdateRequest () {
  const slice = yield select(s => s.useremail)

  if (!slice.dataOrder || !slice.dataOrder.length) {
    yield call(fetch, {})
    return
  }

  if (!slice.lastRequestTimestamp) return

  const payload = {
    order: 'last_activity_on',
    sort: 'desc',
    limit: 100,
    gt_req_ts: slice.lastRequestTimestamp
  }

  try {
    const response = yield callAPI('UserEmail', payload)
    yield put(Actions.useremailAutoUpdateSuccess(response.data))
  } catch (e) {
    const err = getError(e, 'Failed to fetch the user emails. Please try again.')
    yield put(Actions.useremailAutoUpdateFailure(err))
  }
}

export function * create ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('CreateUserEmail', payload)
    ax.form.createSuccess(ax.EVENTS.FORWARD_ADDRESS_FORM)
    resolve()
    yield put(Actions.useremailCreateSuccess(response.data))
  } catch (e) {
    ax.form.createError(ax.EVENTS.FORWARD_ADDRESS_FORM, e.message, e.code)
    reject(new SubmissionError({_error: getError(e, 'Failed to create. Please try again.')}))
  }
}

export function * edit ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('UpdateUserEmail', payload)
    ax.form.editSuccess(ax.EVENTS.FORWARD_ADDRESS_FORM)
    resolve()
    yield put(Actions.useremailEditSuccess(response.data))
  } catch (e) {
    ax.form.editError(ax.EVENTS.FORWARD_ADDRESS_FORM, e.message, e.code)
    reject(new SubmissionError({_error: getError(e, 'Failed to update. Please try again.')}))
  }
}

export function * remove ({ payload, resolve, reject }) {
  try {
    const id = payload.id
    yield callAPI('DeleteUserEmail', payload)
    ax.delete(ax.EVENTS.FORWARD_ADDRESS)
    yield put(Actions.useremailRemoveSuccess(id))
    typeof resolve === 'function' && resolve()
  } catch (e) {
    log.error('Delete API request failed - ', e)
    reject(new SubmissionError({_error: getError(e, 'Failed to update. Please try again.')}))
  }
}

export function * requestConfirmation ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('RequestConfirmationEmail', payload)
    ax.action(ax.EVENTS.FORWARD_ADDRESS, 'Request Confirmation Email')
    yield put(Actions.useremailRequestConfirmationSuccess(response.data))
    resolve()
  } catch (e) {
    log.error('API request failed - ', e)
    reject(new SubmissionError({_error: getError(e, 'Request failed. Please try again.')}))
  }
}

export function * setDefault ({ id }) {
  const userEmails = yield select(s => s.useremail)
  if (userEmails.dataOrder && userEmails.dataOrder.length) {
    const currentDefaultIds = userEmails.dataOrder.filter(id => userEmails.data[id].is_default)
    for (const _id of currentDefaultIds) {
      yield callAPI('UpdateUserEmail', { id: _id, is_default: false })
    }
    yield callAPI('UpdateUserEmail', { id: id, is_default: true })
  }
  yield put(Actions.useremailSetDefaultSuccess(id))
}
