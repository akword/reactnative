import { put, call } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'

import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/UsableUserEmailRedux'
import { getError } from 'commons/Lib/Utils'

export function * fetch ({ requestType: _requestType }) {
  try {
    let searchConfig = {
      type: 'search',
      columns: [ 'name' ],
    }

    const filterConfig = []
    const basePayload = { filter: { is_confirmed: true } }
    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.usableUserEmailClearSearchData, filterConfig, basePayload
    )
    if (isRequestUnnecessary) {
      return
    }
    yield put(Actions.usableUserEmailRequest(requestType))
    const response = yield callAPI('UserEmail', payload)
    yield put(Actions.usableUserEmailSuccess(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch UserEmails. Please try again.')
    yield put(Actions.usableUserEmailFailure(err))
  }
}
