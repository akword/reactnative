import { put, select, call, fork, take, cancel } from 'redux-saga/effects'
import { delay } from 'redux-saga'

import Actions, { ICE_CONNECTION_STATE, EVENT_TYPE, WebrtcTypes } from '../Redux/WebrtcRedux'
import { ChatTypes } from '../Redux/ChatRedux'
import { chatAPI } from './ChatSagas'
import { getContactEmail, getIdentityEmail, getCallId } from '../Selectors/VideoChat'

let webrtcActions = []

export function * localWebrtcActionsCollector (action) {
  webrtcActions.push(action)
}

export function * webrtcAnnouncesDaemon () {
  while(true) {
    yield take(ChatTypes.INBOUND_VIDEO_CALL_OFFER_RECEIVED)
    webrtcActions = []
    const announcer = yield fork(localWebrtcActionsAnnouncer)
    yield take(ChatTypes.END_VIDEO_CALL)
    yield cancel(announcer)
  }
}

export function * localWebrtcActionsAnnouncer () {
  while (true) {
    if (webrtcActions.length) {
      const callId = yield select(getCallId)
      if (!chatAPI.ready) {
        yield take(ChatTypes.CHAT_KEY_HANDSHAKE_SUCCESSFUL)
      }
      const res = yield call([chatAPI, chatAPI.sendRequest], {
        cmd: '/webrtc/announce/event',
        args: {
          call_id: callId,
          call_event_data: JSON.stringify(webrtcActions),
        }
      })
      webrtcActions = []
    }
    yield delay(1000)
  }
}

export function * onEndVideoCall () {
  yield delay(3000)
  yield put(Actions.webrtcReset())
}
