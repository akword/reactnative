import { takeLatest, takeEvery } from 'redux-saga/effects'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { UserTypes } from '../Redux/UserRedux'
import { DashboardTypes } from '../Redux/DashboardRedux'
import { ContactTypes, ContactAPITypesToSagas } from '../Redux/ContactRedux'
import { IdentityTypes, IdentityAPITypesToSagas } from '../Redux/IdentityRedux'
import { PlanAPITypesToSagas, PlanTypes } from '../Redux/PlanRedux'
import { DomainTypes, DomainAPITypesToSagas } from '../Redux/DomainRedux'
import { MailboxDomainTypes } from '../Redux/MailboxDomainRedux'
import { PaymentTypes } from '../Redux/PaymentRedux'
import { PaymentHistoryTypes } from '../Redux/PaymentHistoryRedux'

import { UsableDomainTypes } from '../Redux/UsableDomainRedux'
import { UsableUserEmailTypes } from '../Redux/UsableUserEmailRedux'

import { UserEmailTypes, UserEmailAPITypesToSagas } from '../Redux/UserEmailRedux'
import { RegionTypes } from '../Redux/RegionRedux'
import MailboxActions, { MailboxTypes, MailboxAPITypesToSagas } from '../Redux/MailboxRedux'
import { NotificationTypes } from '../Redux/NotificationRedux'
import { DeviceTypes } from '../Redux/DeviceRedux'
import { AccountHistoryTypes } from '../Redux/AccountHistoryRedux'
import { ChatTypes, ChatAPITypesToSagas } from '../Redux/ChatRedux'
import { CryptoTypes, CryptoAPITypesToSagas } from '../Redux/CryptoRedux'
import { AvatarTypes } from '../Redux/AvatarRedux'
import { WebrtcTypes } from '../Redux/WebrtcRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import * as userSagas from './UserSagas'
import * as dashboardSagas from './DashboardSagas'
import * as contactSagas from './ContactSagas'
import * as identitySagas from './IdentitySagas'
import * as domainSagas from './DomainSagas'
import * as mailboxDomainSagas from './MailboxDomainSagas'
import * as usableDomainSagas from './UsableDomainSagas'
import * as useremailSagas from './UserEmailSagas'
import * as usableUserEmailSagas from './UsableUserEmailSagas'
import * as regionSagas from './RegionSagas'
import * as mailboxSagas from './MailboxSagas'
import { handleNotificationTimeout } from './NotificationSagas'
import { createDevice } from './DeviceSagas'
import * as accountHistorySagas from './AccountHistorySagas'
import * as chatSagas from './ChatSagas/'
import * as cryptoSagas from './CryptoSagas'
import * as planSagas from './PlanSagas'
import * as paymentSagas from './PaymentSagas'
import * as paymentHistorySagas from './PaymentHistorySagas'
import * as avatarSagas from './AvatarSagas'
import * as webrtcSagas from './WebrtcSagas'
import callSagaRules from './ChatSagas/Call'

/* ------------- Connect Types To Sagas ------------- */

export default () => {
  return [
    ...ContactAPITypesToSagas,
    ...IdentityAPITypesToSagas,
    ...UserEmailAPITypesToSagas,
    ...PlanAPITypesToSagas,
    ...DomainAPITypesToSagas,
    ...CryptoAPITypesToSagas,
    ...ChatAPITypesToSagas,
    ...MailboxAPITypesToSagas,

    // Startup
    takeLatest(StartupTypes.STARTUP, startup),

    /* ------------- Auth ------------- */

    // Login
    takeLatest(UserTypes.LOGIN_REQUEST, userSagas.login),
    takeLatest(UserTypes.UPDATE_USER, userSagas.updateUser),
    takeLatest(UserTypes.REFRESH_PROFILE_REQUEST, userSagas.refreshProfileRequest),

    // Signup
    takeLatest(UserTypes.SIGNUP_REQUEST, userSagas.signup),

    // Username & email validation
    takeLatest(UserTypes.USERNAME_CHECK_REQUEST, userSagas.checkUsername),
    takeLatest(UserTypes.EMAIL_CHECK_REQUEST, userSagas.checkEmail),
    takeLatest(UserTypes.EMAIL_CHECK_FOR_ESP_REQUEST, userSagas.checkEmailForESP),

    // Forgot username
    takeLatest(UserTypes.FORGOT_USERNAME_REQUEST, userSagas.forgotUsername),

    // Password Reset
    takeLatest(UserTypes.PASSWORD_RESET_REQUEST, userSagas.requestPasswordReset),
    takeLatest(UserTypes.PASSWORD_RESET_REQUEST_REQUEST, userSagas.requestPasswordResetRequest),

    // Logout
    takeLatest(UserTypes.LOGOUT, userSagas.logout),

    /* ------------- User Account ------------- */

    takeLatest(UserTypes.UPDATE_ACCOUNT_REQUEST, userSagas.updateUserAccount),

    takeLatest(UserTypes.UPDATE_IDENTITY_SETTINGS_REQUEST, userSagas.updateIdentitySettings),

    /* ------------- Dashboard ------------- */

    takeLatest(DashboardTypes.DASHBOARD_DATA_REQUEST, dashboardSagas.fetchDashboardData),

    /* ------------- Contacts ------------- */
    takeLatest(
      [
        ContactTypes.CONTACT_SET_SEARCH_QUERY,
        ContactTypes.CONTACT_SET_SEARCH_FILTERS,
        ContactTypes.TOGGLE_CONTACT_IDENTITY_FILTER,
        ContactTypes.CLEAR_CONTACT_IDENTITY_FILTER,
        ContactTypes.TOGGLE_CONTACT_MSGSAFE_USERS_FILTER,
        ContactTypes.CLEAR_CONTACT_MSGSAFE_USERS_FILTER,
        ContactTypes.CONTACT_SET_SORT_ORDER
      ],
      contactSagas.fetch
    ),
    takeEvery(ContactTypes.CONTACT_FETCH, contactSagas.fetch),
    takeLatest(ContactTypes.CONTACT_AUTO_UPDATE_REQUEST, contactSagas.autoUpdateRequest),
    takeLatest(ContactTypes.CONTACT_CREATE, contactSagas.create),
    takeLatest(ContactTypes.CONTACT_EDIT, contactSagas.edit),
    takeLatest(ContactTypes.CONTACT_REMOVE, contactSagas.remove),

    /* ------------- Payment ------------- */
    takeLatest(PaymentTypes.PAYMENT_REQUEST, paymentSagas.fetch),
    takeLatest(PaymentTypes.PAYMENT_CREATE, paymentSagas.create),
    takeLatest(PaymentTypes.PAYMENT_REMOVE, paymentSagas.remove),
    takeLatest(PaymentTypes.PAYMENT_EDIT, paymentSagas.edit),

    /* ------------- Payment History -------------- */
    takeLatest(
      [
        PaymentHistoryTypes.PAYMENT_HISTORY_FETCH,
        PaymentHistoryTypes.PAYMENT_HISTORY_SET_SEARCH_QUERY
      ],
      paymentHistorySagas.fetch
    ),

    /* ------------- Identities ------------- */

    takeLatest(
      [
        IdentityTypes.IDENTITY_FETCH,
        IdentityTypes.IDENTITY_SET_SEARCH_QUERY,
        IdentityTypes.IDENTITY_SET_SORT_ORDER
      ],
      identitySagas.fetch
    ),
    takeLatest(IdentityTypes.IDENTITY_AUTO_UPDATE_REQUEST, identitySagas.autoUpdateRequest),
    takeLatest(IdentityTypes.IDENTITY_CREATE, identitySagas.create),
    takeLatest(IdentityTypes.IDENTITY_EDIT, identitySagas.edit),
    takeLatest(IdentityTypes.IDENTITY_REMOVE, identitySagas.remove),

    /* ------------- Mailbox ------------- */

    takeLatest(
      [
        MailboxTypes.MAILBOX_FETCH,
        MailboxTypes.MAILBOX_SET_SEARCH_QUERY,
        MailboxTypes.SET_MAILBOX_FILTER,
        MailboxTypes.CLEAR_MAILBOX_FILTER,
        MailboxTypes.TOGGLE_MAILBOX_IDENTITY_FILTER,
        MailboxTypes.CLEAR_MAILBOX_IDENTITY_FILTER,
        MailboxTypes.TOGGLE_MAILBOX_DOMAIN_FILTER,
        MailboxTypes.CLEAR_MAILBOX_DOMAIN_FILTER,
        MailboxTypes.CLEAR_ALL_MAILBOX_RELATION_FILTERS,
        MailboxTypes.TOGGLE_UNREAD_FILTER,
        MailboxTypes.MAILBOX_SET_SORT_ORDER
      ],
      mailboxSagas.fetchMailbox
    ),
    takeLatest(MailboxTypes.MAILBOX_AUTO_UPDATE_REQUEST, mailboxSagas.mailboxAutoUpdateRequest),
    // takeLatest(MailboxTypes.MAILBOX_DETAIL_REQUEST, mailboxSagas.fetchMailboxDetail),
    takeLatest(MailboxTypes.MAILBOX_DETAIL_REQUEST, mailboxSagas.fetchMailboxMIME),
    takeLatest(MailboxTypes.MAILBOX_ANALYTICS_REQUEST, mailboxSagas.fetchMailboxAnalytics),
    takeLatest(MailboxTypes.SEND_MAIL_REQUEST, mailboxSagas.sendMail),
    takeLatest(
      MailboxTypes.MAILBOX_READ_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'mark_as_read'},
      MailboxActions.mailboxReadSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_UNREAD_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'mark_as_unread'},
      MailboxActions.mailboxUnreadSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_TRASH_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'trash'},
      MailboxActions.mailboxTrashSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_DELETE_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'delete'},
      MailboxActions.mailboxDeleteSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_ARCHIVE_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'archive'},
      MailboxActions.mailboxArchiveSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_UNARCHIVE_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'unarchive'},
      MailboxActions.mailboxUnarchiveSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_MOVE_TO_INBOX_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'move_to_inbox'},
      MailboxActions.mailboxMoveToInboxSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_SEND_QUEUED_REQUEST,
      mailboxSagas.updateMailbox,
      {action: '2factor_confirm'},
      MailboxActions.mailboxSendQueuedSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_CLEAR_TRASH_REQUEST,
      mailboxSagas.updateMailbox,
      {action: 'clear_trash'},
      MailboxActions.mailboxClearTrashSuccess
    ),
    ...mailboxSagas.mailboxActionSagas,

    /* ------------ Mailbox Multiselect Sagas ------- */
    takeLatest(
      MailboxTypes.MAILBOX_TRASH_SELECTED_REQUEST,
      mailboxSagas.updateSelectedEmails,
      { action: 'trash' },
      MailboxActions.mailboxTrashSelectedSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_ARCHIVE_SELECTED_REQUEST,
      mailboxSagas.updateSelectedEmails,
      { action: 'archive' },
      MailboxActions.mailboxArchiveSelectedSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_READ_SELECTED_REQUEST,
      mailboxSagas.updateSelectedEmails,
      { action: 'mark_as_read' },
      MailboxActions.mailboxReadSelectedSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_UNREAD_SELECTED_REQUEST,
      mailboxSagas.updateSelectedEmails,
      { action: 'mark_as_unread' },
      MailboxActions.mailboxUnreadSelectedSuccess
    ),
    takeLatest(
      MailboxTypes.MAILBOX_DELETE_SELECTED_REQUEST,
      mailboxSagas.updateSelectedEmails,
      { action: 'delete' },
      MailboxActions.mailboxDeleteSelectedSuccess
    ),


    /* ------------- User owned Domains ------------- */
    takeLatest(
      [
        DomainTypes.DOMAIN_FETCH,
        DomainTypes.DOMAIN_SET_SEARCH_QUERY,
        DomainTypes.DOMAIN_SET_SORT_ORDER,
      ],
      domainSagas.fetch
    ),
    takeLatest(DomainTypes.DOMAIN_CREATE, domainSagas.create),
    takeLatest(DomainTypes.DOMAIN_EDIT, domainSagas.edit),
    takeLatest(DomainTypes.DOMAIN_REMOVE, domainSagas.remove),
    takeLatest(DomainTypes.DOMAIN_REQUEST_WHOIS, domainSagas.requestWhois),
    takeLatest(DomainTypes.DOMAIN_VALIDATE, domainSagas.validate),

    /* --------------- Purchased domains ------------- */
    takeLatest(DomainTypes.DOMAIN_TLD, domainSagas.tlds),
    takeLatest(DomainTypes.DOMAIN_SUGGESTIONS, domainSagas.suggest),
    takeLatest(DomainTypes.DOMAIN_PURCHASE_AVAILABLE, domainSagas.purchaseAvailable),
    takeLatest(DomainTypes.DOMAIN_PURCHASE, domainSagas.purchase),

    /*
    takeLatest(DomainTypes.CREATE_DOMAIN_REQUEST, domainSagas.createDomain),
    takeLatest(DomainTypes.DOMAIN_DETAIL_REQUEST, domainSagas.fetchDomainDetail),
    takeLatest(DomainTypes.DOMAIN_VALIDATION_REQUEST, domainSagas.fetchDomainValidation),
    takeLatest(DomainTypes.DELETE_DOMAIN_REQUEST, domainSagas.deleteDomain),
    takeLatest(DomainTypes.UPDATE_DOMAIN_REQUEST, domainSagas.updateDomain),
    */

    /* ------------- Mailbox-associated Domains ------------- */
    takeLatest(
      [
        MailboxDomainTypes.MAILBOX_DOMAIN_FETCH,
        MailboxDomainTypes.MAILBOX_DOMAIN_SET_SEARCH_QUERY
      ],
      mailboxDomainSagas.fetch
    ),
    /* ------------- System-byod-purchased Domains ------------- */
    takeLatest(
      [
        UsableDomainTypes.USABLE_DOMAIN_FETCH,
        UsableDomainTypes.USABLE_DOMAIN_SET_SEARCH_QUERY,
        UsableDomainTypes.USABLE_DOMAIN_SET_SORT_ORDER
      ],
      usableDomainSagas.fetch
    ),
    /* ------------- Only is_confirmed UserEmails ------------- */

    takeLatest(
      [
        UsableUserEmailTypes.USABLE_USER_EMAIL_FETCH,
        UsableUserEmailTypes.USABLE_USER_EMAIL_SET_SEARCH_QUERY,
        UsableUserEmailTypes.USABLE_USER_EMAIL_SET_SORT_ORDER
      ],
      usableUserEmailSagas.fetch
    ),

    /* ------------ UserEmail ------------- */
    takeLatest(
      [
        UserEmailTypes.USEREMAIL_FETCH,
        UserEmailTypes.USEREMAIL_SET_SEARCH_QUERY,
        UserEmailTypes.USEREMAIL_SET_SORT_ORDER
      ],
      useremailSagas.fetch
    ),
    takeLatest(UserEmailTypes.USEREMAIL_CREATE, useremailSagas.create),
    takeLatest(UserEmailTypes.USEREMAIL_EDIT, useremailSagas.edit),
    takeLatest(UserEmailTypes.USEREMAIL_REMOVE, useremailSagas.remove),
    takeLatest(UserEmailTypes.USEREMAIL_REQUEST_CONFIRMATION, useremailSagas.requestConfirmation),
    takeLatest(UserEmailTypes.USEREMAIL_AUTO_UPDATE_REQUEST, useremailSagas.autoUpdateRequest),
    takeLatest(UserEmailTypes.USEREMAIL_SET_DEFAULT, useremailSagas.setDefault),

    /* ------------- Misc ------------- */

    takeLatest(RegionTypes.ALL_REGIONS_REQUEST, regionSagas.fetchAllRegions),

    /* ------------- Notification ------------- */

    takeLatest(NotificationTypes.DISPLAY_NOTIFICATION, handleNotificationTimeout),

    /* ------------- Device -------------- */
    takeLatest(DeviceTypes.CREATE_DEVICE_REQUEST, createDevice),

    /* ------------- Account History -------------- */
    takeLatest(
      [
        AccountHistoryTypes.ACCOUNTHISTORY_FETCH,
        AccountHistoryTypes.ACCOUNTHISTORY_SET_SEARCH_QUERY
      ],
      accountHistorySagas.fetch
    ),

    /* ------------- Plan ---------------- */
    takeLatest(PlanTypes.upgradePlanSuccess, planSagas.upgradePlanSuccess),
    takeLatest(PlanTypes.upgradePlanError, planSagas.upgradePlanError),

    /* ------------- Chat ---------------- */
    // Text Chat
    takeEvery(ChatTypes.CHAT_INIT, chatSagas.chatInit),
    takeEvery(ChatTypes.CHAT_FETCH, chatSagas.chatFetch),
    takeEvery(ChatTypes.CHAT_SETUP_EXISTING_ROOM, chatSagas.chatSetupExistingRoom),
    takeEvery(ChatTypes.CHAT_CREATE_ROOM_REQUEST, chatSagas.chatCreateRoomRequest),
    takeEvery(ChatTypes.CHAT_JOIN_ROOM_REQUEST, chatSagas.chatJoinRoomRequest),
    takeEvery(ChatTypes.CHAT_LEAVE_ROOM_REQUEST, chatSagas.chatLeaveRoomRequest),
    takeEvery(ChatTypes.CHAT_UPDATE_ROOM_SETTINGS_REQUEST, chatSagas.chatUpdateRoomSettingsRequest),
    takeEvery(ChatTypes.CHAT_GET_MESSAGES_REQUEST, chatSagas.chatGetMessagesRequest),
    takeEvery(ChatTypes.CHAT_SEND_MESSAGE_REQUEST, chatSagas.chatSendMessageRequest),
    takeEvery(ChatTypes.CHAT_SEND_FILE_REQUEST, chatSagas.chatSendFileRequest),
    takeEvery(ChatTypes.CHAT_REFRESH_ROOM_REQUEST, chatSagas.chatRefreshRoom),
    takeEvery(ChatTypes.CHAT_ACK_MESSAGE, chatSagas.chatAckMessage),
    takeEvery(ChatTypes.CHAT_PEEK_NEW_MESSAGE_COUNT_REQUEST, chatSagas.chatPeekNewMessageCountRequest),
    takeEvery(ChatTypes.CHAT_MESSAGE_RECEIVED, chatSagas.chatMessageStatusWatch),
    takeLatest(ChatTypes.CHAT_SOCKET_DISCONNECTED, chatSagas.reconnectChatSocket),

    takeLatest(ChatTypes.CHAT_STARTED_TYPING, chatSagas.chatSendRoomEvent, 'STARTED_TYPING'),
    takeLatest(ChatTypes.CHAT_STOPPED_TYPING, chatSagas.chatSendRoomEvent, 'STOPPED_TYPING'),

    // Video Chat
    takeLatest(ChatTypes.ANSWER_INBOUND_VIDEO_CALL_OFFER, chatSagas.chatAnswerInboundVideoCallOffer),
    takeLatest(ChatTypes.REJECT_VIDEO_CALL, chatSagas.chatRejectVideoCall),
    takeLatest(ChatTypes.OUTBOUND_VIDEO_CALL_ANSWER_RECEIVED, chatSagas.chatOutboundVideoCallAnswerReceived),
    takeLatest(ChatTypes.INBOUND_VIDEO_CALL_ID_RECEIVED, chatSagas.chatInboundVideoCallIdReceived),
    takeLatest(ChatTypes.CANCEL_OUTBOUND_CALL, chatSagas.chatAnnounceCallEvent),
    ...callSagaRules,

    /* ------------- Crypto ------------- */
    takeLatest(CryptoTypes.CRYPTO_GET_KEY, cryptoSagas.getKey),
    takeLatest(CryptoTypes.CRYPTO_USER_EMAIL_SMIME, cryptoSagas.userEmailSmime),

    /* ------------- Avatars ------------- */
    takeLatest(ContactTypes.CONTACT_SUCCESS, avatarSagas.processAvatarForContacts),
    takeLatest(UserEmailTypes.USEREMAIL_SUCCESS, avatarSagas.processAvatarForUserEmails),
    takeLatest(IdentityTypes.IDENTITY_SUCCESS, avatarSagas.processAvatarForIdentities),
    takeLatest(MailboxTypes.MAILBOX_SUCCESS, avatarSagas.processAvatarForMailbox),
    takeLatest(ChatTypes.CHAT_SUCCESS, avatarSagas.processAvatarForChats),
    
    /* ---------------- WebRTC --------------- */
    takeLatest(ChatTypes.END_VIDEO_CALL, webrtcSagas.onEndVideoCall),
    takeLatest(StartupTypes.STARTUP_SUCCESS, webrtcSagas.webrtcAnnouncesDaemon),
    takeEvery(WebrtcTypes.UPDATE_LOCAL_ICE_CONNECTION_STATE, webrtcSagas.localWebrtcActionsCollector),
    takeEvery(WebrtcTypes.UPDATE_LOCAL_ICE_GATHERING_STATE, webrtcSagas.localWebrtcActionsCollector),
    takeEvery(WebrtcTypes.ADD_LOCAL_ICE_CANDIDATE, webrtcSagas.localWebrtcActionsCollector),
  ]
}
