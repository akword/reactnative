import { put, call, select } from 'redux-saga/effects'
import { SubmissionError } from 'redux-form'
import { isEmpty } from 'ramda'
import log from 'commons/Services/Log'
import { callAPI, buildApiReadPayload } from './APISagas'
import Actions, { REDUX_CONFIG } from 'commons/Redux/ContactRedux'
import { getError } from 'commons/Lib/Utils'
import ax from 'commons/Services/Analytics/index'

export function * fetch ({ requestType: _requestType }) {
  try {
    const isRN = yield select(s => s.device.isReactNative)
    const platform = yield select(s => s.device.platform)
    const filterIdentityIds = yield select(s => s.contact.filterIdentityIds)
    const searchFilters = yield select(s => s.contact.searchFilters)
    const shouldUseCache = isRN && platform === 'ios' && !filterIdentityIds.length && isEmpty(searchFilters)

    // If it's react native and there are no filterIdentityIds
    // then we use cache.
    if (shouldUseCache) {
      // If react-native, dispatch contactFetchFromCache
      // so that redux store can be populated with data
      // from SQLite cache. Saga for contactFetchFromCache
      // lives in src/Sagas/LocalDBSagas
      yield put(Actions.contactFetchFromCache(_requestType))
    }

    let searchConfig = {
      type: 'search',
      columns: [ 'contact_email' ]
    }

    const filterConfig = [
      { stateKey: 'filterIdentityIds', payloadKey: 'identity_id' }
    ]

    const { payload, requestType, isRequestUnnecessary } = yield call(
      buildApiReadPayload, _requestType, searchConfig, REDUX_CONFIG.statePrefix,
      Actions.contactClearSearchData, filterConfig
    )
    if (isRequestUnnecessary) {
      return
    }

    yield put(Actions.contactRequest(requestType))
    const endpointName = isRN ? 'ContactsUniqueNative' : 'ContactsUnique'
    const response = yield callAPI(endpointName, payload)
    const successActionCreator = shouldUseCache ? Actions.contactSuccessForCache : Actions.contactSuccess
    yield put(successActionCreator(response.data, requestType))
  } catch (e) {
    const err = getError(e, 'Failed to fetch the contacts. Please try again.')
    yield put(Actions.contactFailure(err))
  }
}

export function * autoUpdateRequest () {
  const slice = yield select(s => s.contact)

  if (!slice.dataOrder || !slice.dataOrder.length) {
    yield call(fetch, {})
    return
  }

  if (!slice.lastRequestTimestamp) return

  const payload = {
    order: 'last_activity_on',
    sort: 'desc',
    limit: 100,
    gt_req_ts: slice.lastRequestTimestamp
  }

  try {
    const response = yield callAPI('ContactsUnique', payload)
    yield put(Actions.contactAutoUpdateSuccess(response.data))
  } catch (e) {
    const err = getError(e, 'Failed to fetch the contacts. Please try again.')
    yield put(Actions.contactAutoUpdateFailure(err))
  }
}

export function * create ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('CreateContact', payload)
    ax.form.createSuccess(ax.EVENTS.CONTACT_FORM)
    typeof resolve === 'function' && resolve()
    yield put(Actions.contactCreateSuccess(response.data))
  } catch (e) {
    ax.form.createError(ax.EVENTS.CONTACT_FORM, e.message, e.code)
    typeof reject === 'function' && reject(new SubmissionError({_error: getError(e, 'Failed to create the contact. Please try again.')}))
  }
}

export function * edit ({ payload, resolve, reject }) {
  try {
    const response = yield callAPI('EditContact', payload)
    ax.form.editSuccess(ax.EVENTS.CONTACT_FORM)
    typeof resolve === 'function' && resolve()
    yield put(Actions.contactEditSuccess(response.data))
  } catch (e) {
    ax.form.editError(ax.EVENTS.CONTACT_FORM, e.message, e.code)
    typeof reject === 'function' && reject(new SubmissionError({_error: getError(e, 'Failed to update the contact. Please try again.')}))
  }
}

export function * remove ({ payload, resolve, reject }) {
  try {
    // NOTE: We key by contact.email since contacts are deduped
    const id = payload.email
    yield callAPI('DeleteContactEmail', payload)
    ax.delete(ax.EVENTS.CONTACT)
    yield put(Actions.contactRemoveSuccess(id))
    typeof resolve === 'function' && resolve()
  } catch (e) {
    log.error('Delete contact API request failed - ', e)
    typeof reject === 'function' && reject(new SubmissionError({_error: getError(e, 'Failed to update the contact. Please try again.')}))
  }
}
