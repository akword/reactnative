import hmac from 'crypto-js/hmac-sha256'
import base64 from 'crypto-js/enc-base64'
import utf8 from 'crypto-js/enc-utf8'

import log from 'commons/Services/Log'

function genNonce (len) {
  /* Note: This is weak with Math.random(); replace with native bits */
  var txt = ''
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  for (var i = 0; i < len; i++) {
    txt += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return txt
}

function b64Url (s) {
  var n = null
  try {
    n = base64.stringify(s)
    n = n.replace(/=+$/, '')
    n = n.replace(/\+/g, '-')
    n = n.replace(/\//g, '_')
    return n
  } catch (e) {
    console.error('error=' + e.message)
  }
}

export function createSignature (userAccessId, userSecretToken) {
  if (!userAccessId || !userSecretToken) {
    log.debug('createSignature: missing param')
    return
  }

  /* Header */
  let hdr = {
    typ: 'JWT',
    alg: 'HS256',
    kid: userAccessId
  }
  let hdr_str = b64Url(utf8.parse(JSON.stringify(hdr)))
  // log.debug('hdr_str=' + hdr_str);

  /* Data to sign */
  let ts = new Date().toISOString()
  let data = {
    sig_ts: ts,
    sig_nonce: genNonce(8)
  }
  // log.debug(data);
  let data_str = b64Url(utf8.parse(JSON.stringify(data)))
  let token = hdr_str + '.' + data_str

  /* Sign it */
  var signed = hmac(token, userSecretToken)
  signed = b64Url(signed)

  const signed_token = `${token}.${signed}`
  return signed_token
}
