import { Log } from 'commons/Services/Log'
import Config from 'app/Config/DebugSettings'

export default new Log({debugOn: Config.apiLogging, errorOn: Config.apiLogging, errorAsLog: true})
