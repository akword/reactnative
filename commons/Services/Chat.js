import uuidv1 from 'uuid/v1'
// import WebSocketClient from 'reconnecting-websocket'

import log from 'commons/Services/Log'

import * as Sodium from 'app/Services/Encryption'

import {
  convertUint8ArrayToStr,
  convertUint8ArrayToBase64Str,
  convertBase64StrToUint8Array,
  concatenateUint8Array
} from 'commons/Lib/Encoding'

const ReconnectingWebsocket = require('reconnecting-websocket')
const DEVKIT_WSS_URL = 'wss://devkit-us.msgsafe.io:443'
// const STAGE_WSS_URL = 'wss://stage-us.msgsafe.io:443'
const STAGE_WSS_URL = 'wss://stage-3-us.msgsafe.io:443'
const PROD_WSS_URL = 'wss://chat.msgsafe.io:443'

const WSS_URL = process.env.NODE_ENV === 'production' ? PROD_WSS_URL : STAGE_WSS_URL

export default class MsgSafeChatAPI {
  static backOffConfig = {
    maxReconnectionDelay: 10000,
    minReconnectionDelay: 1500,
    reconnectionDelayGrowFactor: 1.3,
    connectionTimeout: 5000,
    maxRetries: Infinity,
    debug: false
  }

  constructor (url) {
    this._url = url || WSS_URL

    // Local keypair for chat
    this._keyPair = null

    // Server public key as base64 and binary
    this._serverPublicKeyBase64 = null
    this._serverPublicKeyBinary = null

    this._externalMessageHandler = null
    this._externalDisconnectHandler = null

    // Sequence to requests map
    this._seqToRequestMap = {}

    // Track if socket has been manually closed
    this._manuallyClosed = false

    // Track if socket is currently opened
    this._socketState = 0 // 0: closed, 1: active, 2: connecting, 3: disconnecting

    this.initiated = false

    // Track whether key handshake has been done
    this.ready = false

    // Track if the socket has been in use for a user
    // This will stay true as soon as the first handshake
    // is successful and won't change even on socket disconnects
    // Helps in distinguishing between logged in and out states.
    this.userSessionStarted = false

    // Create the websocket connection
    this.socket = null

    this.setMessageHandler = this.setMessageHandler.bind(this)
    this.removeMessageHandler = this.removeMessageHandler.bind(this)
    this.setDisconnectHandler = this.setDisconnectHandler.bind(this)
    this.resendMessagesInSeq = this.resendMessagesInSeq.bind(this)
    this.close = this.close.bind(this)
  }

  bootstrapKeyPair () {
    if (this._keyPair) return Promise.resolve()

    return new Promise((resolve, reject) => {
      Sodium.crypto_box_keypair().then(keyPair => {
        this._keyPair = keyPair
        resolve()
      })
    })
  }

  connectSocket () {
    // Exit if there's already a socket
    if (this.socket) return

    this.initiated = true
    this._socketState = 2

    // Create the websocket connection
    this.socket = new ReconnectingWebsocket(this._url, [], MsgSafeChatAPI.backOffConfig)
    this.socket.addEventListener('message', this._mainMessageHandler.bind(this))
    this.socket.onopen = e => {
      log.debug('socket open - ', e)
      this._socketState = 1
    }
    this.socket.onclose = e => {
      log.debug('socket closed - ', e)
      this._socketState = 0
      this.ready = false

      if (typeof this._externalDisconnectHandler === 'function') {
        this._externalDisconnectHandler()
      }

      if (this._manuallyClosed) {
        this._cleanUpSeq() // initialize request queue
      }
    }
    this.socket.onerror = (e) => {
      log.debug('socket error - ')
      console.log(e)

      // Connection timeout error causes app crash when ws reconnecting, so added below
      this._socketState = 0
      this.ready = false
    }
  }

  async _mainMessageHandler (event) {
    // If manually closed, ignore all the events
    if (this._manuallyClosed) return

    let data
    try {
      data = JSON.parse(event.data)
    } catch (e) {
      data = await this.decryptBase64Message(event.data)
    }
    log.debug('_mainMessageHandler received data - ', data)

    if (data.seq_id) {
      const cbs = this._getCbForSeq(data.seq_id)
      if (data.seq_id && cbs && typeof cbs[0] === 'function') {
        this._deRegisterForSeq(data.seq_id)
        delete data.seq_id
        cbs[0](data)
        log.debug('_seqToRequestMap state - ', this._seqToRequestMap)
        return
      } else log.debug('SEQ ID DOESNT EXIST', data.seq_id)
    }

    if (typeof this._externalMessageHandler === 'function') {
      this._externalMessageHandler(event)
    }
  }

  resendMessagesInSeq () {
    if (this._manuallyClosed || this._socketState !== 1 || !this._seqToRequestMap) {
      return
    }

    for (const seq in this._seqToRequestMap) {
      if (this._seqToRequestMap.hasOwnProperty(seq)) {
        if (this._seqToRequestMap[seq][1] && this._seqToRequestMap[seq][1].cmd && (this._seqToRequestMap[seq][1].cmd == '/message/add' || this._seqToRequestMap[seq][1].cmd == '/message/add/e2ee')) {
          this.encryptMessageAsBase64(this._seqToRequestMap[seq][1]).then(p => {
            this.socket.send(p)
          })
        }
      }
    }
  }

  _cleanUpSeq () {
    for (const seq in this._seqToRequestMap) {
      if (this._seqToRequestMap.hasOwnProperty(seq)) {
        delete this._seqToRequestMap[seq]
      }
    }
  }

  _registerForSeq (seq, cb) {
    if (typeof cb === 'object' && typeof cb[0] === 'function') {
      this._seqToRequestMap[seq] = cb
    }
  }

  _getCbForSeq (seq) {
    return this._seqToRequestMap[seq]
  }

  _deRegisterForSeq (seq) {
    if (this._seqToRequestMap[seq]) {
      delete this._seqToRequestMap[seq]
    }
  }

  /**
   * Set server's public, given as base 64 encoded string.
   *
   * @param key
   */
  setBase64ServerPublicKey (key) {
    this._serverPublicKeyBase64 = key
    this._serverPublicKeyBinary = convertBase64StrToUint8Array(key)
  }

  /**
   * Set an external message handler.
   *
   * @param handler
   */
  setMessageHandler (handler) {
    this._externalMessageHandler = handler
  }

  /**
   * Remove the external message handler.
   */
  removeMessageHandler () {
    this._externalMessageHandler = null
  }

  /**
   * Set the disconnect handler.
   */
  setDisconnectHandler (handler) {
    this._externalDisconnectHandler = handler
  }

  /**
   * Call when key handshake is successful.
   */
  isReady () {
    this.ready = true
  }

  /**
   * Get client's public key as base 64 encoded string.
   *
   * @returns {string}
   */
  getClientPublicKeyAsBase64 () {
    return convertUint8ArrayToBase64Str(this._keyPair.publicKey)
  }

  /**
   * Encrypt message and return binary (Uint8Array).
   *
   * Prepends the nonce with the encrypted message binary.
   *
   * @param message
   * @param customBase64PublicKey
   */
  async encryptMessageAsBinary (message, customBase64PublicKey) {
    let publicKey = this._serverPublicKeyBinary
    if (customBase64PublicKey) {
      publicKey = convertBase64StrToUint8Array(customBase64PublicKey)
    }

    const nonceBinary = await Sodium.randombytes_buf(Sodium.crypto_box_NONCEBYTES)
    const encryptedBinary = await Sodium.crypto_box_easy(
      message, nonceBinary, publicKey, this._keyPair.privateKey
    )

    return concatenateUint8Array(nonceBinary, encryptedBinary)
  }

  /**
   * Encrypt message and return as base 64 encoded.
   *
   * Uses `encryptMessageAsBinary` under the hood.
   *
   * @param message
   * @param customBase64PublicKey
   * @returns {string}
   */
  async encryptMessageAsBase64 (message, customBase64PublicKey) {
    try {
      if (typeof message === 'object') {
        message = JSON.stringify(message)
      }
    } catch (e) {}
    const binary = await this.encryptMessageAsBinary(message, customBase64PublicKey)
    return convertUint8ArrayToBase64Str(binary)
  }

  async decryptBinaryMessage (encryptedBinary, customBase64PublicKey, decodeBinaryToBase64) {
    let publicKey = this._serverPublicKeyBinary
    if (customBase64PublicKey) {
      publicKey = convertBase64StrToUint8Array(customBase64PublicKey)
    }

    const responseNonce = encryptedBinary.subarray(0, Sodium.crypto_box_NONCEBYTES)
    const encryptedMessageBinary = encryptedBinary.subarray(Sodium.crypto_box_NONCEBYTES)
    const decryptedBinary = await Sodium.crypto_box_open_easy(
      encryptedMessageBinary, responseNonce, publicKey, this._keyPair.privateKey
    )
    const decryptedStr = decodeBinaryToBase64
      ? convertUint8ArrayToBase64Str(decryptedBinary)
      : convertUint8ArrayToStr(decryptedBinary)

    try {
      if (typeof decryptedStr === 'string') {
        return JSON.parse(decryptedStr)
      }
    } catch (e) {}

    return decryptedStr
  }

  /**
   * Decrypt an encrypted message, as base 64 encoded.
   *
   * Returns decrypted object/string.
   *
   * @param encryptedBase64
   * @param customBase64PublicKey
   */
  async decryptBase64Message (encryptedBase64, customBase64PublicKey) {
    const encryptedBinary = convertBase64StrToUint8Array(encryptedBase64)
    return this.decryptBinaryMessage(encryptedBinary, customBase64PublicKey)
  }

  /**
   * Send json of handshake payload over the websocket.
   *
   * Message is serialised as JSON, if possible.
   *
   * @param message
   */
  sendHandShakePayload (message) {
    if (this._manuallyClosed || this._socketState !== 1) return

    try {
      if (typeof message === 'object') {
        message = JSON.stringify(message)
      }
    } catch (e) {}

    if (this._manuallyClosed || this._socketState !== 1) return

    this.socket.send(message)
  }

  /**
   * Returns a promise that resolves when the response for the sent
   * payload is received on socket.
   *
   * @param payload
   * @returns {Promise}
   */
  sendRequest (payload) {
    if (this._manuallyClosed || this._socketState != 1) {
      log.debug('Manual Close: Reject to send:', payload)
      return
    }

    return new Promise((resolve, reject) => {
      const randomSeq = payload.seq_id || uuidv1()
      payload.seq_id = randomSeq
      log.debug('going to send payload - ', payload)
      this.encryptMessageAsBase64(payload).then(p => {
        if (this._manuallyClosed || this._socketState != 1) {
          log.debug('Manual Close: Reject to send:', payload)
          this._registerForSeq(randomSeq, [resolve, payload])
          return
        }
        this.socket.send(p)
        this._registerForSeq(randomSeq, [resolve, payload])
      })
    })
  }

  /**
   * Returns a promise that resolves just after send request. to make clean queue of cb
   *
   * @param payload
   * @returns {Promise}
   */
  sendRequestWithoutResponse (payload) {
    if (this._manuallyClosed || this._socketState != 1) {
      log.debug('Manual Close: Reject to send:', payload)
      return
    }

    return new Promise((resolve, reject) => {
      const randomSeq = payload.seq_id || uuidv1()
      payload.seq_id = randomSeq
      log.debug('going to send payload without response - ', payload)
      this.encryptMessageAsBase64(payload).then(p => {
        if (this._manuallyClosed || this._socketState != 1) {
          log.debug('Manual Close: Reject to send:', payload)
          this._registerForSeq(randomSeq, [resolve, payload])
          return
        }
        resolve(this.socket.send(p))
      })
    })
  }

  close (isManuallyClosed = true) {
    if (!this.socket) return
    this._manuallyClosed = isManuallyClosed
    this._socketState = 3
    this.socket.close()
    // this.socket.close(1000, '', {fastClose: false, delay: 0})
  }
}
