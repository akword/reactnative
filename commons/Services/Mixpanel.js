const makeMixpanel = () => {
  if (process.env.NODE_ENV !== 'production')
    return {}

  const mixpanel = require('mixpanel-browser')

  const keys = {
    stage: 'b19df39793287a911f595a9e73576a9d',
    prod: '1de576d72c5fa7b1f877d1904c31f842'
  }
  let envKey = keys.stage
  if (process.env.NODE_ENV === 'production') {
    envKey = keys.prod
  }
  mixpanel.init(envKey)

  return mixpanel
}


const mixo = makeMixpanel()
export default mixo
