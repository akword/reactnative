import { pushToExternalLogger } from 'app/Services/ExternalLogger'
import CircularJSON from './circular_json'

const defaultConfig = {
  debugOn: process.env.NODE_ENV === 'development',
  errorOn: true,
  errorAsLog: false
}

export class Log {
  constructor (cfg = null) {
    this.cfg = cfg || defaultConfig

    this._buffer = []
    this._bufferEnabled = false
  }

  enableBuffer () {
    console.log('buffer enabled...')
    this._buffer = []
    this._bufferEnabled = true
  }

  pushBufferToExternalService (...args) {
    pushToExternalLogger(this._buffer.join('\n>> '), ...args)
    this._buffer = []
    this._bufferEnabled = true
    console.log('buffer disabled...')
  }

  _pushToBuffer (data) {
    this._buffer.push(data.map(x => typeof x === 'object' ? CircularJSON.stringify(x) : x).join(' - '))
  }

  _log (...args) {
    this._pushToBuffer(args)
    console.log(...args)
  }

  _debug (...args) {
    this._pushToBuffer(args)
    console.debug(...args)
  }

  _error (...args) {
    this._pushToBuffer(args)
    console.error(...args)
  }

  debug (...args) {
    if (!this.cfg.debugOn) {
      return
    }
    this._log(...args)
  }

  error (...args) {
    if (!this.cfg.errorOn && !this.cfg.errorAsLog) {
      return
    }
    if (this.cfg.errorAsLog) {
      this._log(...args)
      return
    }
    this._error(...args)
  }
}

const log = new Log()

log.debug('[log.debug] enabled')

export default log
