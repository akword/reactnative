import mixo from './Service'

import EventDefinition from './EventDefinition'

import * as form from './Utils/form'

const Analytics = {
  mixo,
  service: mixo,
  EVENTS: EventDefinition,
  form,

  track: (...args) => mixo.track(...args),

  pageView: baseName => mixo.track(`${baseName} Pageview`),
  listPageView: baseName => mixo.track(`${baseName} List Pageview`),
  detailPageView: baseName => mixo.track(`${baseName} Detail Pageview`),

  action: (baseName, actionName, values) =>
    mixo.track(`${baseName} Action`, {'Action': actionName, ...values}),

  'delete': baseName => mixo.track(`${baseName} Deleted`)
}

export default Analytics
