const Definitions = {
  HOME: 'Home',
  BENEFITS: 'Benefits',
  MOBILEAPP: 'MobileApp',
  NOT_FOUND: 'Not Found (404)',
  PRIVACY: 'Privacy',
  TERMS: 'Terms',

  HELP_CONTACT_SUPPORT_TEAM_CLICKED: 'Contact Support Team Clicked',

  LOGOUT: 'Logout',

  LOGIN: 'Login',
  SIGNUP: 'Signup',
  FORGOT_USERNAME: 'Forgot Username',
  RESET_PASSWORD: 'Reset Password',
  RESET_PASSWORD_REQUEST: 'Reset Password Request',

  IDENTITY: 'Identity',
  IDENTITY_FORM: 'Identity Form',

  CONTACT: 'Contact',
  CONTACT_FORM: 'Contact Form',

  FORWARD_ADDRESS: 'Forward Address',
  FORWARD_ADDRESS_FORM: 'Forward Address Form',
  FORWARD_ADDRESS_CONFIRMATION: 'Forward Address Confirmation',
  FORWARD_ADDRESS_CRYPTO_CONFIRMATION: 'Forward Address Crypto Confirmation',

  DOMAIN: 'Domain',
  DOMAIN_FORM: 'Domain Form',
  DOMAIN_ZONES: 'Domain Zones',

  MAILBOX: 'Mailbox',
  MAILBOX_COMPOSE: 'Mailbox Compose',
  MAILBOX_PATH_ANALYZER: 'Mailbox Path Analyzer',

  SECURE_VIDEO: 'Secure Video',
  SECURE_VIDEO_GUEST: 'Secure Video Guest',
  SECURE_VIDEO_REQUEST: 'Video Request',
  SECURE_VIDEO_SDP_OFFER: 'Video SDP Offer',

  SECURE_VIDEO_INVITATION_FORM: 'Secure Video Invitation Form',

  DASHBOARD: 'Dashboard',

  ACCOUNT_HISTORY: 'Account History',

  SECURITY_PROFILE: 'Security Profile',
  USER_PROFILE: 'User Profile',
  USER_PLAN: 'User Plan',

  ONBOARDING_STARTED: 'Onboarding Started',
  ONBOARDING_COMPLETE: 'Onboarding Complete',

  PAYMENT_CREDIT_CARD: 'Payment - Credit Card',
  PAYMENT_HISTORY: 'Payment History',

  BUILD_UPDATE_MESSAGE_VIEW: 'WebApp Update Notice',
  BUILD_UPDATE_MESSAGE_CLICK: 'WebApp Updated'
}

export default Definitions
