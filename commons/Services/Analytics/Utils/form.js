import mixo from '../Service'

export const pageview = baseName => {
  mixo.track(`${baseName} Pageview`)
}

export const createPageView = baseName => mixo.track(`${baseName} Pageview`, {'Action': 'Create'})

export const editPageView = baseName => mixo.track(`${baseName} Pageview`, {'Action': 'Edit'})

export const success = (baseName, values = {}) => {
  mixo.track(baseName, {
    'Result': 'Success',
    ...values
  })
}

export const error = (baseName, message, code, values = {}) => {
  mixo.track(baseName, {
    'Result': 'Error',
    'Error': message,
    'Error Code': code,
    ...values
  })
}

export const createSuccess = (baseName, values = {}) => {
  success(baseName, {'Action': 'Create', ...values})
}

export const createError = (baseName, message, code, values = {}) => {
  error(baseName, message, code, {'Action': 'Create', ...values})
}

export const editSuccess = (baseName, values = {}) => {
  success(baseName, {'Action': 'Edit', ...values})
}

export const editError = (baseName, message, code, values = {}) => {
  error(baseName, message, code, {'Edit': 'Create', ...values})
}
