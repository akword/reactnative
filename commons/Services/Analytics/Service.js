import createService from 'app/Services/AnalyticsService'

const keys = {
  stage: 'b19df39793287a911f595a9e73576a9d',
  prod: '1de576d72c5fa7b1f877d1904c31f842'
}

const getService = () => {
  let key = process.env.NODE_ENV === 'production' ? keys.prod : keys.stage
  return createService(key)
}

const service = getService()
export default service
