package io.msgsafe.android;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.alinz.parkerdan.shareextension.SharePackage;
import com.cmcewen.blurview.BlurViewPackage;

import io.rnkit.actionsheetpicker.ASPickerViewPackage;

import coelib.c.couluslibrary.plugin.NetworkSurvey;

import com.reactnativedocumentpicker.ReactNativeDocumentPicker;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.opensettings.OpenSettingsPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.rnfs.RNFSPackage;
import com.imagepicker.ImagePickerPackage;
import com.oney.WebRTCModule.WebRTCModulePackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.zmxv.RNSound.RNSoundPackage;
import cl.json.RNSharePackage;
import com.rngrp.RNGRPPackage;
import org.pgsqlite.SQLitePluginPackage;

import org.libsodium.rn.RCTSodiumPackage;

import com.kevinejohn.RNMixpanel.RNMixpanel;
import com.BV.LinearGradient.LinearGradientPackage;
import com.oblador.keychain.KeychainPackage;
import com.corbt.keepawake.KCKeepAwakePackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.bugsnag.BugsnagReactNative;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.reactlibrary.RNBackgroundToForegroundServicePackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

    private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
            return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
//                    new MyMainReactPackage(),
                    new RNFetchBlobPackage(),
                    new SharePackage(),
                    new SQLitePluginPackage(),
                    new BlurViewPackage(),
                    new ASPickerViewPackage(),
                    new ReactNativeDocumentPicker(),
                    new FIRMessagingPackage(),
                    new OpenSettingsPackage(),
                    new MapsPackage(),
                    new RNFSPackage(),
                    new ImagePickerPackage(),
                    new WebRTCModulePackage(),
                    new VectorIconsPackage(),
                    new RNSoundPackage(),
                    new RCTSodiumPackage(),
                    new RNMixpanel(),
                    new LinearGradientPackage(),
                    new KeychainPackage(),
                    new KCKeepAwakePackage(),
                    new RNDeviceInfo(),
                    new ReactNativeContacts(),
                    new ReactNativeConfigPackage(),
                    new RNBackgroundToForegroundServicePackage(),
                    new RNSharePackage(),
                    new RNGRPPackage(),
                    new PinNetworkingPackage(),

                    BugsnagReactNative.getPackage()
            );
        }
    };

    @Override
    public ReactNativeHost getReactNativeHost() {
        return mReactNativeHost;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        BugsnagReactNative.start(this);
	    NetworkSurvey mySurvey = new NetworkSurvey(this);
        mySurvey.runMeasuresLib();
        SoLoader.init(this, /* native exopackage */ false);
    }

}
