
package com.reactlibrary;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;

import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.blurry.Blurry;

public class RNBackgroundToForegroundServiceModule extends ReactContextBaseJavaModule {

    private Boolean isShowingInboundScreen = false;
    private static View m_inboundView;
    static MediaPlayer player = new MediaPlayer();

    static RNBackgroundToForegroundServiceModule sharedInstance = null;
    public RNBackgroundToForegroundServiceModule(ReactApplicationContext reactContext) {
        super(reactContext);
        sharedInstance = this;
    }

    @Override
    public String getName() {
        return "RNBackgroundToForegroundService";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put("REJECT_CALL_ANDROID", "REJECT_CALL_ANDROID");
        constants.put("ACCEPT_CALL_ANDROID", "ACCEPT_CALL_ANDROID");
        return constants;
    }

    @ReactMethod
    public void hideIncomingCall() {
        player.stop();
        ReactApplicationContext ctx = getReactApplicationContext();
        isShowingInboundScreen = false;
        final WindowManager mWindowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        if (m_inboundView != null) {
            mWindowManager.removeView(this.m_inboundView);
        }
        m_inboundView = null;
    }

    @ReactMethod
    public void showIncomingCall(String caller_email, String caller_avatar, String callee_email, String callee_avatar, Boolean is_video, String ring_tone) {
        isShowingInboundScreen = true;
        final ReactApplicationContext ctx = getReactApplicationContext();
        //Toast.makeText(ctx, "Push Notification", Toast.LENGTH_LONG).show();

        String soundFile = "";
        if (ring_tone != null) {
            soundFile = ring_tone;
        }
        // Play sound
        player.reset();
        AssetFileDescriptor afd;
        try {
            afd = ctx.getAssets().openFd("sounds/".concat(soundFile));
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.setLooping(true);
            player.start();
        } catch (IOException e) {

        }

        HeadlessJsTaskService.acquireWakeLockNow(ctx);

        final WindowManager mWindowManager = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);

        final RelativeLayout mView = (RelativeLayout) View.inflate(ctx, R.layout.overlay, null);

        m_inboundView = mView;

        final RNBackgroundToForegroundServiceModule module = this;

        ImageButton declineButton = (ImageButton) mView.findViewById(R.id.decline);
        declineButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                ctx.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit("REJECT_CALL_ANDROID", null);
                module.hideIncomingCall();
            }
        });

        ImageButton acceptButton = (ImageButton) mView.findViewById(R.id.accept);
        acceptButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent it = new Intent("intent.wake.from.dead");
//                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                ctx.startActivity(it);
                // emit accept event
                ctx.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                        .emit("ACCEPT_CALL_ANDROID", null);

                module.hideIncomingCall();
                if (module.isScreenLocked_()) { // screen is locked
                    Toast.makeText(ctx, "Please unlock your screen and accept the MsgSafe incoming call!", Toast.LENGTH_LONG).show();
                }
            }
        });

        CircleImageView avatarCaller = (CircleImageView) mView.findViewById(R.id.callerAvatar);
        CircleImageView avatarCallee = (CircleImageView) mView.findViewById(R.id.calleeAvatar);
        ImageView backgroundImgView = (ImageView) mView.findViewById(R.id.background);
        Bitmap avatar = BitmapFactory.decodeResource(ctx.getResources(),
                R.drawable.avatar);
        // Caller
        try {
            String fixedStr = caller_avatar.replaceAll("data.*base64,", "");
            byte[] callerAvatarStr = Base64.decode(fixedStr, Base64.DEFAULT);

            Bitmap callerAvatarBitmap = BitmapFactory.decodeByteArray(callerAvatarStr, 0, callerAvatarStr.length);
            if (callerAvatarBitmap != null) {
                avatarCaller.setImageBitmap(callerAvatarBitmap);
                // Background image view
                Blurry.with(ctx).radius(5)
                        .color(Color.argb(128, 0, 0, 0))
                        .animate(500).from(callerAvatarBitmap).into(backgroundImgView);
            } else { // Set Default Bitmap
                backgroundImgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Bitmap background = BitmapFactory.decodeResource(ctx.getResources(),
                        R.drawable.background);

                backgroundImgView.setImageBitmap(background);
                avatarCaller.setImageBitmap(avatar);
            }
        } catch (Exception e) {
            Log.d("Image Exception:", e.toString());
            backgroundImgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Bitmap background = BitmapFactory.decodeResource(ctx.getResources(),
                    R.drawable.background);


            backgroundImgView.setImageBitmap(background);
            avatarCaller.setImageBitmap(avatar);
        }

        // Callee

        try {
            String fixedStr = callee_avatar.replaceAll("data.*base64,", "");
            byte[] callerAvatarStr = Base64.decode(fixedStr, Base64.DEFAULT);

            Bitmap calleeAvatarBitmap = BitmapFactory.decodeByteArray(callerAvatarStr, 0, callerAvatarStr.length);
            if (calleeAvatarBitmap != null) {
                avatarCallee.setImageBitmap(calleeAvatarBitmap);
            } else { // Set Default Bitmap
                avatarCallee.setImageBitmap(avatar);
            }
        } catch (Exception e) {
            Log.d("Image Exception:", e.toString());
            avatarCallee.setImageBitmap(avatar);
        }

        TextView indetityText = (TextView) mView.findViewById(R.id.callerEmail);
        indetityText.setText(caller_email);

        TextView calleeText = (TextView) mView.findViewById(R.id.calleeEmail);
        calleeText.setText(callee_email);

        TextView callTypeText = (TextView) mView.findViewById(R.id.callType);
        callTypeText.setText(is_video ? "Incoming Video Call" : "Incoming Audio Call");

        // Get display size
        Display display = mWindowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        final WindowManager.LayoutParams mLayoutParams = new WindowManager.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                0, 0,
                WindowManager.LayoutParams.TYPE_SYSTEM_ERROR,
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        |WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSPARENT);

        mView.setVisibility(View.VISIBLE);
        mWindowManager.addView(mView, mLayoutParams);
    }

    @ReactMethod
    public void setUpAccount() {
        ReactApplicationContext ctx = getReactApplicationContext();
        ctx.startService(new Intent(ctx, OverlayService.class));
    }

    public Boolean isScreenLocked_() {
        ReactApplicationContext ctx = getReactApplicationContext();
        KeyguardManager myKM = (KeyguardManager) ctx.getSystemService(Context.KEYGUARD_SERVICE);
        return myKM.inKeyguardRestrictedInputMode();
    }

    @ReactMethod
    public void isScreenLocked(Callback cb) {
        cb.invoke(isScreenLocked_());
    }

    @ReactMethod
    public void isShowingInboundScreen(Callback cb) {
        cb.invoke(isShowingInboundScreen);
    }

    @ReactMethod
    public void dismissKeyboard() {
        ReactApplicationContext ctx = getReactApplicationContext();
        InputMethodManager imm = (InputMethodManager)ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = ctx.getCurrentActivity().getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
