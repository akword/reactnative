package com.reactlibrary;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.widget.Toast;

import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.ReactApplicationContext;

/**
 * Created by user on 9/5/17.
 */

public class CallKitService extends ConnectionService {
    static String email = "";
    static String name = "";
    static Context context;
    @Override
    public Connection onCreateIncomingConnection(PhoneAccountHandle connectionManagerPhoneAccount, ConnectionRequest request) {
        final TextToSpeech textToSpeech = new TextToSpeech(this, null);

        Connection conn = new Connection() {
            @Override
            public void onAnswer() {
                setActive();
                // Wake the app
                HeadlessJsTaskService.acquireWakeLockNow(context);
                try {
                    Intent it = new Intent("intent.wake.from.dead");
                    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplication().startActivity(it);
                } catch (Exception e) {

                }
            }

            @Override
            public void onDisconnect() {
                destroy();
            }
        };
        conn.setAddress(Uri.parse(email), TelecomManager.PRESENTATION_ALLOWED);
        conn.setCallerDisplayName(name, TelecomManager.PRESENTATION_ALLOWED);
        return conn;
    }
}