#!/bin/bash

[ $# -ne 1 ] && {
	echo "$0 [build #]"
	exit 0
}

export ROOT=`pwd`
export BUILD_VERSION="1.0.1"
export BUILD_NUMBER=$1
export RELEASE="release_app_"`date "+%Y%m%d"`"-$BUILD_VERSION-$BUILD_NUMBER"

cd $ROOT ; git checkout master ; git pull
cd $ROOT/commons ; git checkout master ; git pull ; git tag -a -m '$RELEASE' $RELEASE ; git push --tags
cd $ROOT/translations ; git checkout master ; git pull ; git tag -a -m '$RELEASE' $RELEASE ; git push --tags
cd $ROOT ; git checkout master ; git pull ; git tag -a -m '$RELEASE' $RELEASE ; git push --tags

# clean before building

rm -rf node_modules
yarn cache clean
watchman watch-del-all
killall node
yarn

REVISION="$(git rev-parse HEAD)"

bundle exec fastlane android beta build_version:$BUILD_VERSION build_number:$BUILD_NUMBER 
./bugsnag-release.py --buildnumber $BUILD_NUMBER --commit $REVISION --notifier 323d3322da613caedd0362f5bce64ca9

bundle exec fastlane ios beta build_version:$BUILD_VERSION build_number:$BUILD_NUMBER 
./bugsnag-release.py --buildnumber $BUILD_NUMBER --commit $REVISION --notifier 30c30e3ade52eecca6b7d24fab6b3bfc

git commit -a -m "Versioning $RELEASE"
git push
